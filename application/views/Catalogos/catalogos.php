<link href="<?php echo base_url(); ?>public/plugins/bootstraptoggle/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>public/plugins/bootstraptoggle/bootstrap-toggle.min.js"></script>
<style type="text/css">
    .toggle{
        width: 107px!important;
    } 
    @font-face {
        font-family: "gisele";
        src: url("<?php echo base_url(); ?>public/fonts/gisele.ttf");
    }
    .titlekids{
        font-family: "gisele";
        text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0,0,0,.1), 0 0 5px rgba(0,0,0,.1), 0 1px 3px rgba(0,0,0,.3), 0 3px 5px rgba(0,0,0,.2), 0 5px 10px rgba(0,0,0,.25), 0 10px 10px rgba(0,0,0,.2), 0 20px 20px rgba(0,0,0,.15);
        font-size: 153px;
        color: white;
    }
    /*--*/

</style>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1></h1>
        <!--<small class="subtitle">Listado de personal</small>-->
    </div>
    <!--
    <div class="col-sm-offset-11">
       	<a class="btn vd_btn vd_bg-blue vd_white" href="NuevoPersonal.php">
       		<span class="menu-icon"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>
    -->
</div>
<div class="vd_content-section clearfix">
	<div class="row">
        <div class="col-md-12">
            <div class="panel widget">
                <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title">  </h3>
                </div>
                <div class="panel-body">
                	<!-------------->
                        <?php if ($_SESSION['perfilid']==3 or $_SESSION['perfilid']==4) {
                            # aqui iran los enlaces de momento se quedan fuera para pruebas
                        }?>
                        
                        <div class="col-md-12 center-block">
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <a href="<?php echo base_url(); ?>Ventas">
                                    <img src="<?php echo base_url(); ?>public/img/QR.png" style="width: 100%;">
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo base_url(); ?>Clientes">
                                    <img src="<?php echo base_url(); ?>public/img/BUSQUEDA.png" style="width: 100%;">
                                </a>
                                
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo base_url(); ?>Clientes/Clienteadd">
                                    <img src="<?php echo base_url(); ?>public/img/nino.png" style="width: 100%;">
                                </a>
                                
                            </div>
                        </div>
                        <div class="col-md-12 center-block">
                            <div class="col-md-3"></div> 
                            <div class="col-md-6">
                                <div class="col-md-6" style="text-align: center;">
                                    <br><br>
                                    <h2>Niños activos</h2>
                                    <h1 class="titlekids"><?php echo $ninost;?></h1>
                                </div>
                                <div class="col-md-6" style="text-align: center;">
                                    <br><br>
                                    <h2>Adultos activos</h2>
                                    <h1 class="titlekids"><?php echo $adultost;?></h1>
                                </div>
                            </div> 
                            <div class="col-md-3"></div> 

                            
                        </div>
                    
                	
                    
                	<!-------------->
                </div>
            </div>
        </div>
    </div>
</div>
