<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Clientes/ModeloCliente');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy=date('Y-m-d H:i:s');
    }

    function index(){

    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Clientes/Clienteslista');
        $this->load->view('Clientes/JsClientes');
        $this->load->view('templates/footer');
    }

    function Ninos($id){
        $arreglo=$this->ModeloCliente->getNinos($id);
        return $arreglo;
    }

    function Clienteadd(){
        $data['Estado']=$this->ModeloCatalogos->getestados();
        $data['Parentesco']=$this->ModeloCatalogos->getparentesco();
        if (isset($_GET['Cli'])){
            $id=$_GET['Cli'];
            $data['Texto']="Edicion de";
            $resultado=$this->ModeloCliente->VerCliente($id);
            foreach ($resultado->result() as $item) {
                $data['titularId']=$item->titularId;
                $data['nombre']=$item->nombre;
                $data['movil']=$item->movil;
                $data['parentesco']=$item->parentesco;
                $data['email']=$item->email;
                $data['tel_emergencia1']=$item->tel_emergencia1;
                $data['tel_emergencia2']=$item->tel_emergencia2;
                $data['pass']=$item->pass;
            }
            }else{
            $data['titularId']="";
            $data['nombre']="";
            $data['apellidos']="";
            $data['movil']="";
            $data['parentesco']="";
            $data['email']="";
            $data['tel_emergencia1']="";
            $data['tel_emergencia2']="";
            $data['pass']="";
        }
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Clientes/Clienteadd',$data);
        $this->load->view('Clientes/JsClientes');
        $this->load->view('templates/footer');
    }

    function RegistrarCliente(){
        $data=$this->input->post();
        $data['reg']=$this->fechahoy;
        echo $this->ModeloCliente->Insert($data,'titular');
    }

    function RegistrarSubTitular(){
        $data=$this->input->post();
        $id=$data['ID'];
        unset($data['ID']);
        $val=count($data);
        $arreglo=$val/4;
        for ($i=0; $i < $arreglo ; $i++) { 
                    $arr['titularId']=$id;
                    $arr['nombre']=$data['NombreS'.$i];
                    $arr['movil']=$data['TelefonoMS'.$i];
                    $arr['parentesco']=$data['ParentescoS'.$i];
                    if($data['NombreS'.$i]!=''){
                        $this->ModeloCliente->Insert($arr,'titular_sub');
                    }
                }
                echo $id;
    }

    function RegistrarMenor(){
        $data=$this->input->post();
        $id=$data['ID'];
        unset($data['ID']);
        $val=count($data);
        $arreglo=$val/3;
        for ($i=0; $i < $arreglo ; $i++) { 
                    $arr['nombre']=$data['NombreM'.$i];
                    $arr['fecha_nacimiento']=$data['FechaN'.$i];
                    $arr['escuela']=$data['Escuela'.$i];
                    $arr['usuario_id']=$id;
                    if ($data['NombreM'.$i]!='') {
                        $this->ModeloCliente->Insert($arr,'ninos');
                    }
                    
                }
                echo $id;
    }

    function UpdateCliente(){
        $data=$this->input->post();
        $ID=$data['ID'];
        unset($data['ID']);
        unset($data['url']);
        $this->ModeloCliente->Update($data,'titularId',$ID,'titular');
        echo $ID;
    }

    function UpdateSubTitular(){
        $data=$this->input->post();
        $id=$data['ID'];
        unset($data['ID']);
        unset($data['contador']);
        $val=count($data);
        $arreglo=$val/5;
        for ($i=0; $i < $arreglo ; $i++) { 
                    $arr['titularId']=$id;
                    $Id=$data['IdSubT'.$i];
                    $arr['nombre']=$data['NombreS'.$i];
                    $arr['movil']=$data['TelefonoMS'.$i];
                    $arr['parentesco']=$data['ParentescoS'.$i];
                    if ($Id=="") {
                    $this->ModeloCliente->InsertU1($arr['titularId'],$arr['nombre'],$arr['movil'],$arr['parentesco']);
                    }else{
                    $this->ModeloCliente->UpdateSubT($Id,$arr['nombre'],$arr['movil'],$arr['parentesco']);
                    }
                }
                echo $id;
    }

    function UpdateMenor(){
        $data=$this->input->post();
        $id=$data['ID'];
        unset($data['ID']);
        unset($data['contador2']);
        $val=count($data);
        $arreglo=$val/4;
        for ($i=0; $i < $arreglo ; $i++) { 
                    $Id=$data['IdM'.$i];
                    $arr['nombre']=$data['NombreM'.$i];
                    $arr['fecha_nacimiento']=$data['FechaN'.$i];
                    $arr['escuela']=$data['Escuela'.$i];
                    $arr['usuario_id']=$id;
                    if ($Id=="") {
                    $this->ModeloCliente->InsertU2($arr['nombre'],$arr['fecha_nacimiento'],$arr['escuela'],$arr['usuario_id']);
                    }else{
                    $this->ModeloCliente->UpdateMenor($arr['nombre'],$arr['fecha_nacimiento'],$arr['escuela'],$Id);
                    }
                }
                echo $id;
    }

    function DeleteAll(){
        $Id=$this->input->post('id');
        $this->ModeloCliente->Delete($Id);
    }

    function DeleteSubt(){
        $Id=$this->input->post('id');
        $this->ModeloCliente->DeleteSubt($Id);
    }

    function DeleteMen(){
        $Id=$this->input->post('id');
        $this->ModeloCliente->DeleteMen($Id);
    }
    function associar(){
        $Id=$this->input->post('id');
        $codigo=$this->input->post('codigo');
        $this->ModeloCliente->associar($Id,$codigo);
    }
    function BuscarCliente(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloCliente->clientesallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr>
                <td><?php echo $item->titularId; ?></td>
                <td><?php echo $item->nombre; ?></td>
                <td><?php echo $item->movil; ?></td>
                <?php $this->load->model('Clientes/ModeloCliente');?>
                <td><?php echo $this->ModeloCliente->getNinos($item->titularId); ?></td>
                <td>
                  <div class="row">
                    <div class="col-md-12" align="center">
                      <a class="btn btn-primary" href="<?php echo base_url();?>Clientes/Clienteadd?Cli=<?php echo $item->titularId; ?>"><i class="ft-edit-3"></i></a>
                      <a class="btn btn-primary" href="<?php echo base_url();?>Ventas/VentasEstacia2?cod=<?php echo $item->titularId; ?>">venta Estancia</a>
                      <a class="btn btn-danger" onclick="DeleteCliente( <?php echo $item->titularId; ?>)"><i class="fa fa-trash-o"></i></a>
                    </div>
                  </div>
                </td>
              </tr>
        <?php }
    }
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloCliente->titularallsearch($usu);
        echo json_encode($results->result());
    }
    public function getlistclientes() {
        $params = $this->input->post();
        $getdata    = $this->ModeloCliente->getlistclientes($params);
        $totaldata  = $this->ModeloCliente->getlistclientes_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
}
?>