    </div>
        </div>

        <footer class="footer footer-static footer-light">
          <p class="clearfix text-muted text-sm-center px-2"><span>Copyright  &copy; 2019 <a href="http://www.mangoo.mx" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2">Mangoo Software </a>, All rights reserved. </span></p>
        </footer>

      </div>
    </div>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/prism.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/sweetalert2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pGenerator.jquery.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/jquery.steps.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/picker.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pickadate/legacy.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/toastr.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    <link href="<?php echo base_url(); ?>app-assets/vendors/js/pace/themes/red/pace-theme-bounce.css" rel="stylesheet" />
    <!-- BEGIN APEX JS-->
    <script src="<?php echo base_url(); ?>app-assets/js/app-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/notification-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/customizer.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/tooltip.js" type="text/javascript"></script>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!--<script src="<?php echo base_url(); ?>app-assets/js/dashboard1.js" type="text/javascript"></script>-->

    <script src="<?php echo base_url(); ?>/app-assets/js/components-modal.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/public/js/plugins.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>app-assets/vendors/js/chartist.min.js" type="text/javascript"></script>
    
    <!--
    <script type="text/javascript" src='<?php echo base_url(); ?>public/plugins/ckeditor/ckeditor.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>public/plugins/ckeditor/adapters/jquery.js'></script>-->
    <link href="<?php echo base_url(); ?>public/css/jasny-bootstrap.min.css" rel="stylesheet">
    <script  src="<?php echo base_url(); ?>public/js/jasny-bootstrap.min.js"></script>
    <link href="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <script  src="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <script src="<?php echo base_url(); ?>public/js/moment.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>public/js/moment-with-locales.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#registro').modal("show");
        });
        
    </script>
    
    
    <!-- END PAGE LEVEL JS-->
  </body> 
</html> 