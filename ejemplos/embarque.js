$(document).ready(function() {
	var tr_pro=1;
    // select2 sin datos===================
    $('#marca').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Producto',
        ajax: {
            url: 'Embarque/searchmarcas',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.marcaId,
                    text: element.marca
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    }).change(function(){
        if ($('#bodega').val() != null) {
            generaclave();
        }
    });
    $('#bodega').select2({
        placeholder: {
            id: 0,
            text: 'Seleccione una Bodega'
        },
        width: '100%',
        allowClear: true
    }).change(function(){
    	if ($('#bodega').val() != null) {
    		camaras($('#bodega').val());
    		generaclave();
    	}
    });
    $("#bodega").val(0).change();
    $('#producto').select2({
        placeholder: {
            id: 0,
            text: 'Seleccione un producto'
        },
        width: '100%',
        allowClear: true
    }).change(function(){
    	if ($('#producto').val() != null) {
    		generaclave();
    	}  	
    });;
    $("#producto").val(0).change();

    $('#categoria').select2({
        placeholder: {
            id: 0,
            text: 'Seleccione una categoria'
        },
        width: '100%',
        allowClear: true
    }).change(function(){
    	if ($('#categoria').val() != null) {
    		generaclave();
    	}
    	
    });
    $("#categoria").val(0).change();
    $('#agregarem').click(function(){
    	var $formproducto = $("#formproducto").valid();
        if ($formproducto) {
        	var cantidad='<input type="text" min="0"  id="scantidad" name="scantidad" class="inputtable" value="'+$('#cantidad').val()+'" readonly>';
        	var productoid='<input type="hidden" min="0"  id="sproductoId" name="sproductoId" class="inputtable" value="'+$('#producto').val()+'">';
        	var producto='<input type="text" min="0"  id="sproducto" name="sproducto" class="inputtable" value="'+$('#producto option:selected').text()+'" readonly>';
        	var categoria='<input type="hidden" min="0"  id="scategoriaoId" name="scategoriaoId" class="inputtable" value="'+$('#categoria option:selected').val()+'" readonly>';
        	var clave='<input type="text" min="0"  id="sclave" name="sclave" class="inputtable" value="'+$('#clave').val()+'" readonly>';
        	var bodega='<input type="hidden" min="0"  id="sbodegaId" name="sbodegaId" class="inputtable" value="'+$('#bodega option:selected').val()+'" readonly>';
        	var camara='<input type="hidden"  id="scamaraId" name="scamaraId" class="inputtable" value="'+$('#camara option:selected').val()+'" readonly>';
            var smarca='<input type="hidden" min="0"  id="smarca" name="smarca" class="inputtable" value="'+$('#addmarcas option:selected').val()+'" readonly>';
            var costofrinput='<input type="text" min="0"  id="scofr" name="scofr" class="inputtable" value="'+$('#pfruta').val()+'" readonly>';
            var costoflinput='<input type="text" min="0"  id="scofl" name="scofl" class="inputtable" value="'+$('#pflete').val()+'" readonly>';
            var costominput='<input type="text" min="0"  id="scom" name="scom" class="inputtable" value="'+$('#pmov').val()+'" readonly>';
            var costocaja='<input type="text" min="0"  id="scajas" name="scajas" class="inputtable" value="'+$('#pcaja').val()+'" readonly>';
            var costoinput='<input type="text" min="0"  id="scosto" name="scosto" class="inputtable" value="'+$('#costo').val()+'" readonly>';
        	var bttn='<a class="btn vd_btn vd_round-btn btn-sm vd_bg-googleplus mgr-10" onclick="detelepro('+tr_pro+')"><i class="fa  fa-minus fa-fw"></i></a>';
            var tipocaja='<input type="text" id="stipocaja" name="stipocaja" class="inputtable" value="'+$('#tipocaja option:selected').text()+'" readonly>';
            var rowtrem='<tr class="tr_pro_'+tr_pro+'">';
               rowtrem+='   <td>'+productoid+''+producto+''+categoria+''+bodega+''+camara+''+smarca+'</td>';
               rowtrem+='   <td>'+clave+'</td><td>'+$('#bodega option:selected').text()+'</td>';
               rowtrem+='   <td>'+$('#camara option:selected').text()+'</td>';
               rowtrem+='   <td>'+tipocaja+'</td>';
               rowtrem+='   <td>'+cantidad+'</td>';
               rowtrem+='   <td>'+costofrinput+'</td>';
               rowtrem+='   <td>'+costoflinput+'</td>';
               rowtrem+='   <td>'+costominput+'</td>';
               rowtrem+='   <td>'+costocaja+'</td>';
               rowtrem+='   <td>'+costoinput+'</td>';
               rowtrem+='   <td>'+bttn+'</td>';
               rowtrem+='</tr>';
    		$('#addproductos').append(rowtrem);
    		carculartotal();
    	tr_pro++;
    	$("#formproducto")[0].reset();
    	$("#producto").val(0).change();
    	$("#categoria").val(0).change();
    	$("#bodega").val(0).change();
    	$("#camara").val(0).change();
        }
    });
    $('#saveembarque').click(function(){
    	var $formembarqueg = $("#formembarqueg").valid();
        if ($formembarqueg) {
        	$.ajax({
                    type:'POST',
                    url: 'Embarque/addembarque',
                    data: {prove:$('#proveedor').val(),
                    		folio:$('#folio').val(),
                    		fecha:$('#fecha').val(),
                    		camion:$("#camion" ).val(),
                    		referencia: $('#referencia').val(),
                    		fechasalida: $('#fechassalida').val()
                    	},
                    async: false,
                    statusCode:{
                        404: function(data){
                            notification("topright","error","fa fa-exclamation-triangle vd_yellow","Error!",'No se encuentra el archivo');
                        },
                        500: function(){
                            notification("topright","error","fa fa-exclamation-triangle vd_yellow","Error!","500");
                        }
                    },
                    success:function(data){
                        notification("topright","success","fa fa-exclamation-triangle vd_yellow","Hecho!","Se ha cargado el nuevo embarque correctamente");
                        console.log(data);
                        var idembarque=data;
                        var DATA  = [];
            			var TABLA   = $("#tableproductos tbody > tr");
            				TABLA.each(function(){
                				item = {};
                				item ["prod"] = $(this).find("input[id*='sproductoId']").val();
				                item ["categ"] = $(this).find("input[id*='scategoriaoId']").val();
				                item ["bodega"] = $(this).find("input[id*='sbodegaId']").val();
				                item ["clave"] = $(this).find("input[id*='sclave']").val();
				                item ["cantidad"] = $(this).find("input[id*='scantidad']").val();
				                item ["costofr"] = $(this).find("input[id*='scofr']").val();
                                item ["costofl"] = $(this).find("input[id*='scofl']").val();
                                item ["costom"] = $(this).find("input[id*='scom']").val();
                                item ["costo"] = $(this).find("input[id*='scosto']").val();
				                item ["camara"] = $(this).find("input[id*='scamaraId']").val();
                                item ["tipocaja"] = $(this).find("input[id*='stipocaja']").val();
                                item ["cajacosto"] = $(this).find("input[id*='scajas']").val();
                                item ["marca"] = $(this).find("input[id*='smarca']").val();
				                item ["idembarque"] =idembarque;
				                DATA.push(item);
            				});
            				INFO  = new FormData();
            				aInfo   = JSON.stringify(DATA);
            				INFO.append('data', aInfo);
				            $.ajax({
				                data: INFO,
				                type: 'POST',
				                url : 'Embarque/addembarquedell',
				                processData: false, 
				                contentType: false,
				                success: function(data){
									$("#formembarqueg")[0].reset();
									$('#addproductos').html('');
									carculartotal();
                                    setTimeout(function(){ location.reload(); }, 2000);
                                    ;
				                }
				            });
                    }
                }); 
        }
    });
    $('#verdetallescamaras').click(function(){
        if ($('#bodega').val()!=null) {
            $('#modaldetallescamaras').modal();
            $.ajax({
                    type:'POST',
                    url: 'Embarque/detallescamaras',
                    data: {bode:$('#bodega option:selected').val()},
                    async: false,
                    statusCode:{
                        404: function(data){
                            notification("topright","error","fa fa-exclamation-triangle vd_yellow","Error!",'No se encuentra el archivo');
                        },
                        500: function(){
                            notification("topright","error","fa fa-exclamation-triangle vd_yellow","Error!","500");
                        }
                    },
                    success:function(data){
                        $('#modaldetallescamara').html(data);
                    }
                });
        }else{
            notification("topright","error","fa fa-exclamation-triangle vd_yellow","Error!","Debe seleccionar una Bodega");
        }
    });
    $('#viewmodalmarcas').click(function(event) {
        $('#modalmarcas').modal();
        $.ajax({
            type:'POST',
            url: 'Embarque/getmarcas',
            
                async: false,
                statusCode:{
                    404: function(data){
                        notification("topright","error","fa fa-exclamation-triangle vd_yellow","Excistente!","No Se encuentra el archivo");
                    },
                    500: function(data){
                        console.log(data.responseText);
                        notification("topright","error","fa fa-exclamation-triangle vd_yellow","Excistente!","500");
                    }
                },
                success:function(data){
                    $('.todaslasmarcas').html(data);
                }
            });
    });
    $('#addmarcassi').click(function(event) {
        if ($('#addmarcas').val()!='') {
            $.ajax({
                type:'POST',
                url: 'Embarque/addmarca',
                data: {
                    marca:$('#addmarcas').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            notification("topright","error","fa fa-exclamation-triangle vd_yellow","Excistente!","No Se encuentra el archivo");
                        },
                        500: function(data){
                            console.log(data.responseText);
                            notification("topright","error","fa fa-exclamation-triangle vd_yellow","Excistente!","500");
                        }
                    },
                    success:function(data){
                        notification("topright","success","fa fa-exclamation-triangle vd_yellow","Hecho!","Marca registrada");
                    }
                });
        }
    });
});
function camaras(bodega){
	params = {};
    params.id = bodega;
	$.ajax({
        url: 'Embarque/gettcamaras',
        type: 'POST',
        data: params,
        dataType: 'json',
        success: function(data) {
			$('#camara').html('');
            $('#camara').select2();
			
            var camara = data;
            var itemcamara = [];

            camara.forEach(function(element) {
                itemcamara.push({
                    id: element.camaraId,
                    text: element.nombre
                });
				console.log(itemcamara);
            });

            $('#camara').select2({
                placeholder: {
                    id: 0,
                    text: 'Seleccione una camara'
                },
                width: '100%',
                allowClear: true,
                data: itemcamara
            });

            $("#camara").val(0).change();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = jqXHR.responseJSON;
            if (jqXHR.status == 401) {
                location.reload();
            }

        }
    });
}
function generaclave(){
	var now = new Date(Date.now());
	var mes= parseInt(now.getMonth())+1;
	var mes= mes<10?'0'+mes:mes;
	var dia =now.getDate();
	var dia=dia<10?'0'+dia:dia;
	var year= now.getFullYear();
	var producto=$('#producto').val()==null?'':$('#producto option:selected').text();
    var marca=$('#marca').val()==null?'':$('#marca option:selected').text();
	var categoria=$('#categoria').val()==null?'':$('#categoria  option:selected').text().replace(/NA| /g,'');
    var camion=$('#camion').val().replace(' ','');
	$('#clave').val(mes+''+dia+''+camion.substring(0,2)+''+producto.substring(0,3)+''+categoria.substring(0,3)+''+marca.substring(0,1));
}
function carculartotal(){
	var totall =0;
    var cantototal=0;
    $("#tableproductos tbody > tr").each(function () {
    	var cantidad=$(this).find("input[id*='scantidad']").val();
    	var costo=$(this).find("input[id*='scosto']").val();
    	var total =parseFloat(cantidad)*parseFloat(costo);
    	//console.log(total+'='+cantidad+'*'+costo);
        cantototal=cantototal+parseFloat(cantidad);
        totall = totall + parseFloat(total);
    });
        $('.classcanttotal').html(cantototal);
	    $('#totalembarque').val(totall);
}
function detelepro(id){
	$('.tr_pro_'+id).remove();
	carculartotal();	
}
function calculartotal(){
    var fruta=$('#pfruta').val()==''?0:$('#pfruta').val();
    var flete=$('#pflete').val()==''?0:$('#pflete').val();
    var movi=$('#pmov').val()==''?0:$('#pmov').val();
    var caja=$('#pcaja').val()==''?0:$('#pcaja').val();
    var totalpa=parseFloat(fruta)+parseFloat(flete)+parseFloat(movi)+parseFloat(caja);
    $('#costo').val(totalpa);
}