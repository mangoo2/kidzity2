<div class="row">
  <div class="col-md-12">
    <h2>Personal</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-11"></div>
    <div class="col-md-1">
      <a href="Personal/Personaladd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de personal</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <!-------------->
          <div class="col-md-12">
            <table class="table table-striped" id="data-tables">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Teléfono</th>
                  <th>Área</th>
                  <th></th>
                  <th>Estado</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($personal->result() as $item){ 
                    if ($item->suspendido==1) {
                      $bottonconfig='btn-danger';
                    }else{
                      $bottonconfig='btn-primary';
                    }
                  ?>
                <tr id="trper_<?php echo $item->personalId; ?>">
                  <td><?php echo $item->nombre; ?></td>
                  <td><?php echo $item->telefono; ?></td>
                  <td><?php echo $item->area; ?></td>
                  <td>
                    
                    <div class="btn-group mr-1 mb-1">
                      <button type="button" class="btn btn-raised btn-icon <?php echo $bottonconfig;?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i>
                      </button>
                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="personal/Personaladd?id=<?php echo $item->personalId; ?>">Editar</a>
                        <?php if ($item->suspendido==1) {?>
                          <a class="dropdown-item" href="#" onclick="dessuspender(<?php echo $item->personalId; ?>);">Reactivar</a>
                        <?php }else{ ?>
                          <a class="dropdown-item" href="#" onclick="suspender(<?php echo $item->personalId; ?>);">Suspender</a>
                        <?php } ?>
                        <a class="dropdown-item" href="#" onclick="personaldelete(<?php echo $item->personalId; ?>);">Eliminar</a>
                        <a class="dropdown-item" href="#" onclick="Horarios(<?php echo $item->personalId; ?>)">Verificar</a>
                        <a class="dropdown-item" href="#" onclick="Creditos(<?php echo $item->personalId; ?>)">Creditos</a>
                      </div>
                    </div>
                  </td>
                  <td <?php if ($item->conect==1) {?> class="table-success" <?php }else{ ?> class="table-warning" <?php } ?> >
                    <?php if ($item->conect==1) { echo "Activo"; }else{ echo "Inactivo"; } ?>
                  </td>
                </tr>
                
                <?php } ?>
                
              </tbody>
            </table>
          </div>
          <!-------------->
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="Horarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registro Puntualidad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
         <input type="hidden" id="IdPersonal" name="IdPersonal">
         <script type="text/javascript">
         </script>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade text-left" id="modalconfirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 ">
                  <h3>¿Desea eliminar al personal?</h3>
                  <input type="hidden" id="pesonaliddelete">
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="aceptareliminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalsuspender" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Suspender</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 ">
                  <h3>¿Desea Suspender al personal?</h3>
                  <input type="hidden" id="pesonalidsuspender">
                  <label>Motivo</label>
                  <textarea id="motivosusmencion" class="form-control"></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="aceptarsuspender">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalcreditos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Creditos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <input type="hidden" id="idpersonal_c">
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Fecha Incio</label>
                    <div class="col-sm-8 controls">
                          <input type="date" placeholder="YYYY-MM-DD" id="fechainicio" name="fechainicio" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Fecha Fin</label>
                    <div class="col-sm-8 controls">
                          <input type="date" placeholder="YYYY-MM-DD" id="fechafin" name="fechafin" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <button class="btn btn-primary" onclick="generarcreditos()">Generar</button>
                </div>
              </div>
              <div class="mostrarcreditos"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#data-tables').dataTable();
  });
</script>