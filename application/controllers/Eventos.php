
<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloFiestas');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Fiestas/ModeloPaquete');
    }

    function index(){
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Catalogos/fiestaslista');
        $this->load->view('Catalogos/fiestaslistajs');
        $this->load->view('templates/footer');
    }
    function Eventosadd(){
        $data['Paquetel']=$this->ModeloPaquete->GetPaqutes(); 
        if (isset($_GET['eve'])){
            $data['title']='Editar';
            $data['titlebtn']='Actualizar';
            $id=$_GET['eve'];
            $data['fiestaId']=$id;
            $resultados=$this->ModeloCatalogos->getselectwhere('fiestas','fiestaId',$id);
            foreach ($resultados->result() as $item){
                $data['nombre']=$item->nombre;
                $data['precio_total']=$item->precio_total;
                $data['precioex']=$item->precioex;
                $data['cantidad']=$item->cantidad;
                $data['fecha_inicio']=$item->fecha_inicio;
                $data['hora_inicio']=$item->hora_inicio;
                $data['hora_fin']=$item->hora_fin;
                $data['paquete']=$item->paquete;
            }
        }else{
            $data['title']='Nuevo';
            $data['titlebtn']='Guardar';
            $data['fiestaId']=0;
            $data['nombre']='';
            $data['precio_total']='';
            $data['precioex']='';
            $data['cantidad']='';
            $data['fecha_inicio']='';
            $data['hora_inicio']='';
            $data['hora_fin']='';
            $data['paquete']='';
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Catalogos/fiestasadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('Catalogos/fiestaslistajs');
    }
    function eventosnew(){
        $datos = $this->input->post();
        $id= $datos['fiestaId'];
        echo $id;
        unset($data['fiestaId']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogo($datos,'fiestaId',$id,'fiestas');
        }else{
            $this->ModeloCatalogos->insertToCatalogo($datos,'fiestas');
        }
    }
    function Buscar(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloFiestas->Search($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr>
                <td><?php echo $item->fiestaId; ?></td>
                <td><?php echo $item->nombre; ?></td>
                <td><?php echo $item->precio_total; ?></td>
                <td><?php echo $item->precioex; ?></td>
                <td><?php echo $item->cantidadt; ?></td>
                <td><?php echo $item->cantidadm; ?></td>
                <td><?php echo $item->fecha_inicio; ?></td>
                <td><?php echo $item->hora_inicio; ?></td>
                <td><?php echo $item->hora_fin; ?></td>
                <td>
                  <div class="row">
                    <div class="col-md-12" align="center">
                      <a class="btn btn-primary" href="<?php echo base_url();?>Eventos/Eventosadd?eve=<?php echo $item->fiestaId; ?>"><i class="ft-edit-3"></i></a>
                      <button class="btn btn-primary" onclick="ninosview(<?php echo $item->fiestaId; ?>)" title="niños registrados"><i class="fa fa-user"></i> </button>
                      <?php if ($item->titular!=0) { ?>
                        <a class="btn btn-primary" href="<?php echo base_url();?>Ventas/VentasEstacia2?cod=<?php echo $item->titular; ?>&event=<?php echo $item->fiestaId; ?>">Venta Estancia</a>
                      <?php } ?>
                    </div>
                  </div>
                </td>
              </tr>
          <?php }
    }
    function ninosavtivos(){
        $idfiesta = $this->input->post('idfiesta');
        $respuesta=$this->ModeloFiestas->consultarninosfiesta($idfiesta);

        ?>
            <table class="table table-sm table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th>Niño</th>
                                    <th>Pulcera</th>
                                    <th>entrada</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($respuesta->result() as $item){ ?>
                                    <tr>
                                        <td><?php echo $item->nino; ?></td>
                                        <td><?php echo $item->pulcera; ?></td>
                                        <td><?php echo $item->reg_abierto; ?></td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
        <?php
    }
    public function getlisteventos() {
        $params = $this->input->post();
        $getdata    = $this->ModeloFiestas->getlisteventos($params);
        $totaldata  = $this->ModeloFiestas->getlisteventos_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }


}
?>