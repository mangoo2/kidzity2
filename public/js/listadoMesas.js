var base_url = $('#base_url').val();
function load() { 
    // Destruye la tabla y la crea de nuevo con los datos cargados  
    table.destroy(); 
    table = $('#tableListadoMesas').DataTable({ 
        "ajax": { 
            "url": "Listadoventasmesas/getListadoVentasMesas" 
        }, 
        "columns": [ 
            {"data": "ventaId",class:"uniqueClassName"}, 
            {"data": "nombre",class:"uniqueClassName"}, 
            {"data": "metodo",class:"uniqueClassName", 
                render:function(data,type,row){ 
                    // Si el estatus es diferente de activo, se mostrará el botón para "Activar"; de lo contrario será suspender 
                    if(data==1){ 
                        return 'Efectivo'; 
                    }else{ 
                        return 'Crédito / Débito'; 
                    }  
                } 
            }, 
            {"data": "monto_total",class:"uniqueClassName"}, 
            {"data": "descuento",class:"uniqueClassName"}, 
            {"data": "monto_cobrado",class:"uniqueClassName"}, 
            {"data": "activo",class:"uniqueClassName",visible:false}, 
            {"data": "mesa",class:"uniqueClassName"}, 
            {"data": "reg",class:"uniqueClassName"}, 
            {"data": "tipo",class:"uniqueClassName", 
                render:function(data,type,row){ 
                    // Si el estatus es diferente de activo, se mostrará el botón para "Activar"; de lo contrario será suspender 
                    if(data==1){ 
                        return 'Venta Realizada'; 
                    }else{ 
                        return 'Venta Impresa'; 
                    }  
                } }, 
            {"data": "ventaId", 
                render:function(data,type,row){ 
                    var actualizacionticket=$('#actualizacionticket').val();
                    // Se muestran todos los botones correspondientes 
                    var html='<div class="btn-group mr-1 mb-1">\
                                    <button type="button" class="btn btn-raised gradient-blackberry white sidebar-shadow dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                    <i class="fa fa-cog"></i>\
                                    </button>\
                                    <div class="dropdown-menu arrow" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -12px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                        <button class="dropdown-item" type="button" onclick="ticket('+row.ventaId+')">\
                                            <i class="fa fa-book"></i> Ticket</button>';
                                            if (actualizacionticket==1) {
                                                html+='<button class="dropdown-item" type="button" onclick="devolucion('+row.ventaId+')">\
                                                    <i class="fa fa-repeat"></i>\
                                                    Actualizar</button>';
                                            }
                                        
                                    html+='</div>\
                                </div>';
                    return html; 
                } 
            } 
        ], 
        "order": [[ 0, "desc" ]], 
        "lengthMenu": [[25, 50, 100], [25, 50, 100]], 
        // Cambiamos lo principal a Español 
        "language": { 
            "lengthMenu": "Desplegando _MENU_ elementos por página", 
            "zeroRecords": "Lo sentimos - No se han encontrado elementos", 
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros", 
            "info": "Mostrando página _PAGE_ de _PAGES_", 
            "infoEmpty": "No hay registros disponibles", 
            "infoFiltered": "(Filtrado de _MAX_ registros totales)", 
            "search": "Buscar : _INPUT_", 
            "paginate": { 
                "previous": "Página previa", 
                "next": "Siguiente página" 
              } 
        } 
    });   
} 
$(document).ready(function () { 
    table = $('#tableListadoMesas').DataTable(); 
    load();
    $('.confirmaciondelete').click(function(event) {
        var motivo = $('#motivocancelar').val();
        if (motivo!='') {
            params = {};
            params.id = $('#cancelapro').val();
            params.moti = motivo;
            $.ajax({
                type:'POST',
                url:base_url+'Listadoventasmesas/procancelt',
                data:params,
                async:false,
                statusCode:{
                        404: function(data){
                            swal({type: "error", title:"Error 404", timer: 5000});
                        },
                        500: function(data){
                            swal({type: "error", title:"Error 500", timer: 5000});
                        }
                },
                success:function(data){
                    swal({type: "success",title:"Cancelado",timer: 2000});
                    $('#modaldevolicion').modal('hide');
                    devolucion(data);
                }
            }); 

        }else{
            toastr.error('Debe de contener un motivo','Error' );
        }
    }); 
    $('#cantidadaregresar').on('input', function(){ 
        var cantidad=$(this).val();
        var cantidadd=$('#cantidadaregresar').data('cantidad');
        if (cantidad>=cantidadd) {
            $(this).val(parseFloat(cantidadd)-1);
        }else if(cantidad<0){
            $(this).val(1);
        }
    });
    $('.confirmaciondeletep').click(function(event) {
        var motivo = $('#motivocancelarp').val();
        var cantidad = $('#cantidadaregresar').val();
        if (motivo!=''&&cantidad>0) {
            params = {};
            params.id = $('#cancelaprop').val();
            params.moti = motivo;
            params.cant = cantidad;
            $.ajax({
                type:'POST',
                url:base_url+'Listadoventasmesas/procanceltp',
                data:params,
                async:false,
                statusCode:{
                        404: function(data){
                            swal({type: "error", title:"Error 404", timer: 5000});
                        },
                        500: function(data){
                            swal({type: "error", title:"Error 500", timer: 5000});
                        }
                },
                success:function(data){
                    swal({type: "success",title:"Cancelado",timer: 2000});
                    $('#modaldevolicion').modal('hide');
                    devoluciondatos(data);
                }
            }); 

        }else{
            toastr.error('Debe de contener un motivo y cantidad mayor a 1','Error' );
        }
    });
    $('#categoriaadd').change(function(event) {
        var categoria=$('#categoriaadd option:selected').val();
        params = {};
        params.id = categoria;
        $.ajax({
            type:'POST',
            url:base_url+'Listadoventasmesas/procat',
            data:params,
            async:false,
            statusCode:{
                    404: function(data){
                        swal({type: "error", title:"Error 404", timer: 5000});
                    },
                    500: function(data){
                        swal({type: "error", title:"Error 500", timer: 5000});
                    }
            },
            success:function(data){
                $('#productoadd').html(data);
                $('#productoadd').select2({
                    dropdownParent: $("#modaldevolicion")
                }).change(function(){
                    if ($('#productoadd').val() != null) {
                        var costo=$('#productoadd option:selected').data('costo');
                        $('#costopro').val(costo);
                    }
                });

            }
        }); 
    });
    var proext=0;
    $('.addproductoextra').click(function(event) {
        var producto = $('#productoadd option:selected').val();
        var productotext = $('#productoadd option:selected').text();
        var costo = $('#costopro').val();
        var cantidad = $('#cantidadpro').val();
        if(cantidad>0){
            var total = parseFloat(costo)*parseFloat(cantidad);
            var html='<tr class="proext_'+proext+'">\
                            <td>\
                                '+productotext+'\
                                <input type="hidden"  id="proaddext" value="'+producto+'" readonly>\
                            </td>\
                            <td>\
                                <input type="number"  id="costoaddext" value="'+costo+'" readonly style="border:0;background:transparent;">\
                            </td>\
                            <td>\
                                <input type="number"  id="cantaddext" value="'+cantidad+'" readonly style="border:0;background:transparent;">\
                            </td>\
                            <td>\
                                '+total+'\
                            </td>\
                            <td><button type="button" class="btn mr-1 mb-1 btn-danger btn-sm" onclick="depeteext('+proext+')"><i class="fa fa-trash-o"></i></button></td>\
                        </tr> ';
            $('.productosextrasadd').append(html);
            proext++;
        }else{
            toastr.error('Debe de agregar una cantidad igual o mayor a 1','Error');
        }
    });
    $('.addproductoextraadd').click(function(event) { 
        if ($('#productosextras  tbody > tr').length>0) {
            //===============================================
            var DATA  = [];
            var TABLA   = $("#productosextras tbody > tr");
            TABLA.each(function(){    
                item = {};
                item ["producto"] = $(this).find("input[id*='proaddext']").val();     
                item ["cantidad"] = $(this).find("input[id*='cantaddext']").val();
                item ["precio"] = $(this).find("input[id*='costoaddext']").val();

              
              DATA.push(item);
            });
            productos   = JSON.stringify(DATA);
            $.ajax({
                type: 'POST',
                url: base_url+'Listadoventasmesas/editarventa',
                data: {
                    ventaId:$('#ventaidadd').val(),
                    productos:productos
                },
                async: false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        
                    },
                },
                success: function(data) {
                        console.log(data);

                        popup = window.open(base_url+"Ticketmesas/dulceria?comp="+data,"popup","width=700,height=300,scrollbars=yes");
                        popup.focus(); 
                        devoluciondatos(data);

                        toastr.success('venta Editada', 'Hecho');
                        $('.productosextrasadd').html('');
                },
            });
            //===============================================

        }else{
            toastr.error('Agregar producto','Error')
        }
    });
    
}); 
 
function ticket(id){ 
    $("#iframeri").modal(); 
    $('#iframereporte').html('<iframe src="'+$('#base_url').val()+'Ticketmesas/dulceria?comp='+id+'"></iframe>'); 
}
function devolucion(id){
    //$('#modaldevolicion').modal('hide');
    $('#modaldevolicion').modal();
    devoluciondatos(id);   
}
function devoluciondatos(id){
    $.ajax({
            type: 'POST',
            url: 'Listadoventasmesas/productos',
            data: {
                venta: id
            },
            async: false,
            statusCode: {
                404: function(data) {toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(data) {toastr.error('Error', '500');}
            },
            success: function(data){
                // Reinicializa el panel de los productos
                $('.listadoproductos').html('');
                $('.listadoproductos').html(data);
                
            }
        });
}
function canceladototal(id,name){
    $('#modalconfirmacion').modal();
    $('.classdpro').html(name);
    $('#cancelapro').val(id);
}
function canceladoparcial(id,name,cantidad){
    $('#modalconfirmacionp').modal();
    $('.classdprop').html(name);
    $('#cancelaprop').val(id);
    $('#cantidadaregresar').val(1);
    $('#cantidadaregresar').data('cantidad',cantidad);
}
function depeteext(id){
    $('.proext_'+id).remove();
}
