<?php
    $detallepro=$this->ModeloCatalogos->getventadetalle($idventa);
    foreach ($configticket->result() as $item){
      $id_ticket = $item->id_ticket;
      $titulo = $item->titulo;
      $mensaje = $item->mensaje;
      $mensaje2 = $item->mensaje2;
      $fuente = $item->fuente;
      $tamano = $item->tamano;
      $margensup = $item->margensup;
      $margenlat = $item->margenlat;
    } 
    $iva=1.16;
    if($metodo==1){
      $tipometodo="Pago en Efectivo";
    }elseif($metodo==2){
      $tipometodo="Pago con Tarjeta";
    }elseif ($metodo==3) {
      $tipometodo="Pago con saldo Kids";
    }else{
      $tipometodo="";
    }
?>
<?php
    //=================================================================
    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    //=======================================================================================
    class MYPDF extends TCPDF {
      //Page header
      public function Header() {
          //$img_header = 'header.jpg';
          //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
          //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
          $imglogo = base_url().'public/img/kidb.png';
          $html = '
              ';
            $this->writeHTML($html, true, false, true, false, '');
      }
        // Page footer
      public function Footer() {
          $html = ' 
          <table width="100%" border="0">
            <tr>
              <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
            </tr>
          </table>';
          //$this->writeHTML($html, true, false, true, false, '');
      }
    } 
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 210), true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('agb');
    $pdf->SetTitle('Ticket');
    $pdf->SetSubject('Ticket');
    $pdf->SetKeywords('Ticket');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins($margenlat, $margensup, $margenlat);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->SetFont('dejavusans', '', $tamano);
    // add a page
    $pdf->AddPage();
?>
<?php
$imglogo = base_url().'public/img/kidb.png';
$html = '<table border="0">';
$html .= '  <tr>
              <td colspan="3" align="center"><img src="'.$imglogo.'" width="130px" ></td>
          </tr>
          <tr>
              <td colspan="3" align="center"></td>
          </tr>
          <tr>
            <td>Folio:</td>
            <td colspan="3" align="center">'.$idventa.'</td>
          </tr>
          <tr>
            <td colspan="3" align="center">'.$reg.'</td>
          </tr>

          ';

$html .= '  <tr>
              <td colspan="3" align="center">'.$titulo.'</td>
          </tr>
         ';
$html .= '  <tr>
              <td colspan="3" align="center">'.$mensaje.'</td>
          </tr>
          <tr>
              <td colspan="3" align="center"></td>
          </tr>
          <tr>
            <td align="center" colspan="3" style="background-color:#000000; color: white;">
                <strong>Cuenta</strong>
            </td>
        </tr>
         ';
$html .= ' 
          
          <tr>
                <th colspan="3" align="center" style="font-size: 9"></th>
            </tr>
            <tr>
                <th style="font-size: 8; " align="center" align="center" width="26%" style="background-color: #9C9B99; ">Cant.</th>
                <th style="font-size: 8; " align="center" align="center" width="46%" style="background-color: #9C9B99; ">Articulo</th>
                <th style="font-size: 8; " align="center" align="center" width="28%" style="background-color: #9C9B99; ">Precio</th>
            </tr>

        ';
$total=0;
$subtotal=0;
foreach ($detallepro->result() as $item) {
  $precioi=$item->precio/$iva;
  $html .='<tr>
                <th style="font-size: 8; " align="center">'.$item->cantidad.'</th>
                <th style="font-size: 8; " align="center" >'.$item->producto.'</th>
                <th style="font-size: 8; " align="center" >'.number_format($precioi,2,'.',',').'</th>
            </tr>';
            $total=$total+($item->cantidad*$item->precio);
            $subtotal=$subtotal+($item->cantidad*$precioi);

}
//$subtotal=$total-($total*$iva);
$ivat=$total-$subtotal;
$html .='<br>
            <tr>
                <th colspan="2" align="center" style="background-color: #9C9B99; ">SubTotal:</th>
                <th style="font-size: 8; " align="center" style="background-color: #9C9B99; ">$ '.number_format($subtotal,2,'.',',').'</th>
            </tr>
            <tr>
                <th colspan="2" align="center" style="background-color: #9C9B99; ">Iva:</th>
                <th style="font-size: 8; " align="center" style="background-color: #9C9B99; ">$ '.number_format($ivat,2,'.',',').'</th>
            </tr>
            <tr>
                <th colspan="2" align="center" style="background-color: #9C9B99; ">Total:</th>
                <th style="font-size: 8; " align="center" style="background-color: #9C9B99; ">$ '.number_format($total,2,'.',',').'</th>
            </tr>
            <tr>
                <th colspan="3" align="center" style="background-color: #9C9B99; ">'.$tipometodo.'</th>
                
            </tr>';
if ($metodo==3) {


  $titular = $this->ModeloCatalogos->getselectvalue1rowwhere('compra_tiempo','titularId','compraId',$compraId);
  $saldo = $this->ModeloCatalogos->sumasaldosdisponibles($titular);
  $html .='<br>
            <tr>
                <th colspan="2" align="center" style="background-color: #9C9B99; ">Saldo Kids disponible:</th>
                <th style="font-size: 8; " align="center" style="background-color: #9C9B99; ">'.$saldo.'</th>
            </tr>';
}

$html .= '  <tr>
              <td colspan="3" align="center">'.$mensaje2.'</td>
          </tr>
          <tr>
              <td colspan="3" align="center"></td>
          </tr>
          <tr>
              <td colspan="3" align="center"></td>
          </tr>
          <tr>
        <td align="center" colspan="3" style="background-color:#000000; color: white;">
            <strong>Politicas de Kids Universe</strong>
        </td>
        </tr>
        <tr>
              <td colspan="3" align="center"></td>
          </tr>
        <tr>
              <td colspan="3" align="center"></td>
          </tr>
          <tr style="font-size: 8px">
            <td colspan="3" align="justify">1.- <strong>En ningún caso sera reintegrado el importe de su envío</strong>, solo el valor declarado
            <br>2.- No nos hacemos responsables por artículos no declarados
            <br>3.- El empaque no es responsabilidad nuestra
            <br>4.- El tiempo para verificar perdidas o extravios es de 60 dias

        </td>
    </tr>
         ';
$html .= '</table>';
      $pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>
