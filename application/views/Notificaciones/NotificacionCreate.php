<div class="">
  <section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0">Notificación</h4>
        </div>
        <div class="card-body">
          <div class="px-3">
            <form class="form" role="form" id="formProducto" name="formProducto" method="post" >
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <fieldset class="form-group">
                        <label for="Mensaje">Mensaje</label>
                        <textarea name="Mensaje" class="form-control" value="" id="Mensaje" rows="4"></textarea>
                      </fieldset>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <h5>Enviar a: </h5>
                      <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                          <fieldset>
                            <label>Enviar a todos: </label>
                            <input type="radio" name="Enviar" id="Todos">
                          </fieldset>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12">
                          <fieldset>
                            <label>Usuario especifico: </label>
                            <input type="radio" name="Enviar" id="Todos">
                          </fieldset>
                        </div>
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-6">
                        <br>
                        <fieldset class="form-group">
                          <input type="text" class="form-control" id="User" name="User">
                        </fieldset>
                      </div>
                      <div class="col-xl-2 col-lg-2 col-md-2">
                        <br>
                        <button type="button" class="btn btn-primary"><i class="icon-magnifier"></i>Buscar</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" align="center">
                  <a class="btn btn-primary" href="" id="" ><i class="icon-rocket"></i>Enviar</a>
                  <a class="btn btn-danger" href="">Cancelar<i class="icon-ban"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>