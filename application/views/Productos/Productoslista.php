<style type="text/css">
	.imgproductos{
		height: 100px;
        border-radius: 5px;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<h2>Productos</h2>
	</div>
	<div class="col-md-12">
		<div class="col-md-11"></div>
		<div class="col-md-1">
			<a href="<?php echo base_url(); ?>Productos/Productosadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
		</div>
	</div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Listado de Productos</h4>
			</div>
			<div class="card-body">
				<div class="card-block form-horizontal">
					<!-------------->
						<table class="table table-sm table-hover" id="tabla">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th></th>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Categoría</th>
                                    <th>Stock</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
					
					<!-------------->
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade text-left" id="modalconfirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirmacion</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
              	<div class="col-md-12">
              		<input type="hidden" id="deleteproid">
              		<p>¿Confirma que desea eliminar <span id="deletepronom"></span>?</p>
              	</div>
              </div>
              
              
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="eliminarpro">Aceptar</button>
            </div>
        </div>
    </div>
</div>