<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppServer extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //error_reporting(0);
        $this->load->model('MovilApp/ModeloMovilApp');
        $this->load->model('ModeloCatalogos');
    }

    public function getKids($usuario_id)
    {
        $this->db->where('usuario_id', $usuario_id);
        $result=$this->db->get("ninos");
        $json = json_encode($result->result());
        echo $json ;
        
    }

    public function getPaquetes()
    {
        $this->db->where('Estado', 1);
        $result=$this->db->get("PaquetesFiesta");
        $json = json_encode($result->result());
        echo $json ;
         
    }

    public function signUp(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $pass = password_hash($data["password"], PASSWORD_BCRYPT);
        if($this->db->insert('titular', array("nombre"=>$data["name"],"email"=>$data["email"],"pass"=>$pass,"app"=>1))){
           $json = json_encode(array("result"=>true,"user"=>array("nombre"=>$data["name"],"email"=>$data["email"])));
            echo $json; 
        }
        else{
           $json = json_encode(array("result"=>false,"error"=>"Error"));
            echo $json;  
        }
        

    }

    public function signIn(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $this->db->where("email",$data["email"]);
        $result=$this->db->get("titular");
        
        if($result->num_rows()>0){
            if(password_verify($data["password"],$result->row()->pass)){
                $json = json_encode(array("result"=>true,"user"=>$result->row()));
                echo $json;
            }
            else{
                $json = json_encode(array("result"=>false,"error"=>"La contraseña es incorrecta"));
                echo $json;
            }
        }
        else{
            $json = json_encode(array("result"=>false,"error"=>"El usuario no existe"));
                echo $json;
        }
    }

    public function signInFacebook(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        $this->db->where("email",$data["email"]);
        $result=$this->db->get("titular");
        
        if($result->num_rows()>0){
            $json = json_encode(array("result"=>true,"user"=>$result->row()));
                echo $json;
        }
        else{ //no existe y lo creamos con los datos obtenidos de la API
        	$pass = password_hash("default", PASSWORD_BCRYPT);
            $this->db->insert('titular', array("nombre"=>$data["name"],"email"=>$data["email"],"pass"=>$pass,"app"=>1, "fb_id"=>$data["id"]));
            $json = json_encode(array("result"=>true,"user"=>array("nombre"=>$data["name"],"email"=>$data["email"])));
            echo $json; 
        }
    }

    public function GetSaldo(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $id=implode($data);
        echo $this->ModeloMovilApp->GetSaldo($id);
    }

    public function ValidCode(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $code = implode($data);

        if ($this->ModeloMovilApp->Fiesta($code)) {
            echo "1";
        }else{
            echo "0";
        }

    }

    public function ChekChild(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $val=implode($data);
        $array=$this->ModeloMovilApp->State($val);
        $valid='0';
        foreach ($array->result() as $key){
            if($this->ModeloMovilApp->SimpleGet($key->ninoid,'ninoid','compra_tiempo_nino','status')==1){
                $valid='1';
                break;
            }
        }
        echo $valid;
    }

    public function addKid()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        //print_r($data);
        if($this->db->insert('ninos', $data)){
            echo "1";
        }
        else{
            echo "Error";
        }

    }

    function ChangeFoto(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $this->ModeloMovilApp->SimpleUpdate($data);
    }
    
    public function getPagos($usuario_id)
    {
        
        $this->db->where('usuario_id', $usuario_id);
        $result=$this->db->get("pagos");
        $json = json_encode($result->result());
        echo $json ;
        
    }
    
    public function getCoTitulares($usuario_id)
    {
        //echo $this->ModeloMovilApp->GetAll('titularId',$usuario_id,'titular_sub');   
    
        $this->db->where('titularId', $usuario_id);
        $result=$this->db->get("titular_sub");
        $json = json_encode($result->result());
        echo $json;
    }
    
    public function addTitular()
    {
        $json = file_get_contents('php://input');
       
        if($this->db->insert('titular_sub', $data)){
            echo "Se agregó el Co-titular :)";
        }
        else{
            echo "Error";
        }
        
    }

    public function updateUser($usuario_id)
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);

        if($data["pass"]==''){
            unset($data["pass"]);
        }else{
            $data["pass"] = password_hash($data["pass"], PASSWORD_BCRYPT);
        }
        
        $this->db->where('titularId', $usuario_id);
        if($this->db->update('titular', $data)){
            echo "Actualización exitosa";
        }
        else{
            echo "Error";
        }
        
    }

    function getCosto(){
        $query="SELECT precio FROM costos WHERE costosId=1";
        echo $this->db->query($query)->row()->precio;
    }

    public function GetConfig(){
        echo $this->ModeloMovilApp->SimpleGet(1,'cofigId','config','valor');
    }

    public function sendMail(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $this->load->library('email');

        $config = array();
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'mocha3024.mochahost.com';
        $config['smtp_user'] = 'eventos@kidsuniverse.mx';
        $config['smtp_pass'] = 'rvGBvNJYnw4p';
        $config['smtp_port'] = 465;
        $config["smtp_crypto"] = 'ssl';

        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);

        $this->email->from('eventos@kidsuniverse.mx', 'Fiestas kids Universe');
        $this->email->to("eventos@kidsuniverse.mx");

        $this->email->subject('Solicitud fiesta (APP)');

        $this->db->where('titularId', $usuario_id);
        $result=$this->db->get("titular");
        $data["usuario"] = $result->row();
        
        $msj=$this->load->view('app/mail', $data, true);
        //echo $msj; die;
        $this->email->message($msj);

        $this->email->send();
        
    }

    public function findMail(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        echo $this->ModeloMovilApp->CheckMail($data);
    }

    public function SaldoAdd(){


        $this->load->library('openpay');

        $openpay = Openpay::getInstance('miit2nzsxh6u9kguhzu3',
          'sk_612fe7d1733f427c9be21faa1b3b06d1');

        $json = file_get_contents('php://input');
          
        $data = json_decode($json,true);


        $titular=$data['usuario_id'];

        $customer = array(
             'name' => $data['name'],
             'email' => $data['email']);

        $chargeData = array(
            'method' => 'card',
            'source_id' => $data['token'],
            'amount' => (float)$data['amount'],
            'description' => "cobro test",
            'device_session_id' => $data['sessionId'],
            'customer' => $customer
        );


        $charge = $openpay->charges->create($chargeData);
        //print_r($charge); die;

        if($charge->status=="completed"){
            $saldo=$data['saldo'];
            $pagado=$data['total'];
            $tiempo=$data['tiempo'];
            $ninos=$data['ninos'];
            $referencia=$charge->authorization;

            if($saldo>0){
                $this->db->insert("compra_tiempo",array("titularId"=>$titular,"pagado"=>$pagado,"referencia"=>$referencia)) ; 
            }
            $temp=array("titularId"=>$titular,"metodo"=>3,"tiempo"=>$tiempo, "pagado"=>$pagado,"referencia"=>$referencia , "tipo"=>0);

            $this->db->insert("compra_tiempo",$temp) ; 
            $id= $this->db->insert_id(); 
            //print_r($id); die;
            foreach ($ninos as $n) {
                $id_nino=$n['id'];
                $this->ModeloMovilApp->compratiemponino($id,$id_nino);
            }

            echo "1";
        }
        else{
            echo "0";
        } 

    }

   

    function GetDeposito(){
        $json = file_get_contents('php://input');
        $data = json_decode($json,true);
        $id=$data["id"];
        $result=array(
                "parte1" => $this->ModeloMovilApp->GetDeposito($id), //obtiene saldos
                "parte2" => $this->ModeloMovilApp->GetDeposito2($id) ); //obtiene tiempo
        echo json_encode($result);

    } 

}
        
        

