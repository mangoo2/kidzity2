<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	/* se puede utilizar BASEPATH o APPPATH segun sea el gusto */
	function __construct(){ /* nada por hacer */ }
	class CI_Atlog
	{

		protected $nombre_log = "log-kidsuniverse.txt";
		protected $ruta_log = "logs_kidsuniverse";
		
		public function __construct(){}
		
		public function setNombreLog($nombre_log)
		{
			$this->nombre_log = $nombre_log;
		}
		public function setRutaLog($ruta)
		{
			$this->ruta_log = $ruta;
		}
		
		public function savelog($texto)
		{
			$ruta = BASEPATH.$this->ruta_log;
			
			//si existe la carpeta, pasamos a verificar el archivo
			if(is_dir($ruta))
			{
				//la carpeta existe, entonces creamos el archivo
				$tiempo = "\n[".date("Y-m-d H:i:s")."] ";
				$texto = $tiempo." \n".$texto."\n";

				$this->setNombreLog("log-kidsuniverse-".date('Ymd').".txt");
				$f1 = fopen($ruta.'/'.$this->nombre_log,"a+");
				if($f1)
				{
					//el archivo no existe, entonces lo creamos
					fwrite($f1,$texto);
					fclose($f1);
				}
				else
				{
					echo "No se puede crear el archivo";
					return false;
				}

			}
			else
			{
				//no existe la carpeta, entonces creamos la carpeta
				if(mkdir($ruta))
				{
					//si todo bien, se llama a savelog
					$this->savelog($texto);
				}
				else
				{
					echo "No se puede crear la carpeta para el log";
					return false;
				}
			}
		}
	}
?> 