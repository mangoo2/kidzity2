<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigGeneral extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Configuraciones/ModeloConfiguracion');
        $this->load->model('ModeloCatalogos');
    }

    function index(){
        //===================================
            if (isset($_SESSION['logueo_kids'])) {
                $this->perfilid=$_SESSION['perfilid']; 
                $this->idpersonal=$_SESSION['idpersonal'];
                $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,21);// 14 es el id del menu
                if ($permiso==0) {
                    redirect('/Sistema');
                }
                $panicorest=$this->ModeloCatalogos->panicorest();
            }else{
                redirect('/Sistema');
            }
            if ($panicorest==1) {
                $panicorest='checked';
            }else{
                $panicorest='';
            }
            $data['panicorest']=$panicorest;
            $data['perfilid']=$this->perfilid;
            $data['idpersonal']=$this->idpersonal;
        //===================================
    	$data['Check']=$this->ModeloConfiguracion->Obtain();
        $precios=$this->ModeloCatalogos->getcostotime();
        $precio=0;
        foreach ($precios->result() as $item) {
            $precio=$item->precio;
        }
        $data['precio']=$precio;

    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Configuraciones/ConfG',$data);
        $this->load->view('templates/footer');
        $this->load->view('Catalogos/catalogosjs');
    }

    function Update(){
    	$data=$this->input->post();
    	$val=count($data);
    	$arreglo=$val/2;
    	for ($i=0; $i < $arreglo; $i++) {
    		$id=$data['id'.$i];
    		$val=$data['valor'.$i];
    		$this->ModeloConfiguracion->Update($val,$id);
    	}
    }
    function Updateprecio(){
        $precio=$this->input->post('precio');
        $data = array('precio' =>$precio);
        $this->ModeloCatalogos->updateCatalogo($data,'costosId',1,'costos');
    }
    function devolucion(){
        $id=$this->input->post('id');
        $monto=$this->input->post('montod');
        $motivo=$this->input->post('motivod');
        $data = array('devolucion_monto' =>$monto,'devolucion_motivo'=>$motivo);
        $this->ModeloCatalogos->updateCatalogo($data,'compranId',$id,'compra_tiempo_nino');
        
    }
    function panico(){
        $panico=$this->input->post('panico');
        $data = array('panico' =>$panico);
        $this->ModeloCatalogos->updateCatalogo($data,'config',1,'config2');
    }

}
?>