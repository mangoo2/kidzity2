function load() { 
    // Destruye la tabla y la crea de nuevo con los datos cargados  
    table.destroy(); 
    table = $('#tableListadoMesas').DataTable({ 
        "ajax": { 
            "url": "Listadoventasmesas/getListadoVentasMesas" 
        }, 
        "columns": [ 
            {"data": "ventaId",class:"uniqueClassName"}, 
            {"data": "nombre",class:"uniqueClassName"}, 
            {"data": "metodo",class:"uniqueClassName", 
                render:function(data,type,row){ 
                    // Si el estatus es diferente de activo, se mostrará el botón para "Activar"; de lo contrario será suspender 
                    if(data==1) 
                    { 
                        return 'Efectivo'; 
                    }   
                    else  
                    { 
                        return 'Crédito / Débito'; 
                    }  
                } 
            }, 
            {"data": "monto_total",class:"uniqueClassName"}, 
            {"data": "descuento",class:"uniqueClassName"}, 
            {"data": "monto_cobrado",class:"uniqueClassName"}, 
            {"data": "activo",class:"uniqueClassName",visible:false}, 
            {"data": "mesa",class:"uniqueClassName"}, 
            {"data": "reg",class:"uniqueClassName"}, 
            {"data": "tipo",class:"uniqueClassName", 
                render:function(data,type,row){ 
                    // Si el estatus es diferente de activo, se mostrará el botón para "Activar"; de lo contrario será suspender 
                    if(data==1) 
                    { 
                        return 'Venta Realizada'; 
                    }   
                    else  
                    { 
                        return 'Venta Impresa'; 
                    }  
                } }, 
            {"data": "ventaId", 
                render:function(data,type,row){ 
                    // Se muestran todos los botones correspondientes 
                    var html='<button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket('+row.ventaId+')" title="Ticket" data-toggle="tooltip" data-placement="top"><i class="fa fa-book"></i></button>'; 
                    return html; 
                } 
            } 
        ], 
        "order": [[ 0, "desc" ]], 
        "lengthMenu": [[25, 50, 100], [25, 50, 100]], 
        // Cambiamos lo principal a Español 
        "language": { 
            "lengthMenu": "Desplegando _MENU_ elementos por página", 
            "zeroRecords": "Lo sentimos - No se han encontrado elementos", 
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros", 
            "info": "Mostrando página _PAGE_ de _PAGES_", 
            "infoEmpty": "No hay registros disponibles", 
            "infoFiltered": "(Filtrado de _MAX_ registros totales)", 
            "search": "Buscar : _INPUT_", 
            "paginate": { 
                "previous": "Página previa", 
                "next": "Siguiente página" 
              } 
        } 
    }); 
 
     
} 
 
    $(document).ready(function () { 
        table = $('#tableListadoMesas').DataTable(); 
 
         
        load(); 
    }); 
 
function ticket(id) 
{ 
    $("#iframeri").modal(); 
    $('#iframereporte').html('<iframe src="'+$('#base_url').val()+'Ticketmesas/dulceria?comp='+id+'"></iframe>'); 
} 