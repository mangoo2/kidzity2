<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones/ModeloConfiguracion');
        $this->iva=0.16;
    }
     
    function configuracion(){
        $data['configticket']=$this->ModeloConfiguracion->configticket();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Configuraciones/Ticket',$data);
        $this->load->view('templates/footer');
    }

    function compra(){
    	$comp=$this->input->get('comp');
    	$resultadoc=$this->ModeloCatalogos->getselectwhere('compra_tiempo','compraId',$comp);
    	foreach ($resultadoc->result() as $item) {
    		$titularId=$item->titularId;
    		$metodo=$item->metodo;
    		$tiempo=$item->tiempo;
    		$pagado=$item->pagado;
            $descuento=$item->descuento;
    		$reg=$item->reg;
    	}
        $data['resultadoarti']=$this->ModeloCatalogos->getselectwhere('compra_tiempo_ventas_pro','compraId',$comp);
    	//$this->load->library('encriptacion');
    	$data['idqr']=$comp;
    	//$data['idqr2']=$this->ModeloCatalogos->decrypturl($this->ModeloCatalogos->encrypturl($comp));
    	$data['compid']=$comp;
    	$data['titularId']=$titularId;
		$data['metodo']=$metodo;
		$data['tiempo']=$tiempo;
		$data['pagado']=$pagado;
        $data['descuento']=$descuento;
		$data['reg']=$reg;
        $data['configticket']=$this->ModeloConfiguracion->configticket();
        //var_dump($data['configticket']->result()); die();
        $data['iva']=$this->iva;
        $this->load->view('Reportes/tcompra',$data);
    }


    function dulceria(){
        $comp=$this->input->get('comp');
        //$id=$this->ModeloCatalogos->decrypturl($comp);
        $resultadoc=$this->ModeloCatalogos->getselectwhere('ventas','ventaId',$comp);
        foreach ($resultadoc->result() as $item) {
            $reg=$item->reg;
            $monto_total=$item->monto_total;
            $metodo=$item->metodo;
            $compraId = $item->compraId;
            
        }

        $data['idventa']=$comp;
        $data['reg']=$reg;
        $data['monto_total']=$monto_total;
        $data['metodo']=$metodo;
        $data['compraId']=$compraId;
        $data['iva']=$this->iva;
        $data['configticket']=$this->ModeloConfiguracion->configticket();
        $this->load->view('Reportes/tdulceria',$data);
    }

    function updateticket(){
        $titulo = $this->input->post('titulo');
        $mensaje1 = $this->input->post('mensaje1');
        $mensaje2 = $this->input->post('mensaje2');
        $fuente = $this->input->post('fuente');
        $tamano = $this->input->post('tamano');
        $margsup = $this->input->post('margsup');
        $marglat = $this->input->post('marglat');
        $this->ModeloConfiguracion->configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup,$marglat);
    }
    function salida(){
        $comp=$this->input->get('comp');
        $resultadoc=$this->ModeloCatalogos->getselectwhere('compra_tiempo_nino','compranId',$comp);
        foreach ($resultadoc->result() as $item) {
            $compranId=$item->compranId;
            $compraId=$item->compraId;
            $ninoid=$item->ninoid;
            $pulcera=$item->pulcera;
            $pagado=$item->pagado;
            $tiempoextra = $item->tiempoextra;
            $pagotiempoextra = $item->pagotiempoextra;
            $reg_abierto=$item->reg_abierto;
            $reg_cerrado=$item->reg_cerrado;
        }
        $data['compranId']=$compranId;
        $data['compraId']=$compraId;
        $data['ninoid']=$ninoid;
        $data['pulcera']=$pulcera;
        $data['pagado']=$pagado;
        $data['tiempoextra']=$tiempoextra;
        $data['pagotiempoextra']=$pagotiempoextra;
        $data['reg_abierto']=$reg_abierto;
        $data['reg_cerrado']=$reg_cerrado;

        $data['iva']=$this->iva;
        $data['idqr']=$this->ModeloCatalogos->encrypturl($comp);
        $data['configticket']=$this->ModeloConfiguracion->configticket();
        $this->load->view('Reportes/tsalida',$data);
    }
    function pulcerastemp(){
        $data['pulcera']=$this->input->get('pulcera');
        $this->load->view('Reportes/pulcerastemp',$data);
    }


}
?>