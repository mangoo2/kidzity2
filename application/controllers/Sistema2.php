<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        if($this->session->userdata('logueo_kids')==true){
            $this->perfilid =$this->session->userdata('perfilid');
        }else{
            //redirect('login'); 
        }
    }
	public function index(){
        $perfilview=$this->perfilid;
        
       	if ($perfilview==1) {
        	redirect('/Inicio');
            //redirect('/Configuracionrentas/servicios');
        }elseif ($perfilview==2) {
        	redirect('/Inicio');
        }elseif ($perfilview==3) {
        	redirect('/Inicio');
        }elseif ($perfilview==4) {
            redirect('/Inicio');
        }elseif ($perfilview==5) {
            redirect('/Inicio');
        }elseif ($perfilview==6) {
            redirect('/Inicio');
        }elseif ($perfilview==7) {
            redirect('/Inicio');
        }elseif ($perfilview==8) {
            redirect('/Inicio');
        }elseif ($perfilview==9) {
            redirect('/Inicio');
        }elseif ($perfilview==10) {
            redirect('/Inicio');
        }elseif ($perfilview==11) {
            redirect('/Inicio');
        }else{
        	redirect('/Login');
        }
	}
   
}
?>