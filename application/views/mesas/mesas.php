<style type="text/css">
  .galeriasimg{height: 249px;border: 4px;border-bottom-style:ridge;border-left-style:ridge;border-right-style:ridge;border-top-style:ridge; border-radius: 18px;  }
  .galeriasimg:hover{-webkit-box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);-moz-box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);}
  .galeriasimg h2{text-align: center;margin-top: 30%;color: black;text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);  }
  .galeriasimg h5{text-align: center;margin-top: 30%;color: black;text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);  }
  .input-group > .input-group-prepend > .btn, .fc .input-group > .input-group-prepend > button, .input-group > .input-group-prepend > .input-group-text, .input-group > .input-group-append:not(:last-child) > .btn, .fc .input-group > .input-group-append:not(:last-child) > button, .input-group > .input-group-append:not(:last-child) > .input-group-text, .input-group > .input-group-append:last-child > .btn:not(:last-child):not(.dropdown-toggle), .fc .input-group > .input-group-append:last-child > button:not(:last-child):not(.dropdown-toggle), .input-group > .input-group-append:last-child > .input-group-text:not(:last-child) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  .input-group-text{font-size: 1rem;font-weight: 400;line-height: 1.5;display: -webkit-box;display: -webkit-flex;display:-moz-box;display: -ms-flexbox;    display:flex;    margin-bottom: 0;    padding: .375rem .75rem;    text-align: center;    white-space: nowrap;    color: #495057;    border: 1px solid #ced4da;    border-radius: .25rem;     background-color: #e9ecef;    -webkit-box-align: center;    -webkit-align-items: center;-moz-box-align: center;    -ms-flex-align: center;align-items: center;}

  .cursor 
  {
    cursor:pointer;
    cursor:hand;
  }
</style>
<div class="row">
  <div class="col-md-12 regresaprincipal">
    <br>
    <button class="cursor btn btn-raised btn-info btn-min-width mr-1 mb-1" onclick="location.reload()">Salir a mesas</button>

  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">      
  
  <div class="col-sm-12">
      <div class="card">
        <!-- Modal -->
          <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <label class="modal-title text-text-bold-600" id="myModalLabel33" >Ingrese Pin de Usuario</label>
                  
                </div>
                    <div class="modal-body">
                  <label>PIN: </label>
                  <div class="form-group">
                    <input type="password" placeholder="Pin" class="form-control pinModalUsuario" id="pinModalUsuario" autocomplete="new-password">
                    <input type="hidden" class="form-control" id="idProductoModal">
                    <input type="hidden" class="form-control" id="banderaRealizarVenta">
                  </div>
                  </div>
                  <div class="modal-footer">
                  <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cerrar" id="botonModalCerrar">
                  <input type="button" class="btn btn-outline-primary btn-lg" value="Ingresar" id="botonModalConfirmar">
                  </div>
                
              </div>
            </div>
          </div>

          <div class="card-header">
              <h4 class="card-title"> Ventas</h4>
              
          </div>


          <div class="card-body">
              <div class="card-block form-horizontal">
                <!--------//////////////-------->
                  <div class="row panelventa1">
                    <div class="card-text getting-started pt-1 mt-2 display-inline-block ">
                        <?php if($this->session->userdata('usuarioid')==38 || $this->session->userdata('usuarioid')==54 || $this->session->userdata('usuarioid')==35 || $this->session->userdata('usuarioid')==1) { ?>
                          <div class="clockCard px-4 py-4 mr-4 mb-4 bg-blue bg-darken-2 box-shadow-2 cursor" onclick="ventascredito()" data-mesa="0"> 
                              <p class="lead mt-2 mb-0">Venta<br>credito</p>
                          </div> 
                        <?php } ?>
                          <div class="clockCard px-4 py-4 mr-4 mb-4 bg-blue bg-darken-2 box-shadow-2 selectedmesa cursor" data-mesa="0"> 
                              <span> </span>
                              <p class="lead mt-2 mb-0">Venta <br> Rápida</p>
                          </div>
                      <?php 
                        $mesa=1;
                        while ($mesa <= $nummesas){ 
                          $activa=$this->model->mesaactiva($mesa);
                          if($activa==1){
                            $activastatus='bg-warning';
                          }else{
                            $activastatus='bg-green';
                          }
                        ?>
                          <div class="clockCard px-4 py-4 mr-4 mb-4 <?php echo $activastatus; ?> bg-darken-2 box-shadow-2 selectedmesa cursor" data-mesa="<?php echo $mesa; ?>"> <span><?php echo $mesa; ?></span> <br>
                            <p class="lead mt-2 mb-0"> Mesa </p>
                          </div>
                        <?php 
                          $mesa++;
                        }
                      ?>
                      
                    </div>  
                    
                  </div>

                  

                 
                  <div class="row">
                      <div class="col-md-6">
                          <!-- DIV DE CATEGORIAS -->
                          <div class="row panelventa2 d-none">
                            <?php foreach ($productospadres->result() as $item){ ?>
                                <div class="col-md-3 galeriasimg flipInX animated classproducto cursor" data-idcate="<?php echo $item->productopId; ?>" style="background: url('<?php echo base_url(); ?>public/img/categoria/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;">
                                  <h2><?php echo $item->categoria;?></h2>
                                </div>
                              <?php } ?>

                          </div>

                          <!-- DIV DE PRODUCTOS -->
                          <div class="row panelventa3 d-none" style="overflow-y: scroll; max-height: 500px">
                              
                          </div>
                      </div> 
                      <!-- DIV DE TABLA DE PRODUCTOS, TOTAL Y VENTA -->
                      <div class="col-md-6" >
                          <div class="row panelventa4 d-none" id="panelventa4">
                            <div class="row col-md-12" style="overflow-y: scroll; height: 500px; max-height: 500px">
                              <div class="col-md-12">
                                <table class="table table-hover" id="tableproductosv">
                                  <thead>
                                    <tr>
                                      <td>Cantidad</td>
                                      <td>Producto</td>
                                      <td>Precio</td>
                                      <td>Total</td>
                                      <td>Acciones</td>
                                    </tr>
                                  </thead>
                                  <tbody class="addselectedprod" id="tableproductosbody" >
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>  

                            <div class="row col-md-12" >
                              <div class="col-md-12 divPadre">
                                  
                                  <div class="col-md-4">
                                    <h3>Total sin Descuento: <span class="totalproductosdescuento">00.00</span></h3>
                                    <input type="hidden" id="totalproductosdescuento" value="0" class="inputHijo">
                                  </div> 
                 
                                  <div class="col-md-5">
                                    <label><h3 for="montoapagar" >Descuento</h3></label>
                                    <input type="number" id="descuentoapagar" class="form-control descuentoapagar col-md-6" name="descuentoapagar" value="0" onclick="habilitaDescuento()" readonly style="color: red;">
                                  </div>
                                  
                                  <div class="col-md-3">
                                    <h3>Total: <span class="totalproductos">00.00</span></h3>
                                    <input type="hidden" id="totalproductos" value="0" class="inputHijo">
                                  </div>  
                                   
                              </div>

                            </div>   

                            <div class="row col-md-12" style="margin-top: 30px;" >

                                <div class="col-md-12 vender" style="display: block">
                                    <label class="col-md-3">Método de pago:</label>
                                    <div class="col-md-3">
                                      <select class="form-control" id="metodopago" style="margin-bottom: 10px">
                                        <option value="1">Efectivo</option>
                                        <option value="2">Tarjeta de crédito / Débito</option>
                                      </select>
                                    </div>

                                  <label class="col-md-3">Monto Pago:</label>
                                  <div class="col-md-3">
                                    <input type="number" id="montoPago" value="" onchange="calculartotal();" class="form-control inputMontoPagar">
                                  </div>  
                                  
                                  
                                    <!-- 
                                    <label class="col-md-2">Cuenta KidZity:</label>
                                    <div class="col-md-4">
                                      <input type="" id="cuentakz" class="form-control" style="margin-bottom: 10px" >
                                    </div>
                                    -->
                                </div>

                            </div>

                            <div class="row col-md-12" style="margin-top: 30px;">

                                <div class="col-md-12 vender" style="display: block">
                                    <div class="col-md-6">
                                      <h3>Cambio: $<span class="cambio">00.00</span></h3>
                                    </div> 
                                </div>

                            </div>
                            
                            <div class="row col-md-12" style="margin-top: 50px;" > 
                              <div class="row col-md-12 divPadreVender"> 
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                  
                                  <button type="button" class="btn btn-raised btn-success btn-min-width col-md-4 ticket imprimir" id="ticket" onclick="clickVender(0)">Imprimir</button>
                                  <div class="col-md-4"></div>
                                  <button type="button" class="btn btn-raised btn-success btn-min-width col-md-4 vender" name="vender" id="vender" onclick="clickVender(1)" disabled="disabled">Vender</button>

                                  <button type="button" class="btn btn-raised btn-success btn-min-width col-md-4 " onclick="limpiarproductos()">Limpiar</button>

                                </div>
                                <div class="col-md-2"></div>
                              </div>
                            </div>
                        </div>
                      </div>
                  </div>

                  
                  
                  
                <!--------//////////////-------->
              </div>
          </div>
      </div>
    </div>
</div>
<div class="col-md-12 ticketventa" style="display: none;">
  
</div>

<div class="modal fade text-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h3 class="modal-title" id="myModalLabel34">Autorización</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      </div>
      
      <div class="modal-body">
        <label>Password: </label>
        <div class="form-group position-relative has-icon-left">
          <input type="password" placeholder="Password" id="passautorizacion" class="form-control" autocomplete="off">
                                  </div>
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" >Cancelar</button>
        <button type="submit" class="btn btn-outline-primary btn-lg modalsalidam" data-dismiss="modal">Aceptar</button>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade text-left" id="inlineForm_pin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <label class="modal-title text-text-bold-600" id="myModalLabel33" >Autorización</label>
      </div>
          <div class="modal-body">
        <label>Password: </label>
        <div class="form-group">
          <input type="password" placeholder="Password" class="form-control pinModalUsuario" id="pass_usuario" autocomplete="new-password">
          <input type="hidden" id="idproducto_p">
        </div>
        </div>
        <div class="modal-footer">
        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cerrar">
        <input type="button" class="btn btn-outline-primary btn-lg" value="Aceptar" onclick="eliminar_producto()">
        </div>
      
    </div>
  </div>
</div>

<div class="modal fade text-left" id="inlineForm_pin2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel22" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <label class="modal-title text-text-bold-600" id="myModalLabel22" >Ingrese Pin de Usuario</label>
      </div>
          <div class="modal-body">
        <label>PIN: </label>
        <div class="form-group">
          <input type="password" placeholder="Pin" class="form-control pinModalUsuario" id="pass_usuario2" autocomplete="new-password">
          <input type="hidden" id="idproducto_p2">
        </div>
        </div>
        <div class="modal-footer">
        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cerrar">
        <input type="button" class="btn btn-outline-primary btn-lg" value="Aceptar" onclick="modificar_producto('',0)">
        </div>
      
    </div>
  </div>
</div>


<div class="modal fade text-left" id="modallimpiar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <label class="modal-title text-text-bold-600" id="myModalLabel33" >Autorización</label>
      </div>
          <div class="modal-body">
        <label>Password: </label>
        <div class="form-group">
          <input type="password" placeholder="Password" class="form-control pinModalUsuario" id="pass_usuario_l" autocomplete="new-password">
        </div>
        </div>
        <div class="modal-footer">
        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cerrar">
        <input type="button" class="btn btn-outline-primary btn-lg" value="Aceptar" onclick="limpiar_productos()">
        </div>
      
    </div>
  </div>
</div>