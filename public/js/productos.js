var base_url=$('#base_url').val();
$(document).ready(function() {
	$('.generacodigo').click(function(event) {
      var d = new Date();
      var dia=d.getDate();//1 31
      var dias=d.getDay();//0 6
      var mes = d.getMonth();//0 11
      var yy = d.getFullYear();//9999
      var hr = d.getHours();//0 24
      var min = d.getMinutes();//0 59
      var seg = d.getSeconds();//0 59
      var yyy = 18;
      var ram = Math.floor((Math.random() * 10) + 1);
      var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
      $('#Codigo').val(codigo);
    });
    $('#saveproducto').click(function(event) {
    	var $valid = $("#formProducto").valid();
    	if ($valid) {
    		$.ajax({
                    type:'POST',
                    url: 'Productosadds',
                    data: {
                        productoid:$('#productoid').val(),
                        codigo: $('#Codigo').val(),
                        producto:$('#Nombre').val(),
                        descripcion:$('#Descripcion').val(),
                        productopId:$('#Categoria option:selected').val(),
                        stock:$('#Stock').val(),
                        stock2:$('#Stock2').val(),
                        precio_compra:$('#PrecioC').val(),
                        precio_venta:$('#PrecioV').val(),
                        regalos:$('#TR').is(':checked')==true?1:0,
                        cafeteria:$('#Cafeteria').is(':checked')==true?1:0,
                        bodega:$('#Bodega').is(':checked')==true?1:0,
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                      console.log(data);
                        var productoid=data;
                        if ($('#imgpro')[0].files.length > 0) {
                            var inputFileImage = document.getElementById('imgpro');
                            var file = inputFileImage.files[0];
                            var data = new FormData();
                            data.append('img',file);
                            data.append('productoid',productoid);
                            $.ajax({
                                url:'imgpro',
                                type:'POST',
                                contentType:false,
                                data:data,
                                processData:false,
                                cache:false,
                                success: function(data) {
                                  var array = $.parseJSON(data);
                                            if (array.ok=true) {
                                              $(".fileinput").fileinput("clear");
                                              toastr.success('Guardado Correctamente','Hecho!');
                                              location.href=''; 
                                              window.location.href = base_url+"Productos";
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                            var data = JSON.parse(jqXHR.responseText);
                                            console.log(data);
                                            if (data.ok=='true') {
                                              $(".fileinput").fileinput("clear");
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }          
                                }
                            });
                        }else{
                            toastr.success('Guardado Correctamente','Hecho!');
                            location.href=''; 
                            window.location.href = base_url+"Productos";
                        }
                    }
                });
    	}
    });
});

function mostrardatos(ids){
  params = {};
  params.id = ids;
  $.ajax({
    type:'POST',
    url:'getviewproduct',
    data:params,
    async:false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', data);
            }
        },
    success:function(data){
      var array = $.parseJSON(data);
      if (array.status==1) {
        $('#productoid').val(array.productoid);
        $('#Codigo').val(array.codigo);
        $('#Nombre').val(array.producto);
        $('#Descripcion').val(array.descripcion);
        $('#Categoria option[value="'+array.productopId+'"]').attr("selected", "selected");
        $('#Stock').val(array.stock);
        $('#Stock2').val(array.stock2);
        $('#PrecioC').val(array.precio_compra);
        $('#PrecioV').val(array.precio_venta);
        if(array.regalos==1){ $('#TR').prop('checked', true);  } 
        if(array.cafeteria==1){ $('#Cafeteria').prop('checked', true);  } 
        if(array.bodega==1){ $('#Bodega').prop('checked', true);  } 
        if (array.img!='') {$('.previewcat').html('<img src="'+base_url+'public/img/productos/'+array.img+'">');}

      }
    }
  });
}