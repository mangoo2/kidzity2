<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corteimpresiones extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Reportes/corteimpresiones');
        $this->load->view('templates/footer');
        $this->load->view('Reportes/corteimpresionesjs');
    }
    function genera(){
        $datos = $this->input->post();
        $fechainicio = $datos['fechainicio'];
        $fechafin = $datos['fechafin'];
        $filtro = $datos['filtro']; //0 ninguno 1 productos 2 ninos
        /*
            1 cafeteria
            2 recepcion
            3 saldos
        */$tablas='';
        switch ($datos['tipo']) {
            case '1':
                $respuestam=$this->ModeloCatalogos->cortecafemimpresion($fechainicio,$fechafin);
                $respuestaprode=$this->ModeloCatalogos->cortetotalcanproductosmimpresion($fechainicio,$fechafin,1);

                $totalp=0;
                $totalpd=0;
                $totalppdd=0;
                
                $tablas.='<table class="table table-sm table-hover table-responsive" id="tabla2">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Fecha</th>
                                                <th>Vendedor</th>
                                                <th>Mesa</th>';
                                                if ($filtro==1) {
                                                    $tablas.='<th>Productos</th>';
                                                }
                                                $tablas.='<th>Metodo</th>
                                                <th>Neto</th>
                                                <th>Descuento</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                            foreach ($respuestam->result() as $item){
                                                $tablas.='<tr>
                                                    <td>'.$item->ventaId.'</td>
                                                    <td>'.$item->reg.'</td>
                                                    <td>'.$item->nombre.'</td>
                                                    <td>'.$item->mesa.'</td>';
                                                    if ($filtro==1) { 
                                                        $detallepro=$this->ModeloCatalogos->getventadetalleMesas($item->ventaId);
                                                        $tablas.='<td>';
                                                            foreach ($detallepro->result() as $items) {
                                                                    $tablas.='<p>'.$items->cantidad.' '.$items->producto.'<p>';
                                                                }
                                                        $tablas.='</td>';
                                                    }
                                                    switch ($item->metodo) {
                                                            case '1':
                                                                $metodo2 ='Efectivo';
                                                                break;
                                                            case '2':
                                                                $metodo2="Tarjeta";
                                                                break;
                                                            default:
                                                                $metodo2='';
                                                                break;
                                                        }
                                                    $tablas.='<td>'.$metodo2.'</td>
                                                    <td>'.$item->monto_total.'</td>
                                                    <td>'.$item->descuento.'</td>
                                                    <td>'.$item->monto_cobrado.'</td>
                                                </tr>';
                                                $totalpd=$totalpd+$item->descuento;
                                                $totalp=$totalp+$item->monto_total;
                                            }
                                            $totalppdd=$totalp-$totalpd;

                                        $tablas.='</tbody>
                        </table>';
                        $tablas.='<div class="col-md-12">
                            <div class="col-md-3"><b>SUBTOTAL</b></div>
                            <div class="col-md-3"><b>$ '.number_format($totalp,2,'.',',').'</b></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3"><b>DESCUENTO</b></div>
                            <div class="col-md-3"><b>$ '.number_format($totalpd,2,'.',',').'</b></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3"><b>TOTAL</b></div>
                            <div class="col-md-3"><b>$ '.number_format($totalppdd,2,'.',',').'</b></div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>

                        ';



                        $tablas.='
                                    <table class="table table-sm table-hover table-responsive" id="tabla">
                                        <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Producto</th>
                                                <th>Precio</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                        foreach ($respuestaprode->result() as $item){
                            $tablas.='
                                    <tr>
                                        <td>'.$item->cantidad.'</td>
                                        <td>'.$item->producto.'</td>
                                        <td>'.$item->precio.'</td>
                                    </tr>
                                    ';

                        }
                        $tablas.='</tbody>
                                </table>';
                
                echo $tablas;
                break;
            default:
                # code...
                break;
        }
    }
   
    
   
    
    
    
    

}