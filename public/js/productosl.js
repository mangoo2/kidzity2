var base_url=$('#base_url').val();
$(document).ready(function () {
	var table=$('#tabla').DataTable({
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        "lengthMenu": [ [10, 25, 50, 100, 200], [10, 25, 50, 100, 200] ],
		"ajax": {
                "url": base_url+"Productos/getlisproductos",
                type: "post",
                "data": {
                    'tipo':0,
                },
            },
        "columns": [
                {"data": "productoid"},
                {"data": null,
                	"render" : function ( url, type, full) {
                		
                		var img='';
                		if (full['img']=='') {
                            img+='<img class="imgproductos" src="'+base_url+'public/img/ops.png">';
                        }else{
                            img+='<img class="imgproductos" src="'+base_url+'public/img/productost/'+full['img']+'">';
                        }
                        return img;
                	}
            	},
                {"data": "codigo"},
                {"data": "producto"},
                {"data": "categoria"},
                {"data": "stock"},
                {"data": null,
                	"render" : function ( url, type, full) {
                		var btton='';
                		btton+="<button type='button' class='mb-0 btn btn-sm btn-icon btn-flat edit'><i class='ft-edit info'></i></button> ";
                		btton+="<button type='button' class='mb-0 btn btn-sm btn-icon btn-flat delete'><i class='ft-trash info'></i></button> ";
                		return btton;
                	}
            	}
            ],
            "order": [[ 0, "desc" ]],
             dom: 'Bfrtip',
                                    buttons: [
                                            //{extend: 'excel',title: 'Reporte de saldos'},
                                            {extend: 'excel'},
                                            {extend: 'pdf'},
                                            {extend:'print'},
                                            'pageLength'
                                    ]
	});
	setInterval( function () {
	    table.ajax.reload();
	}, 1200000 );//120 seg se actualisa
	$('#tabla tbody').on('click', 'button.edit', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            window.location = base_url+'Productos/Productosadd?id=' + data.productoid;
    });
    $('#tabla tbody').on('click', 'button.delete', function () {
            var tr = $(this).closest('tr');
            var row = table.row(tr);
            var data = row.data();
            
            $('#deleteproid').val(data.productoid);
            $('#deletepronom').html(data.producto);
            $('#modalconfirmacion').modal();
    });
    $('#eliminarpro').click(function(event) {
    	$.ajax({
            type:'POST',
            url: base_url+'Productos/eliminarproducto',
            data: {
                id: $('#deleteproid').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                toastr.success('Producto eliminado Correctamente','Hecho!');
                table.ajax.reload();
            }
                
        });
    });
});