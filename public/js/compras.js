var base_url = $('#base_url').val();
var prodlist = 1;
$(document).ready(function() {
    $('#Producto').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Productoss',
        ajax: {
            url: base_url + 'Productos/searchproducto',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                console.log(data);
                var clientes = data;
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.productoid,
                        text: element.codigo + ' / ' + element.producto,
                        precio: element.precio_compra
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        console.log(data);
        $('#PrecioC').val(data.precio);
    });
    $('#Producto').select2('open').on('focus');
    $("#Cantidad").TouchSpin({
        initval: 1,
        min: 1,
        max: 9999999999999999999999999,
    });
    $("#Cantidad").TouchSpin({
        initval: 1,
        min: 1,
        max: 9999999999999999999999999,
    });
    $("#PrecioC").TouchSpin({
        initval: 0,
        min: 0,
        step: 0.01,
        decimals: 2,
        max: 9999999999999999999999999,
    });
    $('.agregarproducto').click(function(event) {
        var can = $('#Cantidad').val();
        var proid = $('#Producto option:selected').val();
        var prona = $('#Producto option:selected').text();
        var precio = $('#PrecioC').val();
        if (precio > 0) {
            var totalproselect = parseFloat(can) * parseFloat(precio);
            var proselect = '<tr class="prodlist_' + prodlist + '">';
            proselect += '<td><input id="scantidad" value="' + can + '" class="scantidad" readonly></td>';
            proselect += '<td>';
            proselect += '<input type="hidden" id="sproductoid" value="' + proid + '" class="sproductoid" readonly>';
            proselect += '<input type="text" id="sproductotext" value="' + prona + '" class="sproductotext" readonly>';
            proselect += '</td>';
            proselect += '<td><input type="text" id="sprecio" value="' + precio + '" class="sprecio" readonly></td>';
            proselect += '<td><input type="text" id="stotal" value="' + totalproselect + '" class="stotal" readonly></td>';
            proselect += '<td><button type="button" class="btn btn-raised btn-danger btn-min-width mr-1 mb-1" onclick="deleteprolist(' + prodlist + ')"><i class="fa fa-trash-o"></i></button></td>';
            proselect += '</tr>';
            $('.tbodyproductos').append(proselect);
            calculartotal();
            prodlist++;
        }
    });
    $('#savecompra').click(function(event) {
        var DATA = [];
        var TABLA = $("#table-compras tbody > tr");
        TABLA.each(function() {
            item = {};
            item["scantidad"] = $(this).find("input[id*='scantidad']").val();
            item["sproductoid"] = $(this).find("input[id*='sproductoid']").val();
            item["sprecio"] = $(this).find("input[id*='sprecio']").val();
            DATA.push(item);
        });
        productos = JSON.stringify(DATA);
        if (TABLA.length > 0) {
            $.ajax({
                type: 'POST',
                url: base_url + 'Compras/addcompra',
                data: {
                    totalcompra: $('#totalcompra').val(),
                    productos: productos
                },
                async: false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {

                    },
                },
                success: function(data) {
                    setTimeout(function() {
                        location.href = base_url + 'Compras';
                    }, 3000);

                    toastr.success('Compra realizada', 'Hecho');
                },
            });
        } else {
            toastr.error('Error!', 'Agrege un producto por minimo');
        }
    });
});
function deleteprolist(id) {
    $('.prodlist_' + id).remove();
}
function calculartotal() {
    var addtp = 0;
    $(".stotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    $('#Total').html(new Intl.NumberFormat('es-MX').format(addtp));
    $('#totalcompra').val(addtp);
}
//=============================
function buscarC() {
    var search = $('#buscar').val();
    if (search.length > 0) {
        $.ajax({
            type: 'POST',
            url: base_url + 'Compras/BuscarCompras',
            data: {
                buscar: $('#buscar').val(),
            },
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function() {
                    toastr.error('Error', '500');
                }
            },
            success: function(data) {
                $('#tbodyresultados').html(data);
            },
            error: function(jqXHR, estado, error) {
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    } else {
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
}
function Detallecompra(id){
	$.ajax({
            type: 'POST',
            url: base_url + 'Compras/detalles',
            data: {
                compra: id,
            },
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function() {
                    toastr.error('Error', '500');
                }
            },
            success: function(data) {
            	$('#detalles').modal();
                $('.detallescompras').html('');
                $('.detallescompras').html(data);
            },
            error: function(jqXHR, estado, error) {
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });
}