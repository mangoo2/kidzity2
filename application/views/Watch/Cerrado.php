<style type="text/css">
  #data-tables td{
    font-size: 13px;
    padding: 0.5rem;
  }
</style>
<input type="hidden" id="statuscompra" value="0" readonly>
<div class="row">
  <div class="col-md-12">
    <h2>Cerrado</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-9"></div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Cerrado </h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          
          
          <table class="table table-striped table-hover" id="data-tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Folio</th>
                <th>Niño</th>
                <th>Titular</th>
                <th>Tiempo</th>
                <th>Cobrado</th>
                <th>Fiesta</th>
                <th></th>
              </tr>
            </thead>
            <tbody>

            
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/ConsultAbierto.js?v=<?php echo date('YmdHis') ?>"></script>