var base_url=$('#base_url').val();
var ninos=0;
var tiempototalm=0;//camtidad minima
var compraid=0;
var inputedit;
var totalproductos=0;
var totalgeneral=0;
$(document).ready(function(){

    Horas();
    $('#registrar').click(function(event) {
      $('.sk-cube-grid').show( "slow" );
      $("#registrar").prop( "disabled", true );
      var pagor = parseFloat($('#pago').val());
      var saldo = parseFloat($('#gastosv').val());
      var numproductos=0;
      var paquetefamiliar =$('#pfamiliar').is(':checked')==true?1:0;
      ninos =$("#list_ninos tbody > tr input[id*='ninoselected']:checked").length;
        if (ninos>0) {
          if (tiempototalm<=pagor) {
            var DATA  = [];
            var TABLA   = $("#list_ninos tbody > tr");
            TABLA.each(function(){    
              if ($(this).find("input[id*='ninoselected']").is(':checked')) {
                item = {};     
                item ["ninoid"] = $(this).find("input[id*='ninoselected']").val();
                item ["pulcera"] = $(this).find("input[id*='brazalete']").val();
                item ["costonino"] = $(this).find("input[id*='costonino']").val();
                item ["costoninoselected"] = $(this).find("input[id*='costonino']").is(':checked')==true?1:0;
                item ["tladulto"] = $(this).find("input[id*='tiempoadul']").is(':checked')==true?1:0;
                if ($(this).find("input[id*='descartarpf']").is(':checked')) {
                  item ["paquetefamiliar"] = 0;
                }else{
                  item ["paquetefamiliar"] = paquetefamiliar;
                }
                

                DATA.push(item);
              }
              
            });
            //========================productos=========================

            var DATAp  = [];
            var TABLAp   = $("#articulos tbody > tr");
            TABLAp.each(function(){    
                itemp = {};
                itemp ["idproductos"] = $(this).find("input[id*='idproductos']").val();     
                itemp ["cantidad"] = $(this).find("input[id*='vscanti']").val();
                itemp ["precio"] = $(this).find("input[id*='vsprecio']").val();
                if ($(this).find("input[id*='vsprecio']").data("precioo")==undefined) {
                  itemp ["descuento"]=0;
                }else{
                  var valorpre1=$(this).find("input[id*='vsprecio']").data("precioo");
                  var valorpre2=$(this).find("input[id*='vsprecio']").val();
                  var descuento =parseFloat(valorpre1)-parseFloat(valorpre2);
                  itemp ["descuento"]=descuento;
                }
                numproductos++;

              
              DATAp.push(itemp);
            });
            //==================================================
            if (pagor<=parseFloat($('#saldo').val())) {
              pagor=pagor;
            }else{
              pagor=$('#saldo').val();
            }
            ninosrow   = JSON.stringify(DATA);
            articulosrow   = JSON.stringify(DATAp);
            $.ajax({
                type: 'POST',
                url: $('#base_url').val() + 'Ventas/compraadd',
                data: {
                    titular: $('#titular').val(),
                    ninosrows:ninosrow,
                    metodo:$('#metodop option:selected').val(),
                    tipo:$('#tipoventa option:selected').val(),
                    tiempo:$('#Estancia').val(),
                    pagado:pagor,
                    numninos:ninos,
                    saldo:saldo,
                    numarticulos:numproductos,
                    articulos:articulosrow,
                    descuentotiempo:$('#descuento').val(),
                    descuentotext:$('#descuentotext option:selected').val(),
                    paquetefamiliar:paquetefamiliar,
                    idpersonalid:$('#idpersonalid').val(),

                },
                async: false,
                statusCode: {
                    404: function(data) {
                      toastr.error('Error', '404');    
                      $("#registrar").prop( "disabled", false );                
                    },
                    500: function(data) {
                      toastr.error('Error', '404');
                      $("#registrar").prop( "disabled", false );
                    },
                },
                success: function(data) {
                    $('.sk-cube-grid').hide( "slow" );
                    $("#registrar").prop( "disabled", true );
                    console.log(data);
                    toastr.success('Compra realizada', 'Hecho');
                    //$('.ticketpago').html('<iframe src="'+base_url+'Ticket/compra?comp='+data+'" class="iframevisitantes"></iframe>'); 
                    popup = window.open(base_url+"Ticket/compra?comp="+data+"&saldo="+saldo,"popup","width=700,height=300,scrollbars=yes");
                    popup.focus();
                    setTimeout(function(){ window.location = base_url+"Inicio"; }, 4000);
                    
                },
            });
            //================================================
          }else{
            toastr.error('Pagar por lo menos una hora por minimo: $'+tiempototalm,'Error');
            $("#registrar").prop( "disabled", false );
          }
        }else{
          toastr.error('Seleccione por lo menos un niño','Error');
          $("#registrar").prop( "disabled", false );
        }
    });
    
    $('#registrarcomp').click(function(event) {
        
          
            var DATA  = [];
            var TABLA   = $("#list_ninos tbody > tr");
            TABLA.each(function(){    
              if ($(this).find("input[id*='ninoselected']").is(':checked')) {
                item = {};     
                item ["ninoid"] = $(this).find("input[id*='ninoselected']").val();
                item ["pulcera"] = $(this).find("input[id*='brazalete']").val();
                DATA.push(item);
              }
              
            });
            ninosrow   = JSON.stringify(DATA);
            $.ajax({
                type: 'POST',
                url: $('#base_url').val() + 'Ventas/compraupdate',
                data: {
                    compra: compraid,
                    ninosrows:ninosrow
                },
                async: false,
                statusCode: {
                    404: function(data) {toastr.error('Error', '404');                    },
                    500: function(data) {toastr.error('Error', '404');},
                },
                success: function(data) {
                    console.log(data);
                    toastr.success('Compra realizada', 'Hecho');
                    //$('.ticketpago').html('<iframe src="'+base_url+'Ticket/compra?comp='+data+'" class="iframevisitantes"></iframe>'); 
                    popup = window.open(base_url+"Ticket/compra?comp="+data,"popup","width=700,height=300,scrollbars=yes");
                    popup.focus();
                    setTimeout(function(){ window.location = base_url+"Inicio"; }, 4000);
                    
                },
            });
            //================================================  
    });
    
    $("#Estancia").TouchSpin({
      initval: 1,
      min: 1,
      max: 24,
    }).change(function(event) {
      selected();
      if ($(event.target).val()>=3) {
        $("#tiempolibre").selected(true);
        if($(event.target).val()>=4){
          $(event.target).val(3);
        }
      }else{
        $("#tiempolibre").selected(false);
      }
    });
    $("#gastosv").TouchSpin({
      initval: 0,
      min: 0,
      max:9999999999999999999999999,
    }).change(function(event) {
      var saldo=$("#gastosv").val();
      $('#saldokidsinfo').val(saldo);
    });
    $("#tinesn").TouchSpin({
      initval: 0,
      min: 0,
      max:9999999999999999999999999,
    });
    $("#otrosarticulosn").TouchSpin({
      initval: 0,
      min: 0,
      max:9999999999999999999999999,
    });
    $("#pago").TouchSpin({
      initval: 0,
      min: 0,
      max:9999999999999999999999999,
    }).change(function(event) {
      selected();
      
      //$("#otrosarticulos").val($("#gastosv").val());
      

    });
    $('#tiempolibre').change(function(event) {
      
      if ($('#tiempolibre').is(':checked')) {
          $('#Estancia').val(3);
      }else{
        $('#Estancia').val(1);
      }
      selected();
    });
    $('#tiempolibrebebe').change(function(event) {
      
      if ($('#tiempolibrebebe').is(':checked')) {
          $('#Estancia').val(3);
          $("#tiempolibre").selected(true);
      }else{
        $('#Estancia').val(1);
        $("#tiempolibre").selected(false);
      }
      selected();
    });
    $('#tines').change(function(event) {
      
      if ($('#tines').is(':checked')) {
          var numtines=$('#tinesn').val();
          var i;
          for (i = 0; i < numtines; i++) { 
            productoselected(22);// productivo es el 22
          }
          $('#tinesn').val(0);
          
      }
    });
    
    $('.generarclavesaleatorias').click(function(event) {
      var TABLA   = $("#list_ninos tbody > tr");
          TABLA.each(function(){    
            if ($(this).find("input[id*='ninoselected']").is(':checked')) {
              $(this).find("input[id*='brazalete']").val(codigoaleatorio());   
            }    
          });
          setTimeout(function(){ 
            var DATAp  = [];
            TABLA.each(function(){    
              if ($(this).find("input[id*='ninoselected']").is(':checked')) {
                item = {}; 
                item ["pulcera"]=$(this).find("input[id*='brazalete']").val();   
                DATAp.push(item);
              }    
            });

            aInfo   = JSON.stringify(DATAp);
            popup = window.open(base_url+"Ticket/pulcerastemp?pulcera="+aInfo,"popup","directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,width=700,height=300,scrollbars=yes");
            popup.focus();

          }, 3000);
    });
    moment.locale('es');
    $('.fecha').html(moment().format("DD/MM/YYYY"));
    //================================================
    $('#otrosarticulos').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Producto',
          ajax: {
            url: base_url+'Ventasp/searchproducto',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.productoid,
                        text: element.codigo+' / '+element.producto
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
      }).on('select2:select', function (e) {
          var data = e.params.data;
          //console.log(data);
          var otrosarticulos=$('#otrosarticulosn').val();
          var i;
          for (i = 0; i < otrosarticulos; i++) { 
            if (data.id==1) {//1 es el id del producto por el cual se ara el descuento o precio a 0
              productoselectedv0(data.id);
            }else if(data.id==2){//2 es el id del producto por el cual se ara el descuento o precio a 0
              productoselectedv0(data.id);
            }else{
             productoselected(data.id); 
            }
            
          }
          $('#otrosarticulosn').val(1);
          $('#otrosarticulos').html('');
    });
    $('.modalsalidam').click(function(event) {
      
      $.ajax({
          type:'POST',
          url: base_url+'Login/vadministrador',
          data: {
            acceso: $('#passautorizacion').val(),
          },
          async: false,
          statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
              toastr.error('Error', '500');
              console.log(data.responseText);
            }
          },
          success:function(data){
            if (data==1) {
              $('#descuento').removeAttr("readonly");
              
              $("#descuento").TouchSpin({
                initval: 0,
                min: 0,
                max:9999999999999999999999999,
              }).change(function(event) {
                selected();$('.descuentotext').show("slow");
              });
            }else{
              toastr.error('Acceso no permitido','Error');
            }
            
          },
          error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
          }
        });
      $('#passautorizacion').val('');
    });
    $('.modalsalidamedit').click(function(event) {
      
      $.ajax({
          type:'POST',
          url: base_url+'Login/vadministrador',
          data: {
            acceso: $('#passautorizacionedit').val(),
          },
          async: false,
          statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
              toastr.error('Error', '500');
              console.log(data.responseText);
            }
          },
          success:function(data){
            if (data==1) {
              $(inputedit).removeAttr("readonly");
              $(inputedit).TouchSpin({
                initval: 0,
                min: 0,
                max:9999999999999999999999999,
              }).change(function(event) {
                selected();
              });
              var precioo=$(inputedit).val();
              console.log(precioo);
              $(inputedit).attr('data-precioo',precioo);
            }else{
              toastr.error('Acceso no permitido','Error');
            }
            
          },
          error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
          }
        });
      $('#passautorizacionedit').val('');
    });
    $('#tipoventa').change(function(event) {
      var tipoventa=$('#tipoventa').val();
      if (tipoventa>0) {
        $('.ocultoeventos').hide();
      }else{
        $('.ocultoeventos').show();
      }
    });
    $('#metodop').change(function(event) {
      var metodop = $('#metodop option:selected').val();
      if (metodop==2) {
        $('#pago').val($('#pagominimo').val());
      }else{
        $('#pago').val(0);
      }
    });

    
});
function Horas() {
    $('#Hora').html(moment().format('h:mm:ss a'));
    //document.getElementById('Hora').innerHTML=Reloj;
    setTimeout("Horas()",1000)
}
function associar(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'Clientes/associar',
        data: {
            id: id,
            codigo:$('.brazalete_'+id).val()
        },
        async: false,
        statusCode: {
            404: function(data) {toastr.error('Error', '404'); },
            500: function(data) {toastr.error('Error', '500');},
        },
        success: function(data) {toastr.success('asociacion correcta', 'Hecho');},
    });
}
/*
function selected(){
  totalproductos=0;
  ninos=0;
  var precio=0;
  var preciominimo=0;
  var horas=$('#Estancia').val()==''?1:$('#Estancia').val();
  var pagado=$('#pago').val()==''?0:$('#pago').val();
  var TABLA   = $("#list_ninos tbody > tr");
  var tibebe =$('#tiempolibrebebe').is(':checked')==true?1:0;
      TABLA.each(function(){
        if ($(this).find("input[id*='ninoselected']").is(':checked')) {
          ninos++;
        }
      });
      $.ajax({
          type:'POST',
          url: base_url+'Categoria/precioestancia',
          data: {
            tipoventa:$('#tipoventa option:selected').val(),
            hora:horas,
            ninos:ninos,
            tbebe:tibebe
          },
          async: false,
          statusCode:{
              404: function(data) {toastr.error('Error', '404'); },
              500: function(data) {toastr.error('Error', '500');},
          },
          success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
              precio=array.total;
              preciominimo=array.minimo;
              if (array.ninoextras==1) {
                toastr.error('Cantidad excedida se comenzara a cobrar precio niño extra', 'Advertencia');
              }
          }
      });
      //console.log(ninos);
      //var tiempototal=parseFloat(horas)*(parseFloat(ninos)*parseFloat(precio));
      $('#saldo2').val(new Intl.NumberFormat('es-MX').format(precio));
      $('#saldo').val(precio);
      //=================================================================================
            var TABLAp   = $("#articulos tbody > tr");
            TABLAp.each(function(){   
                var cantidadt= $(this).find("input[id*='vscanti']").val();
                var preciot= $(this).find("input[id*='vsprecio']").val();
                var rowtotal=parseFloat(cantidadt)*parseFloat(preciot);
                $(this).find("input[id*='vstotal']").val(rowtotal);
                totalproductos=totalproductos+rowtotal;
                
            });
            //totalgeneral=totalgeneral+totalproductos;
      //=================================================================================
      //====================================================
      //var tiempototalinf=(parseFloat(horas)*(parseFloat(ninos)*parseFloat(preciominimo)))-parseFloat(pagado);
      var totaltiempo=parseFloat(precio);
      var tiempomasproductos=parseFloat(totaltiempo)+parseFloat(totalproductos);
      $('.totalgeneral').html('$ '+ new Intl.NumberFormat('es-MX').format(tiempomasproductos));
      var tiempototalinf=parseFloat(tiempomasproductos)-parseFloat(pagado);
      //tiempototalinf>=0?0:(tiempototalinf*+1);
      if (tiempototalinf>0) {
        tiempototalinf=((parseFloat(precio)+parseFloat(totalproductos))-parseFloat(pagado))*-1;
      }else{
        tiempototalinf=tiempototalinf*-1;
      }
      var stringcambio=new Intl.NumberFormat('es-MX').format(tiempototalinf);
      $('.cambios').html('$ '+ stringcambio);
      //pagominimo
      tiempototalm=parseFloat(1)*parseFloat(preciominimo)+parseFloat(totalproductos);
      $('#pagominimo').val(tiempototalm);
      //=================================================================================
            
}*/
function selected(){
  totalproductos=0;
  var preciog=0;
  var tlibre=0;
  var tlibrebebe=0;
  var tiempoestancia=$('#Estancia').val()==''?1:$('#Estancia').val();
  var descuento=$('#descuento').val()==''?1:$('#descuento').val();
  var totaltiempoprecio=0;
  var tipoventa=$('#tipoventa option:selected').val();
  var cantidad=0;
  var precioex=0;
  var paquetefamiliar=0;
  var tiempolibreadulto=0;
  var paquetemasmenos=0;
  var ninos=0;
  var pagado=$('#pago').val()==''?0:$('#pago').val();
  $.ajax({
        type:'POST',
        url:base_url+'Categoria/preciosasignacion',
        data: {
            tipoventa:$('#tipoventa option:selected').val()
        },
        async:false,
        success:function(data){
          //console.log(data);
          var array = $.parseJSON(data);
          //console.log(array.ninos);
          preciog=array.preciogeneral;
          tlibre=array.tiempolibre;
          tlibrebebe=array.tiempolibrebebe;
          precioex=array.precioex;
          cantidad=array.cantidad;

          paquetefamiliar=array.paquetefamiliar;
          tiempolibreadulto=array.tiempolibreadulto;
          paquetemasmenos=array.paquetemasmenos;
        }
      });
  //==========================================================
    var TABLA   = $("#list_ninos tbody > tr");
    TABLA.each(function(){    
      if ($(this).find("input[id*='ninoselected']").is(':checked')) {
        if (tipoventa>0) {
          if (cantidad>0) {
            $(this).find("input[id*='costonino']").val(0);
          }else{
            $(this).find("input[id*='costonino']").val(precioex);
          }
        }else{
            if ($("#pfamiliar").is(':checked')) {
              if ($(this).find("input[id*='descartarpf']").is(':checked')) {
                if (tiempoestancia<3) {
                  if ($(this).find("input[id*='costonino']").is(':checked')) {
                    totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tlibrebebe);
                    $(this).find("input[id*='costonino']").val(tlibrebebe);
                  }else if($(this).find("input[id*='tiempoadul']").is(':checked')){
                    totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tiempolibreadulto);
                    $(this).find("input[id*='costonino']").val(tiempolibreadulto);
                  }else{
                    totaltiempoprecio=parseFloat(totaltiempoprecio)+(parseFloat(tiempoestancia)*parseFloat(preciog));
                    $(this).find("input[id*='costonino']").val(parseFloat(tiempoestancia)*parseFloat(preciog));
                  }
                }else{
                  if ($(this).find("input[id*='costonino']").is(':checked')) {
                    totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tlibrebebe);
                    $(this).find("input[id*='costonino']").val(tlibrebebe);
                  }else{
                    totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tlibre);
                    $(this).find("input[id*='costonino']").val(tlibre);
                  }
                }

              }else{
                  var personasentrandescartadas = $("#list_ninos tbody > tr input[id*='descartarpf']:checked").length;
                  var personasentran = $("#list_ninos tbody > tr input[id*='ninoselected']:checked").length;
                  personasentran= parseFloat(personasentran)-parseFloat(personasentrandescartadas);
                  if(personasentran==4){
                    var costoporpersona=parseFloat(paquetefamiliar)/parseFloat(4);
                    totaltiempoprecio=paquetefamiliar;
                    $(this).find("input[id*='costonino']").val(costoporpersona);
                  }else if(personasentran<4){
                    var pfaltantes = parseFloat(4)-parseFloat(personasentran);
                    var descuento = parseFloat(paquetemasmenos)*parseFloat(pfaltantes); 
                    totaltiempoprecio=parseFloat(paquetefamiliar)-parseFloat(descuento);
                    var costoporpersona=parseFloat(totaltiempoprecio)/parseFloat(personasentran);
                    $(this).find("input[id*='costonino']").val(costoporpersona);

                  }else if(personasentran>4){
                    var pextras = parseFloat(personasentran)-parseFloat(4);
                    var costoextra = parseFloat(paquetemasmenos)*parseFloat(pextras); 
                    totaltiempoprecio=parseFloat(paquetefamiliar)+parseFloat(costoextra);
                    var costoporpersona=parseFloat(totaltiempoprecio)/parseFloat(personasentran);
                    $(this).find("input[id*='costonino']").val(costoporpersona);
                  }
              }
              

            }else{
              if (tiempoestancia<3) {
                if ($(this).find("input[id*='costonino']").is(':checked')) {
                  totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tlibrebebe);
                  $(this).find("input[id*='costonino']").val(tlibrebebe);
                }else if($(this).find("input[id*='tiempoadul']").is(':checked')){
                  totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tiempolibreadulto);
                  $(this).find("input[id*='costonino']").val(tiempolibreadulto);
                }else{
                  totaltiempoprecio=parseFloat(totaltiempoprecio)+(parseFloat(tiempoestancia)*parseFloat(preciog));
                  $(this).find("input[id*='costonino']").val(parseFloat(tiempoestancia)*parseFloat(preciog));
                }
              }else{
                if ($(this).find("input[id*='costonino']").is(':checked')) {
                  totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tlibrebebe);
                  $(this).find("input[id*='costonino']").val(tlibrebebe);
                }else if($(this).find("input[id*='tiempoadul']").is(':checked')){
                  totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tiempolibreadulto);
                    $(this).find("input[id*='costonino']").val(tiempolibreadulto);
                }else{
                  totaltiempoprecio=parseFloat(totaltiempoprecio)+parseFloat(tlibre);
                  $(this).find("input[id*='costonino']").val(tlibre);
                }
              }
            }
      }
        ninos++;
      }else{
        $(this).find("input[id*='costonino']").val(0);
      }
      console.log('niños: '+ninos);
    });
    //================================================
    console.log(totaltiempoprecio);
    $('#saldo2').val(new Intl.NumberFormat('es-MX').format(totaltiempoprecio));
    $('#saldo').val(totaltiempoprecio);
    //=================================================================================
            var TABLAp   = $("#articulos tbody > tr");
            TABLAp.each(function(){   
                var cantidadt= $(this).find("input[id*='vscanti']").val();
                var preciot= $(this).find("input[id*='vsprecio']").val();
                var rowtotal=parseFloat(cantidadt)*parseFloat(preciot);
                $(this).find("input[id*='vstotal']").val(rowtotal);
                totalproductos=totalproductos+rowtotal;
                
            });
    //=================================================================================
    var totaltiempo=parseFloat(totaltiempoprecio);
    var tiempomasproductos=parseFloat(totaltiempo)+parseFloat(totalproductos)-parseFloat(descuento);
    $('.totalgeneral').html('$ '+ new Intl.NumberFormat('es-MX').format(tiempomasproductos));

    var tiempototalinf=parseFloat(tiempomasproductos)-parseFloat(pagado);
      /*
      //tiempototalinf>=0?0:(tiempototalinf*+1);
      if (tiempototalinf>0) {
        tiempototalinf=((parseFloat(totaltiempoprecio)+parseFloat(totalproductos))-parseFloat(pagado))*-1;
      }else{
        tiempototalinf=tiempototalinf*-1;
      }
      var cambio =parseFloat(tiempototalinf)+parseFloat(descuento);
      */
      if (tiempototalinf>0) {
        tiempototalinf=0;
      }else{
        tiempototalinf=tiempototalinf*-1;
      }








      var stringcambio=new Intl.NumberFormat('es-MX').format(tiempototalinf);
      $('.cambios').html('$ '+ stringcambio);
      //pagominimo
      tiempototalm=tiempomasproductos;
      $('#pagominimo').val(tiempomasproductos);
}
function datoscompras(tit,comp){
  params = {};
  params.titular = tit;
  params.compra = comp;
  $.ajax({
        type:'POST',
        url:base_url+'Ventas/datoscompras',
        data:params,
        async:false,
        success:function(data){
          console.log(data);
          //console.log(data.ninos);
          var array = $.parseJSON(data);
          //console.log(array.ninos);
          if (array.compra==1) {
            compraid=array.compraId;
            $('#metodop option[value="'+array.metodo+'"]').prop('selected', true);
            $('#Estancia').val(array.tiempo);
            $('#pago').val(array.pagado);
            $('#tipoventa option[value="'+array.tipo+'"]').prop('selected', true);
            
            var datosninos=$.parseJSON(array.ninos);
            //console.log(datosninos);
            datosninos.forEach(function (element){
                console.log(element);
                $('.ninoselected[value="'+element.ninoid+'"]').prop('checked', true);
            });
            var datosventas=$.parseJSON(array.ventasp);
            datosventas.forEach(function (element){
                console.log(element);
                var otrosarticulos=$('#otrosarticulosn').val();
                var i;
                for (i = 0; i < element.cantidad; i++) { 
                  productoselected(element.productoId);
                }
                //$('.ninoselected[value="'+element.ninoid+'"]').prop('checked', true);
            });
            //$("#costonino").prop( "disabled", true );
            $(".switch .costonino").css("display", "none");
            $("#registrar").css("display", "none");
            $(".ocultoventaapp").css("display", "none");
            $(".danger").css("display", "none");// es el boton de eliminar producto
            $("#registrarcomp").css("display", "block");
            $(".viewcompraapp").css("display", "block");
            
            $("#Estancia").prop( "disabled", true );
            setTimeout(function(){ 
              $(".bootstrap-touchspin-up").css("display", "none"); 
              $(".bootstrap-touchspin-down").css("display", "none"); 
              
            }, 1000);
          }
          
         
        }
      });
}
function verificarpulcera(pulcera){
  var pulc=$(pulcera).val();
  $.ajax({
          type:'POST',
          url: base_url+'Ventas/verificarstatus',
          data: {
            pulceras:pulc
          },
          async: false,
          statusCode:{
              404: function(data) {toastr.error('Error', '404'); },
              500: function(data) {toastr.error('Error', '500');},
          },
          success:function(data){
            if (data==1) {
              toastr.error('Pulcera actualmente ocupada','Advertencia', {"showMethod": "slideDown", "hideMethod": "slideUp", timeOut: 5000});
            }else{

            }
          }
      });
}
function codigoaleatorio(){
  var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789";
       var contrasena = "";
       for (i=0; i<20; i++) contrasena +=caracteres.charAt(Math.floor(Math.random()*caracteres.length)); 
       console.log(contrasena)
     return contrasena;
}
function productoselected(idp){
    $.ajax({
        type: 'POST',
        url: base_url+'Ventasp/agregarproductos',
        data: {id: idp},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
                toastr.error('Error', '500');
                console.log(data.responseText);
            }
        },
        success: function(data) {
            $('.addproductos').html('');
            $('.addproductos').html(data);
        }
    });
    activarediciondes();
    selected();
    //calculartotal();
}
function deletepro(id){
    $.ajax({
        type:'POST',
        url: base_url+'Ventasp/deleteproducto',
        data: {
            idd: id
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                $('.producto_'+id).remove();
            }
        });
    //calculartotal();
    selected();
}
function reactivardescuento(){
  if ($('#descuento').is('[readonly]')) {
    $('#iconForm').modal();
    setTimeout(function(){ 
      console.log('autofocus');
      $('#passautorizacion').focus();
      $("#passautorizacion").keypress(function(event) {
          console.log(event.keyCode);
            if (event.keyCode === 13) {
                $(".modalsalidam").click();
            }
      });
    }, 1000);
    
  }
  
}
function activarediciondes(){
  $('.editar').click(function(event) {

    
    inputedit=event.target;
    if ($(inputedit).is('[readonly]')) {
      $('#iconFormedit').modal();
    }
    /*
    console.log(event);
    console.log(event.target);
    console.log($(event.target).val());
    /* Act on the event */
  });
}
/*
function calculartotal(){
            var TABLAp   = $("#articulos tbody > tr");
            TABLAp.each(function(){   
                var cantidadt= $(this).find("input[id*='vscanti']").val();
                var preciot= $(this).find("input[id*='vsprecio']").val();
                var rowtotal=parseFloat(cantidadt)*parseFloat(preciot);
                $(this).find("input[id*='vstotal']").val(rowtotal);
                totalproductos=totalproductos+rowtotal;
                
            });
            selected();
            totalgeneral=totalgeneral+totalproductos;
            console.log(totalgeneral);
}
*/
function pfamiliar(){
  var numcalcetines=0;
  var TABLA   = $("#list_ninos tbody > tr");
  TABLA.each(function(){    
    if ($(this).find("input[id*='ninoselected']").is(':checked')) {
      if ($(this).find("input[id*='calcetin']").is(':checked')) {
        numcalcetines=numcalcetines+1;
      }

    }
    
  });
  //var calcetines=$("#list_ninos tbody > tr input[id*='calcetin']:checked").length;
  if (numcalcetines>0) {
          var i;
          for (i = 0; i < numcalcetines; i++) { 
            productoselectedv0(22);// productivo es el 22
          }
          $('#tinesn').val(0);
  }
  if($("#pfamiliar").is(':checked')){
    
    $('.col-descartarpf').show('slow');
  }else{
    $('.col-descartarpf').hide();
    $(".descartarpf").selected(false);

  }
  selected();
}
function productoselectedv0(idp){
  $.ajax({
        type: 'POST',
        url: base_url+'Ventasp/agregarproductos',
        data: {
          id: idp,
          des:'x'
        },
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
                toastr.error('Error', '500');
                console.log(data.responseText);
            }
        },
        success: function(data) {
            $('.addproductos').html('');
            $('.addproductos').html(data);
        }
    });
}