var selectedmesa=0;
var valuePIN=0;
var base_url=$('#base_url').val();

$(document).ready(function($) 
{
    regenera();

    $('#pinModalUsuario').val('');
    $('#montoapagar').val(0);

    $('.regresaprincipal').click(function(event) {
        // Reiniciamos el PIN en el modal
        $("#pinModalUsuario").val('');
        $(".pinModalUsuario").val('');
        // Mostramos y ocultamos los divs correspondientes
        $(".panelventa1").css("display","block");
        $(".panelventa2").css("display","none");
        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","none");
        // Reiniciar la Mesa de venta rápida
        eliminaMesa(0);
        $('.classVenta').css("display","block")
    });

    $('#botonModalCerrar').click(function(event) {
        // Reiniciamos el PIN en el modal
        $("#pinModalUsuario").val('');
        $(".pinModalUsuario").val('');
        // Mostramos y ocultamos los divs correspondientes
        $(".panelventa1").css("display","block");
        $(".panelventa2").css("display","none");
        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","none");
        $('.classVenta').css("display","block")
    });

    $('.regresaMesaActual').click(function(event) {
        // Regresamos a las categorías pero en las misma mesa que acabamos de usar
        var pinAuxiliar = $('#pinModalUsuario').val();
        selectedmesa= selectedmesa;
        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","none");
        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
    });

    $(".selectedmesa").click(function(event) 
    {
        // Se selecciona una mesa del listado, y se muestran los divs correspondientes
        selectedmesa= $(this).attr('data-mesa');
        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        // Se oculta el boton de ventas, así como el método de pago
        $('.vender').css("display","none");
        console.log('entrando a mesa: '+selectedmesa);
        
    });

    $(".selectedmesarapida").click(function(event) 
    {
        $('.classVenta').css("display","none")
        $('.vender').css("display","block");
        //$("#inlineForm").modal('show'); 
        selectedmesa= $(this).attr('data-mesa');
        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        console.log('entrando a mesa: '+selectedmesa);
        
    });

    $("#botonModalConfirmar").click(function(event) 
    {
        valuePIN = $("#pinModalUsuario").val();
        enterModal(valuePIN);
    });

    $("#pinModalUsuario").keypress(function(e) {
        if (e.which == 13) 
        {
            valuePIN = $("#pinModalUsuario").val();
            enterModal(valuePIN);
        }
    });
    
    $(".classproducto").click(function(event) 
    {
        //console.log('entrando a categoria');
        productoid= $(this).attr('data-idcate');
        cargaproductos(productoid);
        $(".panelventa2").css("display","none");
        $(".panelventa3").css("display","block");
        
    });

    $(".classVenta").click(function(event) 
    {
        console.log('La mesa actual es: '+selectedmesa);
        $("#banderaRealizarVenta").val(1);

        if($('#pinModalUsuario').val()=='')
        {
            $("#inlineForm").modal('show'); 
        }
        else
        {
            $(".panelventa2").css("display","none");
            $("#panelventa4_"+selectedmesa).css("display","block");
        }        
        
        /*
        if($("#panelventa4_"+selectedmesa+" > .divPadreVender ").find('#banderaTicket').val()==1)
        {
            console.log('Vender es visible');
            $('#vender_'+selectedmesa).css("display","inline");
        }
        else
        {
            $('#ticket_'+selectedmesa).css("display","inline");
        }
        */
        
    });

    $('.modalsalidam').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+'Login/vadministrador',
            data: {
                acceso: $('#passautorizacion').val(),
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(data){ 
                    toastr.error('Error', '500');
                    console.log(data.responseText);
                }
            },
            success:function(data){
                if (data==1) 
                {   
                    $('.descuentoapagar').removeAttr("readonly");
                    $(".descuentoapagar").TouchSpin({
                        initval: 0,
                        min: 0,
                        max:9999999999999999999999999,
                    }).change(function(event) {
                        calculartotal(selectedmesa);
                    });
                }
                else{
                    toastr.error('Acceso no permitido','Error');
                }
            },
            error: function(jqXHR, estado, error){
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });
        $('#passautorizacion').val('');
    });

});

function regenera()
{
    setTimeout(session_regenerate(),15000);
    setTimeout(session_regenerate(),30000);
    setTimeout(session_regenerate(),60000);
}

function session_regenerate()
{
    $.ajax({
            url: base_url+'Mesas/session_regenerate',
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(data){ 
                    toastr.error('Error', '500');
                    console.log(data.responseText);
                }
            },
            success:function(data){
                console.log('sesión regenerada');
            },
            error: function(jqXHR, estado, error){
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });

}

    function cargaproductos(idp){
        $.ajax({
            type: 'POST',
            url: 'Mesas/cargarproductos',
            data: {id: idp,
                   selectedmesa: selectedmesa},
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');

                },
                500: function(data) {
                    toastr.error('Error', '500');
                    console.log(data.responseText);
                }
            },
            success: function(data) {
                $('.panelventa3').html('');
                $('.panelventa3').html(data);
            }
        });
    }

    function preproductoselected(idp)
    {

        if($('#pinModalUsuario').val()=='')
        {
            console.log('entrando a preproductoselected sin pin');
            $("#inlineForm").modal(); 
            $("#idProductoModal").val(idp);
        }
        else
        {
            console.log('entrando a preproductoselected con pin');
            $("#idProductoModal").val(idp); 
            valuePIN = $("#pinModalUsuario").val();
            
            var idProductoModal = $("#idProductoModal").val();
            // Si no viene vacio, es para agregar un producto, de lo contrario solo es para venta de mesa
            if(idProductoModal!='' || idProductoModal!=0)
            {
                productoselected($("#idProductoModal").val());  
            }    
        }        
    }

    function productoselected(idp)
    {
        // Volvemos invisible el DIV con los productos
        $(".panelventa3").css("display","none");
        // Dejamos el DIV vacio
        $(".panelventa3").html("");
        // Definimos el Div en cual trabajaremos, en base a la mesa seleccionada previamente
        var mesaActual = "panelventa4_"+selectedmesa;

        // Si este DIV ya existe, solo lo mostramos
        if($('#panelventa4_'+selectedmesa).length != 0)
        {
            $('#panelventa4_'+selectedmesa).css("display","block");
        }
        // Si no existe, lo clonamos del original
        else
        {
            var $template = $('#panelventa4'),
                            $clone = $template
                            .clone()
                            .show()
                            .removeAttr('id')
                            .attr('id',mesaActual)
                            .insertAfter("#panelventa4");
            $(mesaActual).css("display","block");

            // Cambiar el atributo del input hidden para agregar el que sea de la mesa que se está trabajando
            $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputHijo').attr('id','totalproductos_'+selectedmesa);

            // Cambiar el atributo del input para el cambio, que corresponda a la mesa que se está trabajando
            $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputMontoPagar').attr('id','montoapagar_'+selectedmesa);  

            // Cambiar el atributo del input para el boton de vender, que corresponda a la mesa que se está trabajando
            $("#panelventa4_"+selectedmesa+" > .divPadreVender ").find('.vender').attr('id','vender_'+selectedmesa);          

            // Cambiar el atributo del input para el boton de imprimir ticket, que corresponda a la mesa que se está trabajando
            $("#panelventa4_"+selectedmesa+" > .divPadreVender ").find('.ticket').attr('id','ticket_'+selectedmesa);          
        }
        
        // Llamamos al ajax para agregar los productos
        $.ajax({
            type: 'POST',
            url: 'Mesas/agregarproductos',
            data: {pin: $("#pinModalUsuario").val(), 
                   id: idp,
                   selectedmesa: selectedmesa},
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');

                },
                500: function(data) {
                    toastr.error('Error', '500');
                    console.log(data.responseText);
                }
            },
            success: function(data) 
            {
                //$("#pinModalUsuario").val('');
                $("#idProductoModal").val('')
                if(data!='usuarioinvalido')
                {
                    // Borramos lo que contenga el TBODY de la tabla
                    $("#"+mesaActual+" > div > table > tbody" ).html(' ');
                    // Agregamos el nuevo contenido
                    $("#"+mesaActual+" > div > table > tbody" ).html(data);
                }
                else
                {
                    alert('El PIN no coincide con el mesero');
                    $('#panelventa4_'+selectedmesa).css("display","none");
                    $(".panelventa1").css("display","block");
                }
            }
        });

        // Calculamos el total, y le pasamos el número de la mesa que estamos manipulando
        calculartotal(selectedmesa);
        
    }

    function calculartotalVacio(id)
    {
        var aux = id.split("_");
        calculartotal(aux[1]);
    }
    
    function calculartotal(mesa)
    {
        console.log('entrando a funcion de calculartotal()');
        // Declaramos la variable que tendrá la suma del total de los productos
        var addtp = 0;
        $('.vstotal_'+mesa).each(function() 
        {
            var vstotal = $(this).val();
            addtp += Number(vstotal);
        });
        $('.totalproductos').html(new Intl.NumberFormat('es-MX').format(addtp));

        // Obtenemos el descuento
        var descuentoapagar = 0;
        $('.descuentoapagar').each(function() 
        {
            var descuentoapagaraux = $(this).val();
            descuentoapagar += Number(descuentoapagaraux);
        });
        
        $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputHijo').val(addtp);

        // Total de la cuenta
        var addtpConvertido = parseFloat(addtp);
        // Monto ingresado para pagar
        var montoPagoConvertido = parseFloat($("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputMontoPagar').val());

        // Al total de los productos, se le resta el descuento
        var totalConDescuento = parseFloat(addtpConvertido-descuentoapagar); 

        // Se calcula el cambio restando al Monto del pago el total con descuento incluido
        var cambio = parseFloat(montoPagoConvertido-totalConDescuento); 
        
         if (cambio<=0) 
        {
            cambio=0;
        }
        else
        {
            cambio=cambio;
        }
        $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.cambio').html('');
        $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.cambio').html(new Intl.NumberFormat('es-MX').format(cambio));
    }

    function deletepro(id,mesa,row){
        $.ajax({
            type:'POST',
            url: 'Mesas/deleteproducto',
            data: {
                idd: id,
                mesa: mesa,
                row: row
                },
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data)
                {
                    $('.producto_'+row).remove();
                }
            });
        calculartotal(mesa);
    }

    // Reducimos la cantidad de articulos de un mismo producto en una fila
    function deleteproindividual(id,mesa,row)
    {
        // Enviamos mediante AJAX la solicitud
        $.ajax({
            type:'POST',
            url: 'Mesas/deleteproductoindividual',
            data: {
                idd: id,
                mesa: mesa,
                row: row
                },
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Atención!', 'No se puede realizar la operación');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data)
                {
                    // Si aún hay cantidad de un mismo artículo, solo se decuentan
                    if(data>0)
                    {
                        var TABLA = $("#panelventa4_"+mesa+" > div > table > tbody > .producto_"+row+" > td > #vscanti").val(data);
                        // Obtenemos el precio del producto
                        var precio = $("#panelventa4_"+mesa+" > div > table > tbody > .producto_"+row+" > td > #vsprecio").val();
                        // Obtenemos el total del producto por la cantidad y la guardamos en la tabla
                        var total = precio * data;
                        var TABLA = $("#panelventa4_"+mesa+" > div > table > tbody > .producto_"+row+" > td > #vstotal_"+mesa).val(total);
                    }
                    // Si no, se elimina toda la fila de la tabla
                    else
                    {
                        $('.producto_'+row).remove();
                    }
                }
            });
        calculartotal(mesa);
    }

    function addproindividual(id,mesa,row){
        $.ajax({
            type:'POST',
            url: 'Mesas/addproductoindividual',
            data: {
                idd: id,
                mesa: mesa,
                row: row
                },
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Atención!', 'No se puede realizar la operación');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data)
                {
                    // Modificamos la cantidad del producto en la tabla
                    var TABLA = $("#panelventa4_"+mesa+" > div > table > tbody > .producto_"+row+" > td > #vscanti").val(data);
                    // Obtenemos el precio del producto
                    var precio = $("#panelventa4_"+mesa+" > div > table > tbody > .producto_"+row+" > td > #vsprecio").val();
                    // Obtenemos el total del producto por la cantidad y la guardamos en la tabla
                    var total = precio * data;
                    var TABLA = $("#panelventa4_"+mesa+" > div > table > tbody > .producto_"+row+" > td > #vstotal_"+mesa).val(total);
                }
            });
        calculartotal(mesa);
    }


    //$('#vender').click(function(event) 
    function clickVender(mesa)
    {
        console.log('entrando a clickVender');
        var pin = $("#pinModalUsuario").val(); 

        // Obtenemos el descuento
        var descuentoapagarventa = 0;
        $('.descuentoapagar').each(function() 
        {
            var descuentoapagarauxventa = $(this).val();
            descuentoapagarventa += Number(descuentoapagarauxventa);
        });

        var aux = mesa.split("_");
        var aux2 = aux[1];
        //var montotoal=$('#totalproductos').val();
        //var montotoal=$('.inputHijo').val();
        var montotoal = $("#panelventa4_"+aux2+" > .divPadre ").find('.inputHijo').val()
        //var pago =$('#montoapagar').val();
        //var pago =$('#montoapagar').val();
        var pago = $("#panelventa4_"+aux2+" > .divPadre ").find('.inputMontoPagar').val()
        
        if(parseFloat(pago) >= (parseFloat(montotoal)-parseFloat(descuentoapagarventa))) 
        {
            var DATA  = [];
            //var TABLA   = $("#tableproductosv tbody > tr");
            var TABLA = $("#panelventa4_"+aux2+" > div > table > tbody > tr");
            TABLA.each(function()
            {    
                item = {};
                item ["idproductos"] = $(this).find("input[id*='idproductos']").val();     
                item ["cantidad"] = $(this).find("input[id*='vscanti']").val();
                item ["precio"] = $(this).find("input[id*='vsprecio']").val();

                DATA.push(item);
            });
            productos   = JSON.stringify(DATA);
            

            $.ajax(
            {
                type: 'POST',
                url: base_url+'Mesas/addventas',
                data: {
                    //monto_total:$('#totalproductos').val(),
                    pinaux: pin,
                    monto_total: montotoal,
                    selectedmesa: aux2,
                    descuento: descuentoapagarventa,
                    pulcera:$('#cuentakz').val(),
                    metodo:$('#metodopago option:selected').val(),
                    productos:productos,
                    tipo: 1
                },
                async: false,
                statusCode: 
                {
                    404: function(data) 
                    {
                        toastr.error('Error', '404');
                    },
                    500: function(data) 
                    {
                        
                    },
                },
                success: function(data) 
                {
                    if(data=='usuarioinvalido')
                    {
                        alert('El PIN no coincide con el mesero');
                    }
                    else if(data=='pininvalido')
                    {
                        alert('El PIN ingresado no coincide con un usuario registrado en el sistema, la venta no se puede realizar');   
                    }
                    else
                    {
                        // Despliega ventana de ticket
                        popup = window.open(base_url+"Ticketmesas/dulceria?comp="+data,"popup","width=700,height=300,scrollbars=yes");
                        popup.focus();  
                        // Indica venta realizada
                        toastr.success('venta realizada', 'Hecho');
                        $("#panelventa4_"+aux2+" > .divPadreVender ").find('#banderaTicket').val(0)
                        setTimeout(2000);
                        // Elimina la mesa que se acaba de vender
                        eliminaMesa(aux2);
                        $('.descuentoapagar').attr('readonly', true);
                        // Restablece el PIN del mesero
                        $(".pinModalUsuario").val('');
                        // Reinicia la bandera de realizar venta, que se activa cuando se selecciona 
                        // "Realizar Venta" en la selección de productos de la mesa
                        $("#banderaRealizarVenta").val(0);
                    }
                },
            });
            
            
        }
        else
        {
            toastr.error('Error!', 'Agregar monto igual o mayor al total');
        }
    }

    function eliminaMesa(numeroMesa)
    {
        $.ajax({
            type:'POST',
            url: 'Mesas/deleteMesa',
            data: {
                mesa: numeroMesa
                },
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data)
                {
                    $("#panelventa4_"+numeroMesa).remove();
                    $(".panelventa1").css("display","block");
                    $('#vender_'+numeroMesa).attr("style","display: none");
                }
            });
    }

    function clickTicket(mesa)
    {
        console.log('entrando a clickTicket');
        console.log('mesa: '+mesa);
        var pin = $("#pinModalUsuario").val(); 

        // Obtenemos el descuento
        var descuentoapagarventa = 0;
        $('.descuentoapagar').each(function() 
        {
            var descuentoapagarauxventa = $(this).val();
            descuentoapagarventa += Number(descuentoapagarauxventa);
        });

        var aux = mesa.split("_");
        var aux2 = aux[1];
        //var montotoal=$('#totalproductos').val();
        //var montotoal=$('.inputHijo').val();
        var montotoal = $("#panelventa4_"+aux2+" > .divPadre ").find('.inputHijo').val()
        //var pago =$('#montoapagar').val();
        //var pago =$('#montoapagar').val();
        var pago = $("#panelventa4_"+aux2+" > .divPadre ").find('.inputMontoPagar').val()
        console.log('pago: '+pago);
        console.log('montotoal: '+montotoal);
        console.log('descuentoapagarventa: '+descuentoapagarventa);
        if(parseFloat(pago) >= (parseFloat(montotoal)-parseFloat(descuentoapagarventa))) 
        {
            var DATA  = [];
            //var TABLA   = $("#tableproductosv tbody > tr");
            var TABLA = $("#panelventa4_"+aux2+" > div > table > tbody > tr");
            TABLA.each(function()
            {    
                item = {};
                item ["idproductos"] = $(this).find("input[id*='idproductos']").val();     
                item ["cantidad"] = $(this).find("input[id*='vscanti']").val();
                item ["precio"] = $(this).find("input[id*='vsprecio']").val();

                DATA.push(item);
            });
            productos   = JSON.stringify(DATA);
            

            $.ajax(
            {
                type: 'POST',
                url: base_url+'Mesas/addventas',
                data: {
                    //monto_total:$('#totalproductos').val(),
                    pinaux: pin,
                    monto_total: montotoal,
                    selectedmesa: aux2,
                    descuento: descuentoapagarventa,
                    pulcera:$('#cuentakz').val(),
                    metodo:$('#metodopago option:selected').val(),
                    productos:productos,
                    tipo: 0
                },
                async: false,
                statusCode: 
                {
                    404: function(data) 
                    {
                        toastr.error('Error', '404');
                    },
                    500: function(data) 
                    {
                        
                    },
                },
                success: function(data) 
                {
                    if(data=='usuarioinvalido')
                    {
                        alert('El PIN no coincide con el mesero');
                    }
                    else if(data=='pininvalido')
                    {
                        alert('El PIN ingresado no coincide con un usuario registrado en el sistema, la venta no se puede realizar');   
                    }
                    else
                    {
                        popup = window.open(base_url+"Ticketmesas/dulceria?comp="+data,"popup","width=700,height=300,scrollbars=yes");
                        popup.focus();  
                        toastr.success('Ticket impreso', 'Hecho');
                        $("#panelventa4_"+aux2+" > .divPadreVender ").find('#banderaTicket').val(1)
                        // Ocultamos el botón de impresión de Ticket
                        $('#ticket_'+aux2).attr("style","display: none");
                        // Mostramos el botón de venta
                        $('#vender_'+aux2).attr("style","display: inline");
                    }
                },
            });
            
            
        }
        else
        {
            toastr.error('Error!', 'Agregar monto igual o mayor al total');
        }
            
    }

function habilitaDescuento()
{
    if ($('#descuentoapagar').is('[readonly]')) 
    {
        $('#iconForm').modal();
    }
}

function enterModal(valuePIN)
{
    console.log('enterModal function');
    // Calculamos el total, y le pasamos el número de la mesa que estamos manipulando
    calculartotal(selectedmesa);
    if(valuePIN.trim()=='')
    {
        alert('No puede ingresar un PIN vacio');
    }
    else if(valuePIN.length <4)
    {
        alert('El PIN debe contener al menos 4 caracteres');
    }
    else
    {
        $.ajax({
            type:'POST',
            url: base_url+'index.php/mesas/getUsuarioPorPin/'+valuePIN,
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(data){ 
                    toastr.error('Error', '500');
                }
            },
            success:function(data)
            {
                console.log('Succes de getUsuarioPorPin');
                if(data=='pininvalido')
                {
                    alert('El PIN ingresado no coincide con un usuario registrado en el sistema, la venta no se puede realizar');
                    $(".pinModalUsuario").val('');   
                }
                else if($("#banderaRealizarVenta").val()==1)
                {
                    console.log('La banderaRealizarVenta==1');
                    $.ajax({
                        type:'POST',
                        url: base_url+'index.php/mesas/comparaPinRealizarVenta',
                        data: {
                            pin: valuePIN,
                            mesa: selectedmesa
                        },
                        async: false,
                        statusCode:{
                            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                            500: function(data)
                            { 
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data)
                        {
                            if(data=='usuarioinvalido')
                            {
                                alert('El PIN no coincide con el mesero');
                            }
                            else
                            {
                                $(".panelventa2").css("display","none");
                                $(".panelventa3").css("display","none");
                                
                                //$("#panelventa4_"+selectedmesa).css("display","block");
                                console.log('respuesta de consulta, en mesa: '+selectedmesa);
                                // Si este DIV ya existe, solo lo mostramos
                                if($('#panelventa4_'+selectedmesa).length > 0)
                                {
                                    $('#panelventa4_'+selectedmesa).css("display","block");
                                    $("#inlineForm").modal('hide');
                                    calculartotal(selectedmesa);
                                    $('#ticket_'+selectedmesa).css("display","inline");
                                }
                                // Si no existe, lo clonamos del original
                                else
                                {
                                    var mesaActual = "panelventa4_"+selectedmesa;
                                    var $template = $('#panelventa4'),
                                                    $clone = $template
                                                    .clone()
                                                    .show()
                                                    .removeAttr('id')
                                                    .attr('id',mesaActual)
                                                    .insertAfter("#panelventa4");
                                    $(mesaActual).css("display","block");

                                    // Cambiar el atributo del input hidden para agregar el que sea de la mesa que se está trabajando
                                    $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputHijo').attr('id','totalproductos_'+selectedmesa);

                                    // Cambiar el atributo del input para el cambio, que corresponda a la mesa que se está trabajando
                                    $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputMontoPagar').attr('id','montoapagar_'+selectedmesa);  

                                    // Cambiar el atributo del input para el boton de vender, que corresponda a la mesa que se está trabajando
                                    $("#panelventa4_"+selectedmesa+" > .divPadreVender ").find('.vender').attr('id','vender_'+selectedmesa);          

                                    // Cambiar el atributo del input para el boton de imprimir ticket, que corresponda a la mesa que se está trabajando
                                    $("#panelventa4_"+selectedmesa+" > .divPadreVender ").find('.ticket').attr('id','ticket_'+selectedmesa);

                                    // Borramos lo que contenga el TBODY de la tabla
                                    $("#panelventa4_"+selectedmesa+" > div > table > tbody" ).html(' ');
                                    // Agregamos el nuevo contenido
                                    $("#panelventa4_"+selectedmesa+" > div > table > tbody" ).html(data);
                                    //$("#panelventa4_"+selectedmesa).css("display","block");
                                    $("#inlineForm").modal('hide');
                                    calculartotal(selectedmesa);
                                    $('#ticket_'+selectedmesa).css("display","inline");
                                }
                            }
                              
                        },
                        error: function(jqXHR, estado, error){
                            console.log(error);
                        }
                    });
                }
                else
                {
                    $("#inlineForm").modal('hide');
                    var idProductoModal = $("#idProductoModal").val();
                    // Si no viene vacio, es para agregar un producto, de lo contrario solo es para venta de mesa
                    if(idProductoModal!='' || idProductoModal!=0)
                    {
                        productoselected($("#idProductoModal").val());  
                    }
                }
            },
            error: function(jqXHR, estado, error){
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });
    }

}