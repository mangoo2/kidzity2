<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paquetes extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Fiestas/ModeloPaquete');
    }

    public function index(){
    	$data['Paquete']=$this->ModeloPaquete->GetPaqutes(); 
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Fiestas/Paquete', $data);	
        $this->load->view('templates/footer');
    }

    function GetData(){
    	$id=$this->input->post('Id');
    	echo $this->ModeloPaquete->Especific($id);
    }

    function EditarFiesta(){
    	$data=$this->input->post();
    	$id = $data['IdPaquete'];
    	unset($data['IdPaquete']);
    	unset($data['Paquete']);
    	$this->ModeloPaquete->Editar($data,'IdPaquete',$id,'PaquetesFiesta');
    }

    function CrearFiesta(){
    	$data=$this->input->post();
    	unset($data['IdPaquete']);
    	unset($data['Paquete']);
    	$this->ModeloPaquete->Insertar($data,'PaquetesFiesta');
    }

    function DeleteFiesta(){
        $data=$this->input->post();
        $id = $data['IdPaquete'];
        $this->ModeloPaquete->Delete($id);
    }

    



}
?>