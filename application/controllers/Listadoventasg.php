<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listadoventasg extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
    }
	public function index(){
            $pages=10;
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('ventas/listadog');
            $this->load->view('templates/footer');
	}

    public function getlistventas() {
        $params = $this->input->post();
        $getdata    = $this->ModeloVentas->getlistventas_g($params);
        $totaldata  = $this->ModeloVentas->getlistventas_g_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    

       
    
}
