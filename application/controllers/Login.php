<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloSession');
        $this->load->model('ModeloVentas');
        $this->load->helper('url');
    }
	public function index()
	{
            $this->load->view('login/header');
            $this->load->view('login/index');         
            $this->load->view('login/footer');
            $this->load->view('login/pages-logintpl');
            
	}   
    public function session(){
        $usu = $this->input->post('usu');
        $pass = $this->input->post('passw');
        $respuesta = $this->ModeloSession->login($usu,$pass);
        echo $respuesta;
    }   
    public function exitlogin() {
        if (isset($_SESSION['idpersonal'])) {
            $this->ModeloSession->salir($_SESSION['idpersonal']);
            $this->ModeloVentas->borraVentasMesasTemporales();
        }
        
        $this->load->view('login/exit');
    }
    function vadministrador(){
        $acceso = $this->input->post('acceso');
        $respuesta = $this->ModeloSession->accesoadmin($acceso);
        echo $respuesta;
    }
    /*
    function loghak(){
        $data = array(
                    'logueo_kids'=>true,
                    'usuarioid'=>1,
                    'usuario'=>'admin',
                    'perfilid'=>1,
                    'idpersonal'=>1,
                    'foto'=>'',
                    'mesas_detalle'=>array(),
                    'mesas'=>array(),
                    'mesero'=>0
                );
                 
            $this->session->set_userdata($data);
    }
    */
}
