<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy=date('Y-m-d H:i:s');
    } 
    function getproductopadre(){
        $strq = "SELECT prop.productopId,prop.categoria,prop.img
                from producto_hijo as proh
                inner join producto_padre as prop on prop.productopId=proh.productopId
                where prop.activo=1
                group by prop.productopId
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getproductoshijos($id){
        $strq = "SELECT *
                from producto_hijo 
                where activo=1 and productopId=$id and stock>0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function addventas($monto_total,$pulcera,$metodo){
        $idpersonal=$_SESSION['idpersonal'];
        if ($metodo==3) {
            $compraId=$this->verificarpulceracompra($pulcera);
                     $this->restasaldo($compraId,$monto_total);
            $strq="INSERT INTO ventas(personalId, metodo, pulcera, compraId, monto_total,reg) VALUES ($idpersonal,$metodo,$pulcera,$compraId,$monto_total,'$this->fechahoy')";
        }else{
            $strq="INSERT INTO ventas(personalId, metodo, monto_total,reg) VALUES ($idpersonal,$metodo,$monto_total,'$this->fechahoy')";
        }
        $this->db->query($strq);
        $idventa=$this->db->insert_id();
        return $idventa;
    }

    function addventasMesas($idpersonal,$monto_total,$pulcera,$metodo,$mesa,$descuento,$monto_cobrado,$tipo){

        //$idpersonal=$_SESSION['idpersonal'];
        date_default_timezone_set("America/Mexico_City");
        $hoy = date('Y-m-d H:i:s');
        if ($metodo==3) {
            $compraId=$this->verificarpulceracompra($pulcera);
                     $this->restasaldo($compraId,$monto_total);
            $strq="INSERT INTO ventasm(personalId, metodo, pulcera, compraId, monto_total, mesa, reg, descuento, monto_cobrado, tipo) VALUES ($idpersonal,$metodo,$pulcera,$compraId,$monto_total,$mesa,'$hoy',$descuento,$monto_cobrado,$tipo)";
        }else{
            $strq="INSERT INTO ventasm(personalId, metodo, monto_total, mesa, reg, descuento, monto_cobrado, tipo) VALUES ($idpersonal,$metodo,$monto_total,$mesa,'$hoy',$descuento,$monto_cobrado,$tipo)";
        }
        $this->db->query($strq);
        $idventa=$this->db->insert_id();
        return $idventa;
    }

    function verificarpulceracompra($codigo){
        $strq="SELECT cnino.compraId
                FROM compra_tiempo_nino as cnino
                inner join compra_tiempo as comp on comp.compraId=cnino.compraId
                WHERE cnino.status=1 AND (cnino.pulcera='$codigo' or comp.titularId='$codigo')";
        $query = $this->db->query($strq);
        $this->db->close();
        $compraId=0;
        foreach ($query->result() as $row) {
            $compraId =$row->compraId;
        } 
        return $compraId;
    }
    function addventasdetalle($idventas,$idproductos,$cantidad,$precio,$descuento){
        $strq="INSERT INTO venta_detalle(ventaId, productoId, cantidad, precio,descuento) 
                    VALUES ($idventas,$idproductos,$cantidad,$precio,'$descuento')";
        $this->db->query($strq);
        $this->db->close();
    }
    function descuentostock($idproductos,$cantidad){
        $strq="UPDATE producto_hijo set stock=stock-$cantidad where productoid=$idproductos";
        $this->db->query($strq);
    }

    function addventasdetalleMesas($idventas,$idproductos,$cantidad,$precio){
        $strq="INSERT INTO ventasm_detalle(ventaId, productoId, cantidad, precio) VALUES ($idventas,$idproductos,$cantidad,$precio)";
        $this->db->query($strq);
        $this->db->close();
    }

    function restasaldo($compraId,$monto_total){
        $strq="SELECT titularId FROM compra_tiempo where compraId=$compraId";
        $query = $this->db->query($strq);
        $this->db->close();
        $titularId=0;
        foreach ($query->result() as $row) {
            $titularId =$row->titularId;
        } 
        //===================================================
        $strq="SELECT * FROM abono_saldos where titularId=$titularId and vence>NOW()";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $saldoId =$row->saldoId;
            $cantidad =$row->cantidad;
            //===================================
            if ($monto_total>0) {
                if ($monto_total<$cantidad) {
                    $strq="UPDATE abono_saldos SET cantidad=cantidad-$monto_total WHERE saldoId=$saldoId";
                    $monto_total=0;
                }else{
                    $monto_total=$monto_total-$cantidad;
                    $strq="UPDATE abono_saldos SET cantidad=0 WHERE saldoId=$saldoId";
                }
                $this->db->query($strq);
                $this->db->close();
            }
        } 
    }
    //=================================================================
        /*
            function filasg(){
                $strq = "SELECT COUNT(*) as total FROM ventas where activo=1 and metodo<3";
                $query = $this->db->query($strq);
                $this->db->close();
                foreach ($query->result() as $row) {
                    $total =$row->total;
                } 
                return $total;
            }
            function Listg($por_pagina,$segmento){
                if ($segmento!='') {
                    $segmento=','.$segmento;
                }else{
                    $segmento='';
                }
                $strq="SELECT ven.ventaId,ven.reg,ven.monto_total,ven.metodo, per.nombre
                        FROM ventas as ven
                        left join personal as per on per.personalId=ven.personalId
                        WHERE ven.activo=1 AND ven.metodo<3 LIMIT $por_pagina $segmento;";
                $resp=$this->db->query($strq);
                return $resp;
            }
        */
    //====================================================================================
        /*
        function filasv(){
            $strq = "SELECT COUNT(*) as total FROM ventas where activo=1 and metodo=3";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
                $total =$row->total;
            } 
            return $total;
        }
        function Listv($por_pagina,$segmento){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            $strq="SELECT ven.ventaId,ven.reg,ven.monto_total,ven.metodo, per.nombre
                    FROM ventas as ven
                    left join personal as per on per.personalId=ven.personalId
                    WHERE ven.activo=1 AND ven.metodo=3 LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }
        */
    //===========================================================================
        /*
        function filasr(){
            $strq = "SELECT COUNT(*) as total FROM ventasm where activo=1";
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
                $total =$row->total;
            } 
            return $total;
        }
        function Listr($por_pagina,$segmento){
            if ($segmento!='') {
                $segmento=','.$segmento;
            }else{
                $segmento='';
            }
            $strq="SELECT ven.ventaId,per.nombre,ven.reg,ven.monto_total,ven.metodo
                    FROM ventasm as ven
                    inner JOIN personal as per on per.personalId=ven.personalId
                    WHERE ven.activo=1 AND ven.metodo>0 LIMIT $por_pagina $segmento;";
            $resp=$this->db->query($strq);
            return $resp;
        }   
        */
    //============================================================================
    function getListadoVentasMesas()
    {
        $strq = "SELECT
        v.ventaId,
        p.nombre,
        v.metodo,
        v.monto_total,
        v.descuento,
        v.monto_cobrado,
        v.activo,
        v.mesa,
        v.reg,
        v.tipo
        FROM 
        ventasm as v
        INNER JOIN personal as p ON v.personalId=p.personalId";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function insertaRegistroElimiando($data)
    {
        $this->db->insert('registro_productos_eliminados', $data);
        $id = $this->db->insert_id();
        return $id;
    }

    function getListadoProductosEliminados()
    {
        $strq = "SELECT
        r.id,
        r.mesa,
        ph.producto,
        p.nombre,
        r.fechaRegistro
        FROM registro_productos_eliminados as r
        INNER JOIN personal as p ON r.idUsuario=p.personalId
        INNER JOIN producto_hijo as ph ON r.idProducto=ph.productoid";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function addventasMesasSimulada($monto_total,$pulcera,$metodo,$mesa){
        $idpersonal=$_SESSION['idpersonal'];
        if ($metodo==3) {
            $compraId=$this->verificarpulceracompra($pulcera);
                     $this->restasaldo($compraId,$monto_total);
            $strq="INSERT INTO ventasm_simulada(metodo, pulcera, compraId, monto_total, mesa) VALUES ($metodo,$pulcera,$compraId,$monto_total,$mesa)";
        }else{
            $strq="INSERT INTO ventasm_simulada(metodo, monto_total, mesa) VALUES ($metodo,$monto_total,$mesa)";
        }
        $this->db->query($strq);
        $idventa=$this->db->insert_id();
        return $idventa;
    }

    function addventasdetalleMesasSimulada($idventas,$idproductos,$cantidad,$precio){
        $strq="INSERT INTO ventasm_detalle_simulada(ventaId, productoId, cantidad, precio) VALUES ($idventas,$idproductos,$cantidad,$precio)";
        $this->db->query($strq);
        $this->db->close();
    }

    function borraVentasMesasTemporales()
    {
        $strqs="DELETE v,vd 
                   FROM ventasm_simulada as v
                LEFT JOIN ventasm_detalle_simulada as vd
                ON vd.ventaId=v.ventaId";
        $this->db->query($strqs);
        $this->db->close();
    }   

    function actualizaVenta($idVenta, $data)
    {
        $this->db->set($data);
        $this->db->where('ventaId', $idVenta);
        return $this->db->update('ventasm');
    }

    function eliminaDetallesVenta($idVenta)
    {
        $this->db->where('ventaId', $idVenta);
        return $this->db->delete('ventasm_detalle');
    }
    public function updateproducto($idproductos,$cantidad)
    { 
       $sql = "UPDATE producto_hijo SET stock=stock-$cantidad WHERE productoid=$idproductos";
       $this->db->query($sql);
    }
    //=================================================================================
        function getlistventas_g($params){
            $columns = array( 
                0=>'ven.ventaId',
                1=>'ven.reg',
                2=>'ven.monto_total',
                3=>'ven.metodo', 
                4=>'per.nombre',
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas ven');
            $this->db->join('personal per', 'per.personalId=ven.personalId','left');
            $this->db->where(array('ven.activo'=>1));
            $this->db->where('ven.metodo < 3');


            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        public function getlistventas_g_total($params){
            $columns = array( 
                0=>'ven.ventaId',
                1=>'ven.reg',
                2=>'ven.monto_total',
                3=>'ven.metodo', 
                4=>'per.nombre',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventas ven');
            $this->db->join('personal per', 'per.personalId=ven.personalId','left');
            $this->db->where(array('ven.activo'=>1));
            $this->db->where('ven.metodo < 3');
            
            //$where = ;
            //$this->db->where(array('facturaabierta'=>1));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            
            $query=$this->db->get();

            return $query->row()->total;
        }
    //=================================================================================
        function getlistventas_v($params){
            $columns = array( 
                0=>'ven.ventaId',
                1=>'ven.reg',
                2=>'ven.monto_total',
                3=>'ven.metodo', 
                4=>'per.nombre',
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas ven');
            $this->db->join('personal per', 'per.personalId=ven.personalId','left');
            $this->db->where(array('ven.activo'=>1));
            $this->db->where('ven.metodo = 3');


            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        public function getlistventas_v_total($params){
            $columns = array( 
                0=>'ven.ventaId',
                1=>'ven.reg',
                2=>'ven.monto_total',
                3=>'ven.metodo', 
                4=>'per.nombre',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventas ven');
            $this->db->join('personal per', 'per.personalId=ven.personalId','left');
            $this->db->where(array('ven.activo'=>1));
            $this->db->where('ven.metodo = 3');
            
            //$where = ;
            //$this->db->where(array('facturaabierta'=>1));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            
            $query=$this->db->get();

            return $query->row()->total;
        }
    //=================================================================================
    //=================================================================================
        function getlistventas_r($params){
            $columns = array( 
                0=>'ven.ventaId',
                1=>'ven.reg',
                2=>'ven.monto_total',
                3=>'ven.metodo', 
                4=>'per.nombre',
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventasm ven');
            $this->db->join('personal per', 'per.personalId=ven.personalId','left');
            $this->db->where(array('ven.activo'=>1));
            $this->db->where('ven.metodo > 0');


            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        public function getlistventas_r_total($params){
            $columns = array( 
                0=>'ven.ventaId',
                1=>'ven.reg',
                2=>'ven.monto_total',
                3=>'ven.metodo', 
                4=>'per.nombre',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventasm ven');
            $this->db->join('personal per', 'per.personalId=ven.personalId','left');
            $this->db->where(array('ven.activo'=>1));
            $this->db->where('ven.metodo > 0');
            
            //$where = ;
            //$this->db->where(array('facturaabierta'=>1));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            
            $query=$this->db->get();

            return $query->row()->total;
        }
    //=================================================================================
}