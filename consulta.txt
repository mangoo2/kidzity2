ALTER TABLE ventasm ADD descuento FLOAT NULL DEFAULT NULL AFTER reg, 
ADD monto_cobrado FLOAT NULL DEFAULT NULL AFTER descuento;

ALTER TABLE ventasm MODIFY reg datetime;