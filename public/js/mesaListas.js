var selectedmesa=0;
var valuePIN=0;
var base_url=$('#base_url').val();
$(document).ready(function($) 
{
    $('.regresaprincipal').click(function(event) {
        $(".panelventa1").css("display","block");
        $(".panelventa2").css("display","none");
        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","none");
    });

	$(".selectedmesa").click(function(event) 
    {
        //$("#inlineForm").modal('show'); 
        selectedmesa= $(this).attr('data-mesa');
    	$(".panelventa1").css("display","none");
    	$(".panelventa2").css("display","block");
        
    });

    $("#botonModalConfirmar").click(function(event) 
    {
        valuePIN = $("#pinModalUsuario").val();
        $("#inlineForm").modal('hide');
        var idProductoModal = $("#idProductoModal").val();
        // Si no viene vacio, es para agregar un producto, de lo contrario solo es para venta de mesa+
        console.log();
        if(idProductoModal!='' || idProductoModal!=0)
        {
            productoselected($("#idProductoModal").val());  
        }
    });
    
    $(".classproducto").click(function(event) 
    {
        //console.log('entrando a categoria');
    	productoid= $(this).attr('data-idcate');
    	cargaproductos(productoid);
    	$(".panelventa2").css("display","none");
    	$(".panelventa3").css("display","block");
    });

    $(".classVenta").click(function(event) 
    {
        $(".panelventa2").css("display","none");
        $("#inlineForm").modal('show'); 
        $("#panelventa4_"+selectedmesa).css("display","block");
        
    });

});

    function cargaproductos(idp){
    	$.ajax({
            type: 'POST',
            url: 'Mesas/cargarproductos',
            data: {id: idp,
                   selectedmesa: selectedmesa},
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');

                },
                500: function(data) {
                	toastr.error('Error', '500');
                	console.log(data.responseText);
                }
            },
            success: function(data) {
            	$('.panelventa3').html('');
                $('.panelventa3').html(data);
            }
        });
    }

    function preproductoselected(idp)
    {
        $("#inlineForm").modal('show'); 
        $("#idProductoModal").val(idp); 
    }

    function productoselected(idp)
    {
        // Volvemos invisible el DIV con los productos
    	$(".panelventa3").css("display","none");
        // Dejamos el DIV vacio
    	$(".panelventa3").html("");
    	// Definimos el Div en cual trabajaremos, en base a la mesa seleccionada previamente
        var mesaActual = "panelventa4_"+selectedmesa;

        // Si este DIV ya existe, solo lo mostramos
        if($('#panelventa4_'+selectedmesa).length != 0)
        {
            $('#panelventa4_'+selectedmesa).css("display","block");
        }
        // Si no existe, lo clonamos del original
        else
        {
            var $template = $('#panelventa4'),
                            $clone = $template
                            .clone()
                            .show()
                            .removeAttr('id')
                            .attr('id',mesaActual)
                            .insertAfter("#panelventa4");
            $(mesaActual).css("display","block");

            // Cambiar el atributo del input hidden para agregar el que sea de la mesa que se está trabajando
            $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputHijo').attr('id','totalproductos_'+selectedmesa);

            // Cambiar el atributo del input para el cambio, que corresponda a la mesa que se está trabajando
            $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputMontoPagar').attr('id','montoapagar_'+selectedmesa);  

            // Cambiar el atributo del input para el boton de vender, que corresponda a la mesa que se está trabajando
            $("#panelventa4_"+selectedmesa+" > .divPadreVender ").find('.vender').attr('id','vender_'+selectedmesa);          
        }
        
        // Llamamos al ajax para agregar los productos
        $.ajax({
            type: 'POST',
            url: 'Mesas/agregarproductos',
            data: {pin: $("#pinModalUsuario").val(), 
                   id: idp,
                   selectedmesa: selectedmesa},
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error!', 'No Se encuentra el archivo');

                },
                500: function(data) {
                    toastr.error('Error', '500');
                    console.log(data.responseText);
                }
            },
            success: function(data) 
            {
                $("#pinModalUsuario").val('');
                $("#idProductoModal").val('')
                if(data!='usuarioinvalido')
                {
                    // Borramos lo que contenga el TBODY de la tabla
                    $("#"+mesaActual+" > div > table > tbody" ).html(' ');
                    // Agregamos el nuevo contenido
                    $("#"+mesaActual+" > div > table > tbody" ).html(data);
                }
                else
                {
                    alert('El PIN no coincide con el mesero');
                    $('#panelventa4_'+selectedmesa).css("display","none");
                    $(".panelventa1").css("display","block");
                }
            }
        });

        // Calculamos el total, y le pasamos el número de la mesa que estamos manipulando
        calculartotal(selectedmesa);
        
    }

    function calculartotalVacio(id)
    {
        var aux = id.split("_");
        calculartotal(aux[1]);
    }
    
    function calculartotal(mesa)
    {
        // Declaramos la variable que tendrá la suma del total
        var addtp = 0;
        $('.vstotal_'+mesa).each(function() 
        {
            var vstotal = $(this).val();
            addtp += Number(vstotal);
        });
        $('.totalproductos').html(new Intl.NumberFormat('es-MX').format(addtp));

        //console.log(addtp);
        $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputHijo').val(addtp);

        //$('#totalproductos').val(addtp);
        //$('#totalproductos'+mesa).val(addtp);

        //var cambio=parseFloat($('#montoapagar').val())-parseFloat(addtp);
        var addtpConvertido = parseFloat(addtp);
        var montoaPagarConvertido = parseFloat($("#panelventa4_"+selectedmesa+" > .divPadre ").find('.inputMontoPagar').val());
        
        var cambio = parseFloat(montoaPagarConvertido-addtpConvertido); 
        
         if (cambio<=0) 
        {
            cambio=0;
        }
        else
        {
            cambio=cambio;
        }
        $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.cambio').html('');
        $("#panelventa4_"+selectedmesa+" > .divPadre ").find('.cambio').html(new Intl.NumberFormat('es-MX').format(cambio));
    }

    function deletepro(id,mesa){
        $.ajax({
            type:'POST',
            url: 'Mesas/deleteproducto',
            data: {
                idd: id,
                mesa: mesa
                },
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data)
                {
                    $('.producto_'+id).remove();
                }
            });
        calculartotal(mesa);
    }


    //$('#vender').click(function(event) 
    function clickVender(mesa)
    {
        var pin = $("#pinModalUsuario").val(); 

        var aux = mesa.split("_");
        var aux2 = aux[1];
        //var montotoal=$('#totalproductos').val();
        //var montotoal=$('.inputHijo').val();
        var montotoal = $("#panelventa4_"+aux2+" > .divPadre ").find('.inputHijo').val()
        //var pago =$('#montoapagar').val();
        //var pago =$('#montoapagar').val();
        var pago = $("#panelventa4_"+aux2+" > .divPadre ").find('.inputMontoPagar').val()
        
        if(parseFloat(pago) >= parseFloat(montotoal)) 
        {
            var DATA  = [];
            //var TABLA   = $("#tableproductosv tbody > tr");
            var TABLA = $("#panelventa4_"+aux2+" > div > table > tbody > tr");
            TABLA.each(function()
            {    
                item = {};
                item ["idproductos"] = $(this).find("input[id*='idproductos']").val();     
                item ["cantidad"] = $(this).find("input[id*='vscanti']").val();
                item ["precio"] = $(this).find("input[id*='vsprecio']").val();

              
              DATA.push(item);
            });
            productos   = JSON.stringify(DATA);
            //console.log(productos);
            
            $.ajax(
            {
                type: 'POST',
                url: base_url+'Mesas/addventas',
                data: {
                    //monto_total:$('#totalproductos').val(),
                    pinaux: pin,
                    monto_total: montotoal,
                    selectedmesa: aux2,
                    pulcera:$('#cuentakz').val(),
                    metodo:$('#metodopago option:selected').val(),
                    productos:productos
                },
                async: false,
                statusCode: 
                {
                    404: function(data) 
                    {
                        toastr.error('Error', '404');
                    },
                    500: function(data) 
                    {
                        
                    },
                },
                success: function(data) 
                {
                    if(data!='usuarioinvalido')
                    {
                        $('.ticketventa').html('<iframe src="'+base_url+'Ticketmesas/dulceria?comp='+data+'" class="iframevisitantes"></iframe>');   
                        toastr.success('venta realizada', 'Hecho');
                        setTimeout(2000);
                        eliminaMesa(aux2);
                    }
                    else
                    {
                        alert('El PIN no coincide con el mesero');
                    }
                },
            });
            
            
        }
        else
        {
            toastr.error('Error!', 'Agregar monto igual o mayor al total');
        }
    }

    function eliminaMesa(numeroMesa)
    {
        $.ajax({
            type:'POST',
            url: 'Mesas/deleteMesa',
            data: {
                mesa: numeroMesa
                },
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data)
                {
                    $("#panelventa4_"+numeroMesa).remove();
                    $(".panelventa1").css("display","block");
                }
            });
    }
