<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy=date('Y-m-d H:i:s');
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
    }
    function getadminsu(){
        $strq = "CALL SP_GET_PERSONAL_ADMINSU";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getestados(){
        $strq = "SELECT * FROM estado";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    public function getselectwheren($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getselectwhere2($tables,$cols1,$values1,$cols2,$values2){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols1,$values1);
        $this->db->where($cols2,$values2);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function getselectwhere3($tables,$cols1,$values1,$cols2,$values2,$cols3,$values3){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols1,$values1);
        $this->db->where($cols2,$values2);
        $this->db->where($cols3,$values3);
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    function categoriaadd($nom){
        $strq = "INSERT INTO producto_padre(categoria) VALUES ('$nom')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        
        return $id;
    }
    function categoriupdate($nom,$id){
        $strq = "UPDATE producto_padre SET categoria='$nom' WHERE productopId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function categoriaaddimg($img,$id){
        $strq = "UPDATE producto_padre SET img='$img' WHERE productopId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function categoriadell($id){
        $strq = "UPDATE producto_padre SET activo=0 WHERE productopId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function insertToCatalogo($data,$catalogo){
        $this->db->insert($catalogo, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);
        $this->db->update($catalogo);
        return $id;
    }
    function updateCatalogo2($data,$idname1,$id1,$idname2,$id2,$catalogo){
        $this->db->set($data);
        $this->db->where($idname1, $id1);
        $this->db->where($idname2, $id2);
        $this->db->update($catalogo);
        //return $id;
    }
     function updateCatalogon($data,$where,$catalogo){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($catalogo);
        //return $id;
    }
    function productosaddimg($img,$id){
        $strq = "UPDATE producto_hijo SET img='$img' WHERE productoid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function getproductosall(){
        $sql = "SELECT pro.productoid,pro.codigo,pro.producto,pro.descripcion,prop.categoria,pro.stock,pro.img
                FROM producto_hijo as pro 
                inner join producto_padre as prop on prop.productopId=pro.productopId
                where pro.activo=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function eliminarrow($table,$idname,$id){
        $this->db->set('activo',0);
        $this->db->where($idname, $id);
        return $this->db->update($table);
    }
    function getareas(){
        $strq = "SELECT * FROM perfiles WHERE perfilId>2";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getparentesco(){
        $strq = "SELECT * FROM parentescos";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getcostotime(){
        $strq = "SELECT * FROM costos";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getselectvalue1rowwhere($table,$colv,$colw,$valw){
        $this->db->select($colv.' as valor');
        $this->db->from($table);
        $this->db->where($colw,$valw);
        $query=$this->db->get();
        $value=0;
        foreach ($query->result() as $row) {
            $value =$row->valor;
        } 
        return $value;

    }
    function compratiempo($titular,$metodo,$tiempo,$pagado,$tipo,$fecha,$descuento,$idpersonal,$desctext){
        $strq = "INSERT INTO compra_tiempo(titularId, metodo, tiempo, pagado,descuento,tipo,personalId,reg,descuentotext) 
        VALUES ('$titular','$metodo','$tiempo','$pagado','$descuento','$tipo','$idpersonal','$fecha','$desctext')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        //$this->db->close();
        return $id;
    }
    function compratiempon2($compraid,$ninoid,$pagado){
        $strq = "INSERT INTO `compra_tiempo_nino`(`compraId`, `ninoid`, `pagado`) VALUES ($compraid,$ninoid,'$pulcera','$pagado')";
        $query = $this->db->query($strq);
        $this->db->close();
    }
 
    function compratiempon($compraid,$ninoid,$pulcera,$pagado,$tlbebe,$tladulto,$fecha,$descuento,$pfam){
        $strq = "INSERT INTO `compra_tiempo_nino`(`compraId`, `ninoid`, `pulcera`, `pagado`,tlbebe,`tladulto`,descuento,reg_abierto,paquetefamiliar) 
        VALUES ($compraid,$ninoid,'$pulcera','$pagado','$tlbebe','$tladulto','$descuento','$this->fechahoy','$pfam')";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function addsaldo($titular,$saldo){
        $resultado=$this->getselectwhere('config','configId',1);
        $diasv=0;
        foreach ($resultado->result() as $row) {
            $diasv =$row->valor;
        } 
        $saldod=$this->sumasaldosdisponibles($titular);
        
        $saldo=$saldo-$saldod;
        //==============================================================================================================
        if ($saldo>0) {
           $strq = "INSERT INTO abono_saldos(titularId, cantidad,vence) VALUES ($titular,'$saldo',NOW() + INTERVAL $diasv day)";
            $query = $this->db->query($strq);
            $this->db->close();
        }
        
        //return $strq;
    }
    function SaldoAdd($titular,$saldo){
        $resultado=$this->getselectwhere('config','configId',1);
        $diasv=0;
        foreach ($resultado->result() as $row) {
            $diasv =$row->valor;
        } 
        //==============================================================================================================
           $strq = "INSERT INTO abono_saldos(titularId, cantidad,vence) VALUES ($titular,'$saldo',NOW() + INTERVAL $diasv day)";
            $query = $this->db->query($strq);
            $this->db->close();
        
        return $strq;
    }
    function Insertar($tabla, $valores){
        $this->db->insert(''.$tabla, $valores);
        //$this->db->close();
    }

    function GetDataTurno($punto_venta){
        $strq = "SELECT * FROM turno WHERE punto_venta='$punto_venta' and status=1";
        $query = $this->db->query($strq);
        return $query->row();
    }
    function editarTurno($fechacierre, $horac ,$id) {
        $strq = "UPDATE turno SET fechacierre='$fechacierre', horac='$horac' ,status=0  where id='$id';";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function updatestockpro($cantidad ,$id) {
        $strq = "UPDATE producto_hijo SET stock=stock+'$cantidad'  where productoid='$id';";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function sumasaldosdisponibles($titular){
        $fechahoy=date('Y-m-d H:i:s');
        $strq = "SELECT sum(cantidad) as cantidad FROM abono_saldos where titularId=$titular and vence>'$fechahoy'";
        $query = $this->db->query($strq);
        $this->db->close();
        $saldod=0;
        foreach ($query->result() as $row) {
            $saldod =$row->cantidad;
        } 
        return $saldod;
    }
    function sumasaldosdisponiblesanterior($titular,$fecha){
        $fechahoy=date('Y-m-d H:i:s');
        $strq = "SELECT sum(cantidad) as cantidad FROM abono_saldos where titularId=$titular and vence>'$fechahoy' and inicio<'$fecha'";
        $query = $this->db->query($strq);
        $this->db->close();
        $saldod=0;
        foreach ($query->result() as $row) {
            $saldod =$row->cantidad;
        } 
        return $saldod;
    }
    function sumasaldosdisponiblesnuevo($titular,$fecha){
        $fechahoy=date('Y-m-d H:i:s');
        $strq = "SELECT sum(cantidad) as cantidad FROM abono_saldos where titularId=$titular and vence>'$fechahoy' and inicio>='$fecha'";
        $query = $this->db->query($strq);
        $this->db->close();
        $saldod=0;
        foreach ($query->result() as $row) {
            $saldod =$row->cantidad;
        } 
        return $saldod;
    }
    function comprasninos($compraid){
        $strq = "SELECT nin.nombre 
                FROM compra_tiempo_nino as comn 
                inner JOIN ninos as nin on nin.id=comn.ninoid
                WHERE comn.compraId=$compraid";
        $query = $this->db->query($strq);
        $this->db->close();
        $nombre='';
        foreach ($query->result() as $row) {
            $nombre .='<p>'.$row->nombre.'</p>';
        } 
        return $nombre;
    }
    function comprasfiesta($compraid){
        $strq = "SELECT fi.nombre 
                FROM compra_tiempo as comp 
                inner JOIN fiestas as fi on fi.fiestaId=comp.tipo 
                WHERE comp.compraId=$compraid";
        $query = $this->db->query($strq);
        $this->db->close();
        $nombre='';
        foreach ($query->result() as $row) {
            $nombre =$row->nombre;
        } 
        return $nombre;
    }
    function comprastotalpagado($compraid){
        $strq = "SELECT sum(pagado) as pagado FROM compra_tiempo_nino WHERE compraId=$compraid";
        $query = $this->db->query($strq);
        $this->db->close();
        $pagado=0;
        foreach ($query->result() as $row) {
            $pagado =$row->pagado;
        } 
        return $pagado;
    }
    function verificarsaldo($codigo){
        $strq = "SELECT sum(abo.cantidad) as cantidad
                FROM compra_tiempo_nino as cnino
                inner JOIN compra_tiempo as comp on comp.compraId=cnino.compraId
                inner JOIN abono_saldos as abo on abo.titularId=comp.titularId
                WHERE cnino.status=1 AND abo.vence>=NOW() AND cnino.pulcera='$codigo'";
        $query = $this->db->query($strq);
        $this->db->close();
        $cantidad=0;
        foreach ($query->result() as $row) {
            $cantidad =$row->cantidad;
        } 
        if ($cantidad==null) {
            $cantidad=0;
        }
        if ($cantidad==0) {
            $strq = "SELECT sum(abo.cantidad) as cantidad
                    FROM abono_saldos as abo 
                    WHERE abo.vence>=NOW() AND abo.titularId='$codigo'";
            $query = $this->db->query($strq);
            $this->db->close();
            $cantidad=0;
            foreach ($query->result() as $row) {
                $cantidad =$row->cantidad;
            }
            if ($cantidad==null) {
                $cantidad=0;
            }
        }
        return $cantidad;
    }
    function getninoscompras($id){
        $strq = "SELECT 
                    comptn.pulcera,
                    nin.nombre,
                    comptn.pagado,
                    comptn.status,
                    nin.fecha_nacimiento,
                    comptn.ninoid,

                    comptn.tlbebe,
                    comptn.tladulto,
                    comptn.paquetefamiliar

            FROM compra_tiempo_nino as comptn
            inner JOIN ninos as nin on nin.id=comptn.ninoid
            WHERE comptn.compraId=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function encrypturl($string) {
        $key='kidzitymanguito';
       $result = '';
       for($i=0; $i<strlen($string); $i++) {
              $char = substr($string, $i, 1);
              $keychar = substr($key, ($i % strlen($key))-1, 1);
              $char = chr(ord($char)+ord($keychar));
              $result.=$char;
       }

       $result=base64_encode($result);
       $result = str_replace(array('+','/','='),array('-','_','.'),$result);
       return $result;
    } 
    function decrypturl($string) {
        $key='kidzitymanguito';
       $string = str_replace(array('-','_','.'),array('+','/','='),$string);
       $result = '';
       $string = base64_decode($string);
       for($i=0; $i<strlen($string); $i++) {
              $char = substr($string, $i, 1);
              $keychar = substr($key, ($i % strlen($key))-1, 1);
              $char = chr(ord($char)-ord($keychar));
              $result.=$char;
       }
       return $result;
    }
    function getventadetalle($id){
        $sql = "SELECT ved.cantidad,ved.precio,ved.descuento, pro.producto
                FROM venta_detalle as ved
                inner JOIN producto_hijo as pro on pro.productoid=ved.productoId
                WHERE ved.ventaId=$id";
        $query = $this->db->query($sql);
        return $query;
    }
    function pulceravigente($value){
        $strq = "SELECT ctn.compranId,
                        ctn.compraId,
                        ctn.ninoid,
                        ctn.pulcera,
                        ctn.pagado,
                        ctn.tlbebe,
                        ctn.tladulto,
                        ctn.reg_abierto,
                        ct.tiempo,
                        ct.tipo,
                        fi.hora_fin,
                        ni.fecha_nacimiento,
                        ctn.paquetefamiliar
                from compra_tiempo_nino as ctn 
                inner join compra_tiempo as ct on ct.compraId=ctn.compraId
                left join fiestas as fi on fi.fiestaId=ct.tipo
                left join ninos as ni on ni.id=ctn.ninoid
                where ctn.pulcera='$value' and ctn.status=1";
        $query = $this->db->query($strq);
        $this->db->close();        
        return $query;
    }
    function ninosalida($id,$pago,$tiempoex,$idpersonal){
        date_default_timezone_set('America/Mexico_City');
        $fechahoy=date('Y-m-d H:i:s');
        $strq = "UPDATE compra_tiempo_nino SET 
                    tiempoextra='$tiempoex', 
                    pagotiempoextra='$pago',
                    reg_cerrado='$fechahoy',
                    personalId_salida='$idpersonal',
                    status=0 
                    WHERE compranId=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function ninosalidamanual($id,$idpersonalid){
        date_default_timezone_set('America/Mexico_City');
        $fechahoy=date('Y-m-d H:i:s');
        $strq = "UPDATE `compra_tiempo_nino` SET reg_cerrado='$fechahoy',personalId_salida='$idpersonalid',status=0 WHERE `compranId`=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }
    function getventadetalleMesas($id){
        $sql = "SELECT ved.vdetalleId,ved.cantidad,ved.precio, pro.producto
                FROM ventasm_detalle as ved
                inner JOIN producto_hijo as pro on pro.productoid=ved.productoId
                WHERE ved.ventaId=$id and ved.cancelado!=1";
        $query = $this->db->query($sql);
        return $query;
    }
    function getventadetalleMesasSimulado($id){
        $sql = "SELECT ved.cantidad,ved.precio, pro.producto
                FROM ventasm_detalle_simulada as ved
                inner JOIN producto_hijo as pro on pro.productoid=ved.productoId
                WHERE ved.ventaId=$id";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getListadoTurnoMesero(){
        $sql = "SELECT  p.nombre,
                v.metodo,
                v.monto_total,
                v.reg
                FROM ventasm as v
                INNER JOIN personal as p ON p.personalId = v.personalId
                inner join turno as t on v.reg>concat(t.fecha,' ',t.horaa)
                where v.activo= 1 and t.punto_venta=5 and t.status=1;";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getListadoTurnoVentasRegalos(){
        $sql = "SELECT  p.nombre,
                v.metodo,
                v.monto_total,
                v.reg
                FROM ventas as v
                INNER JOIN personal as p ON p.personalId = v.personalId
                inner join usuarios as u ON u.personalId = p.personalId
                inner join turno as t on v.reg>concat(t.fecha,' ',t.horaa)
                where v.activo= 1 and t.punto_venta=4 and t.status=1 and u.perfilId=4;";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getListadoTurnoVentasCafeteria(){
        $sql = "SELECT  p.nombre,
                v.metodo,
                v.monto_total,
                v.reg
                FROM ventas as v
                INNER JOIN personal as p ON p.personalId = v.personalId
                inner join usuarios as u ON u.personalId = p.personalId
                inner join turno as t on v.reg>concat(t.fecha,' ',t.horaa)
                where v.activo= 1 and t.punto_venta=3 and t.status=1 and u.perfilId=3;";
        $query = $this->db->query($sql);
        return $query;
    }
    function getturnos(){
        $strq = "SELECT * FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    public function VentasTurno($id,$ventas){
        $strq = '';
        if($ventas==5){
        $strq = "SELECT  p.nombre,
        v.metodo,
        v.monto_total,
        v.reg
        FROM ventasm as v
        INNER JOIN personal as p ON p.personalId = v.personalId
        inner join turno as t on v.reg>concat(t.fecha,' ',t.horaa) and v.reg<concat(t.fechacierre,' ',t.horac)
        where   v.activo= 1 and t.punto_venta=5 and id=$id;";    
        }
        else{
       $strq = "SELECT  p.nombre,
        v.metodo,
        v.monto_total,
        v.reg
        FROM ventas as v
        INNER JOIN personal as p ON p.personalId = v.personalId
        inner join usuarios as u ON u.perfilId = p.personalId
        inner join turno as t on v.reg>concat(t.fecha,' ',t.horaa) and v.reg<concat(t.fechacierre,' ',t.horac)
        where   v.activo= 1 and t.punto_venta=5 and id=$id;";  

        }
        $query = $this->db->query($strq);
        return $query;
    }
    function updatefiestascant($id) {
        $strq = "UPDATE fiestas SET cantidad=cantidad-1,cantidadm=cantidadm+1  where fiestaId='$id';";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    //=========== COMPRAS ========================
    function filasCompras(){
        $strq = "SELECT COUNT(*) as total FROM compras where Activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->total;
        } 
        return $total;
    }
    function ListCompras($por_pagina,$segmento){
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq="SELECT 
        comp.compraId,
        comp.monto_total,
        per.nombre as usuario,
        comp.Activo,
        comp.reg

        FROM compras as comp 
        inner join personal as per on per.personalId=comp.usuario
        WHERE  comp.Activo=1 ORDER BY comp.compraId DESC LIMIT $por_pagina $segmento;";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function searchproducto($buscar){
        $strq="SELECT * FROM producto_hijo 
                where 
                activo=1 and codigo='$buscar' or
                activo=1 and producto like '%$buscar%' or
                activo=1 and descripcion like '%$buscar%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function comprassallsearch($buscar){
        $strq="SELECT 
                comp.compraId,
                comp.monto_total,
                per.nombre as usuario,
                comp.Activo,
                comp.reg

                FROM compras as comp 
                inner join personal as per on per.personalId=comp.usuario
                WHERE  
                comp.Activo=1 and comp.monto_total like '$buscar' or 
                comp.Activo=1 and per.nombre like '%$buscar%' or
                comp.Activo=1 and comp.reg like '%$buscar%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function detallescompras($id){
        $strq="SELECT 
                comp.id_detalle_compra,
                comp.cantidad,
                comp.precio,
                pro.producto
                FROM compras_detalle as comp 
                inner join producto_hijo as pro on pro.productoid=comp.productoid
                WHERE comp.compraId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function entradas($id,$inicio,$fin){
        if ($id==0) {
            $personal="";
        }else{
            $personal="con.IdPersonal=".$id." AND ";
        }
        $strq="SELECT per.nombre, con.FechaEntrada,con.HoraEntrada,con.HoraSalida 
                FROM conexiones as con 
                inner JOIN personal as per on per.personalId=con.IdPersonal
                WHERE $personal con.FechaEntrada BETWEEN '$inicio' and '$fin'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    //===================================
    function ninosactivos(){
        $strq = "SELECT COUNT(*) as total FROM compra_tiempo_nino where status=1 and tladulto=0";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->total;
        } 
        return $total;
    }
    function adultosactivos(){
        $strq = "SELECT COUNT(*) as total FROM compra_tiempo_nino where status=1 and tladulto=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->total;
        } 
        return $total;
    }
    function saldos($titular){
        if ($titular==0) {
            $titularl='';
        }else{
            $titularl=' abo.titularId=2 AND ';
        }
        $strq="SELECT abo.saldoId,abo.cantidad,abo.referencia,abo.inicio,abo.vence, tit.nombre 
                FROM abono_saldos as abo 
                inner join titular as tit on tit.titularId=abo.titularId
                WHERE $titularl abo.vence>NOW()";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortecafe($fechainicio,$fechafin){
        $strq="SELECT ve.ventaId,ve.reg,ve.metodo,ve.pulcera,ve.compraId,ve.monto_total,per.nombre
                from ventas as ve
                inner join personal as per on per.personalId=ve.personalId
                WHERE ve.activo=1 and ve.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:59'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortecafem($fechainicio,$fechafin,$metodopago2){
        if($metodopago2>0){
            $wheremetodo=" ve.metodo=$metodopago2 ";
        }else{
            $wheremetodo=' ve.metodo>0 ';
        }
        $strq="SELECT ve.ventaId,ve.reg,ve.metodo,ve.monto_total,ve.descuento,ve.monto_cobrado,per2.nombre as 'nombreAbre',per.nombre,ve.mesa
                from ventasm as ve
                inner join personal as per on per.personalId=ve.personalId
                left join personal as per2 on per2.personalId=ve.personalId_abre
                WHERE 
                    ve.activo=1 and $wheremetodo and ve.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:59' and ve.tipo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortecompratiempo($fechainicio,$fechafin,$tipov,$vende,$metodopago){
        //log_message('error', '.... tipov:'.$tipov.'  vendedor:'.$vende);
        if ($tipov==1) {
            if ($vende>0) {
                $where=' and comn.pagado>0 and com.personalId='.$vende.'';
            }else{
                $where='';
            }
        }else if ($tipov==2) {
            if ($vende>0) {
                $where=' and comn.pagotiempoextra>0 and comn.personalId_salida='.$vende.' ';
            }else{
                $where='';
            }
        }else{
            if ($vende>0) {
                $where=' and ((comn.pagado>0 and com.personalId='.$vende.') or (comn.pagotiempoextra>0 and comn.personalId_salida='.$vende.')) ';
            }else{
                $where='';
            }
        }
        if($metodopago>0){
            $wheremetodo=" and com.metodo=$metodopago ";
        }else{
            $wheremetodo="";
        }



        $strq="SELECT comn.compranId,comn.compraId,nin.nombre as nino,tit.nombre as titular,com.tiempo,comn.pagado,com.metodo,com.referencia,comn.reg_abierto,comn.reg_cerrado,comn.tiempoextra,comn.pagotiempoextra,
            per.nombre,per.personalId as perIde,
            comn.descuento,com.descuentotext,comn.tlbebe,comn.tladulto,
            pers.nombre as personalsalida,pers.personalId as perIds,
            comn.devolucion_monto,comn.devolucion_motivo,comn.paquetefamiliar
            FROM compra_tiempo_nino as comn
            inner join ninos as nin on nin.id=comn.ninoid
            inner JOIN compra_tiempo as com on com.compraId=comn.compraId
            inner JOIN titular as tit on tit.titularId=com.titularId
            left join personal as per on com.personalId=per.personalId
            left join personal as pers on comn.personalId_salida=pers.personalId
            WHERE com.tipo=0 $where $wheremetodo and com.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:59'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortecompratiempoproductos($fechainicio,$fechafin,$tipov,$vende,$metodopago){
        if ($tipov==1) {
            if ($vende>0) {
                $where=' and ven.personalId='.$vende.'';
            }else{
                $where='';
            }
        }else if ($tipov==2) {
            if ($vende>0) {
                $where=' and  ven.personalId='.$vende.' ';
            }else{
                $where='';
            }
        }else{
            if ($vende>0) {
                $where=' and  ven.personalId='.$vende.' ';
            }else{
                $where='';
            }
        }
        if($metodopago>0){
            $wheremetodo=" and ven.metodo=$metodopago ";
        }else{
            $wheremetodo="";
        }

        $strq="SELECT vend.vdetalleId,vend.cantidad,vend.precio,vend.descuento,pro.producto,ven.metodo
                FROM ventas as ven
                INNER JOIN venta_detalle as vend on vend.ventaId=ven.ventaId
                INNER JOIN producto_hijo as pro on pro.productoid=vend.productoId
                WHERE ven.activo=1 $where $wheremetodo and ven.reg BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'
                group by vend.vdetalleId
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortecompratiempodevolucion($fechainicio,$fechafin){
        /*
        $strq="SELECT comp.compraId,tit.nombre as titular,comp.tiempo,(SELECT sum(compn.pagado) FROM compra_tiempo_nino as compn WHERE compn.compraId=comp.compraId) as pagado,comp.metodo,comp.referencia,comp.reg,fi.nombre
            FROM compra_tiempo as comp
            inner join titular as tit on tit.titularId=comp.titularId
            left JOIN fiestas as fi on fi.fiestaId=comp.tipo
            WHERE comp.status=0 and comp.tipo=0 and comp.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:59'";
            */
        $strq="SELECT comn.compranId,comn.compraId,nin.nombre as nino,tit.nombre as titular,com.tiempo,comn.pagado,com.metodo,com.referencia,comn.reg_abierto,comn.reg_cerrado,comn.tiempoextra,comn.pagotiempoextra,per.nombre,comn.descuento,comn.tlbebe,pers.nombre as personalsalida,comn.devolucion_monto
            FROM compra_tiempo_nino as comn
            inner join ninos as nin on nin.id=comn.ninoid
            inner JOIN compra_tiempo as com on com.compraId=comn.compraId
            inner JOIN titular as tit on tit.titularId=com.titularId
            left join personal as per on com.personalId=per.personalId
            left join personal as pers on comn.personalId_salida=pers.personalId
            WHERE com.tipo=0 and comn.devolucion_monto>0 and com.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:59'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortetotalcanproductos($fechainicio,$fechafin,$efe){
        if ($efe==1) {
            $whereefe='';
        }else{
            $whereefe='ven.metodo=2 and';
        }
        $strq="SELECT sum(vd.cantidad) as cantidad, pro.producto,sum(vd.cantidad*vd.precio) as precio
                from venta_detalle as vd
                inner join producto_hijo as pro on pro.productoid=vd.productoId
                inner JOIN ventas as ven on ven.ventaId=vd.ventaId
                WHERE $whereefe ven.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:29'
                GROUP by vd.productoId";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortetotalcanproductosm($fechainicio,$fechafin,$efe,$metodopago2){
        if ($efe==1) {
            $whereefe='';
            if($metodopago2>0){
                $whereefe=" ven.metodo=$metodopago2 and ";
            }else{
                $whereefe=" ven.metodo>0 and ";
            }
        }else{
            $whereefe='ven.metodo=2 and';
        }
        $strq="SELECT sum(vd.cantidad) as cantidad, pro.producto,sum(vd.cantidad*vd.precio) as precio
                from ventasm_detalle as vd
                inner join producto_hijo as pro on pro.productoid=vd.productoId
                inner JOIN ventasm as ven on ven.ventaId=vd.ventaId
                WHERE ven.tipo=1 and vd.cancelado=0 and $whereefe ven.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:29' 
                GROUP by vd.productoId";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortefiestasactivos($fechainicio,$fechafin){
        $strq="SELECT fi.fiestaId,fi.nombre as fiesta,fi.cantidadt,fi.cantidadm,fi.fecha_inicio,fi.hora_inicio,fi.hora_fin,fi.precio_total
            FROM fiestas as fi
            WHERE fi.activo=1 AND fi.fecha_inicio BETWEEN '$fechainicio' and '$fechafin'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortecompras($fechainicio,$fechafin){
        $strq="SELECT 
        comp.compraId,
        comp.monto_total,
        per.nombre as usuario,
        comp.Activo,
        comp.reg

        FROM compras as comp 
        inner join personal as per on per.personalId=comp.usuario
        WHERE  comp.Activo=1 AND comp.reg BETWEEN '$fechainicio' and '$fechafin'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function fiestatitular($id){
        $strq="SELECT tit.nombre
                FROM compra_tiempo as comp
                inner JOIN titular as tit on tit.titularId=comp.titularId
                WHERE comp.tipo=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        $titular='';
        foreach ($query->result() as $item){
            $titular.='<p>'.$item->nombre.'</p>';
        }
        return $titular;
    }
    function fiestaninostotal($id){
        $strq="SELECT count(* ) as total
                FROM compra_tiempo as comp
                inner JOIN compra_tiempo_nino as compn on compn.compraId=comp.compraId
                WHERE comp.tipo=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        $total=0;
        foreach ($query->result() as $item){
            $total=$item->total;
        }
        return $total;
    }
    function fiestaninostotalcosto($id){
        $strq="SELECT sum(compn.pagado) as total
                FROM compra_tiempo as comp
                inner JOIN compra_tiempo_nino as compn on compn.compraId=comp.compraId
                WHERE comp.tipo=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        $total=0;
        foreach ($query->result() as $item){
            $total=$item->total;
        }
        return $total;
    }
    function cortecafemimpresion($fechainicio,$fechafin){
        $strq="SELECT ve.ventaId,ve.reg,ve.metodo,ve.monto_total,ve.descuento,ve.monto_cobrado,per2.nombre as 'nombreAbre',per.nombre,ve.mesa
                from ventasm as ve
                inner join personal as per on per.personalId=ve.personalId
                left join personal as per2 on per2.personalId=ve.personalId_abre
                WHERE ve.activo=1 and ve.metodo>0 and ve.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:59' and ve.tipo=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function cortetotalcanproductosmimpresion($fechainicio,$fechafin,$efe){
        if ($efe==1) {
            $whereefe='';
        }else{
            $whereefe='ven.metodo=2 and';
        }
        $strq="SELECT sum(vd.cantidad) as cantidad, pro.producto,sum(vd.cantidad*vd.precio) as precio
                from ventasm_detalle as vd
                inner join producto_hijo as pro on pro.productoid=vd.productoId
                inner JOIN ventasm as ven on ven.ventaId=vd.ventaId
                WHERE $whereefe ven.reg BETWEEN '$fechainicio 00:00:00' and '$fechafin 23:59:29' and ven.tipo=0
                GROUP by vd.productoId";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getcajeros(){
        $strq="SELECT usu.personalId,per.nombre 
            FROM usuarios as usu 
            inner join personal as per on per.personalId=usu.personalId
            WHERE usu.perfilId=4 and per.estatus=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getcajerosadmin(){
        $strq="SELECT usu.personalId,per.nombre 
            FROM usuarios as usu 
            inner join personal as per on per.personalId=usu.personalId
            WHERE usu.perfilId=1 and per.estatus=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getcajeros2(){
        $strq="SELECT per.personalId,per.nombre
            FROM personal as per
            WHERE per.estatus=1 ORDER by per.nombre asc";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    public function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        //$this->db->close();
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    } 
    public function cortegetallcreditos($fechainicio,$fechafin,$vender)
    {   if($vender >= 1){
          $where = "WHERE c.personalIdc = $vender AND c.reg BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";      
        }else{
          $where = "WHERE c.reg BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";  
        }
        $sql = "SELECT c.idc,c.reg,per.nombre,p.producto,cd.cantidad,cd.precio FROM creditos AS c 
                INNER JOIN creditos_detalle AS cd ON cd.idc = c.idc
                INNER JOIN producto_hijo AS p ON p.productoid = cd.productoid
                INNER JOIN personal AS per ON per.personalId = c.personalIdc
                $where";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    public function cortegetallcreditos_c($fechainicio,$fechafin,$vender)    {   
        if($vender >= 1){
          $where = "WHERE c.personalIdc = $vender AND c.reg BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";      
        }else{
          $where = "WHERE c.reg BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";  
        }
        $sql = "SELECT c.idc,c.reg,per.nombre,p.producto,sum(cd.cantidad) as cantidad,sum(cd.cantidad*cd.precio) as precio FROM creditos AS c 
                INNER JOIN creditos_detalle AS cd ON cd.idc = c.idc
                INNER JOIN producto_hijo AS p ON p.productoid = cd.productoid
                INNER JOIN personal AS per ON per.personalId = c.personalIdc
                $where
                GROUP by p.productoid";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    function panicorest(){
        $strq = "SELECT panico FROM config2 WHERE config=1";
        $query = $this->db->query($strq);
        //$this->db->close();
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->panico;
        }
        return $total; 
    }

}