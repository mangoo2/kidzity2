<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCliente extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function filasCliente(){
    	$strq = "SELECT COUNT(*) as total FROM titular where Activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->total;
        } 
        return $total;
    }

    function ContarSubT($id){
    	$strq = "SELECT COUNT(*) as Sub FROM titular_sub WHERE titularId= $id AND Activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->Sub;
        } 
        return $total;
    }

    function ContarMenor($id){
    	$strq = "SELECT COUNT(*) as Men FROM ninos WHERE usuario_id = $id AND Activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->Men;
        } 
        return $total;
    }

    function VerSubT($id){
    	$strq = "SELECT * FROM titular_sub WHERE titularId= $id AND Activo=1";
        $query = $this->db->query($strq);
        return $query;
    }

    function VerMenor($id){
    	$strq = "SELECT * FROM ninos WHERE usuario_id = $id AND Activo=1";
        $query = $this->db->query($strq);
        return $query;
    }

    function VerCliente($id){
    	$strq="SELECT * FROM titular WHERE titularId=$id";
    	$query=$this->db->query($strq);
    	return $query;
    }

    function getNinos($id){
    	$strq="SELECT nombre FROM ninos WHERE usuario_id= $id AND Activo=1";
    	$query=$this->db->query($strq);
    	$total="";
    	foreach ($query->result() as $row) {
        $total=$total.$row->nombre." / ";
        }
        return $total;
    }

    function ListCliente($por_pagina,$segmento){
    	if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq="SELECT * FROM titular WHERE  Activo=1 LIMIT $por_pagina $segmento;";
        $resp=$this->db->query($strq);
        return $resp;
    }

    function Insert($data,$Tabla){
    	$this->db->insert(''.$Tabla, $data);
    	$id=$this->db->insert_id();
    	return $id;
    }

    function InsertU1($id,$Nombre,$Movil,$Parentesco){
        $string="INSERT INTO `titular_sub`(`titularId`, `nombre`, `movil`, `parentesco`) VALUES ('$id','$Nombre','$Movil','$Parentesco')";
        $this->db->query($string);
        $this->db->close();
    }

    function InsertU2($Nombre,$FechaN,$Escuela,$Id){
        $string="INSERT INTO `ninos`(`nombre`, `fecha_nacimiento`, `escuela`, `usuario_id` ) VALUES ('$Nombre','$FechaN','$Escuela','$Id')";
        $this->db->query($string);
        $this->db->close();
    }

    function Update($data,$column,$ID,$tabla){
    	 $this->db->set($data);
    	 $this->db->where($column,$ID);
    	 $this->db->update($tabla);
    	 $this->db->close();
    }

    function UpdateSubT($Id,$Nombre,$Movil,$Parentesco){
    	$query="UPDATE `titular_sub` SET `nombre`='$Nombre',`movil`='$Movil',`parentesco`='$Parentesco' WHERE `titularsubId`= ".$Id." ";
    	$this->db->query($query);
    	$this->db->close();
    }

    function UpdateMenor($Nombre,$FechaN,$Escuela,$id){
    	$query="UPDATE `ninos` SET `nombre`='$Nombre',`fecha_nacimiento`='$FechaN',`escuela`='$Escuela' WHERE `id`= $id";
    	$this->db->query($query);
    	$this->db->close();
    }

    function Delete($Id){
         $query="UPDATE `ninos` SET `Activo`= 0 WHERE usuario_id= $Id";
         $this->db->query($query);
         $query2="UPDATE `titular_sub` SET `Activo`= 0 WHERE titularId = $Id";
         $this->db->query($query2);
         $query3="UPDATE `titular` SET `Activo`= 0 WHERE `titularId` = $Id ";
         $this->db->query($query3);
         $this->db->close();
    }

    function DeleteSubt($Id){
        $query2="UPDATE `titular_sub` SET `Activo`= 0 WHERE titularsubId = $Id";
        $this->db->query($query2);

    }

    function DeleteMen($Id){
        $query="UPDATE `ninos` SET `Activo`= 0 WHERE id = $Id";
        $this->db->query($query);
    }
    function associar($id,$codigo){
        $query2="UPDATE `ninos` SET `brazalete`= '$codigo' WHERE id = $id";
        $this->db->query($query2);

    }
    function clientesallsearch($usu){
        $strq = "SELECT * FROM titular 
                where Activo=1 and nombre like '%".$usu."%' or Activo=1 and movil like '%".$usu."%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function titularallsearch($usu){
        $strq = "SELECT * FROM titular where Activo=1 and nombre like '%".$usu."%' ORDER BY nombre ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getlistclientes($params){
        $columns = array( 
            0=>'tit.titularId',
            1=>'tit.nombre',
            2=>'tit.movil',
            3=>'(SELECT GROUP_CONCAT(nin.nombre SEPARATOR " / ") FROM ninos as nin WHERE nin.usuario_id=tit.titularId AND nin.Activo=1) as ninos',
        );
        $columnsss = array( 
            0=>'tit.titularId',
            1=>'tit.nombre',
            2=>'tit.movil',
            3=>'(SELECT GROUP_CONCAT(nin.nombre SEPARATOR " / ") FROM ninos as nin WHERE nin.usuario_id=tit.titularId AND nin.Activo=1)',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('titular tit');
        $this->db->where(array('tit.activo'=>1));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistclientes_total($params){
        $columns = array( 
            0=>'tit.titularId',
            1=>'tit.nombre',
            2=>'tit.movil',
            3=>'(SELECT GROUP_CONCAT(nin.nombre SEPARATOR " / ") FROM ninos as nin WHERE nin.usuario_id=tit.titularId AND nin.Activo=1)',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('titular tit');
        $this->db->where(array('tit.activo'=>1));
        
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        
        $query=$this->db->get();

        return $query->row()->total;
    }

}
?>