<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesas extends CI_Controller 
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->helper('url');
        session_regenerate_id();
        date_default_timezone_set('America/Mexico_City');
        $this->load->model('ModeloMesas','model');
        $this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
        $this->load->library('atlog');
        $this->idpersonal=$_SESSION['idpersonal'];
    }
    
    
    public function index()
    {
        $this->load->model('ModeloVentas');
        $data['productospadres']=$this->ModeloVentas->getproductopadre();
        $data['nummesas']=25;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/mesas',$data);
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesa');

    }

    // VIEWS 
    public function listadoProductosEliminados() 
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/listadoProductosEliminados');
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesalistado');
    }

    public function getListadoProductosEliminados()
    {
        $this->load->model('ModeloVentas');
        $productos= $this->ModeloVentas->getListadoProductosEliminados();
        $json_data = array("data" => $productos);
        echo json_encode($json_data);
    }

    // carga productos en display izquierdo ========================================================

    public function cargarproductos() 
    {
        $this->load->model('ModeloVentas');
        $id = $this->input->post('id');
        
        $respuesta=$this->ModeloVentas->getproductoshijos($id);
        foreach ($respuesta->result() as $item){ ?>
        <div class="col-md-3 galeriasimg flipInX animated classproductohijo cursor" data-idcate="<?php echo $item->productoid; ?>" style="background: url('<?php echo base_url(); ?>public/img/productos/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;" onclick="agregar_producto(<?php echo $item->productoid; ?>)" >
            <h5><?php echo $item->producto;?></h5>
        </div>
      <?php }
    }


    // ============= FUNCIONES DE AGREGAR MESA Y VALIDAR PIN ==========================================================

    public function getUsuarioPorPin() //regresar si es correcto, en caso de que si regresa la tabla de productos de la mesa
    {
        
        $id_mesa=$this->input->post("mesa");
        $pin=$this->input->post("pin");
        $pin_valido=false; 
         
        $total=0;
        $datosUsuario = $this->ModeloUsuarios->getUsuarioPorPin($pin);

        // Si existe el usuario 
        if (!empty($datosUsuario)) 
        {
            $pin_valido=true;
            $_SESSION['mesero']=$datosUsuario->personalId;
            // Llamamos a la función que crea la tabla de la mesa y los datos de venta de la misma, pasándole el número de la mesa indicada
            $data = $this->creaDatosMesa($id_mesa);

        }
        else
        {
            $data['tabla'] = '';
        }
        $data['pin_valido'] = $pin_valido;
            
        // Se crea el arreglo para enviar al LOG
        $informacion = array('pin' => $pin, 'mesa' => $id_mesa, 'data' => $data);

        // Se envía al LOG con el tipo 1
        $this->guardaLog(1, $informacion);
        
        // Responde al JS
        echo json_encode($data); 

    }

    // ============= FUNCIONES DE AGREGAR, ELIMINAR Y MODIFICAR PRODUCTOS ==========================================================


    public function agregar_producto(){

        $id_mesa=$this->input->post("mesa");
        $id_producto=$this->input->post("idProducto");

        $mesas=$_SESSION['mesas_detalle'];
        $result=""; 
        
        // Busca el producto en los registros de la BD para saber si existe ya o no
        $productosExistentes=$this->model->buscaProductoMesa($id_producto, $id_mesa);

        // Si devuelve NULL, quiere decir que el producto no se encuentra en la mesa, por lo tanto agregaremos el registro 
        if($productosExistentes==NULL)
        {
            // Obtiene los datos del producto a insertar
            $producto=$this->model->getProducto($id_producto);
            $data = array('idProducto' => $producto->productoid, 'cantidad' => 1, 'producto' => $producto->producto, 'precio' => $producto->precio_venta, 'idMesa' => $id_mesa, 'total' => $producto->precio_venta);
            $this->model->insertaProductoMesa($data);
        }
        else
        {
            // Obtiene los datos del producto a insertar
            $producto=$this->model->getProducto($id_producto);

            // Suma el precio y las cantidades a lo actual
            $cantidad = $productosExistentes->cantidad + 1;
            $total = $cantidad * $producto->precio_venta;

            // Actualizamos el registro
            $data = array('cantidad' => $cantidad, 'total' => $total);
            $this->model->actualizaProductoMesa($data, $id_producto, $id_mesa);
        }

        // Actualizamos el total de la mesa
        $this->calculaTotalmesa($id_mesa);

        // Llamamos a la función que crea la tabla de la mesa y los datos de venta de la misma, pasándole el número de la mesa indicada
        $data = $this->creaDatosMesa($id_mesa);

        // Se crea el arreglo para enviar al LOG
        $informacion = array('mesero' => $_SESSION["mesero"], 'mesa' => $id_mesa, 'idProducto' => $id_producto, 'data' => $data);

        // Se envía al LOG con el tipo 2
        $this->guardaLog(2, $informacion);

        // Responde al JS
        echo json_encode($data);  
        
    }

    //-----------------------------------------------------------------------------------------------------------------------------------

    public function eliminar_producto(){

        $id_mesa=$this->input->post("mesa");
        $id_producto=$this->input->post("idProducto");
        $pass_usuario=$this->input->post("pass_usuario");
        //$passlist = $this->model->pass_usuario($pass_usuario);
        $respuesta = $this->ModeloSession->accesoadmin($pass_usuario);
        //$pass_xx = 0;
        //foreach ($passlist as $item) {
        //    $pass_xx = $item->UsuarioID;
        //}
        if($respuesta == 1){
            // Eliminamos el registro en base al id del producto y de la mesa
            $this->model->eliminaProductoMesa($id_producto, $id_mesa);

            // Actualizamos el total de la mesa
            $this->calculaTotalmesa($id_mesa);

            // Llamamos a la función que crea la tabla de la mesa y los datos de venta de la misma, pasándole el número de la mesa indicada
            $data = $this->creaDatosMesa($id_mesa);

            // Guardamos el registro del Producto que fue eliminado
            $infoProducto = $this->model->getProducto($id_producto);
            $row = 'Producto Eliminado: '.$id_producto.'. Nombre del Producto: '.$infoProducto->producto;

            // Se crea el arreglo para enviar al LOG
            $informacion = array('mesero' => $_SESSION["mesero"], 'mesa' => $id_mesa, 'row' => $row, 'data' => $data);
            // Se envía al LOG con el tipo 3
            $this->guardaLog(3, $informacion);

            echo json_encode($data);
        }else{
            echo 0;
        }
  
    }
    public function eliminar_productos(){

        $id_mesa=$this->input->post("mesa");
        $pass_usuario=$this->input->post("pass_usuario");
        //$passlist = $this->model->pass_usuario($pass_usuario);
        log_message('error', '....'.$pass_usuario);
        $respuesta = $this->ModeloSession->accesoadmin($pass_usuario);
        //$pass_xx = 0;
        //foreach ($passlist as $item) {
        //    $pass_xx = $item->UsuarioID;
        //}
        if($respuesta == 1){
            // Eliminamos el registro en base al id del producto y de la mesa
            $this->model->eliminaProductosMesa($id_mesa);

            // Actualizamos el total de la mesa
            $this->calculaTotalmesa($id_mesa);

            // Llamamos a la función que crea la tabla de la mesa y los datos de venta de la misma, pasándole el número de la mesa indicada
            $data = $this->creaDatosMesa($id_mesa);

            // Guardamos el registro del Producto que fue eliminado
            //$infoProducto = $this->model->getProducto($id_producto);
            //$row = 'Producto Eliminado: '.$id_producto.'. Nombre del Producto: '.$infoProducto->producto;

            // Se crea el arreglo para enviar al LOG
            //$informacion = array('mesero' => $_SESSION["mesero"], 'mesa' => $id_mesa, 'row' => $row, 'data' => $data);
            // Se envía al LOG con el tipo 3
            //$this->guardaLog(3, $informacion);

            echo json_encode($data);
        }else{
            echo 0;
        }
  
    }


    //-----------------------------------------------------------------------------------------------------------------------------------

    public function modificar_producto(){

        $id_mesa=$this->input->post("mesa");
        $id_producto=$this->input->post("idProducto");
        $accion=$this->input->post("accion");
        $pass_usuario=$this->input->post("pass_usuario");
        $passlist = $this->model->pass_usuario($pass_usuario);
        $pass_xx = 0;
        foreach ($passlist as $item) {
            $pass_xx = $item->UsuarioID;
        }
        if($accion==0){
            if($pass_xx == 79 || $pass_xx==82 || $pass_xx==80 || $pass_xx==1){
                // Busca el producto en los registros de la BD 
                $productosExistentes=$this->model->buscaProductoMesa($id_producto, $id_mesa);
                $cantidadActual = $productosExistentes->cantidad;
                $precio = $productosExistentes->precio;

                // Con esta sentencia sabremos si es para agregar una unidad mas del producto o para eliminar
                if($accion)
                {   // si es agregar una unidad mas
                    $cantidadActual++;
                }
                else
                {   // si es eliminar una unidad
                    if($cantidadActual>1)
                    { //si solo hay un item no resta más
                        $cantidadActual--;
                    }
                }
                // Calculamos el total del mismo producto
                $total = $cantidadActual * $precio;

                // Actualizamos el registro
                $data = array('cantidad' => $cantidadActual, 'total' => $total);        
                $this->model->actualizaProductoMesa($data, $id_producto, $id_mesa);

                // Actualizamos el total de la mesa
                $this->calculaTotalmesa($id_mesa);

                // Llamamos a la función que crea la tabla de la mesa y los datos de venta de la misma, pasándole el número de la mesa indicada
                $data = $this->creaDatosMesa($id_mesa);

                // Se crea el arreglo para enviar al LOG
                // Definimos si se agregó o se eliminó la cantidad de un producto seleccionado
                if($accion){ 
                    $accionLog = 'Aumentó la cantidad del producto: '.$id_producto.', '.$productosExistentes->producto;
                }
                else{ 
                    $accionLog = 'Disminuyó la cantidad del producto: '.$id_producto.', '.$productosExistentes->producto;
                }
                $informacion = array('mesero' => $_SESSION["mesero"], 'mesa' => $id_mesa, 'accion' => $accionLog, 'data' => $data);
                // Se envía al LOG con el tipo 4
                $this->guardaLog(4, $informacion);

                echo json_encode($data);  
            }else{
                echo 0;
            }
        }else{
            $productosExistentes=$this->model->buscaProductoMesa($id_producto, $id_mesa);
            $cantidadActual = $productosExistentes->cantidad;
            $precio = $productosExistentes->precio;

            // Con esta sentencia sabremos si es para agregar una unidad mas del producto o para eliminar
            if($accion)
            {   // si es agregar una unidad mas
                $cantidadActual++;
            }
            else
            {   // si es eliminar una unidad
                if($cantidadActual>1)
                { //si solo hay un item no resta más
                    $cantidadActual--;
                }
            }
            // Calculamos el total del mismo producto
            $total = $cantidadActual * $precio;

            // Actualizamos el registro
            $data = array('cantidad' => $cantidadActual, 'total' => $total);        
            $this->model->actualizaProductoMesa($data, $id_producto, $id_mesa);

            // Actualizamos el total de la mesa
            $this->calculaTotalmesa($id_mesa);

            // Llamamos a la función que crea la tabla de la mesa y los datos de venta de la misma, pasándole el número de la mesa indicada
            $data = $this->creaDatosMesa($id_mesa);

            // Se crea el arreglo para enviar al LOG
            // Definimos si se agregó o se eliminó la cantidad de un producto seleccionado
            if($accion){ 
                $accionLog = 'Aumentó la cantidad del producto: '.$id_producto.', '.$productosExistentes->producto;
            }
            else{ 
                $accionLog = 'Disminuyó la cantidad del producto: '.$id_producto.', '.$productosExistentes->producto;
            }
            $informacion = array('mesero' => $_SESSION["mesero"], 'mesa' => $id_mesa, 'accion' => $accionLog, 'data' => $data);
            // Se envía al LOG con el tipo 4
            $this->guardaLog(4, $informacion);

            echo json_encode($data);  
        }
    }

    // FUNCIONES DE DESCUENTO Y VENTA ===================================================================================================================

    public function agregarDescuento(){
        $id_mesa=$this->input->post("mesa");
        $descuento=$this->input->post("descuento");
        $acceso=$this->input->post("contrasenaAcceso");
        
        // Actualizamos los datos en la mesa
        $data = array('descuento' => $descuento);
        $this->model->actualizaMesa($id_mesa, $data);

        // Actualizamos el total de la mesa
        $this->calculaTotalmesa($id_mesa);

        // Llamamos a la función que crea la tabla de la mesa y los datos de venta de la misma, pasándole el número de la mesa indicada
        $data = $this->creaDatosMesa($id_mesa);

        // Se crea el arreglo para enviar al LOG
        // Buscamos el nombre de quien autoriza el descuento
        $this->load->model('ModeloSession');
        $respuesta = $this->ModeloSession->getNombreUsuarioPorContrasena($acceso);
        // Se arma el Array para el LOG
        $informacion = array('mesero' => $_SESSION["mesero"], 'mesa' => $id_mesa, 'descuento' => $descuento, 'nombreAutoriza' => $respuesta);
        // Se envía al LOG con el tipo 5
        $this->guardaLog(5, $informacion);

        echo json_encode($data);
    }


    //-----------------------------------------------------------------------------------------------------------------------------------

    public function realizar_venta(){ // en venta revisar que si es tipo 0 guarde este en primer lugar y luego actualizar no insertar
        $id_mesa=$this->input->post("mesa");
        $metodo_pago=$this->input->post("metodo");
        $tipo_venta=$this->input->post("tipo");

        // Verificamos en los datos de la mesa
        $datosMesa = $this->model->getInformacionMesa($id_mesa);

        // Guardamos datos en la base de datos
        $venta=array(
            "personalId"=>$_SESSION['mesero'],
            "metodo"=>$metodo_pago,
            "monto_total"=>$datosMesa->total,
            "activo"=>1,
            "mesa"=>$id_mesa,
            "reg"=>date("Y-m-d H:i:s"),
            "descuento"=>$datosMesa->descuento,
            "monto_cobrado"=>$datosMesa->montoPorCobrar,
            "tipo"=>$tipo_venta,
            "personalId_abre"=>$_SESSION["mesero"],
        );

        $productosMesa =$this->model->getDetallesMesa($id_mesa);
        
        if($tipo_venta==0 && $datosMesa->idVenta==NULL)
        {
            // Guarda el registro
            $id_venta=$this->model->saveVenta($venta);

            foreach($productosMesa as $productoMesa)
            {
                $temp=array(
                    "ventaId"=>$id_venta,
                    "productoId"=>$productoMesa->idProducto,
                    "cantidad"=>$productoMesa->cantidad,
                    "precio"=>$productoMesa->precio
                );
                $this->model->saveProductos($temp);
                $this->ModeloVentas->descuentostock($productoMesa->idProducto,$productoMesa->cantidad);
            }
            // Actualizamos los datos en la mesa
            $data = array('idVenta' => $id_venta, 'tipo' => 0);
            $this->model->actualizaMesa($id_mesa, $data);
            echo $id_venta;
            
        }
        else if($tipo_venta==0 && $datosMesa->idVenta!=NULL)
        {
            $id_venta=$datosMesa->idVenta;
            $this->model->eliminaDetallesVenta($datosMesa->idVenta);
            //=============================================================================
                $verificarvm=$this->model->getselectwheren('ventasm',array('ventaId'=>$id_venta));
                if($verificarvm->num_rows()==0){
                    $venta['ventaId']=$id_venta;
                    $this->model->Insert('ventasm',$venta);
                }
            //=============================================================================

            foreach($productosMesa as $productoMesa)
            {
                $temp=array(
                "ventaId"=>$id_venta,
                "productoId"=>$productoMesa->idProducto,
                "cantidad"=>$productoMesa->cantidad,
                "precio"=>$productoMesa->precio
                );
                $this->model->saveProductos($temp);
                $this->ModeloVentas->descuentostock($productoMesa->idProducto,$productoMesa->cantidad);
            }

            // Quitamos los datos que no sean necesario para la actualización de la mesa
            unset($venta["personalId_abre"]);
            unset($venta["tipo"]);
            //unset($venta["reg"]);
            $this->model->actualizaVenta($datosMesa->idVenta,$venta);
            $id_venta = $datosMesa->idVenta;
            echo $id_venta;
        }
        else if($tipo_venta==1 && $datosMesa->idVenta!=NULL)
        {   
            $id_venta=$datosMesa->idVenta;
            // Eliminamos el detalle de productos e insertamos los nuevos
            $this->model->eliminaDetallesVenta($datosMesa->idVenta);

            //=============================================================================
                $verificarvm=$this->model->getselectwheren('ventasm',array('ventaId'=>$id_venta));
                if($verificarvm->num_rows()==0){
                    $venta['ventaId']=$id_venta;
                    $this->model->Insert('ventasm',$venta);
                }
            //=============================================================================
                
            foreach($productosMesa as $productoMesa)
            {
                $temp=array(
                    "ventaId"=>$datosMesa->idVenta,
                    "productoId"=>$productoMesa->idProducto,
                    "cantidad"=>$productoMesa->cantidad,
                    "precio"=>$productoMesa->precio
                );
                $this->model->saveProductos($temp);
                $this->ModeloVentas->descuentostock($productoMesa->idProducto,$productoMesa->cantidad);
            }
    
            // Quitamos los datos que no sean necesario para la actualización de la mesa y cambiamos el tipo
            unset($venta["personalId_abre"]);
            //unset($venta["reg"]);
            $venta['tipo'] = 1;
            $this->model->actualizaVenta($datosMesa->idVenta,$venta);
                
            // Eliminamos de la tabla de mesas, los detalles
            foreach($productosMesa as $productoMesa)
            {
                $this->model->eliminaProductoMesa($productoMesa->idProducto, $id_mesa);
            }
            // Actualizamos la mesa para dejarla libre
            $mesaLibre = array(
                    "personalId"=>null,
                    "tipo"=>null,
                    "total"=>null,
                    "descuento"=>null,
                    "montoPorCobrar"=>null,
                    "activo"=>0,
                    "idVenta"=>null
                );
            $this->model->actualizaMesa($id_mesa, $mesaLibre);
            echo $datosMesa->idVenta;
        }
        // Mesa de venta rápida
        else if($tipo_venta==1 && $datosMesa->idVenta==NULL)
        {
            $id_venta=$datosMesa->idVenta;
            unset($venta["personalId_abre"]);
            //unset($venta["reg"]);
            $venta['tipo'] = 1;

            // Guarda el registro
            $id_venta=$this->model->saveVenta($venta);

            foreach($productosMesa as $productoMesa)
            {
                $temp=array(
                    "ventaId"=>$id_venta,
                    "productoId"=>$productoMesa->idProducto,
                    "cantidad"=>$productoMesa->cantidad,
                    "precio"=>$productoMesa->precio
                );
                $this->model->saveProductos($temp);
                $this->ModeloVentas->descuentostock($productoMesa->idProducto,$productoMesa->cantidad);
            }

            // Eliminamos de la tabla de mesas, los detalles
            foreach($productosMesa as $productoMesa)
            {
                $this->model->eliminaProductoMesa($productoMesa->idProducto, $id_mesa);
            }
            // Actualizamos la mesa para dejarla libre
            $mesaLibre = array(
                    "personalId"=>null,
                    "tipo"=>null,
                    "total"=>null,
                    "descuento"=>null,
                    "montoPorCobrar"=>null,
                    "activo"=>0,
                    "idVenta"=>null
                );
            $this->model->actualizaMesa($id_mesa, $mesaLibre);
            echo $id_venta;
        }

        // Se crea el arreglo para enviar al LOG
       // $informacion = array('mesero' => $_SESSION["mesero"], 'mesa' => $id_mesa, 'metodo_pago' => $metodo_pago, 'tipo_venta' => $tipo_venta, 'datosVenta' => $venta, 'productos' => $productosMesa, 'idVenta' => $id_venta);
        // Se envía al LOG con el tipo 6
        //$this->guardaLog(6, $informacion);

        //echo $id_venta;

    }

    // FUNCIONES DE APOYO =======================================================================================================================


    private function getDescuento($id_mesa){
        $mesas=$_SESSION['mesas'];
        $descuento=0;
        foreach ($mesas as $mesa) {
            if($mesa["no_mesa"]==$id_mesa){
                $descuento=$mesa["descuento"];
            }
        }
        return $descuento;
    }

    private function getTotalMesa($id_mesa){
        $mesas=$_SESSION['mesas_detalle'];
        $total=0;
        foreach ($mesas as $mesa) {
            if($mesa["mesa"]==$id_mesa){
                $total+=$mesa["precio"]*$mesa["cantidad"];
            }
        }
        return $total;
    }

    public function getDatosMesa($id_mesa){
        $mesas=$_SESSION['mesas']; //print_r($mesas);
        $result=array();
        foreach ($mesas as $m) {
            if($m["no_mesa"]==$id_mesa){
                $result=$m;
            }
        }
        return $result;
    }

    public function delete_session_mesa($id_mesa){
        $mesas=$_SESSION['mesas']; //print_r($mesas); die;

        $row=0;

        foreach ($mesas as $index=>$mesa) {
            if($mesa["no_mesa"]==$id_mesa){
                $row=$index;
            }
        }

        unset($mesas[$row]);
        $_SESSION['mesas']=$mesas;
    }

    public function delete_session_prod($id_mesa){
        $mesas=$_SESSION['mesas_detalle'];
        $arreglo=array(); 
        foreach ($mesas as $index=>$mesa) {
            if($mesa["mesa"]==$id_mesa){
                array_push($arreglo, $index);
            }
        }
         
        foreach ($arreglo as $a) {
            unset($mesas[$a]);
        } 
        $_SESSION['mesas_detalle']=$mesas;
    }

    public function calculaTotalmesa($id_mesa)
    {
        // Para esto, buscamos todos los registros que tenga esa mesa, 
        // y a través de su total, lo sumaremos y actualizamos el registro principal de la mesa
        $productosRegistrados = $this->model->getDetallesMesa($id_mesa);
        $totalMesa = 0;
        foreach ($productosRegistrados as $productoRegistrado) 
        {
            $totalMesa = $totalMesa + $productoRegistrado->total;
        }

        // Luego calculamos el monto a Cobrar, restando al total de la mesa el descuento (si es que existe)
        // Para esto, verificamos en los datos de la mesa
        $datosMesa = $this->model->getInformacionMesa($id_mesa);
        $montoPorCobrar = null;
        
        if($datosMesa->descuento!='' && $datosMesa->descuento!=null && $datosMesa->descuento!=0)
        {
            $montoPorCobrar = $totalMesa - $datosMesa->descuento;
        }
        else
        {
            $montoPorCobrar = $totalMesa;
        }   

        // Actualizamos los datos en la mesa
        $data = array('total' => $totalMesa, 'montoPorCobrar' => $montoPorCobrar);
        
        $this->model->actualizaMesa($id_mesa, $data);
    }


    public function rowString($id_producto,$cantidad,$producto,$precio){

        return "<tr>"
                . "<td>$cantidad</td>"
                . "<td>$producto</td>"
                . "<td>".number_format($precio,2)."</td>"
                . "<td>".number_format($precio*$cantidad
                    ,2)."</td>"
                . "<td>"
                    ."<button type='button' onclick='eliminar_producto_modal($id_producto)' class='btn btn-sm mb-0 btn-outline-danger'><i class='ft-trash'></i></button> "
                    ."<button type='button' onclick='modificar_producto2($id_producto,1)' class='btn btn-sm mb-0 btn-success btn-flat'><i class='ft-plus'></i></button>"
                    ."<button type='button' onclick='modificar_producto_modal($id_producto,0)' class='btn btn-sm mb-0 btn-info btn-flat'><i class='ft-minus'></i></button>"
                ."</td>"
            . "</tr>";
    }

    public function creaDatosMesa($idMesa)
    {
        $informacionMesa=$this->model->getInformacionMesa($idMesa);

        // Si la mesa está en estatus activo=0, indicará que la mesa es libre y la puede tomar cualquier mesero
        if($informacionMesa->activo==0)
        {
            // Actualizamos la mesa para guardar el ID del personal que abre la mesa y cambiarla a estatus activo=1
            // Actualizamos los datos en la mesa
            $dataMesa = array('personalId' => $_SESSION['mesero'], 'activo' => 1);
            $this->model->actualizaMesa($idMesa, $dataMesa);
        }

        $productos=$this->model->getDetallesMesa($idMesa);
        $data = array();
        $result= '';

        if(!empty($productos))
        {
            foreach($productos as &$prod){
                $result.=$this->rowString($prod->idProducto,$prod->cantidad, $prod->producto, $prod->precio);
            }
            $data["tipo"]=$informacionMesa->tipo;
            $data["descuento"]=$informacionMesa->descuento;
            $data["tabla"]=$result;
            $data["total"]=$informacionMesa->total;
            $data["montoPorCobrar"]=$informacionMesa->montoPorCobrar;
        }   
        else
        {
            $data["tabla"]='';
            $data["tipo"]=null;
            $data["total"]=0;
            $data["montoPorCobrar"]=0;

        }
        return $data;
    }   

    public function guardaLog($tipoLog, $informacion)
    {
        
        switch ($tipoLog)
        {   
            // Si es tipo 1, es para cuando se accede a la función getUsuarioPorPin()
            case 1:
                // Buscamos el nombre del personal a quien corresponde el pin que ejecuta la acción
                $datos = $this->ModeloUsuarios->getUsuarioPorPin($informacion['pin']);
                
                if(!is_null($datos)){
                    $nombreEmpleado = $datos->nombre;
                }else{
                    $nombreEmpleado = 'No existe';
                }
                
                // Inicializamos la variable que irá al LOG
                $stringLog="--------------------- Función getUsuarioPorPin() ---------------------\n\n";
                // ESCRIBE EN EL LOG 
                $stringLog.="Pin de usuario ingresado: ".$informacion['pin'].". Nombre: ".$nombreEmpleado.". \n";
                $stringLog.="Mesa seleccionada: ".$informacion['mesa'].". \n";
                $stringLog.="Respuesta de búsqueda de empleado: ".$informacion['data']['pin_valido'].". \n";
                $stringLog.="Tabla: ".$informacion['data']['tabla'].". \n";
                if(isset($informacion['total'])) { $stringLog.="Total: ".$informacion['data']['total'].". \n"; }
                if(isset($informacion['descuento'])) { $stringLog.="Descuento: ".$informacion['data']['descuento'].". \n"; }
                if(isset($informacion['tipo'])) { $stringLog.="Total: ".$informacion['data']['tipo'].". \n\n"; }

                // Guardamos a través de la librería $informacion['pin']
                $this->atlog->savelog($stringLog);
                break;
            // Si es tipo 2, es para cuando se accede a la función agregar_producto()
            case 2:
                // Buscamos el nombre del personal a quien corresponde el pin que ejecuta la acción
                $datos = $this->ModeloUsuarios->getUsuarioPorPersonalId($informacion['mesero']);
                // Inicializamos la variable que irá al LOG
                $stringLog="--------------------- Función agregar_producto() ---------------------\n\n";
                
                // ESCRIBE EN EL LOG 
                $stringLog.="Persona que ingresa: ".$informacion['mesero'].". Nombre: ".$datos->nombre.". \n";
                $stringLog.="Mesa seleccionada: ".$informacion['mesa'].". \n";
                $stringLog.="Id de Producto seleccionado: ".$informacion['idProducto'].". \n";
                $stringLog.="Tabla: ".$informacion['data']['tabla'].". \n";
                if(isset($informacion['total'])) { $stringLog.="Total: ".$informacion['data']['total'].". \n"; }
                if(isset($informacion['descuento'])) { $stringLog.="Descuento: ".$informacion['data']['descuento'].". \n"; }
                if(isset($informacion['tipo'])) { $stringLog.="Total: ".$informacion['data']['tipo'].". \n\n"; }

                // Guardamos a través de la librería 
                $this->atlog->savelog($stringLog);
                break;
            // Si es tipo 3, es para cuando se accede a la función eliminar_producto()
            case 3:
                // Buscamos el nombre del personal a quien corresponde el pin que ejecuta la acción
                $datos = $this->ModeloUsuarios->getUsuarioPorPersonalId($informacion['mesero']);
                // Inicializamos la variable que irá al LOG
                $stringLog="--------------------- Función eliminar_producto() ---------------------\n\n";
                
                // ESCRIBE EN EL LOG 
                $stringLog.="Persona que ingresa: ".$informacion['mesero'].". Nombre: ".$datos->nombre.". \n";
                $stringLog.="Mesa seleccionada: ".$informacion['mesa'].". \n";
                $stringLog.="Registro eliminado: ".$informacion['row'].". \n";
                $stringLog.="Tabla: ".$informacion['data']['tabla'].". \n";
                if(isset($informacion['total'])) { $stringLog.="Total: ".$informacion['data']['total'].". \n"; }
                if(isset($informacion['descuento'])) { $stringLog.="Descuento: ".$informacion['data']['descuento'].". \n"; }
                if(isset($informacion['tipo'])) { $stringLog.="Total: ".$informacion['data']['tipo'].". \n\n"; }

                // Guardamos a través de la librería 
                $this->atlog->savelog($stringLog);
                break;    
            // Si es tipo 4, es para cuando se accede a la función modificar_producto()
            case 4:
                // Buscamos el nombre del personal a quien corresponde el pin que ejecuta la acción
                $datos = $this->ModeloUsuarios->getUsuarioPorPersonalId($informacion['mesero']);
                // Inicializamos la variable que irá al LOG
                $stringLog="--------------------- Función modificar_producto() ---------------------\n\n";
                
                // ESCRIBE EN EL LOG 
                $stringLog.="Persona que ingresa: ".$informacion['mesero'].". Nombre: ".$datos->nombre.". \n";
                $stringLog.="Mesa seleccionada: ".$informacion['mesa'].". \n";
                $stringLog.="Acción realizada: ".$informacion['accion'].". \n";
                $stringLog.="Tabla: ".$informacion['data']['tabla'].". \n";
                if(isset($informacion['total'])) { $stringLog.="Total: ".$informacion['data']['total'].". \n"; }
                if(isset($informacion['descuento'])) { $stringLog.="Descuento: ".$informacion['data']['descuento'].". \n"; }
                if(isset($informacion['tipo'])) { $stringLog.="Total: ".$informacion['data']['tipo'].". \n\n"; }

                // Guardamos a través de la librería 
                $this->atlog->savelog($stringLog);
                break;
            // Si es tipo 5, es para cuando se accede a la función agregarDescuento()
            case 5:
                // Buscamos el nombre del personal a quien corresponde el pin que ejecuta la acción
                $datos = $this->ModeloUsuarios->getUsuarioPorPersonalId($informacion['mesero']);
                // Inicializamos la variable que irá al LOG
                $stringLog="--------------------- Función agregarDescuento() ---------------------\n\n";
                
                // ESCRIBE EN EL LOG 
                $stringLog.="Persona que ingresa: ".$informacion['mesero'].". Nombre: ".$datos->nombre.". \n";
                $stringLog.="Mesa seleccionada: ".$informacion['mesa'].". \n";
                $stringLog.="Descuento insertado: ".$informacion['descuento'].". \n";
                $stringLog.="Nombre de quien autoriza descuento: ".$informacion['nombreAutoriza'].". \n";

                // Guardamos a través de la librería 
                $this->atlog->savelog($stringLog);
                break;
            // Si es tipo 6, es para cuando se accede a la función realizar_venta()
            case 6:
                // Buscamos el nombre del personal a quien corresponde el pin que ejecuta la acción
                $datos = $this->ModeloUsuarios->getUsuarioPorPersonalId($informacion['mesero']);
                // Inicializamos la variable que irá al LOG
                $stringLog="--------------------- Función realizar_venta() ---------------------\n\n";
                
                // ESCRIBE EN EL LOG 
                $stringLog.="Persona que ingresa: ".$informacion['mesero'].". Nombre: ".$datos->nombre.". \n";
                $stringLog.="Mesa seleccionada: ".$informacion['mesa'].". \n";
                $stringLog.="Método de pago: ".$informacion['metodo_pago'].". \n";
                $stringLog.="Tipo de venta: ".$informacion['tipo_venta'].". \n";
                $stringLog.="Id de venta: ".$informacion['idVenta'].". \n";
                $stringLog.="Datos de la venta: ".json_encode($informacion['datosVenta']).". \n";
                $stringLog.="Productos de la venta: ".json_encode($informacion['productos']).". \n\n";

                // Guardamos a través de la librería 
                $this->atlog->savelog($stringLog);
                break;    
        }
        

    }
    public function credito()
    {
        $data['productospadres']=$this->ModeloVentas->getproductopadre();
        $data['empleados']=$this->model->getEmpledos(); 
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/credito',$data);
        $this->load->view('templates/footer');
        $this->load->view('mesas/jscredito');
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }
    function cargarproductos_credito(){
        $id = $this->input->post('id');
        $respuesta=$this->ModeloVentas->getproductoshijos($id);
        foreach ($respuesta->result() as $item){ ?>
        <div class="col-md-3 galeriasimg flipInX animated classproductohijo" data-idcate="<?php echo $item->productoid; ?>" style="background: url('<?php echo base_url(); ?>public/img/productos/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;" onclick="productoselected(<?php echo $item->productoid; ?>)" >
            <h2><?php echo $item->producto;?></h2>
        </div>
      <?php }
    }
    function agregarproductos(){
        $id = $this->input->post('id');
        $des=$this->input->post('des');
        //log_message('error', '....'.$this->input->post('des'));
        if ($des=='') {
            $desc=1;
        }else{
            $desc=0;
        }
        
        $cant=1;
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','productoid',$id);
        foreach ($resultado->result() as $item) {
            if ($desc==0) {
                $precioventa=0;
            }else{
                $precioventa=$item->precio_venta;
            }
           
            $oProducto = array('productoid' =>$item->productoid,
                'codigo' =>$item->codigo,
                'producto' =>$item->producto,
                'descripcion' =>$item->descripcion,
                'precio_venta' =>$precioventa,
                'img' =>$item->img
            );
        }
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
        }
        //==========================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
            $precio=$fila['precio_venta'];
            $cantotal=$Cantidad*$precio;
            ?>
                <tr class="producto_<?php echo $count;?>">
                    <td>
                        <input 
                            type="number" 
                            name="vscanti" 
                            id="vscanti" 
                            value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                        <input type="hidden" id="idproductos" value="<?php echo $fila['productoid'];?>">
                    </td>
                    <td style="background: url('<?php echo base_url(); ?>public/img/productost/<?php echo $fila['img'];?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;"></td>
                    <td><?php echo $fila['producto'];?></td>
                    <td>$ <input 
                                type="text" 
                                name="vsprecio" 
                                id="vsprecio" 
                                value="<?php echo $precio;?>" 
                                class="editar"
                                readonly 
                                style="background: transparent;border: 0px;width: 100px;"
                                >
                    </td>
                    <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>
                    <td>
                        <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>)">
                            <i class="ft-trash font-medium-3"></i>
                        </a>
                    </td>
                </tr>
            <?php
            $count++;
        }
    }
    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
    }
    function addventas(){
        $personalId=$this->input->post('personalId');
        $productos=$this->input->post('productos');
        $data = array('personalId' => $this->idpersonal, 'personalIdc' => $personalId);
        $idcredito=$this->ModeloCatalogos->insertToCatalogo($data,'creditos');
        $DATA = json_decode($productos);
        for ($i = 0; $i < count($DATA); $i++) {
            $idproductos=$DATA[$i]->idproductos;
            $cantidad=$DATA[$i]->cantidad;
            $precio=$DATA[$i]->precio;
            $this->model->creditos_detalle($idcredito,$idproductos,$cantidad,$precio);
            $this->model->updateproducto($idproductos,$cantidad);
        }
        $this->productoclear();
        echo $idcredito;
    }
    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }
    function viewproducto(){
        $codigo =$this->input->post('codigo');
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','codigo',$codigo);
        $idc=0;
        foreach ($resultado->result() as $item) {
            $idc=$item->productoid;
        }
        echo $idc;
    }
}