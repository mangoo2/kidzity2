<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }
    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Reportes/corte');
        $this->load->view('templates/footer');
        $this->load->view('Reportes/cortejs');
    }
    function genera(){
        $datos = $this->input->post();
        $fechainicio = $datos['fechainicio'];
        $fechafin = $datos['fechafin'];
        $filtro = $datos['filtro']; //0 ninguno 1 productos 2 ninos
        /*
            1 cafeteria
            2 recepcion
            3 saldos
        */$tablas='';
        switch ($datos['tipo']) {
            case '1':
                $respuestam=$this->ModeloCatalogos->cortecafem($fechainicio,$fechafin);
                $respuestaprode=$this->ModeloCatalogos->cortetotalcanproductosm($fechainicio,$fechafin);

                $totalp=0;
                $totalpd=0;
                $totalppdd=0;
                
                $tablas.='<table class="table table-sm table-hover table-responsive" id="tabla2">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Fecha</th>
                                                <th>Vendedor</th>
                                                <th>Mesa</th>';
                                                if ($filtro==1) {
                                                    $tablas.='<th>Productos</th>';
                                                }
                                                $tablas.='<th>Metodo</th>
                                                <th>Neto</th>
                                                <th>Descuento</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                            foreach ($respuestam->result() as $item){
                                                $tablas.='<tr>
                                                    <td>'.$item->ventaId.'</td>
                                                    <td>'.$item->reg.'</td>
                                                    <td>'.$item->nombre.'</td>
                                                    <td>'.$item->mesa.'</td>';
                                                    if ($filtro==1) { 
                                                        $detallepro=$this->ModeloCatalogos->getventadetalleMesas($item->ventaId);
                                                        $tablas.='<td>';
                                                            foreach ($detallepro->result() as $items) {
                                                                    $tablas.='<p>'.$items->cantidad.' '.$items->producto.'<p>';
                                                                }
                                                        $tablas.='</td>';
                                                    }
                                                    switch ($item->metodo) {
                                                            case '1':
                                                                $metodo2 ='Efectivo';
                                                                break;
                                                            case '2':
                                                                $metodo2="Tarjeta";
                                                                break;
                                                            default:
                                                                $metodo2='';
                                                                break;
                                                        }
                                                    $tablas.='<td>'.$metodo2.'</td>
                                                    <td>'.$item->monto_total.'</td>
                                                    <td>'.$item->descuento.'</td>
                                                    <td>'.$item->monto_cobrado.'</td>
                                                </tr>';
                                                $totalpd=$totalpd+$item->descuento;
                                                $totalp=$totalp+$item->monto_total;
                                            }
                                            $totalppdd=$totalp-$totalpd;

                                        $tablas.='</tbody>
                        </table>';
                        $tablas.='<div class="col-md-12">
                            <div class="col-md-3"><b>SUBTOTAL</b></div>
                            <div class="col-md-3"><b>$ '.number_format($totalp,2,'.',',').'</b></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3"><b>DESCUENTO</b></div>
                            <div class="col-md-3"><b>$ '.number_format($totalpd,2,'.',',').'</b></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3"><b>TOTAL</b></div>
                            <div class="col-md-3"><b>$ '.number_format($totalppdd,2,'.',',').'</b></div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>

                        ';



                        $tablas.='
                                    <table class="table table-sm table-hover table-responsive" id="tabla">
                                        <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Producto</th>
                                                <th>Precio</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                        foreach ($respuestaprode->result() as $item){
                            $tablas.='
                                    <tr>
                                        <td>'.$item->cantidad.'</td>
                                        <td>'.$item->producto.'</td>
                                        <td>'.$item->precio.'</td>
                                    </tr>
                                    ';

                        }
                        $tablas.='</tbody>
                                </table>';
                
                echo $tablas;
                break;
            case '2':
                $respuesta=$this->ModeloCatalogos->cortecompratiempo($fechainicio,$fechafin);
                $respuestatabladevolucion=$this->ModeloCatalogos->cortecompratiempodevolucion($fechainicio,$fechafin);
                $efectivo=0;
                $tarjeta=0;
                $descuentoe=0;
                $descuentot=0;
                $totalextras=0;
                $devoluciontotal=0;
                $tablas.='
                        <table class="table table-sm table-hover table-responsive" id="tabla3">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Folio</th>
                                    <th>Niño</th>
                                    <th>Titular</th>
                                    <th>Tiempo</th>
                                    <th>Cobrado</th>
                                    <th>Descuento</th>
                                    <th>Devolución</th>
                                    <th></th>
                                    <th>Motivo de Devolución</th>
                                    <th>Metodo</th>
                                    <th>Referencia</th>
                                    <th>Tiempo Extra</th>
                                    <th>Cobrado Extra</th>
                                    <th>Vendedor</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>';
                            $ninosvisita=0;
                            foreach ($respuesta->result() as $item){
                                $ninosvisita++;
                                $devoluciontotal=$devoluciontotal+$item->devolucion_monto;
                                if ($item->pagotiempoextra==null) {
                                        $pagotiempoextra=0;
                                    }else{
                                        $pagotiempoextra=$item->pagotiempoextra;
                                    }
                                if ($item->metodo==1) {
                                    
                                    $efectivo=$efectivo+$item->pagado;
                                    $descuentoe=$descuentoe+$item->descuento;
                                }
                                if ($item->metodo==2) {
                                    $tarjeta=$tarjeta+$item->pagado;
                                    $descuentot=$descuentot+$item->descuento;
                                }
                                $totalextras=$totalextras+$item->pagotiempoextra;
                                if ($item->tlbebe==1) {
                                    $tlbebe=' (bb) ';
                                }else{
                                    $tlbebe='';
                                }
                                if ($item->devolucion_monto>0) {
                                    $bttondevolucion='<button type="button" class="btn btn-raised btn-icon btn-danger mr-1 bttondevolucion" data-motivo="'.$item->devolucion_motivo.'"><i class="fa fa-eye"></i></button>';
                                }else{
                                    $bttondevolucion='';

                                }
                                     $tablas.='<tr>
                                                    <td>'.$item->compranId.'</td>
                                                    <td>'.$item->compraId.'</td>
                                                    <td>'.$item->nino.'</td>
                                                    <td>'.$item->titular.'</td>
                                                    <td>'.$item->tiempo.' '.$tlbebe.' </td>
                                                    <td>'.$item->pagado.'</td>
                                                    <td>'.$item->descuento.'</td>
                                                    <td>'.$item->devolucion_monto.'</td>
                                                    <td>'.$bttondevolucion.'</td>
                                                    <td>'.$item->devolucion_motivo.'</td>
                                                    ';
                                               
                                                switch ($item->metodo) {
                                                            case '1':
                                                                $metodo3='Efectivo';
                                                                break;
                                                            case '2':
                                                                $metodo3="Tarjeta";
                                                                break;
                                                            default:
                                                                $metodo3='';
                                                                break;
                                                        }
                                            $tablas.='
                                                    <td>'.$metodo3.'</td>';
                                                    if ($item->referencia!='') {
                                                        $referencia=$item->referencia.' / APP';
                                                    }else{
                                                        $referencia='';
                                                    }
                                                    if ($item->tiempoextra==0 and $item->pagotiempoextra>0) {
                                                        $tiempoextra=3-$item->tiempo;
                                                    }else{
                                                        $tiempoextra=$item->tiempoextra;
                                                    }
                                          $tablas.='<td>'.$referencia.'</td>
                                                    <td>'.$tiempoextra.'</td>
                                                    <td>'.$item->pagotiempoextra.'</td>
                                                    <td>
                                                    <p style="color:green">'.$item->nombre.'</p>
                                                    <p style="color:red">'.$item->personalsalida.'</p>
                                                    </td>
                                                    <td>'.$item->reg_abierto.'</td>
                                                </tr>';
                            }
                                        $tablas.='</tbody>
                                                  <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td colspan="2"></td>
                                                        
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        
                                                        
                                                    </tr>
                                                  </tfoot>
                                        
                        </table>';
                        $totaltotal=$efectivo-$descuentoe+$tarjeta-$descuentot+$totalextras-$devoluciontotal;
                       $tablas.='<div class="col-md-12">
                                    <div class="col-md-3">Poblacion</div>
                                    <div class="col-md-3">'.number_format($ninosvisita,0,'.',',').'</div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-3">SUBTOTAL EFECTIVO</div>
                                    <div class="col-md-3">$ '.number_format($efectivo,2,'.',',').'</div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-3">Descuento EFECTIVO</div>
                                    <div class="col-md-3">$ '.number_format($descuentoe,2,'.',',').'</div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-3">SUBTOTAL TARJETA</div>
                                    <div class="col-md-3">$ '.number_format($tarjeta,2,'.',',').'</div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-3">Descuento TARJETA</div>
                                    <div class="col-md-3">$ '.number_format($descuentot,2,'.',',').'</div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-3">Extras</div>
                                    <div class="col-md-3">$ '.number_format($totalextras,2,'.',',').'</div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-3">Devolución</div>
                                    <div class="col-md-3">$ '.number_format($devoluciontotal,2,'.',',').'</div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="col-md-3"><b>TOTAL</b></div>
                                    <div class="col-md-3"><b>$ '.number_format($totaltotal,2,'.',',').'</b></div>
                                  </div>


                                  ';
                                  $respuestatabladevolucionrow=0;
                                  foreach ($respuestatabladevolucion->result() as $item){
                                    $respuestatabladevolucionrow=1;
                                  }
                                  if ($respuestatabladevolucionrow==1) {

                                      $tablas.='<div class="col-md-12" style="text-align: center;">
                                                    <hr>
                                                    <br><br><br>
                                                    <h4><b>Devoluciones</b></h4>
                                                </div>
                                                <table class="table table-sm table-hover table-responsive" id="tabla">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Folio</th>
                                                            <th>Niño</th>
                                                            <th>Titular</th>
                                                            <th>Devolución</th>
                                                            <th>Vendedor</th>
                                                            <th>Fecha</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>';
                                        foreach ($respuestatabladevolucion->result() as $item){
                                            $respuestatabladevolucionrow=1;
                                
                                     $tablas.='<tr>
                                                    <td>'.$item->compranId.'</td>
                                                    <td>'.$item->compraId.'</td>
                                                    <td>'.$item->nino.'</td>
                                                    <td>'.$item->titular.'</td>
                                                    <td>'.$item->devolucion_monto.'</td>
                                                    <td>
                                                        <p style="color:green">'.$item->nombre.'</p>
                                                        <p style="color:red">'.$item->personalsalida.'</p>
                                                    </td>
                                                    <td>'.$item->reg_abierto.'</td>
                                                </tr>';
                            
                                        }
                                  }








                        echo $tablas;
                break;
            case '3':
                $titu=0;
                $respuestasaldos=$this->ModeloCatalogos->saldos($titu);
                    ?>
                        <table class="table table-sm table-hover table-responsive" id="tabla">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Titular</th>
                                                <th>Cantidad</th>
                                                <th>Referencia</th>
                                                <th>Fecha abono</th>
                                                <th>Fecha vencimiento</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($respuestasaldos->result() as $item){ ?>
                                                <tr>
                                                    <td><?php echo $item->saldoId; ?></td>
                                                    <td><?php echo $item->nombre ; ?></td>
                                                    <td><?php echo $item->cantidad; ?></td>
                                                    <td><?php echo $item->referencia; ?></td>
                                                    <td><?php echo $item->inicio; ?></td>
                                                    <td><?php echo $item->vence; ?></td>
                                                </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                    <?php
                break;
            case '4':
                # # nombre de fiesta, titular, numero de niños, numeros de niños extras, fecha de inicio, hora de inicio, hora de termino, costo subtotal de fiesta, costo subtotal de niños extra, total de totales

                $respuesta=$this->ModeloCatalogos->cortefiestasactivos($fechainicio,$fechafin);
                $tablas.='
                        <table class="table table-sm table-hover table-responsive" id="tabla">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Fiesta</th>
                                                <th>Titular</th>
                                                <th>Numero de niños</th>
                                                <th>Numero de niños extras</th>
                                                <th>Fecha Inicio</th>
                                                <th>Hora Inicio</th>
                                                <th>Hora Fin</th>
                                                <th>Costo subtotal</th>
                                                <th>Costo subtotal niño extras</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        foreach ($respuesta->result() as $item){
                                     $tablas.='<tr>
                                                    <td>'.$item->fiestaId.'</td>
                                                    <td>'.$item->fiesta.'</td>';
                                                    $titularl=$this->ModeloCatalogos->fiestatitular($item->fiestaId);
                                                    //$ninostotal=$this->ModeloCatalogos->fiestaninostotal($item->fiestaId);
                                                    $ninostotal=$this->ModeloCatalogos->fiestaninostotalcosto($item->fiestaId);
                                          $tablas.='<td>'.$titularl.'</td>';
                                          $ninosextras=$item->cantidadm-$item->cantidadt;
                                          if ($ninosextras<0) {
                                              $ninosextras=0;
                                          }
                                          $totall=$item->precio_total+$ninostotal;
                                          $tablas.='<td>'.$item->cantidadm.'</td>
                                                    <td>'.$ninosextras.'</td>
                                                    <td>'.$item->fecha_inicio.'</td>
                                                    <td>'.$item->hora_inicio.'</td>
                                                    <td>'.$item->hora_fin.'</td>
                                                    <td>$ '.number_format($item->precio_total,0,'.',',').'</td>
                                                    <td>$ '.number_format($ninostotal,0,'.',',').'</td>
                                                    <td>$ '.number_format($totall,0,'.',',').'</td>
                                                </tr>';
                                        }
                                        $tablas.='</tbody>
                        </table>';

                        echo $tablas;

                break;
            case '5':

                $respuesta=$this->ModeloCatalogos->cortecompras($fechainicio,$fechafin);
                $tablas.='
                        <table class="table table-sm table-hover table-responsive" id="tabla">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>monto</th>';
                                                if ($filtro==1) {
                                                $tablas.='<th>Productos</th>';
                                                }
                                                $tablas.='<th>Usuario</th>
                                                <th>Fecha</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        foreach ($respuesta->result() as $item){
                                     $tablas.='<tr>
                                                    <td>'.$item->compraId.'</td>
                                                    <td>$ '.number_format($item->monto_total,0,'.',',').'</td>';
                                                    if ($filtro==1) {
                                                       $tablas.='<td>'; 
                                                        $resultadod=$this->ModeloCatalogos->detallescompras($item->compraId);
                                                        foreach ($resultadod->result() as $itemp){ 
                                                            $tablas.='<p>'.$itemp->producto.'</p>';
                                                        }
                                                         $tablas.='</td>'; 
                                                }
                                                    $tablas.='<td>'.$item->usuario.'</td>
                                                    <td>'.$item->reg.'</td>
                                                </tr>';
                                        }
                                        $tablas.='</tbody>
                        </table>';

                        echo $tablas;

                break;
            case '6':
                $respuesta=$this->ModeloCatalogos->cortecafe($fechainicio,$fechafin);
                $respuestaprode=$this->ModeloCatalogos->cortetotalcanproductos($fechainicio,$fechafin);
                $totalr=0;
                $rowproductos=0;
                $tablas.='
                        <table class="table table-sm table-hover table-responsive" id="tabla">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Fecha</th>
                                                <th>Vendedor</th>';
                                                if ($filtro==1) {
                                                $tablas.='<th>Productos</th>';
                                                $tablas.='<th>Costo de producto</th>';
                                                $tablas.='<th>Descuento</th>';
                                                }
                                      $tablas.='<th>Metodo</th>
                                                <th>Monto</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        foreach ($respuesta->result() as $item){
                                            $detallepro=$this->ModeloCatalogos->getventadetalle($item->ventaId);
                                     $tablas.='<tr>
                                                    <td>'.$item->ventaId.'</td>
                                                    <td>'.$item->reg.'</td>
                                                    <td>'.$item->nombre.'</td>';
                                                if ($filtro==1) { 
                                                    
                                                    $tablas.='<td>';
                                                            foreach ($detallepro->result() as $items) {
                                                                $tablas.='<p>'.$items->cantidad.' '.$items->producto.'<p>';
                                                            }
                                                    $tablas.='</td>';
                                                    $tablas.='<td>';
                                                            foreach ($detallepro->result() as $items) {
                                                                $precioreal=$items->precio+$items->descuento;
                                                                $tablas.='<p>'.$precioreal.'<p>';
                                                            }
                                                    $tablas.='</td>';





                                                    $tablas.='<td>';
                                                            foreach ($detallepro->result() as $items) {
                                                                $tablas.='<p>'.$items->descuento.'<p>';
                                                            }
                                                    $tablas.='</td>';
                                                }
                                                switch ($item->metodo) {
                                                            case '1':
                                                                $metodo1='Efectivo';
                                                                break;
                                                            case '2':
                                                                $metodo1="Tarjeta";
                                                                break;
                                                            case '3':
                                                                $metodo1="Cuenta kid ".$item->compraId.' ('.$item->pulcera.' )';
                                                                break;
                                                            
                                                            default:
                                                                $metodo1='';
                                                                break;
                                                        }
                                                $tablas.='<td>'.$metodo1.'</td>
                                                    <td>'.$item->monto_total.'</td>
                                                </tr>';
                                                $totalr=$totalr+$item->monto_total;
                                                foreach ($detallepro->result() as $items) {
                                                                $rowproductos=$rowproductos+$items->cantidad;
                                                            }
                                               
                                        }
                                        $tablas.='</tbody>
                        </table>';

                        $tablas.='
                                    <table class="table table-sm table-hover table-responsive" id="tabla2">
                                        <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Producto</th>
                                                <th>Precio</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                        foreach ($respuestaprode->result() as $item){
                            $tablas.='
                                    <tr>
                                        <td>'.$item->cantidad.'</td>
                                        <td>'.$item->producto.'</td>
                                        <td>'.$item->precio.'</td>
                                    </tr>
                                    ';

                        }
                        $tablas.='</tbody>
                                </table>';







                $tablas.='
                        <div class="col-md-12">
                                    <div class="col-md-3"><b>Cantidad de productos</b></div>
                                    <div class="col-md-3"><b>'.number_format($rowproductos,2,'.',',').'</b></div>
                                  </div>
                        <div class="col-md-12">
                                    <div class="col-md-3"><b>TOTAL</b></div>
                                    <div class="col-md-3"><b>$ '.number_format($totalr,2,'.',',').'</b></div>
                                  </div>';
                echo $tablas;
                break;
            default:
                # code...
                break;
        }
    }
   
    
   
    
    
    
    

}