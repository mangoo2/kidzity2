var base_url = $('#base_url').val();
$(document).ready(function() {
	$('#genera').click(function(event) {
        
            var datos = $('#formcorte').serialize();
            $.ajax({
                type: 'POST',
                url: base_url+'Corteimpresiones/genera',
                data: datos,
                async: false,
                statusCode: {
                    404: function(data) {toastr.error('Error', '404');},
                    500: function() {toastr.error('Error', '500');}
                },
                success: function(data) {
                    $('.datoscorte').html(data);
                    $('#tabla').DataTable({
                                dom: 'Bfrtip',
                                buttons: [
                                        //{extend: 'excel',title: 'Reporte de saldos'},
                                        {extend: 'excel'},
                                        {extend: 'pdf'},
                                        {extend:'print'},
                                        'pageLength'
                                ]
                            });
                    $('#tabla2').DataTable({
                                dom: 'Bfrtip',
                                buttons: [
                                        {extend: 'excel'},
                                        {extend: 'pdf'},
                                        {extend:'print'},
                                        'pageLength'
                                ]
                            });
                    $('#tabla3').DataTable({
                                dom: 'Bfrtip',
                                buttons: [
                                        //{extend: 'excel',title: 'Reporte de saldos'},
                                        {extend: 'excel', footer: true},
                                        {extend: 'pdf', footer: true},
                                        {extend:'print', footer: true},
                                        'pageLength'
                                ],
                                "footerCallback": function ( row, data, start, end, display ) {
                                    var api = this.api(), data;
                                    bttondevolucion();
                                    // Remove the formatting to get integer data for summation
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                            i.replace(/[\$,]/g, '')*1 :
                                            typeof i === 'number' ?
                                                i : 0;
                                    };
                         
                                    // Total over all pages
                                    total = api
                                        .column( 5 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                    total1 = api
                                        .column( 6 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                    total2 = api
                                        .column( 13 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                         
                                    // Total over this page
                                    pageTotal = api
                                        .column( 5, { page: 'current'} )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                    pageTotal1 = api
                                        .column( 6, { page: 'current'} )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                    pageTotal2 = api
                                        .column( 13, { page: 'current'} )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                         
                                    // Update footer
                                    $( api.column( 5 ).footer() ).html(
                                        '$'+pageTotal +' ( $'+ total +' total)'
                                        //'$'+pageTotal +' ( $'+ total +' total)'
                                    );
                                    $( api.column( 6 ).footer() ).html(
                                        '$'+pageTotal1 +' ( $'+ total1 +' total)'
                                        //'$'+pageTotal +' ( $'+ total +' total)'
                                    );
                                    $( api.column( 13 ).footer() ).html(
                                        '$'+pageTotal2 +' ( $'+ total2 +' total)'
                                        //'$'+pageTotal2 +' ( $'+ total2 +' total)'
                                    );
                                },
                                "columnDefs": [{
                                                    "targets": [ 9 ],
                                                    "visible": false,
                                                    "searchable": false
                                                }
                                                 ]
                            });
                    setTimeout(function(){ 
                        bttondevolucion();
                    }, 3000);
                    
                },
                error: function(jqXHR, estado, error) {
                    console.log("Estado: " + estado);
                    console.log(jqXHR);
                    console.log("Error: " + error);
                },
            });
        
    });
	
});
function selecciontipor(){
    var tipo = $('#tipo').val();
    $('#filtro0').click();
    if (tipo==1) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "block");

    }
    if (tipo==2) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "none");
    }
    if (tipo==3) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "none");
    }
    if (tipo==4) {
        $(".radioninos").css("display", "block");
        $(".radiopro").css("display", "none");
    }
    if (tipo==5) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "block");

    }
    if (tipo==6) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "block");

    }
}
function bttondevolucion(){
    $('.bttondevolucion').click(function(event) {
        var motivo=$(this).data("motivo");
        $('.datosmotivod').html(motivo);
        $('#modalmotivodevolucion').modal();
    });
}