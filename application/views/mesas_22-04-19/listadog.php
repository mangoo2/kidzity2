<div class="row">
  <div class="col-md-12">
    <h2></h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Ventas de Mesas</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <table class="table table-striped" id="tableListadoMesas">
                    <thead>
                      <tr>
                        <th># de Venta</th>
                        <th>Empleado</th>
                        <th>Método de Pago</th>
                        <th>Neto</th>
                        <th>Descuento</th>
                        <th>Monto Cobrado</th>
                        <th></th>
                        <th>Mesa</th>
                        <th>Fecha</th>
                        <th>Tipo de Venta</th>
                        <th>Ticket</th>
                      </tr>
                    </thead>
                    <tbody class="tbody_lcompras">
                      
                    </tbody>
                  </table>
                  <div class="col-md-12">
                    <div class="col-md-9">
                    </div>
                    <div class="col-md-3">
                    </div>
                  </div>
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>

<div class="col-md-12 ticketventa" style="display: none;">
  
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listadoMesas.js"></script>


<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>