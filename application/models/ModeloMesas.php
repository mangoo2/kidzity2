<?php
class ModeloMesas extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }


    public function getProducto($id){
        $sql = "SELECT * FROM producto_hijo WHERE productoid=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function saveVenta($data){
    	$this->db->insert('ventasm', $data);
        return $this->db->insert_id();
    }

    public function saveProductos($data){
    	$this->db->insert('ventasm_detalle', $data);
        return $this->db->insert_id();
    }

     public function eliminaDetallesVenta($idVenta)
    {
        $this->db->where('ventaId', $idVenta);
        return $this->db->delete('ventasm_detalle');
    }

    public function eliminaVenta($idVenta)
    {
        $this->db->where('ventaId', $idVenta);
        return $this->db->delete('ventasm');
    }

    public function actualizaVenta($idVenta, $data)
    {
        $this->db->set($data);
        $this->db->where('ventaId', $idVenta);
        return $this->db->update('ventasm');
    }

    public function getDetallesMesa($idMesa)
    {
        $sql = "SELECT * FROM mesas_detalle WHERE idMesa=$idMesa";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getInformacionMesa($idMesa)
    {
        $sql = "SELECT * FROM mesas WHERE id=$idMesa";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function buscaProductoMesa($idProducto, $idMesa)
    {
        $sql = "SELECT * FROM mesas_detalle WHERE idProducto=".$idProducto." AND idMesa=".$idMesa;
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function insertaProductoMesa($data)
    {
        $this->db->insert('mesas_detalle', $data);
    }

    public function actualizaProductoMesa($data, $idProducto, $idMesa)
    {
        $this->db->set($data);
        $this->db->where('idProducto', $idProducto);
        $this->db->where('idMesa', $idMesa);
        return $this->db->update('mesas_detalle');
    }

    public function eliminaProductoMesa($idProducto, $idMesa)
    {
        $this->db->where('idProducto', $idProducto);
        $this->db->where('idMesa', $idMesa);
        return $this->db->delete('mesas_detalle');
    }
    public function eliminaProductosMesa($idMesa){
        $this->db->where('idMesa', $idMesa);
        return $this->db->delete('mesas_detalle');
    }

    public function actualizaMesa($idMesa, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $idMesa);
        return $this->db->update('mesas');
    }
    public function getEmpledos()
    {
        $sql="SELECT personalId, nombre FROM personal WHERE estatus = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function creditos_detalle($idcredito,$idproductos,$cantidad,$precio){
        $strq="INSERT INTO creditos_detalle(idc, productoId, cantidad, precio) 
                    VALUES ($idcredito,$idproductos,$cantidad,$precio)";
        $this->db->query($strq);
        //$this->db->close();
    }
    public function updateproducto($idproductos,$cantidad)
    { 
       $sql = "UPDATE producto_hijo SET stock=stock-$cantidad WHERE productoid=$idproductos";
       $this->db->query($sql);
    }
    public function pass_usuario($pass)
    {
        $sql = "SELECT UsuarioID FROM usuarios WHERE pin = '$pass'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function mesaactiva($mesa){
        $strq = "SELECT * FROM mesas_detalle where idMesa=".$mesa;
        $query = $this->db->query($strq);
        $activa=0;
        foreach ($query->result()  as $item) {
            $activa=1;
        }
        return $activa;

    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
}