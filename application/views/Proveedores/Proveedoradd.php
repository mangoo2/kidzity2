<div class="container">
<section class="basic-elements">
<div class="center">
<div class="card">
<div class="card-header">
<h4 class="card-title mb-0"> <?php echo $Texto; ?> proveedor</h4>
</div>
<div class="card-body">
<div class="px-3">
<form class="form" role="form" id="formProveedor" name="formProveedor" method="post" >
<div class="form-body">
<div class="row">
<div class="container">
  <?php if ($ID=="") {
  }else{?> <input type="hidden" id="ID" name="ID" value="<?php echo $ID; ?>"> <?php }?>
 <div class="col-xl-12 col-lg-12 col-md-12">
  <fieldset class="form-group">
   <label for="RazonSocial">Razón social</label>
    <input type="text" class="form-control" value="<?php echo $RazonSocial; ?>" id="RazonSocial" name="RazonSocial">
   </fieldset>                                
 </div>
 <div class="col-xl-12 col-lg-12 col-md-12">
  <fieldset class="form-group">
   <label for="Domicilio">Domicilio</label>
    <input type="text" class="form-control" value="<?php echo $Domicilio; ?>" id="Domicilio" name="Domicilio">
   </fieldset>                                
 </div>
 <div class="col-xl-6 col-lg-6 col-md-6">
  <fieldset class="form-group">
   <label for="Ciudad">Ciudad</label>
    <input type="text" class="form-control" value="<?php echo $Ciudad; ?>" id="Ciudad" name="Ciudad">
   </fieldset>                                
 </div>
 <div class="col-xl-6 col-lg-6 col-md-6">
   <fieldset class="form-group">
     <label for="Estado">Seleccione Estado</label>
     <select id="Estado" class="custom-select d-block w-100" name="Estado">
     <?php foreach ($Estado->result() as $item) { ?> 
     <option <?php if ($EstadoV == $item->EstadoId){echo "selected";}?> value="<?php echo $item->EstadoId; ?>"> <?php echo $item->Nombre; ?> </option>
     <?php } ?>
     </select>
   </fieldset>  
  </div>
  <div class="col-xl-6 col-lg-6 col-md-6">
  <fieldset class="form-group">
   <label for="RFC">RFC</label>
    <input type="text" class="form-control" value="<?php echo $RFC; ?>" id="RFC" name="RFC">
   </fieldset>                                
 </div>
 <div class="col-xl-12 col-lg-12 col-md-12">
 	<hr style="width: 100%; color: black; height: 1px; background-color:black;" />
 </div>
 <div class="col-xl-12 col-lg-12 col-md-12">
 	<h5>Datos de contacto</h5>
 </div>
 <div class="col-xl-12 col-lg-12 col-md-12">
  <fieldset class="form-group">
   <label for="Nombre">Nombre</label>
    <input type="text" class="form-control" value="<?php echo $Nombre; ?>" id="Nombre" name="Nombre">
   </fieldset>                                
 </div>
 <div class="col-xl-12 col-lg-12 col-md-12">
  <fieldset class="form-group">
   <label for="Correo">Correo electrónico</label>
    <input type="mail" class="form-control" value="<?php echo $Correo; ?>" id="Correo" name="Correo">
   </fieldset>                                
 </div>
 <div class="col-xl-6 col-lg-6 col-md-6">
  <fieldset class="form-group">
   <label for="Movil">Teléfono Móvil</label>
    <input type="text" class="form-control" value="<?php echo $Movil; ?>" id="Movil" name="Movil">
   </fieldset>                                
 </div>
 <div class="col-xl-12 col-lg-12 col-md-12">
  <fieldset class="form-group">
   <label for="Notas">Notas</label>
    <input type="text" class="form-control" value="<?php echo $Notas; ?>" id="Notas" name="Notas">
   </fieldset>                                
 </div>
</div>
</div>
</div>
<div class="row">
  <div class="col-md-12" align="center">
   <a class="btn btn-primary" href="<?php echo base_url();?>Proveedores" id="SaveProveedor" ><i class="fa fa-save"></i>Guardar</a>
   <a class="btn btn-danger" href="<?php echo base_url();?>Proveedores">Cancelar<i class="fa fa-trash-o"></i></a>
  </div>
</div>
</form>
</div>
</div>
</div>
</div>
</section>
</div>