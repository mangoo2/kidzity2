<input type="hidden" id="titular" value="<?php echo $_GET['cod'];?>">
<input type="text"  autocomplete="on" name="users" class="form-control" style="border: 0; background: transparent;" readonly>
<input type="hidden" id="idpersonalid" value="<?php echo $idpersonalid; ?>" readonly>
<style type="text/css">
  .iframevisitantes{
    width: 100%;
    border: 0;
    min-height: 290px;
  }
  .textformmy{
    text-transform: uppercase;
    font-weight: bold;
    font-size: 15px;
    /*color:#3e71ac;    
    /*text-shadow: 2px 0 white, -2px 0 white, 0 2px white, 0 -2px white, 1px 1px black, -1px -1px white, -1px 1px white, 1px -1px white;*/
  }
  #vsprecio{
        border: solid #cacdd0 1px !important;

  }
  .viewcompraapp{
    text-align: center;
    color: red;
  }
  .ocultotemporal{
    /*display: none; */
  }
  .ocultotemporalfamiliar{
    display: none; 
  }

}
</style>
<div class="">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-7">
        
      </div>
      <div class="col-md-2">
        <label>TIPO DE VENTA:
            
        </label>
      </div>
      <div class="col-md-3">
            <select class="form-control" id="tipoventa" name="tipoventa">
              <option value="0">General</option>
              <?php foreach ($fiestasl->result() as $item) {?>
                <option value="<?php echo $item->fiestaId;?>" <?php if (isset($_GET['event'])) { if ($_GET['event']==$item->fiestaId) { echo 'selected';} } ?>     ><?php echo $item->nombre;?></option>
              <?php } ?>
            </select>
      </div>
    </div>
  </div>
  
  <section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0">Ventas de mostrador</h4>
          <div align="right"><h3 class="fecha"></h3></div>
          <div align="right"> <h3 id="Hora"><i class="fa-clock-o"></i></h3> </div>
        </div>
        <div class="col-md-12 viewcompraapp" style="display: none;">
          <h3><b>Compra desde APP</b></h3>
        </div>
        <div class="card-body">
          <div class="px-3">
              <div class="form-body">
                <div class="row">
                  <div class="">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <?php foreach ($rtitular->result() as $item) {?>
                      <div class="col-xl-1 col-lg-1 col-md-1">
                        <label class="textformmy">Titular: </label>
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-6 fadeInDown animated">
                        <label id="Titular" class="textformmy" style="font-weight: normal"><?php echo $item->nombre;?></label>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1">
                        <label class="textformmy">Teléfono: </label>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3">
                        <label id="TelTitular" class="textformmy" style="font-weight: normal"><?php echo $item->movil;?></label><!--aqui va el Telefono del titular-->
                      </div>
                      <?php } ?>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <?php foreach ($rstitular->result() as $item) {?>
                      <div class="col-xl-2 col-lg-2 col-md-2">
                        <label>Co-Titular: </label>
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-6 fadeInDown animated">
                        <label id="CoTitular" class="textformmy"><?php echo $item->nombre;?></label>
                      </div>
                      <div class="col-xl-1 col-lg-1 col-md-1">
                        <label class="textformmy">Teléfono: </label>
                      </div>
                      <div class="col-xl-3 col-lg-3 col-md-3">
                        <label id="TelCoTitular" class="textformmy"><?php echo $item->movil;?></label><!--aqui va el Telefono del co-titular-->
                      </div>
                      <?php } ?>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-4">
                        <button class="btn btn-primary"  id="imprimirqr"><i class="fa fa-qrcode"></i> Genera etiqueta</button>
                      </div>
                      
                      
                    </div>
                    
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <div class="col-xl-12 col-lg-12 col-md-12" autocomplete="off">
                        <h5><b>Usuarios</b></h5>
                        <!--<form autocomplete="off">-->
                        <table class="table table-hover" id="list_ninos" style="font-size: 14px;">
                          <thead>
                            <tr>
                              <th>Entrada</th>
                              <th>Calcetines</th>
                              <th>Nombre</th>
                              <th>Edad</th>
                              <th>Pulsera</th>
                              <th class="col-descartarpf" style="display: none;">Descartar PF</th>
                              <th class="ocultotemporal">Tiempo libre bebe</th>
                              <th class="ocultotemporal">Tiempo libre</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($rsninos->result() as $item) {?>
                            <tr>
                              <td>
                                  <label class="switch">
                                    <input type="checkbox" onchange="selected();" value="<?php echo $item->id;?>" id="ninoselected" class="ninoselected" >
                                    <div class="slider round"></div>
                                  </label>

                              </td>
                              <td>
                                  <label class="switch">
                                    <input type="checkbox" id="calcetin" class="calcetin" >
                                    <div class="slider round"></div>
                                  </label>

                              </td>
                              <td><label id="NombreN"><?php echo $item->nombre;?></label></td>
                              <td>
                                <?php 
                                      $cumpleanos = new DateTime($item->fecha_nacimiento);
                                      $hoy = new DateTime();
                                      $annos = $hoy->diff($cumpleanos);
                                      $edad= $annos->y;
                                      if ($edad>0&&$edad<99) {
                                        echo $edad;
                                      }
                                ?></td>
                              <td><input type="text" onchange="verificarpulcera(this);" placeholder="Brazalete id:" class="form-control brazalete_<?php echo $item->id;?>" id="brazalete" name="brazalete" value="<?php echo $item->brazalete;?>"   autocomplete="noped" ></td>

                              <td class="col-descartarpf" style="display: none;">
                                <label class="switch">
                                    <input type="checkbox" id="descartarpf" class="descartarpf" onchange="selected();" >
                                    <div class="slider round"></div>
                                  </label>
                              </td>
                              <td class="ocultotemporal">
                                  <label class="switch ocultoventaapp">
                                    <input type="checkbox" onchange="selected();" value="0" id="costonino" name="costonino" class="costonino" data-on="fff" data-off="ddd">
                                    <div class="slider round"></div>
                                  </label>
                              </td>
                              <td class="ocultotemporal">
                                  <label class="switch ocultoventaapp">
                                    <input type="checkbox" onchange="selected();" value="0" id="tiempoadul" name="tiempoadul" class="tiempoadul">
                                    <div class="slider round"></div>
                                  </label>
                              </td>
                              <!--<td><button class="btn btn-raised btn-success btn-min-width mr-1 mb-1" onclick="associar(<?php echo $item->id;?>)" >Associar</button> </td>-->
                            </tr>
                          <?php } ?>
                          </tbody>
                          
                        </table>
                        <!--</form>-->
                      </div>
                    </div>
                    <div class="col-md-12 ocultoeventos">
                      <div class="col-md-10">
                        
                      </div>
                      <div class="col-md-2" >
                        <button class="btn btn-raised btn-success btn-min-width mr-1 mb-1 generarclavesaleatorias">Generar codigos</button>
                      </div>
                    </div>
                    <div class="col-md-12 ocultoeventos">
                      <hr>
                    </div>
                    <div class="col-md-12 ocultoeventos">
                      <label>Venta:</label>
                    </div>
                    <div class="col-md-12 ocultoeventos">
                      <div class="col-md-6">
                        <div class="col-md-12">
                          <label class="col-md-5">Tiempo de estancia: </label>
                          <div class="col-md-7">
                            <input type="number" id="Estancia" class="form-control" name="Estancia" min="1" value="1" >
                          </div>
                        </div>
                        <div class="col-md-12 ocultoventaapp">
                          <label class="col-md-5">¿Tiempo libre Niños? </label>
                          <div class="col-md-7">
                            <label class="switch">
                                    <input type="checkbox" id="tiempolibre" class="tiempolibre" >
                                    <div class="slider round"></div>
                                  </label>
                          </div>
                        </div>
                        <div class="col-md-12" style="display: none">
                          <label class="col-md-5">¿Tiempo libre bebe? </label>
                          <div class="col-md-7">
                            <label class="switch">
                                    <input type="checkbox" id="tiempolibrebebe" class="tiempolibrebebe" >
                                    <div class="slider round"></div>
                                  </label>
                          </div>
                        </div>
                        <div class="col-md-12 ocultoventaapp">
                          <label class="col-md-5">¿Calcetines? </label>
                          <div class="col-md-2">
                            <label class="switch">
                                    <input type="checkbox" id="tines" class="tines" >
                                    <div class="slider round"></div>
                                  </label>
                          </div>
                          <div class="col-md-5">
                            <input type="number" id="tinesn" class="form-control" name="tinesn" min="1" value="1" >
                          </div>
                        </div>
                        <div class="col-md-12 ocultoventaapp">
                          <label class="col-md-5">Saldo Kid's Universe</label>
                          <div class="col-md-7">
                            <input type="number" id="gastosv" class="form-control"  min="0" value="<?php echo $saldod;?>" title="Para compra" >
                          </div>
                        </div>
                        <div class="col-md-12 ocultoventaapp">
                          <label class="col-md-4">Otros Artículos</label>
                          <div class="col-md-4">
                            
                            <select class="form-control" id="otrosarticulos" name="otrosarticulos"></select>
                          </div>
                          <div class="col-md-4">
                            <input type="number" id="otrosarticulosn" class="form-control" name="otrosarticulosn" min="1" value="1" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md-1">
                        
                      </div>
                      <div class="col-md-5">
                        <div class="col-md-12">Resumen:</div>
                        <!-------------->
                        <label class="col-md-5">Tiempo de estancia:</label>
                        <div class="col-md-7">
                          <input type="text" id="saldo2" class="form-control" name="Saldo" value="0" readonly style="color: red;">
                            <input type="hidden" id="saldo">
                        </div>
                        <!------------->
                        <label class="col-md-5">Saldo Kid's Universe:</label>
                        <div class="col-md-7">
                          <input type="text" id="saldokidsinfo" class="form-control" name="saldokidsinfo" value="<?php echo $saldod;?>" readonly style="color: red;">
                        </div>
                        <div class="col-md-12">
                          
                        </div>
                        <label class="col-md-5 ocultoventaapp">Descuento Especial:</label>
                        <div class="col-md-7 ocultoventaapp">
                          <input type="number" id="descuento" class="form-control" name="descuento" value="0" onclick="reactivardescuento();" readonly style="color: red;">
                        </div>
                        <label class="col-md-5 descuentotext" style="display: none;">Descuento Tipo:</label>
                        <div class="col-md-7 descuentotext" style="display: none;">
                          <select class="form-control" id="descuentotext" name="descuentotext">
                            <option value=""></option>
                            <option >Pigui Wallet</option>
                            <option >Tines</option>
                            <option >2x1</option>
                            <option >Cuponerapp</option>
                            <option >Cortesía</option>
                            <option >Promo del día</option>
                            <option >Cumpleaños</option>
                            <option >Locatarios</option>
                            <option >Proveedores</option>
                            <option >Empleados</option>
                          </select>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-12">
                      <table class="table table-hover" id="articulos">
                        <tbody class="addproductos">
                          
                        </tbody>
                        
                      </table>
                    </div>
                    <div class="col-md-12">
                      <hr>
                    </div>
                    <div class="col-md-12">
                      
                    </div>
                    <input type="hidden" id="pagominimo" readonly>
                    <div class="col-md-12">
                      <label class="col-md-2 ocultoventaapp">Método de pago: </label>
                      <div class="col-md-2 ocultoventaapp">
                        <select class="form-control " id="metodop">
                          <option value="1">Efectivo</option>
                          <option value="2">Débito/Crédito</option>
                        </select>
                      </div>
                      <div class="col-md-2 ocultoventaapp">
                          <input type="number" id="pago" class="form-control ocultoventaapp" min="0" value="0" oninput="selected();">
                      </div>
                      <label class="col-md-2 ocultoventaapp">Cambio:<span class="cambios"></span> </label>
                      
                      <label class="col-md-2" style=" font-size: 30px">Total: </label>
                      <div class="col-md-2 totalgeneral" style="color: red;  font-size: 30px">
                          $ 0.00
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xl-12 ocultotemporal ocultotemporalfamiliar">
                      <div class="col-md-2">
                        <label>Paquete Familiar</label>
                      </div> 
                      <div class="col-md-3">
                        <label class="switch">
                            <input type="checkbox" id="pfamiliar" class="pfamiliar" onchange="pfamiliar();">
                            <div class="slider round"></div>
                        </label>
                      </div>
                      
                    </div>
                    
                  




                    
                    
                   
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      <br>
                      <br>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" align="center">
                  <button class="btn btn-primary" id="registrar" ><i class="ft-log-in"></i> Registrar ingreso</button>
                  <button class="btn btn-primary" id="registrarcomp" style="display: none;" ><i class="ft-log-in"></i> Ingresar</button>
                  <a class="btn btn-danger" href="<?php echo base_url(); ?>Inicio" ><i class="ft-slash"></i> Cancelar</a>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal fade text-left" id="modalvistantes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Etiqueta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="input-group">
                    <input type="text" placeholder="Acompañante" id="acompanante" class="form-control">
                  </div>
                  <div class="input-group">
                    <input type="number" class="form-control" id="numeroprint" value="1" min="1">
                    <div class="input-group-append">
                      <span class="input-group-btn" id="button-addon2">
                        <button class="btn btn-raised btn-primary" id="imprimirqr2">Generar</button>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-12 printetiqueta">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 ticketpago" style="display: none;">
</div>
<div class="modal fade text-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                      <h3 class="modal-title" id="myModalLabel34">Autorización</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      </div>
                      
                      <div class="modal-body">
                        <label>Password: </label>
                        <div class="form-group position-relative has-icon-left">
                          <input type="password" placeholder="Password" id="passautorizacion" class="form-control" autocomplete="off">
                                                  </div>
                      </div>
                      <div class="modal-footer">
                        <button type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" >Cancelar</button>
                        <button type="submit" class="btn btn-outline-primary btn-lg modalsalidam" data-dismiss="modal">Aceptar</button>
                      </div>
                      
                    </div>
                    </div>
</div>
<div class="modal fade text-left" id="iconFormedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                      <h3 class="modal-title" id="myModalLabel34">Autorización</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      </div>
                      <div class="modal-body">
                        <label>Password: </label>
                        <div class="form-group position-relative has-icon-left">
                          <input type="password" placeholder="Password" id="passautorizacionedit" class="form-control" autocomplete="off">
                          
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" >Cancelar</button>
                        <button type="submit" class="btn btn-outline-primary btn-lg modalsalidamedit" data-dismiss="modal">Aceptar</button>
                      </div>
                      
                    </div>
                    </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
  $('#imprimirqr').click(function(event) {
    var idqr=$('#titular').val();
    var titular=$('#Titular').text();
    var acomp=$('#acompanante').val();
    if (idqr!='') {
      $('.printetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiqueta?venta='+idqr+'&nomtitu='+titular+'&print=0&printn=1&acompa='+acomp+'" class="iframevisitantes"></iframe>');
      $('#modalvistantes').modal();
    }else{
      toastr.error('Error', 'Código invalido');
    }
  });
  $('#imprimirqr2').click(function(event) {
    var numero=$('#numeroprint').val();
    var idqr=$('#titular').val();
    var acomp=$('#acompanante').val();
    var titular=$('#Titular').text();
    $('.printetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiqueta?venta='+idqr+'&nomtitu='+titular+'&print=1&printn='+numero+'&acompa='+acomp+'" class="iframevisitantes"></iframe>');
  });
  <?php if (isset($_GET['com'])) { ?>
    datoscompras(<?php echo $_GET['cod'];?>,<?php echo $_GET['com'];?>);
  <?php }?>
});
</script>