<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoyl = date("Y-m-d H:i:s");
    }

    function index(){
        $pages=10;
        $this->load->library('pagination');
        $config['base_url'] = base_url().'Compras/view/';
        $config['total_rows'] = $this->ModeloCatalogos->filasCompras();
        $config['per_page'] = $pages;
        $config['num_links'] = 3;
        $config['first_link'] = 'Primera';
        $config['last_link'] = 'Última';
        $config["uri_segment"] = 3;
        $config['next_link'] = 'Siguiente';
        $config['prev_link'] = 'Anterior';
        $config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["compras"] = $this->ModeloCatalogos->ListCompras($pagex,$config['per_page']);
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Compras/Compraslista',$data);
        $this->load->view('templates/footer');
        $this->load->view('Compras/Compraaddjs');
    }

    function Compraadd(){
    	$data['Estado']=$this->ModeloCatalogos->getestados();
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Compras/Compraadd',$data);
        $this->load->view('templates/footer');	
        $this->load->view('Compras/Compraaddjs');
    }
    function addcompra(){
        $totalcompra = $this->input->post('totalcompra');
        $productos = $this->input->post('productos');
        $datacompra = array('monto_total' => $totalcompra,'usuario' =>$_SESSION['idpersonal']);
        $compraid=$this->ModeloCatalogos->insertToCatalogo($datacompra,'compras');

        $DATA = json_decode($productos);
        for ($i = 0; $i < count($DATA); $i++) {
            $scantidad = $DATA[$i]->scantidad;
            $sproductoid = $DATA[$i]->sproductoid;
            $sprecio = $DATA[$i]->sprecio;

            $datacomprad = array('compraId' => $compraid,'productoid' => $sproductoid,'cantidad' => $scantidad,'precio' =>  $sprecio);
           
            $this->ModeloCatalogos->insertToCatalogo($datacomprad,'compras_detalle');

            $datacompradu = array('stock' => 'stock +'.$scantidad);
            $this->ModeloCatalogos->updatestockpro($scantidad,$sproductoid);

        }
    }
    function BuscarCompras(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloCatalogos->comprassallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr>
                <td><?php echo $item->compraId; ?></td>
                <td><?php echo $item->usuario; ?></td>
                <td><?php echo $item->monto_total; ?></td>
                <td><?php echo $item->reg; ?></td>
                <td>
                  <div class="row">
                    <div class="col-md-12" align="center">
                      <a class="btn btn-primary" onclick="Detallecompra( <?php echo $item->compraId; ?>)">Detalle</a>
                    </div>
                  </div>
                </td>
              </tr>
        <?php }
    }
    function detalles(){
        $id = $this->input->post('compra');
        $resultado=$this->ModeloCatalogos->detallescompras($id);
        ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td>Cantidad</td>
                        <td>Producto</td>
                        <td>Precio</td>
                        <td>Subtotal</td>
                      </tr>
                </thead>
                <tbody>
                    
                
                
        <?php
            $total=0;
        foreach ($resultado->result() as $item){ 
            $total=$total+($item->cantidad*$item->precio);?>
            <tr>
                <td><?php echo $item->cantidad; ?></td>
                <td><?php echo $item->producto; ?></td>
                <td><?php echo $item->precio; ?></td>
                <td><?php echo $item->cantidad*$item->precio; ?></td>
              </tr>
        <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php echo $total; ?></td>
                      </tr>
                </tfoot>
            </table>
        <?php
    }
}
?>