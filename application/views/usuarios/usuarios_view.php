<div class="row">
                <div class="col-md-12">
                  <h2>Usuarios</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Usuarios</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
									<!-------------------------------->	  
									<div class="col-md-5">
										<table class="table table-striped table-hover">
											<thead>
												<tr><th>Usuario</th><th>Perfil</th><th></th></tr>
											</thead>
											<tbody id="viewusuarios">
												<?php foreach ($usuarios->result() as $item){ ?>
                  						 			<tr  class="rowlink" style="cursor: pointer" data-id="<?php echo $item->UsuarioID; ?>" onclick="datosclic(<?php echo $item->UsuarioID; ?>)">
						                          		<td><?php echo $item->Usuario; ?></td>
						                          		<td><?php echo $item->nombre; ?></td>
						                          		<td></td>
						                     
						                        </tr>


						                        
				                          
				                          
                						<?php } ?>
											</tbody>
										</table>
									</div>                  			
									<div class="col-md-7" id="form">
										<form  autocomplete="off">
										<div class="form-group">
											<label class="col-md-5 control-label">Nombre de usuario</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="txtUsuario" name="txtUsuario"  autocomplete="new-password" onchange="verificarusu()" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Contraseña</label>
											<div class="col-md-6">
												<input type="password" class="form-control" id="txtPass" name="txtPass"  autocomplete="new-password" placeholder="Favor de introducir 6 caracteres minimo" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Verificar la contraseña</label>
											<div class="col-md-6">
												<input type="password" class="form-control" id="txtPass2" name="txtPass2"  autocomplete="new-password" placeholder="" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Empleado</label>
											<div class="col-md-6">
												<select id="cmbPersonal" name="cmbPersonal" class="form-control">
													<?php foreach ($personal->result() as $item){ ?>
                  						 			
													<option value="<?php echo $item->personalId; ?>"><?php echo $item->nombre; ?></option>
													<?php } ?>
												</select> 
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">Perfil</label>
											<div class="col-md-6">
												<select id="cmbPerfiles" name="cmbPerfiles" class="form-control">
													<?php foreach ($perfiles->result() as $item){ ?>
                  						 			
													<option value="<?php echo $item->perfilId; ?>"><?php echo $item->nombre; ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-5 control-label">PIN (Para ventas en Restaurante)</label>
											<div class="col-md-6">
												<input type="password" class="form-control" id="pinMesero" name="pinMesero" placeholder="" />
											</div>
										</div>
										<div class="form-group">
											<button class="btn" id="btnCancelar">Cancelar</button>
											<button class="btn vd_btn vd_bg-green vd_white" id="guardarus">Guardar</button>
										</div>
										<input type="hidden" id="txtId" />
									</form>
									</div>



	                    			<!-------------------------------->
	                    		<!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
	<script type="text/javascript">
		$(document).ready(function() {
				$('.table').dataTable();
				setTimeout(function(){ 
					$('#txtUsuario').val('');
					$('#txtPass').val('');
				}, 1000);
		} );
		
</script>
		