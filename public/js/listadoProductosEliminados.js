function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tablaListadoProductosEliminados').DataTable({
        "ajax": {
            "url": "getListadoProductosEliminados"
        },
        "columns": [
            {"data": "id"},
            {"data": "mesa"},
            {"data": "producto"},
            {"data": "nombre"},
            {"data": "fechaRegistro"}
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });

    
}

    $(document).ready(function () 
    {
        table = $('#tablaListadoProductosEliminados').DataTable();
        load();
    });

