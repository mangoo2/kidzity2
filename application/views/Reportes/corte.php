<link href="<?php echo base_url(); ?>public/plugins/iCheck/skins/polaris/polaris.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>public/plugins/iCheck/icheck.js"></script>
<!--
<link href="<?php echo base_url(); ?>public/css/spinner.css" rel="stylesheet">
<div class="sk-cube-grid" style="display: none;">
  <div class="sk-cube sk-cube1"></div>
  <div class="sk-cube sk-cube2"></div>
  <div class="sk-cube sk-cube3"></div>
  <div class="sk-cube sk-cube4"></div>
  <div class="sk-cube sk-cube5"></div>
  <div class="sk-cube sk-cube6"></div>
  <div class="sk-cube sk-cube7"></div>
  <div class="sk-cube sk-cube8"></div>
  <div class="sk-cube sk-cube9"></div>
</div>
-->
<style type="text/css">
  table{
    font-size: 11px;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Personal</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Corte</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                <!--------//////////////-------->
                
                  <div class="row">
                    <form id="formcorte" class="col-md-11">
                      <div class="col-md-3">
                        <label>Fecha Inicio</label>
                        <input type="date" name="fechainicio" id="fechainicio"  class="form-control" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                      <div class="col-md-3">
                        <label>Fecha Termino</label>
                        <input type="date" name="fechafin" id="fechafin"  class="form-control" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                      <div class="col-md-3">
                        <label>Tipo Reporte</label>
                        <select name="tipo" id="tipo" class="form-control" onchange="selecciontipor()" >
                          <option value="1">Cafetería/Mesas</option>
                          <option value="2">Visitas</option>
                          <option value="3">Saldos</option>
                          <option value="4">Fiesta</option>
                          <option value="5">Compras</option>
                          <option value="6">Regalos/Recepción</option>
                          <option value="7">Cafetería/Mesas sin Realizar</option>
                          <option value="8">Creditos Personal</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <div class="col-md-12">
                          <label  for="filtro0"><input type="radio" name="filtro" value="0" id="filtro0" class="i-checks" checked>Ninguno</label>
                        </div>
                        <div class="col-md-12 radiopro">
                          
                          <label for="filtro1"><input type="radio" name="filtro" value="1" class="i-checks"  id="filtro1" >Productos</label>
                          
                        </div>
                        <div class="col-md-12 radioninos" style="display: none;">
                          
                          <label for="filtro2"><input type="radio" name="filtro" class="i-checks" value="2" id="filtro2" >Niños</label>
                          
                        </div>

                        
                        
                        
                      </div>
                    </form>
                    <div class="col-md-1">
                      <label class="col-md-12" style="color: transparent;"> x</label>
                      <button class="btn btn-primary"  id="genera"> Generar</button>
                    </div>
                  </div>
                  <div class="row viewrecepcion" style="display: none;">
                    <div class="col-md-12">
                      <label class="col-md-2 classcredito">Tipo vendedor</label>
                      <div class="col-md-4 classcredito">
                          <label><input type="radio" name="tipov"  class="i-checks checkes0" value="0" checked>Entrada/Salida</label>
                          <label><input type="radio" name="tipov"  class="i-checks checkes1" value="1">Entrada</label>
                          <label><input type="radio" name="tipov"  class="i-checks checkes2" value="2">Salida</label>
                      </div>
                      <label class="col-md-2">Metodo de pago</label>
                      <div class="col-md-2">
                        <select id="metodopago" class="form-control">
                          <option value="0">Todos</option>
                          <option value="1">Fectivo</option>
                          <option value="2">Tarjeta</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <label class="col-md-2">Personal....</label>
                      <div class="col-md-4">
                        <select class="form-control" id="vendedorselected">
                            <option value="0">Todosx</option>
                          <?php foreach ($cajeros->result() as $item) {
                            echo '<option value="'.$item->personalId.'">'.$item->nombre.'</option>';
                          } ?>
                          <?php foreach ($cajerosadmin->result() as $item) {
                            echo '<option value="'.$item->personalId.'">'.$item->nombre.'</option>';
                          } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row viewmesas" >
                    <label class="col-md-2">Metodo de pago</label>
                      <div class="col-md-2">
                        <select id="metodopago2" class="form-control">
                          <option value="0">Todos</option>
                          <option value="1">Fectivo</option>
                          <option value="2">Tarjeta</option>
                        </select>
                      </div>
                  </div>
                
                <div class="row datoscorte" style="overflow: auto;">

                </div>
                 
                <!--------//////////////-------->
              </div>
          </div>
      </div>
    </div>
</div>
<div class="modal fade" id="modalmotivodevolucion" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Motivo Devolución</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
        <div class="col-md-12 datosmotivod">
          
        </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>