<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turnos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }
    function index(){
        $data['areas']=$this->ModeloCatalogos->getareas();
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Turnos/Turnoadd',$data);
        $this->load->view('Turnos/jsTurno');
        $this->load->view('templates/footer');
    }
    function InsertTurno(){
        $arr=$this->input->post();
        $this->ModeloCatalogos->Insertar("turno",$arr);
    }
    function mostrar_cantidad(){
        $arr=$this->input->post('punto_venta');
        $datos = $this->ModeloCatalogos->GetDataTurno($arr);
        echo json_encode($datos);
    }
    function editarTurno() {
        $id = $this->input->post('id');
        $fechacierre = $this->input->post('fechacierre');
        $horac = $this->input->post('horac');
        //update
        $result = $this->ModeloCatalogos->editarTurno($fechacierre, $horac ,$id);
        echo json_encode($result);
    }
    function reporte(){
        $reporte = $this->input->post('reporte');
        $cinicial = $this->input->post('cinicial');
        $total=0;
        $html='';
        switch ($reporte) {
            case '3':
                $resultado = $this->ModeloCatalogos->getListadoTurnoVentasCafeteria();
                $html.='<table class="table table-striped table-hover" id="data-tables3">';
                        $html.='<thead>';
                          $html.='<tr>';
                            $html.='<th>Vendedor</th>';
                            $html.='<th>Metodo de pago</th>';
                            $html.='<th>Monto total</th>';
                          $html.='</tr>';
                        $html.='</thead>';
                        $html.='<tbody id="tbodyresultados">';
                            foreach ($resultado->result() as $item){
                                $html.='<tr>';
                                $html.='<td>'.$item->nombre.'</td>';
                                if($item->metodo==1){
                                $html.='<td>efectivo</td>';
                                }else if($item->metodo==2){
                                $html.='<td>targeta</td>';
                                }else if($item->metodo==3){
                                $html.='<td>Kid</td>';
                                }
                                $html.='<td>'.'$ '.$item->monto_total.'</td>';
                                $html.='</tr>';
                                $total=$total+ $item->monto_total;
                            }
                        $html.='</tbody>';
                    $html.='</table>';
                    //============================ mesas
                    $resultadom = $this->ModeloCatalogos->getListadoTurnoMesero();
                    $html.='<table class="table table-striped table-hover" id="data-tables2">';
                            $html.='<thead>';
                              $html.='<tr>';
                                $html.='<th>Mesero</th>';
                                $html.='<th>Metodo de pago</th>';
                                $html.='<th>Monto total</th>';
                              $html.='</tr>';
                            $html.='</thead>';
                            $html.='<tbody id="tbodyresultados">';
                                foreach ($resultadom->result() as $item){
                                    $html.='<tr>';
                                    $html.='<td>'.$item->nombre.'</td>';
                                    if($item->metodo==1){
                                    $html.='<td>efectivo</td>';
                                    }else if($item->metodo==2){
                                    $html.='<td>targeta</td>';
                                    }
                                    $html.='<td>'.'$ '.$item->monto_total.'</td>';
                                    $html.='</tr>';
                                    $total=$total+ $item->monto_total;
                                }
                            $html.='</tbody>';
                        $html.='</table>';





                break;
            case '4':
                $resultado = $this->ModeloCatalogos->getListadoTurnoVentasRegalos();
                $html.='<table class="table table-striped table-hover" id="data-tables3">';
                        $html.='<thead>';
                          $html.='<tr>';
                            $html.='<th>Mesero</th>';
                            $html.='<th>Metodo de pago</th>';
                            $html.='<th>Monto total</th>';
                          $html.='</tr>';
                        $html.='</thead>';
                        $html.='<tbody id="tbodyresultados">';
                            foreach ($resultado->result() as $item){
                                $html.='<tr>';
                                $html.='<td>'.$item->nombre.'</td>';
                                if($item->metodo==1){
                                $html.='<td>efectivo</td>';
                                }else if($item->metodo==2){
                                $html.='<td>targeta</td>';
                                }
                                $html.='<td>'.'$ '.$item->monto_total.'</td>';
                                $html.='</tr>';
                                $total=$total+ $item->monto_total;
                            }
                        $html.='</tbody>';
                    $html.='</table>';
                break;
            
           
        }



        $totalg=$cinicial+$total;
            $html.='<div class="col-xl-6 col-lg-6 col-md-6 border border-dark">';
              $html.='<h5>Resumen</h5>';
              $html.='<div class="col-xl-6 col-lg-6 col-md-6">';
                $html.='<label>Monto inicial de caja: </label>';
              $html.='</div>';
              $html.='<div class="col-xl-6 col-lg-6 col-md-6">';
                $html.='<label id="MIC">$ '.$cinicial.'</label>';
              $html.='</div>';
              $html.='<div class="col-xl-6 col-lg-6 col-md-6">';
                $html.='<label>Monto total ventas: </label>';
              $html.='</div>';
              $html.='<div class="col-xl-6 col-lg-6 col-md-6">';
                $html.='<label id="MFC">$ '.$total.'</label>';
              $html.='</div>';
              $html.='<div class="col-xl-6 col-lg-6 col-md-6">';
                $html.='<label>Total terminal: </label>';
              $html.='</div>';
              $html.='<div class="col-xl-6 col-lg-6 col-md-6">';
                $html.='<label id="TT">$ '.$totalg.'</label>';
              $html.='</div>';
              $html.='<div class="col-xl-12 col-lg-12 col-md-12">';
                $html.='<br>';
                $html.='<br>';
                $html.='<br>';
              $html.='</div>';
            $html.='</div>';

            echo $html;
        
    }

}
?>  