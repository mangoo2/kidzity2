$(document).ready(function(){	
    
    $('.rowlink').click(function(){
		var id = $(this).attr('data-id');
		$.ajax({
			type:'POST',
			url:'usuarios/consultar',
			data:{id:id},
			async:false,
			success:function(data){
				var array = $.parseJSON(data);
				$('#txtId').val(array.id);
				$('#txtUsuario').val(array.usuario);
				$('#txtPass').val(array.pass);
				$('#txtPass2').val(array.pass);
                $('#pinMesero').val(array.pin);
				$('#cmbPerfiles option[value='+array.idPerfil+']').prop('selected','true');
                $('#cmbPersonal option[value='+array.idPersona+']').prop('selected','true');
			}
		});
	});

	$('#btnCancelar').click(function(){
		$('#txtId').val('');
		$('#txtUsuario').val('');
        $('#txtPass').val('');
        $('#txtPass2').val('');
        var select = $('#cmbPerfiles');
        select.val($('option:first', select).val());
        var select = $('#cmbPersonal');
        select.val($('option:first', select).val());
	});
    $('#guardarus').click(function(){
        var id= $('#txtId').val();
        var usu= $('#txtUsuario').val();
        var pass= $('#txtPass').val();
        var pass2= $('#txtPass2').val();
        var pin= $('#pinMesero').val();
        var perf = $('#cmbPerfiles option:selected').val();
        var pers = $('#cmbPersonal option:selected').val();
        if (id!=''||usu!=''||pass!=''||pass2!='') {
            if (pass==pass2) {
                if (pass.length>=6) {
                    $.ajax({
                        type:'POST',
                        url:'usuarios/guardar',
                        data:{ide:id,usua:usu,passw:pass,perfi:perf,perso:pers,pin1: pin},
                        async:false,
                        success:function(data){
                            toastr.success('Se a guardado el nuevo usuario', 'Hecho');
                            mostrarusuarios();
                            $('#btnCancelar').click();
                        }
                    });
                }else{
                    toastr.error('Debe de introducir mas de 6 caracteres','Error');
                }
                
            }else{
                toastr.error('Contraseñas deben de ser iguales','Error');
            }
            
        }else{
            toastr.error('Debe de completar todos los campos','Error');
        }
    });
    function mostrarusuarios(){
       $.ajax({
            type:'POST',
            url:'usuarios/mostrarusuarios',
            //data:{usua:usu},
            async:false,
            success:function(data){
                $('#viewusuarios').html(data)
            }
        }); 
    }
});

function verificarusu(){
    var usu = $('#txtUsuario').val();
    $.ajax({
        type:'POST',
        url:'usuarios/verfusu',
        data:{usua:usu},
        async:false,
        success:function(data){
            console.log(data);
            if (data==0) {
                toastr.error('El usuario ya existe','Error');
                $( "#guardarus" ).prop( "disabled", true );
            }else{
                $( "#guardarus" ).prop( "disabled", false );
            }
        }
    });

}
function datosclic(id){
    //var id = $(this).attr('data-id');
        $.ajax({
            type:'POST',
            url:'usuarios/consultar',
            data:{id:id},
            async:false,
            success:function(data){
                var array = $.parseJSON(data);
                $('#txtId').val(array.id);
                $('#txtUsuario').val(array.usuario);
                $('#txtPass').val(array.pass);
                $('#txtPass2').val(array.pass);
                $('#pinMesero').val(array.pin);
                $('#cmbPerfiles option[value='+array.idPerfil+']').prop('selected','true');
                $('#cmbPersonal option[value='+array.idPersona+']').prop('selected','true');
            }
        });
}
