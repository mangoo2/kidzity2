<style type="text/css">
  .galeriasimg{height: 249px;border: 4px;border-bottom-style:ridge;border-left-style:ridge;border-right-style:ridge;border-top-style:ridge; border-radius: 18px;  }
  .galeriasimg:hover{-webkit-box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);-moz-box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);}
  .galeriasimg h2{    text-align: center;    margin-top: 30%;    color: white;    text-shadow: 0 1px 0 #ccc,               0 2px 0 #c9c9c9,               0 3px 0 #bbb,               0 4px 0 #b9b9b9,               0 5px 0 #aaa,               0 6px 1px rgba(0,0,0,.1),               0 0 5px rgba(0,0,0,.1),               0 1px 3px rgba(0,0,0,.3),               0 3px 5px rgba(0,0,0,.2),               0 5px 10px rgba(0,0,0,.25),               0 10px 10px rgba(0,0,0,.2),               0 20px 20px rgba(0,0,0,.15);  }
  .input-group > .input-group-prepend > .btn, .fc .input-group > .input-group-prepend > button, .input-group > .input-group-prepend > .input-group-text, .input-group > .input-group-append:not(:last-child) > .btn, .fc .input-group > .input-group-append:not(:last-child) > button, .input-group > .input-group-append:not(:last-child) > .input-group-text, .input-group > .input-group-append:last-child > .btn:not(:last-child):not(.dropdown-toggle), .fc .input-group > .input-group-append:last-child > button:not(:last-child):not(.dropdown-toggle), .input-group > .input-group-append:last-child > .input-group-text:not(:last-child) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  .input-group-text{    font-size: 1rem;    font-weight: 400;    line-height: 1.5;    display: -webkit-box;    display: -webkit-flex;    display:    -moz-box;    display: -ms-flexbox;    display:         flex;    margin-bottom: 0;    padding: .375rem .75rem;    text-align: center;    white-space: nowrap;    color: #495057;    border: 1px solid #ced4da;    border-radius: .25rem;     background-color: #e9ecef;    -webkit-box-align: center;    -webkit-align-items: center;       -moz-box-align: center;    -ms-flex-align: center;            align-items: center;}
</style>
<div class="row">
  <div class="col-md-12 regresaprincipal">
    <h2><button class="cursor btn btn-raised btn-info btn-min-width mr-1 mb-1">Agregar nuevo producto</button></h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title"> Ventas</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                <!--------//////////////-------->
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="cantidad">Empleado</label>
                        <select class="form-control" id="personalId" name="personalId">
                          <option value="0" >Selecciona empleado</option>
                          <?php foreach ($empleados as $item){ ?>
                           <option value="<?php echo $item->personalId; ?>" ><?php echo $item->nombre; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>  
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="entercodigo"><i class="fa fa-barcode fa-2x"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Cósdigo" aria-describedby="entercodigo" id="codigobarras">
                      </div>
                    </div>
                  </div>
                  <div class="row panelventa1">
                    
                      <?php foreach ($productospadres->result() as $item){ ?>
                        <div class="col-md-3 galeriasimg flipInX animated classproducto" data-idcate="<?php echo $item->productopId; ?>" style="background: url('<?php echo base_url(); ?>public/img/categoria/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;">
                          <h2><?php echo $item->categoria;?></h2>
                        </div>
                      <?php } ?>
                    
                  </div>
                  <div class="row panelventa2" style="display: none;">
                    
                  </div>
                  <div class="row panelventa3" style="display: none;">
                    <div class="col-md-12">
                      <table class="table table-hover" id="tableproductosv">
                        <thead>
                          <tr>
                            <td>Cantidad</td>
                            <td></td>
                            <td>Producto</td>
                            <td>Precio</td>
                            <td>Total</td>
                            <td></td>
                          </tr>
                        </thead>
                        <tbody class="addselectedprod">
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-10"></div>
                      <div class="col-md-2">
                        <h3>Total: $<span class="totalproductos">00.00</span></h3>
                        <input type="hidden" id="totalproductos" value="0">
                      </div>
                      <div class="col-md-10"></div>
                      <div class="col-md-2">
                      </div>
                      <div class="col-md-10"></div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-4"></div>
                      <div class="col-md-4">
                        <button type="button" class="btn btn-raised btn-success btn-min-width mr-12 mb-12 col-md-12" id="vender">Vender</button>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    
                  </div>
                  
                <!--------//////////////-------->
              </div>
          </div>
      </div>
    </div>
</div>
