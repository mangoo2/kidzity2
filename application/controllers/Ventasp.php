 <?php 

defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Ventasp extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
    }

    public function index(){
            $data['productospadres']=$this->ModeloVentas->getproductopadre();
    	    $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('ventas/ventasp',$data);
            $this->load->view('templates/footer');
            $this->load->view('ventas/jsventasp');
            unset($_SESSION['pro']);
            $_SESSION['pro']=array();
            unset($_SESSION['can']);
            $_SESSION['can']=array();
    }
    function cargarproductos(){
        $id = $this->input->post('id');
        $respuesta=$this->ModeloVentas->getproductoshijos($id);
        foreach ($respuesta->result() as $item){ ?>
        <div class="col-md-3 galeriasimg flipInX animated classproductohijo" data-idcate="<?php echo $item->productoid; ?>" style="background: url('<?php echo base_url(); ?>public/img/productos/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;" onclick="productoselected(<?php echo $item->productoid; ?>)" >
            <h2><?php echo $item->producto;?></h2>
        </div>
      <?php }
    }
    function agregarproductos(){
        $id = $this->input->post('id');
        $des=$this->input->post('des');
        //log_message('error', '....'.$this->input->post('des'));
        if ($des=='') {
            $desc=1;
        }else{
            $desc=0;
        }
        
        $cant=1;
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','productoid',$id);
        foreach ($resultado->result() as $item) {
            if ($desc==0) {
                $precioventa=0;
            }else{
                $precioventa=$item->precio_venta;
            }
           
            $oProducto = array('productoid' =>$item->productoid,
                'codigo' =>$item->codigo,
                'producto' =>$item->producto,
                'descripcion' =>$item->descripcion,
                'precio_venta' =>$precioventa,
                'img' =>$item->img
            );
        }
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
        }
        //==========================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
            $precio=$fila['precio_venta'];
            $cantotal=$Cantidad*$precio;
            ?>
                <tr class="producto_<?php echo $count;?>">
                    <td>
                        <input 
                            type="number" 
                            name="vscanti" 
                            id="vscanti" 
                            value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                        <input type="hidden" id="idproductos" value="<?php echo $fila['productoid'];?>">
                    </td>
                    <td style="background: url('<?php echo base_url(); ?>public/img/productost/<?php echo $fila['img'];?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;"></td>
                    <td><?php echo $fila['producto'];?></td>
                    <td>$ <input 
                                type="text" 
                                name="vsprecio" 
                                id="vsprecio" 
                                value="<?php echo $precio;?>" 
                                class="editar"
                                readonly 
                                style="background: transparent;border: 0px;width: 100px;"
                                >
                    </td>
                    <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>
                    <td>
                        <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>)">
                            <i class="ft-trash font-medium-3"></i>
                        </a>
                    </td>
                </tr>
            <?php
            $count++;
        }
    }
    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
    }
    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }
    function viewproducto(){
        $codigo =$this->input->post('codigo');
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','codigo',$codigo);
        $idc=0;
        foreach ($resultado->result() as $item) {
            $idc=$item->productoid;
        }
        echo $idc;
    }
    function verificarsaldo(){
        $codigo =$this->input->post('codigo');
        echo $this->ModeloCatalogos->verificarsaldo($codigo);
    }
    function addventas(){
        $monto_total=$this->input->post('monto_total');
        $pulcera=$this->input->post('pulcera');
        $metodo=$this->input->post('metodo');
        $productos=$this->input->post('productos');
        $idventas=$this->ModeloVentas->addventas($monto_total,$pulcera,$metodo);
        $DATA = json_decode($productos);
        for ($i = 0; $i < count($DATA); $i++) {
            $idproductos=$DATA[$i]->idproductos;
            $cantidad=$DATA[$i]->cantidad;
            $precio=$DATA[$i]->precio;
            $this->ModeloVentas->updateproducto($idproductos,$cantidad);
            $this->ModeloVentas->addventasdetalle($idventas,$idproductos,$cantidad,$precio,0);
        }
        $this->productoclear();
        echo $idventas;
    }
    function searchproducto(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike('producto_hijo','producto',$pro);
        //echo $results;
        echo json_encode($results->result());
    }
}
?>