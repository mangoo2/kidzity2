var ruta=$('#ruta').val();
$('#Paquete').change(function() {
	console.log("holi");
    if ($('#Paquete').val() == "") {
          $('#IdPaquete').val("");
          $('#nombre').val("");
          $('#Precio').val("");
          $('#Descripcion').val("");
    }else{
	var id=$('#Paquete').val();
	$.ajax({
		type: 'POST',
		url: 'Paquetes/GetData',
		data: {
			Id: id
		},
		async: false,
		statusCode: {
            404: function(data) {
               toastr.error('Error', '404');
            },
            500: function() {
               toastr.error('Error', '500');
            }
        },
        success: function(data) {
        	console.log(data);
        	var arr= $.parseJSON(data);
        	console.log(arr[0].Nombre); 
        	$('#IdPaquete').val(arr[0].IdPaquete);
        	$('#nombre').val(arr[0].Nombre);
        	$('#Precio').val(arr[0].Precio);
        	$('#Descripcion').val(arr[0].Detalle);
        },
        error: function(jqXHR, estado, error) {
            console.log("Estado3: " + estado);
            console.log(jqXHR);
            console.log("Error3: " + error);
        },
	});
    }
});

$('#saveFiesta').click(function(){
	var ru="";
	var data=$('#formPaquete').serialize();
	 if ($('#IdPaquete').val() == "") {
	 	console.log("nuevo");
	 	var ru='CrearFiesta';
	 	var mensaje="Agregado Correctamente";
	 }else{
	 	console.log("edicion");
	 	var ru='EditarFiesta';
	 	var mensaje="Editado Correctamente";
	 }
	$.ajax({
		type: 'POST',
		url: 'Paquetes/'+ru+'',
		data: data,
		async: false,
		statusCode: {
            404: function(data) {
               toastr.error('Error', '404');
            },
            500: function() {
               toastr.error('Error', '500');
            }
        },
        success: function(data) {
        	toastr.success(mensaje);
            
            setTimeout (location.reload(), 3000); 
        },
        error: function(jqXHR, estado, error) {
            console.log("Estado3: " + estado);
            console.log(jqXHR);
            console.log("Error3: " + error);
        },
	});
});

$('#Delete').click(function(){
    var Idd=$('#IdPaquete').val();
    $.ajax({
        type: 'POST',
        url: 'Paquetes/DeleteFiesta',
        data:{
            IdPaquete: Idd,
        },
        async: false,
        statusCode: {
            404: function(data) {
               toastr.error('Error', '404');
            },
            500: function() {
               toastr.error('Error', '500');
            }
        },
        success: function(data) {
            toastr.success('Borrado correctamente');
            
            setTimeout (location.reload(), 3000); 
        },
        error: function(jqXHR, estado, error) {
            console.log("Estado3: " + estado);
            console.log(jqXHR);
            console.log("Error3: " + error);
        },
    }); 
});