var base_url= $('#base_url').val();
$(document).ready(function(){
	$('#pulcerasalida').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				$.ajax({
			        type: 'POST',
			        url: base_url + 'Salida/view',
			        data: {
			            id: $('#pulcerasalida').val()
			        },
			        async: false,
			        statusCode: {
			            404: function(data) {
			                toastr.error('Error', '404');
			            },
			            500: function(data) {
			                toastr.error('Error', '500');
			            },
			        },
			        success: function(data) {
			            $('.infonpulcera').html(data);
			        },
			        error: function(jqXHR, estado, error) {
			            console.log(estado);
			            console.log(jqXHR);
			            console.log(error);

			        },

			    });

			}
	});
	
});
	setInterval(function(){ Horas() }, 1000);
	function Horas() {
	    var cadena="<i class='ft-clock'></i>"
	    var Tiempo=new Date();
	    var hora=Tiempo.getHours();
	    var minuto=Tiempo.getMinutes();
	    var seg=Tiempo.getSeconds();
	    var MarK="";
	    str_minuto=new String(minuto);
	    str_seg=new String(seg);
	    if (str_minuto.length==1) {
	      minuto="0"+minuto;
	    }
	    if (str_seg.length==1) {
	      seg="0"+seg;
	    }
	    if (hora <= 12) {
	      MarK="AM";
	    }
	    else{
	      MarK="PM";
	      str_hora=new String(hora);
	      hora=hora-12;
	    }
	    var Reloj="<span class='zoomInUp animated'>"+cadena+" "+hora+":"+minuto+":"+seg+" "+MarK+"</span>";
	    $('#Hora').html(Reloj);
	}
function limpiar(){
	$("#pulcerasalida").val('');
	$("#pulcerasalida").focus();
	$('.infonpulcera').html('');

}
function salir(){
	var totalpago=$('#totalpago').val();
	var montopago=parseFloat($('#montopago').val())+parseFloat($('#compensacion').val());
	if (montopago>=totalpago) {
		$.ajax({
	        type: 'POST',
	        url: base_url + 'Salida/salir',
	        data: {
	            id: $('#compranId').val(),
	            pago: $('#totalpago').val(),
	            tiempoex: $('#tiempoextra').val(),
	            idpersonal: $('#idpersonalid').val()
	        },
	        async: false,
	        statusCode: {
	            404: function(data) {
	                toastr.error('Error', '404');
	            },
	            500: function(data) {
	                toastr.error('Error', '500');
	            },
	        },
	        success: function(data) {
	            $('.infonpulcera').html(data);
	            limpiar();
	            toastr.success('Salida realizada Correctamente','Hecho!');
	            if (montopago>0) {
	            	//$('.ticketsalida').html('<iframe src="'+base_url+'Ticket/salida?comp='+data+'" class="iframevisitantes"></iframe>');
	            	popup = window.open(base_url+"Ticket/salida?comp="+data,"popup","width=700,height=300,scrollbars=yes");
                    popup.focus(); 
	            }
	        },
	        error: function(jqXHR, estado, error) {
	            console.log(estado);
	            console.log(jqXHR);
	            console.log(error);

	        },

	    });
	}else{
		toastr.error('No es Posible salir', 'Error');
	}

}
