<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPersonal extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getpersonal() {
        $strq = "CALL SP_GET_ALL_PERSONAL";
        $query = $this->db->query($strq);
        return $query;
    }
    
    function personalview($id) {
        $strq = "SELECT *  FROM personal where personalId='$id'";
        $query = $this->db->query($strq);
        return $query;
    }
    public function personaldelete($id){
            $strq = "CALL SP_DEL_PERSONAL($id)";
            $this->db->query($strq);
    }
    public function suspencionpersonal($id,$mot){
            $strq = "UPDATE personal SET suspendido=1,motivosuspencion='$mot' where personalId=$id";
            $this->db->query($strq);
    }
    function notificarasignacion() {
        $strq = "SELECT * FROM config";
        $query = $this->db->query($strq);
        $this->db->close();
        $notf =0;
        foreach($query->result() as $valor){
            $notf=$valor->notfig_tutor;
        }
        return $notf;
    }
    public function reactivarpersonal($id){
        $strq = "UPDATE personal SET suspendido=0 where personalId=$id";
        $this->db->query($strq);
    }

    public function Horarios($id){
        $strq = "SELECT *  FROM conexiones where IdPersonal='$id' ORDER BY FechaEntrada DESC LIMIT 10";
        $query = $this->db->query($strq);
        return $query;
    }
    function personaladdimg($cargaimagen,$idper){
        $strq = "UPDATE personal SET foto='$cargaimagen' where personalId=$idper";
            $this->db->query($strq);
    }
    public function getallcreditos($idper,$fechainicio,$fechafin)
    {
        $sql = "SELECT c.reg,p.producto,cd.cantidad,cd.precio FROM creditos AS c 
                INNER JOIN creditos_detalle AS cd ON cd.idc = c.idc
                INNER JOIN producto_hijo AS p ON p.productoid = cd.productoid
                WHERE c.personalIdc = $idper AND cd.activo = 1 AND c.reg BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query = $this->db->query($sql);
        return $query->result();        
    } 
}
