var base_url=$('#base_url').val();
$(document).ready(function () {
        "use strict";   
        var form_register = $('#frmPersonal');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                nombre: {
                    //minlength: 7,
                    required: true
                    //email: true
                },
                email:{
                    email: true
                }

                /*,
                apellido:{
                    required: true  
                },
                calle:{
                    required: true  
                },
                noexte:{
                    required: true  
                },
                estado:{
                    required: true  
                },
                municipio:{
                    required: true  
                },
                
                telcel:{
                    required: true
                },
                telcasa:{
                    required: true
                },
                colonia:{
                    required: true
                },
                cp:{
                    required: true
                }
                */

            },
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit   
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },
            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            },
            submitHandler: function (form) {
                
                
                //error1.hide();
            }
        });
        $('#savep').click(function(event) {
            var $valid = $("#frmPersonal").valid();
            if ($valid) {
                var id = $('#persolnalid').val();                
                $.ajax({
                    type:'POST',
                    url: 'addpersonal',
                    data: {personalId:id,
                            nombre:$('#nombre').val(),
                            Email:$('#email').val(),
                            fechanacimiento:$('#fechanacimineto').val(),
                            area: $('#areas option:selected').val(),
                            domicilio:$('#domicilio').val(),
                            horae:$('#horae').val(),
                            horas: $('#horas').val(),
                            telefono:$('#tel').val(),
                            movil:$('#movil').val(),
                        },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        console.log(data);
                        var personalId=data;
                        if ($('#imgpro')[0].files.length > 0) {
                            var inputFileImage = document.getElementById('imgpro');
                            var file = inputFileImage.files[0];
                            var data = new FormData();
                            data.append('img',file);
                            data.append('personalId',personalId);
                            $.ajax({
                                url:'EditImage',
                                type:'POST',
                                contentType:false,
                                data:data,
                                processData:false,
                                cache:false,
                                success: function(data) {
                                  var array = $.parseJSON(data);
                                            if (array.ok=true) {
                                              $(".fileinput").fileinput("clear");
                                              toastr.success('Guardado Correctamente','Hecho!');
                                              
                                              setTimeout(function(){ window.location = base_url+"Personal"; }, 3000);
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                            var data = JSON.parse(jqXHR.responseText);
                                            console.log(data);
                                            if (data.ok=='true') {
                                              $(".fileinput").fileinput("clear");
                                            }else{
                                              toastr.error('Error', data.msg);
                                            }          
                                }
                            });
                        }else{
                            toastr.success('Guardado Correctamente','Hecho!');
                            window.location = base_url+"Personal";
                            location.href=base_url+'Personal'; 
                            
                        }
                        
                    }
                });
                
            }
        });

        $('#aceptareliminar').click(function(event) {
            var idd=$('#pesonaliddelete').val();
            $.ajax({
                type:'POST',
                url: 'Personal/deletepersonal',
                data: {id:idd},
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    console.log(data);
                    //location.reload();
                    toastr.success('Hecho!', 'Se Elimino correctamente');
                    var row = document.getElementById('trper_'+idd);
                                row.parentNode.removeChild(row);
                    
                }
            });
        });
        $('#aceptarsuspender').click(function(event) {
            var idd=$('#pesonalidsuspender').val();
            var motivo=$('#motivosusmencion').val();
            if (motivo!='') {
                $.ajax({
                    type:'POST',
                    url: 'Personal/suspencionpersonal',
                    data: {id:idd,mot:motivo},
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        console.log(data);
                        location.reload();
                        toastr.success('Hecho!', 'Se suspendio correctamente');
                        //var row = document.getElementById('trper_'+idd);
                        //            row.parentNode.removeChild(row);
                        
                    }
                });
            }else{
                toastr.error('Error!', 'Debe de ingresar un motivo');
            }
            
        });

});
function personaldelete(id){
    $('#pesonaliddelete').val(id);
    $('#modalconfirmacion').modal();
}
function suspender(id){
    $('#pesonalidsuspender').val(id);
    $('#modalsuspender').modal();
}
function dessuspender(idd){
    $.ajax({
        type:'POST',
        url: 'Personal/reactivarpersonal',
        data: {id:idd},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            console.log(data);
            location.reload();
            toastr.success('Hecho!', 'Se Reactivo correctamente');            
        }
    });
}

function Horarios(id){
    $.ajax({
        type:'POST',
        url: 'Personal/HorarioPersonal',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            console.log(data);
            $('#Horarios').modal();
            $('#place').html(data);
        }
    });
}
function Creditos(idpersonal) {
    $('#modalcreditos').modal();
    $('#idpersonal_c').val(idpersonal);
    $('.mostrarcreditos').html('');
}
function generarcreditos() {
    $.ajax({
        type:'POST',
        url: base_url+'Personal/creditostabla',
        data: {idpersona:$('#idpersonal_c').val(),fechainicio:$('#fechainicio').val(),fechafin:$('#fechafin').val()},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.mostrarcreditos').html(data);
            $('#tables_creditos').dataTable();
        }
    });
}