$(document).ready(function() {
    $('#btnabrir').click(function(){
      var Direccion='InsertTurno';
      if ($('#ID').length){
        console.log("El input existe");
        Direccion='UpdateTurno';
      }else{
        console.log("el input no existe")
        Direccion='InsertTurno';
      }
        var datos=$('#formTurno').serialize();
        $.ajax({
            type: 'POST',
            url: 'Turnos/'+Direccion,
            data: datos,
            async: false,
            statusCode: {
              404: function(data) {
                  toastr.error('Error', '404');
              },
              500: function() {
                  toastr.error('Error', '500');
              }
            },
            success: function(data){
                toastr.success('Turno agregado');

                $('#cantidad').prop('disabled', true);
                $('#btnabrir').prop('disabled', true);
                $('#btncerrar').prop('disabled', false);
            },
            error: function(jqXHR, estado, error){
                    console.log("Estado: "+estado);
                    console.log(jqXHR);
                    console.log("Error: "+error);
            },
        });
    });
    $('#btncerrar').unbind().click(function(){
                var fechac = $('#fecha').val();
                var horac = $('#horaa').val();
                var id = $('#id').val();
                var Direccion='editarTurno';
                $.ajax({
                type: 'POST',
                async: false,
                url: 'Turnos/'+Direccion,
                data:{id:id,fechacierre:fechac,horac:horac},
                dataType: 'json',
                success: function(data){
                        toastr.success('Turno cerro!', 'Hecho');
                        $('#cantidad').val('');
                        $('#id').val('');
                        $('#btnabrir').prop('disabled', false);
                        $('#btncerrar').prop('disabled', true); 
                        $('#cantidad').prop('disabled', false);
                  },
                error: function(jqXHR,estado,error){
                  console.log(jqXHR);
                  console.log(estado);
                  console.log(error);
                }
               });
    });
});
function verificar(){
    var nombre = $('#punto_venta').val();
                var Direccion='mostrar_cantidad';
                $.ajax({
                type: 'POST',
                async: false,
                url: 'Turnos/' + Direccion,
                data:{punto_venta:nombre},
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    if (data!=null) {
                        $('#cantidad').val(data.cantidad);
                        $('#id').val(data.id);
                        $('#btnabrir').prop('disabled', true);
                        $('#cantidad').prop('disabled', true);
                        $('#btncerrar').prop('disabled', false);
                        $('#btninspeccionar').prop('disabled', false);
                    }else{
                        $('#cantidad').val('');
                        $('#id').val('');
                        $('#btnabrir').prop('disabled', false);
                        $('#cantidad').prop('disabled', false);
                        $('#btncerrar').prop('disabled', true);
                        $('#btninspeccionar').prop('disabled', true);
                    }
                  },
                error: function(jqXHR,estado,error){
                  console.log(jqXHR);
                  console.log(estado);
                  console.log(error);
                }
            });
}
function listado_turno_mesero(){
    $.ajax({
                type: 'POST',
                async: false,
                url: 'Turnos/reporte',
                data:{
                  reporte:$('#punto_venta option:selected').val(),
                  cinicial:$('#cantidad').val()
                },
                 async: false,
                statusCode:{
                  404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                  500: function(){ toastr.error('Error', '500');}
                }, 
                success: function(data){
                    console.log(data);
                    $('.datostabla').html(data);
                    $('#data-tables3').DataTable();
                    $('#data-tables2').DataTable();
                  },
                error: function(jqXHR,estado,error){
                  console.log(jqXHR);
                  console.log(estado);
                  console.log(error);
                }
    });
}
function ventas(id,punto_venta){
    $.ajax({
        type:'POST',
        url: 'ListadoTurno/VentasTurno',
        data: {id:id, punto_venta:punto_venta},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            console.log(data);
            $('#VentasH').modal();
            $('#place').html(data);
            $('#data-tableTurno').DataTable();
        }
    });
}