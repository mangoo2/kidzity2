<?php
ini_set("session.cookie_lifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia
ini_set("session.gc_maxlifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia
//
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function login($usu,$pass) {
        $strq = "CALL SP_GET_SESSION('$usu');";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId; 
            $foto = $row->foto; 
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=$perfil;
                $_SESSION['idpersonal']=$idpersonal;
                $_SESSION['foto']=$foto;
                $active="UPDATE personal SET conect=1 WHERE personalId = $idpersonal";
                $this->db->query($active);
                $this->db->close();
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        /*
        if ($count==0) {
            $residentes = $this->db->query("CALL SP_GET_SESSIONRESIDENTES('$usu','$pass')");
            $this->db->close();

            foreach ($residentes->result() as $row) {

                $id = $row->AspiranteId;
                $nom =$row->Nombre;
            
            
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=3;
                $_SESSION['idpersonal']=0;
                $count=1;
            
           
            }
        }
        */
        if ($count==1) {
            $this->verificar($idpersonal);
        }
        echo $count;
        //echo $strq;
    }
    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query;

    }

    public function salir($id){
        $this->verificar($id);
        $active="UPDATE personal SET conect=0 WHERE personalId = $id";
        $this->db->query($active);
        $this->db->close();
    }

    function verificar($idpersonal){
        date_default_timezone_set('America/Mexico_City');
        $fecha=date("Y-m-d");
        $hora=date("H:i:s");
        $query="";
        $verfi="SELECT * FROM conexiones WHERE IdPersonal=$idpersonal AND FechaEntrada='$fecha'";
        $Execute=$this->db->query($verfi);
            if ($Execute->num_rows()==0) {
                $query="INSERT INTO conexiones (`IdPersonal`, `FechaEntrada`, `HoraEntrada`) VALUES ('$idpersonal','$fecha','$hora')";
            }else{
                $query="UPDATE conexiones SET `FechaSalida`= '$fecha',`HoraSalida`= '$hora' WHERE FechaEntrada = '$fecha' AND IdPersonal=$idpersonal";  
            }
            
            $this->db->query($query);

    }
    function accesoadmin($pass){
        $sql = "SELECT * FROM usuarios where perfilId=1 or perfilId=2 ";
        $query = $this->db->query($sql);
        $verificado=0;
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $verificado=1;
            }

        }
        return $verificado;
    }

}
