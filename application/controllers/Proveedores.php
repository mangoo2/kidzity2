<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Proveedor/ModeloProveedor');
    }

    function index(){
         $pages=10;
         $this->load->library('pagination');
         $config['base_url'] = base_url().'Proveedores/view/';
         $config['total_rows'] = $this->ModeloProveedor->filasproveedor();
         $config['per_page'] = $pages;
         $config['num_links'] = 3;
         $config['first_link'] = 'Primera';
         $config['last_link'] = 'Última';
         $config["uri_segment"] = 3;
         $config['next_link'] = 'Siguiente';
         $config['prev_link'] = 'Anterior';
         $config['reuse_query_string'] = TRUE;
         $this->pagination->initialize($config);
         $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
         $data["Proveedores"] = $this->ModeloProveedor->ListProveedor($pagex,$config['per_page']);
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Proveedores/Proveedoreslista',$data);
        $this->load->view('Proveedores/jsProveedores');
        $this->load->view('templates/footer');
    }

    function Proveedoradd(){
        if (isset($_GET['Prov'])){
            $data['Texto']="Edicion de";
            $data['Boton']="Editar";
            $resultado=$this->ModeloProveedor->VerProveedor($_GET['Prov']);
            foreach ($resultado->result() as $item) {
                $data['ID']=$item->IdProveedor;
                $data['RazonSocial']=$item->RazonSocial;
                $data['Domicilio']=$item->Domicilio;
                $data['Ciudad']=$item->Ciudad;
                $data['EstadoV']=$item->Estado;
                $data['RFC']=$item->RFC;
                $data['Nombre']=$item->Nombre;
                $data['Correo']=$item->Correo;
                $data['Movil']=$item->Movil;
                $data['Notas']=$item->Notas;
            }

        }
        else{
            $data['Texto']="Agregar";
            $data['ID']="";
            $data['RazonSocial']="";
            $data['Domicilio']="";
            $data['Ciudad']="";
            $data['EstadoV']="";
            $data['RFC']="";
            $data['Nombre']="";
            $data['Correo']="";
            $data['Movil']="";
            $data['Notas']="";
        }

    	$data['Estado']=$this->ModeloCatalogos->getestados();
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Proveedores/Proveedoradd',$data);
        $this->load->view('Proveedores/jsProveedores');
        $this->load->view('templates/footer');	
    }

    function InsertProveedor(){
        $arr=$this->input->post();
        $this->ModeloProveedor->Insertar("proveedores",$arr);
    }

    function BuscarProveedor(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloProveedor->ProveedorSearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr>
            <td><?php echo $item->IdProveedor; ?></td>
            <td><?php echo $item->RazonSocial; ?></td>
            <td><?php echo $item->Nombre; ?></td>
            <td><?php echo $item->Movil; ?></td>
            <td>
             <div class="btn-group mr-1 mb-1">
              <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i></button>
              <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
              <a class="dropdown-item" href="<?php echo base_url();?>Proveedores/Proveedoradd?Prov=<?php echo $item->IdProveedor; ?>">Editar</a>
              <a onclick="DeleteProvedor( <?php echo $item->IdProveedor; ?>)" class="dropdown-item">Eliminar</a>
              </div>
             </div>
            </td>
          </tr>
          <?php }
    }

    function UpdateProveedor(){
        $data=$this->input->post();
        $ID=$data['ID'];
        unset($data['ID']);
        $this->ModeloProveedor->ProveedorUpdate($ID,$data);
    }

    function ProveedorDell(){
        $id=$this->input->post('id');
        $this->ModeloProveedor->DellProveedor($id);
    }
}
?>