<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (isset($_SESSION['logueo_kids'])) {
            $this->perfilid=$_SESSION['perfilid'];
            $this->idpersonal=$_SESSION['idpersonal']; 
        }else{
            redirect('/Sistema');
        }
    }
	public function index(){
        $data['ninost']=$this->ModeloCatalogos->ninosactivos();
        $data['adultost']=$this->ModeloCatalogos->adultosactivos();
        
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        if (!isset($_SESSION['perfilid'])) {
            $perfilview=0;
        }else{
            $perfilview=$this->perfilid;
        }
        if ($perfilview>=6 && $perfilview<=9) {
            
            $this->load->view('Catalogos/registro');
        }else{
            $this->load->view('Catalogos/catalogos',$data);
        }
        
        $this->load->view('templates/footer');
        
	
    }
    function pruebahorario(){
        echo date("Y-m-d h:i:s");
    }

    
 
}