<style type="text/css">
  #data-tables td{
    font-size: 13px;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Eventos</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-9"></div>
    <div class="col-md-1">
      <a href="<?php echo base_url(); ?>Eventos/Eventosadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo evento</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de eventos</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <table class="table table-striped table-hover" id="data-tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Total</th>
                <th>Precio extras</th>
                <th>Número de niños</th>
                <th>Número de niños activos</th>
                <th>Fecha de Evento</th>
                <th>Hora</th>
                <th>Hora fin</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="modalninoslistado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Niños</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 tablelistaninos">
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>