<?php
foreach ($configticket->result() as $item){
      $id_ticket = $item->id_ticket;
      $titulo = $item->titulo;
      $mensaje = $item->mensaje;
      $mensaje2 = $item->mensaje2;
      $fuente = $item->fuente;
      $tamano = $item->tamano;
      $margensup = $item->margensup;
      $margenlat = $item->margenlat;
    } 
?>

<div class="row">
    <div class="col-md-12">
      <h2>Ticket </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Configuración</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Titulo:</label>
                            <div class="col-sm-10 controls">
                              <textarea id="titulo" name="titulo" class="form-control"><?php echo $titulo;?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mensaje A:</label>
                            <div class="col-sm-10 controls">
                              <textarea id="mensaje1" name="mensaje1" class="form-control"><?php echo $mensaje; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mensaje B:</label>
                            <div class="col-sm-10 controls">
                              <textarea id="mensaje2" name="mensaje2" class="form-control"><?php echo $mensaje2; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Fuente:</label>
                          <div class="col-sm-8 controls">
                            <select id="fuente" class="form-control">
                              <option value="1" <?php if ($fuente==1) { echo "selected"; } ?> style="font-family: 'arial';">Arial</option>
                              <option value="2" <?php if ($fuente==2) { echo "selected"; } ?> style="font-family: Times New Roman;">Times New Roman</option>
                              <option value="3" <?php if ($fuente==3) { echo "selected"; } ?> style="font-family: 'Open Sans';">Open Sans</option>
                              <option value="4" <?php if ($fuente==4) { echo "selected"; } ?> style="font-family: 'Calibri'">Calibri</option>
                            </select>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Tamaño:</label>
                          <div class="col-sm-8 controls">
                            <select id="tamano" class="form-control">
                              <option value="7" <?php if ($tamano=='7') { echo "selected"; } ?>  style="font-size: 7px;">7</option>
                              <option value="8" <?php if ($tamano=='8') { echo "selected"; } ?> style="font-size: 8px;">8</option>
                              <option value="9" <?php if ($tamano=='9') { echo "selected"; } ?> style="font-size: 9px;">9</option>
                              <option value="10" <?php if ($tamano=='10') { echo "selected"; } ?> style="font-size: 10px;">10</option>
                              <option value="11" <?php if ($tamano=='11') { echo "selected"; } ?> style="font-size: 11px;">11</option>
                              <option value="12" <?php if ($tamano=='12') { echo "selected"; } ?> style="font-size: 12px;">12</option>
                              <option value="13" <?php if ($tamano=='13') { echo "selected"; } ?> style="font-size: 13px;">13</option>
                              <option value="14" <?php if ($tamano=='14') { echo "selected"; } ?> style="font-size: 14px;">14</option>
                            </select>
                          </div>  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Margen Superior:</label>
                          <div class="col-sm-8 controls">
                            <select id="margsup" class="form-control">
                              <option value="1" <?php if ($margensup=='1') { echo "selected"; } ?> >1</option>
                              <option value="2" <?php if ($margensup=='2') { echo "selected"; } ?> >2</option>
                              <option value="3" <?php if ($margensup=='3') { echo "selected"; } ?> >3</option>
                              <option value="4" <?php if ($margensup=='4') { echo "selected"; } ?> >4</option>
                              <option value="5" <?php if ($margensup=='5') { echo "selected"; } ?> >5</option>
                              <option value="6" <?php if ($margensup=='6') { echo "selected"; } ?> >6</option>
                              <option value="7" <?php if ($margensup=='7') { echo "selected"; } ?> >7</option>
                              <option value="8" <?php if ($margensup=='8') { echo "selected"; } ?> >8</option>
                              <option value="9" <?php if ($margensup=='9') { echo "selected"; } ?> >9</option>
                              <option value="10" <?php if ($margensup=='10') { echo "selected"; } ?> >10</option>
                              <option value="11" <?php if ($margensup=='11') { echo "selected"; } ?> >11</option>
                              <option value="12" <?php if ($margensup=='12') { echo "selected"; } ?> >12</option>
                              
                            </select>
                            
                          </div>  
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Margen Lateral:</label>
                          <div class="col-sm-8 controls">
                            <select id="marglat" class="form-control">
                              <option value="1" <?php if ($margenlat=='1') { echo "selected"; } ?> >1</option>
                              <option value="2" <?php if ($margenlat=='2') { echo "selected"; } ?> >2</option>
                              <option value="3" <?php if ($margenlat=='3') { echo "selected"; } ?> >3</option>
                              <option value="4" <?php if ($margenlat=='4') { echo "selected"; } ?> >4</option>
                              <option value="5" <?php if ($margenlat=='5') { echo "selected"; } ?> >5</option>
                              <option value="6" <?php if ($margenlat=='6') { echo "selected"; } ?> >6</option>
                              <option value="7" <?php if ($margenlat=='7') { echo "selected"; } ?> >7</option>
                              <option value="8" <?php if ($margenlat=='8') { echo "selected"; } ?> >8</option>
                              <option value="9" <?php if ($margenlat=='9') { echo "selected"; } ?> >9</option>
                              <option value="10" <?php if ($margenlat=='10') { echo "selected"; } ?> >10</option>
                              <option value="11" <?php if ($margenlat=='11') { echo "selected"; } ?> >11</option>
                              <option value="12" <?php if ($margenlat=='12') { echo "selected"; } ?> >12</option>
                            </select>
                            
                          </div>  
                        </div>
                      </div>
                    <div class="col-md-12">
                      <br>
                      <br>
                      <button class="btn btn-raised gradient-purple-bliss white" id="guardar" ><i class="icon-ok"></i> Guardar</button>
                    </div>
                    
                </div>
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
    var UR=$('#url').val();
    $('#guardar').click(function(){
        params = {};
        params.titulo = $('#titulo').val();
        params.mensaje1 = $('#mensaje1').val();
        params.mensaje2 = $('#mensaje2').val();
        params.fuente = $('#fuente option:selected').val();
        params.tamano = $('#tamano option:selected').val();
        params.margsup = $('#margsup option:selected').val();
        params.marglat = $('#marglat option:selected').val();
        $.ajax({
            type:'POST',
            url: '<?php echo base_url(); ?>Ticket/updateticket',
            data:params,
            async:false,
            success:function(data){
                console.log(data);
                toastr.success('Guardado Correctamente','Hecho!');

            }
        });
    });
});
</script>

