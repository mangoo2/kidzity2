var base_url= $('#base_url').val();
var statuscompra = $('#statuscompra').val();
var table;
$(document).ready(function() {
	table = $('#data-tables').DataTable();
	if(statuscompra==1){
		loadtable();	
	}else{
		loadtable0();
	}
	
});
function loadtable(){
    table.destroy();
    table=$('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
                "url": base_url+"ConsultAbierto/getlistConsultAbierto",
                type: "post",
                "data": {
                    'tipo':statuscompra,
                },
            },
        "columns": [
        		{"data": "compranId"},
				{"data": "compraId"},
				{"data": "nino"},
				{"data": "titular"},
				{"data": "tiempo"},
				{"data": "pagado",
					"render" : function (data,type,row) {
						var html='';
                        
	                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.pagado);
	                    return html;
					}
				},
				{"data": "fiesta"},
				{"data": "devolucion_monto"},
				{"data": "devolucion_motivo"},

                {"data": null,
                    "render" : function (data,type,row) {
                        var html='';
                            html+='<div class="btn-group mr-1 mb-1">\
				                      <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
				                      <i class="icon-settings"></i>\
				                      </button>\
				                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">';
				                        if(statuscompra==1){
				                        	html+='<a class="dropdown-item" href="#" onclick="Saldo('+row.titularId+');">Agregar saldo</a>\
				                          			<a class="dropdown-item" href="#" onclick="salida('+row.compranId+');">Salida manual</a>\
				                          			<a class="dropdown-item" href="#" onclick="devolucion('+row.compranId+','+row.pagado+');">Devolucion</a>';
				                        }
				                          
				                          	html+='<a class="dropdown-item" href="'+base_url+'Ticket/compra?comp='+row.compraId+'" target="_blank">Reimpresión</a>\
				                      </div>\
				                    </div>';
                        return html;
                    }
                }
            ],
        order: [[0, 'desc']]
    });
}
function loadtable0(){
    table.destroy();
    table=$('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
                "url": base_url+"ConsultAbierto/getlistConsultAbierto",
                type: "post",
                "data": {
                    'tipo':statuscompra,
                },
            },
        "columns": [
        		{"data": "compranId"},
				{"data": "compraId"},
				{"data": "nino"},
				{"data": "titular"},
				{"data": "tiempo"},
				{"data": "pagado",
					"render" : function (data,type,row) {
						var html='';
                        
	                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.pagado);
	                    return html;
					}
				},
				{"data": "fiesta"},
				
                {"data": null,
                    "render" : function (data,type,row) {
                        var html='';
                            html+='<div class="btn-group mr-1 mb-1">\
				                      <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
				                      <i class="icon-settings"></i>\
				                      </button>\
				                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">';
				                          	html+='<a class="dropdown-item" href="'+base_url+'Ticket/compra?comp='+row.compraId+'" target="_blank">Reimpresión</a>\
				                      </div>\
				                    </div>';
                        return html;
                    }
                }
            ],
        order: [[0, 'desc']]
    });
}