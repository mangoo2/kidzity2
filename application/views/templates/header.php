<!DOCTYPE html>
<html lang="es" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="author" content="agb">
        <title>kid's universe</title>
        <link rel="icon" href="<?php echo base_url(); ?>public/img/fav.png" sizes="32x32" />
        <link rel="icon" href="<?php echo base_url(); ?>public/img/fav.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>public/img/fav.png" />
        <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>public/img/fav.png" />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><!--colocar el mismo color a la barra de estado superiori-->
        <meta name="description" content="Sistema de punto de venta kid's universe">
        <meta name="theme-color" content="#ef1e03"/>
        <meta name="msapplication-navbutton-color" content="#ef1e03"> 
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/switch.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">-->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/datatable/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/datatable/responsive.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/toastr.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chosen.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/balloon.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
        <!-- END VENDOR CSS-->
        <!-- BEGIN APEX CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">  
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
        <!--<script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>-->
        <script src="<?php echo base_url(); ?>app-assets/vendors/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/theme.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/wizard.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/animate.css">
        <!-- END APEX CSS-->
        <!-- BEGIN Page Level CSS-->
        <!-- END Page Level CSS-->
        <!-- BEGIN Custom CSS-->
        <!-- END Custom CSS-->
        <input type="hidden" name="ssessius" id="ssessius" value="<?php if(isset($_SESSION['perfilid'])){ echo $_SESSION['perfilid'];} ?>" readonly>
    </head>
    <style type="text/css">
        .vd_red{
            font-weight: bold;
            color: red;
          }
          .valid{
            color: #009688!important; 
          } 
      .card{
        background: white url(<?php echo base_url(); ?>public/img/ops.webp) !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
      }
      .card .card-header,.card .card-body{
        background-color: rgba(251, 251, 251, 0.93) !important;
      }
      .navbar{
        background: url(<?php echo base_url(); ?>public/img/ops.webp) !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
      }
    </style>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <body data-col="2-columns" class=" 2-columns ">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <?php 
            $menucompacto=1;// 1 COMPACTA ,0 EXPANDIDA
            if ($menucompacto==1) {
                $GLOBALS['wrapper']='nav-collapsed menu-collapsed';
                $GLOBALS['menutoggle']='collapsed';
                $GLOBALS['menutoggleico']='toggle-icon ft-toggle-left';
            }else{
                $GLOBALS['wrapper']='';
                $GLOBALS['menutoggle']='expanded';
                $GLOBALS['menutoggleico']='ft-toggle-right toggle-icon';
            }
        ?>
            <div class="wrapper <?php echo $GLOBALS['wrapper'];?>">
<link href="<?php echo base_url(); ?>public/css/spinner.css" rel="stylesheet">
<div class="sk-cube-grid" style="display: none;">
  <div class="sk-cube sk-cube1"></div>
  <div class="sk-cube sk-cube2"></div>
  <div class="sk-cube sk-cube3"></div>
  <div class="sk-cube sk-cube4"></div>
  <div class="sk-cube sk-cube5"></div>
  <div class="sk-cube sk-cube6"></div>
  <div class="sk-cube sk-cube7"></div>
  <div class="sk-cube sk-cube8"></div>
  <div class="sk-cube sk-cube9"></div>
</div>
<input type="password" style="display:none">