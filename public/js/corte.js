var base_url = $('#base_url').val();
$(document).ready(function() {
	$('#genera').click(function(event) {
            $('.sk-cube-grid').show( "slow" );
            var tipov=$('input[name=tipov]:checked').val();
            var vende=$('#vendedorselected').val();
            var metodopago = $('#metodopago option:selected').val();
            var metodopago2 = $('#metodopago2 option:selected').val();
            var datos = $('#formcorte').serialize()+'&tipov='+tipov+'&vende='+vende+'&metodopago='+metodopago+'&metodopago2='+metodopago2;
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: base_url+'Corte/genera',
                    data: datos,
                    async: false,
                    statusCode: {
                        404: function(data) {toastr.error('Error', '404');},
                        500: function() {toastr.error('Error', '500');}
                    },
                    beforeSend:function(data){
                        $('.sk-cube-grid').show( "slow" );
                    },
                    success: function(data) {
                        $('.datoscorte').html(data);
                        $('.sk-cube-grid').hide( "slow" );
                        $('#tabla').DataTable({
                                    dom: 'Bfrtip',
                                    buttons: [
                                            //{extend: 'excel',title: 'Reporte de saldos'},
                                            {extend: 'excel'},
                                            {extend: 'pdfHtml5',
                                                orientation: 'landscape',
                                                pageSize: 'LEGAL'

                                            },
                                            {extend:'print',
                                                customize: function ( win ) {
                                                    var adddatos =$('.adddatos').html();
                                                    $(win.document.body)
                                                        .append(
                                                            adddatos
                                                        );
                                 
                                                    
                                                }
                                            },
                                            'pageLength'
                                    ]
                                });
                        $('#tabla2').DataTable({
                                    dom: 'Bfrtip',
                                    buttons: [
                                            {extend: 'excel'},
                                            {extend: 'pdf'},
                                            {extend:'print',
                                                customize: function ( win ) {
                                                    var adddatos =$('.adddatos').html();
                                                    $(win.document.body)
                                                        .append(
                                                            adddatos
                                                        );
                                 
                                                    
                                                }
                                            },
                                            'pageLength'
                                    ]
                                });
                        $('#tabla3').DataTable({
                                    dom: 'Bfrtip',
                                    buttons: [
                                            //{extend: 'excel',title: 'Reporte de saldos'},
                                            {extend: 'excel', footer: true},
                                            {
                                                extend: 'pdfHtml5',
                                                orientation: 'landscape',
                                                pageSize: 'LEGAL', 
                                                footer: true
                                            },
                                            {extend:'print',
                                                customize: function ( win ) {
                                                    var adddatos =$('.adddatos').html();
                                                    $(win.document.body)
                                                        .append(
                                                            adddatos
                                                        );
                                 
                                                    
                                                }
                                            },
                                            'pageLength'
                                    ],
                                    /*
                                    "footerCallback": function ( row, data, start, end, display ) {
                                        var api = this.api(), data;
                                        bttondevolucion();
                                        // Remove the formatting to get integer data for summation
                                        var intVal = function ( i ) {
                                            return typeof i === 'string' ?
                                                i.replace(/[\$,]/g, '')*1 :
                                                typeof i === 'number' ?
                                                    i : 0;
                                        };
                             
                                        // Total over all pages
                                        total = api
                                            .column( 5 )
                                            .data()
                                            .reduce( function (a, b) {
                                                return intVal(a) + intVal(b);
                                            }, 0 );
                                        total1 = api
                                            .column( 6 )
                                            .data()
                                            .reduce( function (a, b) {
                                                return intVal(a) + intVal(b);
                                            }, 0 );
                                        total2 = api
                                            .column( 13 )
                                            .data()
                                            .reduce( function (a, b) {
                                                return intVal(a) + intVal(b);
                                            }, 0 );
                             
                                        // Total over this page
                                        pageTotal = api
                                            .column( 5, { page: 'current'} )
                                            .data()
                                            .reduce( function (a, b) {
                                                return intVal(a) + intVal(b);
                                            }, 0 );
                                        pageTotal1 = api
                                            .column( 6, { page: 'current'} )
                                            .data()
                                            .reduce( function (a, b) {
                                                return intVal(a) + intVal(b);
                                            }, 0 );
                                        pageTotal2 = api
                                            .column( 13, { page: 'current'} )
                                            .data()
                                            .reduce( function (a, b) {
                                                return intVal(a) + intVal(b);
                                            }, 0 );
                             
                                        // Update footer
                                        $( api.column( 5 ).footer() ).html(
                                            '$'+pageTotal +' ( $'+ total +' total)'
                                            //'$'+pageTotal +' ( $'+ total +' total)'
                                        );
                                        $( api.column( 6 ).footer() ).html(
                                            '$'+pageTotal1 +' ( $'+ total1 +' total)'
                                            //'$'+pageTotal +' ( $'+ total +' total)'
                                        );
                                        $( api.column( 13 ).footer() ).html(
                                            '$'+pageTotal2 +' ( $'+ total2 +' total)'
                                            //'$'+pageTotal2 +' ( $'+ total2 +' total)'
                                        );
                                    },*/
                                    "columnDefs": [{
                                                        "targets": [ 9 ],
                                                        "visible": false,
                                                        "searchable": false
                                                    }
                                                     ]
                                });
                        setTimeout(function(){ 
                            bttondevolucion();
                        }, 3000);
                        
                    },
                    error: function(jqXHR, estado, error) {
                        console.log("Estado: " + estado);
                        console.log(jqXHR);
                        console.log("Error: " + error);
                    },
                });
            }, 1000);
                
            
        
    });
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_polaris',
        radioClass: 'iradio_polaris'
    });	
});
function selecciontipor(){
    $('#vendedorselected').val(0);
    var tipo = $('#tipo').val();
    $('#filtro0').click();
    if (tipo==1) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "block");
        $(".viewrecepcion").hide( "slow" );

        $( ".viewmesas" ).show( "slow" );

    }
    if (tipo==2) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "none");
        
        $( ".viewrecepcion" ).show( "slow" );

        $(".classcredito").show( "slow" );
        $( ".viewmesas" ).hide( "slow" );
    }
    if (tipo==3) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "none");
        $(".viewrecepcion").hide( "slow" );
        $( ".viewmesas" ).hide( "slow" );
    }
    if (tipo==4) {
        $(".radioninos").css("display", "block");
        $(".radiopro").css("display", "none");
        $(".viewrecepcion").hide( "slow" );
        $( ".viewmesas" ).hide( "slow" );
    }
    if (tipo==5) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "block");
        $(".viewrecepcion").hide( "slow" );
        $( ".viewmesas" ).hide( "slow" );

    }
    if (tipo==6) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "block");
        $(".viewrecepcion").hide( "slow" );
        $( ".viewmesas" ).hide( "slow" );
    }
    if (tipo==7) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "none");
        $(".viewrecepcion").hide( "slow" );
        $( ".viewmesas" ).hide( "slow" );
    }
    if (tipo==8) {
        $(".radioninos").css("display", "none");
        $(".radiopro").css("display", "none");
        $(".viewrecepcion").show( "slow" );
        $(".classcredito").hide( "slow" );
        $( ".viewmesas" ).hide( "slow" );
    }
    if (tipo==8) {
        params = {};
        params.id = 0;
        $.ajax({
            type:'POST',
            url:base_url+'Corte/listadepersonal',
            data:params,
            async:false,
            statusCode:{
                    404: function(data){
                        swal({type: "error", title:"Error 404", timer: 5000});
                    },
                    500: function(data){
                        swal({type: "error", title:"Error 500", timer: 5000});
                    }
            },
            success:function(data){
                $('#vendedorselected').html(data);
                

            }
        }); 
    }else{
        params = {};
        params.id = 1;
        $.ajax({
            type:'POST',
            url:base_url+'Corte/listadepersonal',
            data:params,
            async:false,
            statusCode:{
                    404: function(data){
                        swal({type: "error", title:"Error 404", timer: 5000});
                    },
                    500: function(data){
                        swal({type: "error", title:"Error 500", timer: 5000});
                    }
            },
            success:function(data){
                $('#vendedorselected').html(data);

            }
        }); 
    }

}
function bttondevolucion(){
    $('.bttondevolucion').click(function(event) {
        var motivo=$(this).data("motivo");
        $('.datosmotivod').html(motivo);
        $('#modalmotivodevolucion').modal();
    });
}