<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Embarque extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloProducto');
    }
	public function index(){
            $data['producto']=$this->ModeloProducto->getproductos();
            $data['categoria']=$this->ModeloProducto->getcategoria();
            $data['bodegas']=$this->ModeloProducto->getbodegas();
            $data['getmarcas']=$this->ModeloProducto->getmarcas();
            $data['ultimocamionregistrado']=$this->ModeloProducto->ultimocamionregistrado();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('Embarque/modal');
            $this->load->view('Embarque/embarque',$data);
            $this->load->view('templates/footer');
            $this->load->view('Embarque/jsembarque');
	}
    public function gettbodegas(){
       $results = $this->ModeloProducto->getbodegas();
       echo json_encode($results->result());

    }
    public function gettcamaras(){
        $id = $this->input->post('id');
        $results = $this->ModeloProducto->getCamaras($id);
       echo json_encode($results->result());
    }
    function addembarque(){
        
        $prove = $this->input->post('prove');
        $folio = $this->input->post('folio');
        $fecha = $this->input->post('fecha');
        $camion = $this->input->post('camion');
        $referencia = $this->input->post('referencia');
        $fechasalida = $this->input->post('fechasalida');
        $usu=$_SESSION['idpersonal'];
        echo $this->ModeloProducto->addembarque($prove,$folio,$fecha,$camion,$referencia,$fechasalida,$usu);
    }
    function addembarquedell(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i = 0; $i < count($DATA); $i++) {
            $prod = $DATA[$i]->prod;
            $categ = $DATA[$i]->categ;
            $bodega = $DATA[$i]->bodega;
            $clave = strtoupper($DATA[$i]->clave);
            $cantidad = $DATA[$i]->cantidad;
            $costofr = $DATA[$i]->costofr;
            $costofl = $DATA[$i]->costofl;
            $costom = $DATA[$i]->costom;
            $costo = $DATA[$i]->costo;
            $idembarque = $DATA[$i]->idembarque;
            $camara = $DATA[$i]->camara;
            $tipocaja = $DATA[$i]->tipocaja;
            $cajacosto = $DATA[$i]->cajacosto;
            $marca = $DATA[$i]->marca;
            $this->ModeloProducto->addembarquedell($prod,$categ,$bodega,$clave,$cantidad,$costo,$idembarque,$camara,$tipocaja,$costofr,$costofl,$costom,$cajacosto,$marca);
        }
    }
    function detallescamaras(){
        $bode = $this->input->post('bode');
        $datoscam = $this->ModeloProducto->getdetallcamara($bode);
        
        ?>
        <table class="table table-striped" id="data-tables-desatallecamara">
            <thead>
                <tr>
                    <th>Camara</th>
                    <th>Cantidad</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($datoscam->result() as $item){ ?>
                <tr>
                    <td><?php echo $item->nombre; ?></td>
                    <td><?php echo $item->total; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
         <?php
    }
    function datosEmbarque(){
        $id = $this->input->post('id');
        $datos=$this->ModeloProducto->datosEmbarque($id);
        echo json_encode($datos); 
    }
    function datosdecamara(){
        $id = $this->input->post('id');
        $datos=$this->ModeloProducto->datosdecamara($id);
        echo json_encode($datos);
    }
    public function searchmarcas(){
        $pro = $this->input->get('search');
        $results=$this->ModeloProducto->marcasallsearch($pro);
        echo json_encode($results->result());
    }
    function getmarcas(){
        $resultado = $this->ModeloProducto->getmarcas();
        ?>
        <table class="table table-striped" id="data-tables">
            <thead>
                <tr>
                    <th>Marca</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($resultado->result() as $item){ ?>
                <tr>
                    <td><?php echo $item->marca; ?></td>
                    <td></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
         <?php

    }
    function addmarca(){
        $marca = $this->input->post('marca');
        $datos=$this->ModeloProducto->addmarca($marca);
    }
      
    
}
