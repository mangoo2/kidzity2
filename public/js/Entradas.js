var base_url = $('#base_url').val();
$(document).ready(function() {
	$('#consultar').click(function(event) {
		var personal= $('#personal option:selected').val();
        var inicio=$('#fechainicio').val();
        var fin=$('#fechafin').val();

		$.ajax({
                type:'POST',
                url:base_url+"Entradas/consultass",
                data:{ciclo:$('#newcliclo').val()},
                data:{
                	pe:personal,
                	in:inicio,
                	fi:fin
                },
                async:false,
                success:function(data){
                	$('.cargadatos').html('');
                	$('.cargadatos').html(data);
                	$('#tabla').DataTable({
			            'rowsGroup': [0],
			            dom: 'Bfrtip',
				        buttons: [          
				            {
				                extend: 'excel',
				                footer: true,
				                title: 'Reporte de entradas'
				            },
				            {
				                extend: 'pdf',
				                footer: true,
				                title: 'Reporte de entradas'
				            },
				            'pageLength'
				        ]
			        });
                }
            });
	});
});