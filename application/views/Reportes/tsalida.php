<?php
      $compra_tiempo=$this->ModeloCatalogos->getselectwhere('compra_tiempo_nino','compranId',$compranId);
   

      foreach ($configticket->result() as $item){
      $id_ticket = $item->id_ticket;
      $titulo = $item->titulo;
      $mensaje = $item->mensaje;
      $mensaje2 = $item->mensaje2;
      $fuente = $item->fuente;
      $tamano = $item->tamano;
      $margensup = $item->margensup;
      $margenlat = $item->margenlat;
    } 

?>
<?php
    //=================================================================
    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    //=======================================================================================
    class MYPDF extends TCPDF {
      //Page header
      public function Header() {
          //$img_header = 'header.jpg';
          //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
          //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
          $imglogo = base_url().'public/img/kidb.png';
          $html = '
              ';
            $this->writeHTML($html, true, false, true, false, '');
      }
        // Page footer
      public function Footer() {
          $html = ' 
          <table width="100%" border="0">
            <tr>
              <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
            </tr>
          </table>';
          //$this->writeHTML($html, true, false, true, false, '');
      }
    } 
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 210), true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('agb');
    $pdf->SetTitle('Ticket');
    $pdf->SetSubject('Ticket');
    $pdf->SetKeywords('Ticket');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins($margenlat, $margensup, $margenlat);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->SetFont('dejavusans', '', $tamano);
    // add a page
    $pdf->AddPage();
?>
<?php
$imglogo = base_url().'public/img/kidb.png';
$html = '<table border="0">';
$html .= '<tr>
            <td colspan="2" align="center"><img src="'.$imglogo.'" width="130px" ></td>
        </tr>';

$html .= '  <tr>
              <td colspan="3" align="center">'.$titulo.'</td>
          </tr>
         ';
$html .= ' <tr>
              <td colspan="3" align="center">'.$mensaje.'</td>
          </tr>
         ';



foreach ($compra_tiempo->result() as $item) {
  $subtotal=$item->pagotiempoextra-($item->pagotiempoextra*$iva);
  $ivat=$item->pagotiempoextra*$iva;
  $html .= '<tr>
              <td  style="background-color: #9C9B99; ">Pulcera:</td>
              <td  align="center">'.$item->pulcera.'</td>
            </tr>
            <tr>
              <td  style="background-color: #9C9B99; ">Subtotal:</td>
              <td  align="center">$ '.number_format($subtotal,2,'.',',').'</td>
            </tr>
            <tr>
              <td  style="background-color: #9C9B99; ">Iva:</td>
              <td  align="center">$ '.number_format($ivat,2,'.',',').'</td>
            </tr>
            <tr>
              <td  style="background-color: #9C9B99; ">Total:</td>
              <td  align="center">$ '.number_format($item->pagotiempoextra,2,'.',',').'</td>
            </tr>
            <tr>
              <td  style="background-color: #9C9B99; ">Abierto:</td>
              <td  align="center">'.date('h:i A',strtotime($item->reg_abierto)).'</td>
            </tr>
            <tr>
              <td  style="background-color: #9C9B99; ">Cerrado:</td>
              <td  align="center">'.date('h:i A',strtotime($item->reg_cerrado)).'</td>
            </tr>
           ';
}


$styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
$params = $pdf->serializeTCPDFtagParameters(array($idqr, 'QRCODE,L', '', '', 40, 40, $styleQR, 'N'));

$html .= '  <tr>
              <td colspan="3" align="center">'.$mensaje2.'</td>
          </tr>
         ';        

$html .= '</table>';
      $pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>