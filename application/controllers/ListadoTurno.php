<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListadoTurno extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }

    function index(){
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $data['turno']=$this->ModeloCatalogos->getturnos();
        $this->load->view('Turnos/Listado',$data);
        $this->load->view('Turnos/jsTurno');
        $this->load->view('templates/footer');
    }

    public function VentasTurno(){
        $id=$this->input->post('id');
        $punto_venta=$this->input->post('punto_venta');
        $full;
        $x=0;
        $table='<table class="table table-striped" id="data-tableTurno">';
        $table.='<thead>';
            $table.='<tr><th>Nombre</th><th>Metodo</th><th>Monto total</th><th>Fecha</th></tr>';
        $table.='</thead>';
        $table.='<tbody>';
        $data=$this->ModeloCatalogos->VentasTurno($id,$punto_venta); 
        foreach ($data->result() as $key) {
            $table.='<tr>';
            $table.="<td>".$key->nombre."</td>";
            if($key->metodo==1){
                $table.="<td>Efectivo</td>";    
            }else{
                $table.="<td>Targeta</td>";
            }
            $table.="<td>".$key->monto_total."</td>";
            $table.="<td>".$key->reg."</td>";
            $table.='</tr>';
        }
        $table.='</tbody>';
        $table.='</table>';
        echo $table;
    } 

}
?>    