<div class="row">
  <div class="col-md-12">
    <h2></h2>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="col-md-9"></div>
    <div class="col-md-2">
      <select id="sucursalselected" class="form-control" onchange="seleccionar()">
        <option value="1">Generales</option> 
        <option value="2">Visitas</option> 
        <option value="3">Restaurante</option> 
      </select>
    </div>
    
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Ventas General</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <table class="table table-striped" id="data-tables">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Monto</th>
                        <th>Método</th>
                        <th>Vendedor</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody class="tbody_lcompras">
                      
                    </tbody>
                  </table>
                  
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#buscarlproducto').click(function(){
        params = {};
        params.fechain = $('#productoscin').val();
        params.fechafin = $('#productoscfin').val();
        $.ajax({
            type:'POST',
            url: 'Listacompras/consultar',
            data: params,
            async: false,
            success:function(data){
              $('.tbody_lcompras').html('');
              $('.tbody_lcompras').html(data);
            }
        });
    });
});
function seleccionar(){
    var tv=$('#sucursalselected option:selected').val();
    if (tv==1) {
      location.href="<?php echo base_url(); ?>Listadoventasg";
    }
    if (tv==2) {
      location.href="<?php echo base_url(); ?>Listadoventasv";
    }
    if (tv==3) {
      location.href="<?php echo base_url(); ?>Listadoventasr";
    }
}

function ticket(id)
{
    $("#iframeri").modal();
    $('#iframereporte').html('<iframe src="'+$('#base_url').val()+'Ticket/dulceria?comp='+id+'"></iframe>');
}
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listadog.js?v=<?php echo date('YmdHis') ?>"></script>