<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloFiestas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function filas(){
    	$strq = "SELECT COUNT(*) as total FROM fiestas where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->total;
        } 
        return $total;
    }
    function Lista($por_pagina,$segmento){
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq="SELECT * FROM fiestas WHERE  activo=1 LIMIT $por_pagina $segmento;";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function fiestasdisponibles(){
        $strq="SELECT * FROM fiestas WHERE activo=1 and estatus=1 and fecha_inicio=CURDATE()";
        $resp=$this->db->query($strq);
        return $resp;
    }
    function Search($buscar){
        $strq="SELECT * 
            FROM fiestas 
            WHERE 
            activo=1 and nombre LIKE '%$buscar%' or
            activo=1 and fecha_inicio LIKE '%$buscar%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    
    function consultarninosfiesta($id){
        $strq="SELECT comn.pulcera,comn.reg_abierto,nin.nombre as nino FROM
                compra_tiempo_nino as comn
                inner JOIN ninos as nin on nin.id=comn.ninoid
                inner join compra_tiempo as com on com.compraId=comn.compraId
                WHERE com.tipo=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }


    function getlisteventos($params){
        $columns = array( 
            0=>'fiestaId',
            1=>'nombre',
            2=>'precio_total',
            3=>'precioex',
            4=>'cantidadt',
            5=>'cantidadm',
            6=>'fecha_inicio',
            7=>'hora_inicio',
            8=>'hora_fin',
            9=>'titular',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('fiestas');
        $this->db->where(array('activo'=>1));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlisteventos_total($params){
        $columns = array( 
            0=>'fiestaId',
            1=>'nombre',
            2=>'precio_total',
            3=>'precioex',
            4=>'cantidadt',
            5=>'cantidadm',
            6=>'fecha_inicio',
            7=>'hora_inicio',
            8=>'hora_fin',
            9=>'titular',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('fiestas');
        $this->db->where(array('activo'=>1));
        
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        
        $query=$this->db->get();

        return $query->row()->total;
    }

}
?>