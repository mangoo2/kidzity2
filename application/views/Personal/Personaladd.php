<?php 
if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $personalv=$this->ModeloPersonal->personalview($id);
    foreach ($personalv->result() as $item){
      $id=$item->personalId;
      $nom=$item->nombre;
      $email=$item->Email;
      $fnaci=$item->fechanacimiento;
      $area=$item->area;
      $dom=$item->domicilio;
      $tel=$item->telefono;
      $cel=$item->movil;
      $horae=$item->horae;
      $horas=$item->horas;
      $Fot=$item->foto;
      $label='Editar';
    }
}else{
  $id='';
  $nom='';
  $email='';       
  $area=0;
  $dom='';
  $fnaci='';
  $tel='';
  $cel='';
  $horae='';
  $horas='';
  $Fot='';
  $label='Nuevo';    
} 
?>
<style type="text/css">
  .controls{
    margin-bottom: 10px; 
  }
  .previewimg{
    width: 100%; height: 250px; border: solid #a6a9ae 1px; border-radius: 12px;
    <?php if($Fot!=''){?>
      background:url(<?php echo base_url(); ?>/public/img/personal/<?php echo $Fot; ?>);   
      background-size: contain;
      background-repeat: no-repeat;
      background-position: center;                 
      <?php }?>
  }
 
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Personal</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title"><?php echo $label;?> Personal</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                <!--------//////////////-------->
                  <div class="row">
                    <div class="col-md-12">
                        <form method="post" id="frmPersonal" role="form" >
                          <!------------------------------------------------------>
                          <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group">
                                  <label class="col-sm-4 control-label">Nombre</label>
                                  <div class="col-sm-7 controls">
                                        <input type="text" placeholder="Username" id="nombre" name="nombre" value="<?php echo $nom; ?>" class="form-control">
                                        <input type="text" placeholder="Username" id="persolnalid" value="<?php echo $id; ?>" hidden>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label class="col-sm-4 control-label">Fecha de nacimiento</label>
                                  <div class="col-sm-7 controls">
                                        <input type="date" placeholder="YYYY-MM-DD" id="fechanacimineto" name="fechanacimineto" value="<?php echo $fnaci;?>" class="form-control">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="col-sm-4 control-label">Área</label>
                                    <div class="col-sm-7 controls">
                                          <select id="areas" name="areas" class="form-control">
                                            <?php foreach ($getareas->result() as $item){ ?>
                                              <option value="<?php echo $item->id; ?>" <?php if($area==$item->id){echo "selected";} ?> ><?php echo $item->area; ?></option>
                                            <?php } ?>
                                            
                                          </select>
                                    </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 100%">
                                <div class="fileinput-preview thumbnail previewcat previewimg" data-trigger="fileinput">
                                  <?php if($Fot!=''){?>
                                    <!--<img src="<?php echo base_url(); ?>/public/img/personal/<?php echo $Fot; ?>">-->
                                  <?php }?>
                                </div>
                                <div>
                                    <span class="btn btn-primary btn-file">
                                        <span class="fileinput-new">Foto</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="imgpro" id="imgpro" data-allowed-file-extensions='["jpg", "png"]'>
                                    </span>
                                    <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                </div>
                              </div>
                            </div>
                          </div>
                         
                          <div class="col-md-12">
                              
                              
                              <div class="col-md-12">
                                <hr/>  
                              </div>
                              
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Domicilio</label>
                                    <div class="col-sm-10 controls">
                                          <input type="text" id="domicilio" class="form-control" value="<?php echo $dom;?>">
                                    </div>
                                  </div>
                            </div>
                            
                            <div class="col-md-6">
                              <div class="form-group">
                                  <label class="col-sm-4 control-label">Teléfono local</label>
                                  <div class="col-sm-7 controls">
                                        <input type="tel" placeholder="Telefono local" id="tel" name="tel" value="<?php echo $tel; ?>" class="form-control">
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                  <label class="col-sm-4 control-label">Teléfono móvil</label>
                                  <div class="col-sm-7 controls">
                                      <input type="tel" placeholder="Telefono movil" id="movil" name="movil" value="<?php echo $cel; ?>" class="form-control">
                                  </div>
                                </div>
                            </div>
                            
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-sm-4 control-label">Correo</label>
                                <div class="col-sm-7 controls">
                                      <input type="email" placeholder="ejemplo@ejemplo.com" id="email" name="email" value="<?php echo $email; ?>" class="form-control">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-sm-4 control-label">Hora Entrada</label>
                                <div class="col-sm-7 controls">
                                      <input type="time" placeholder="00:00:00" id="horae" name="horae" value="<?php echo $horae; ?>" class="form-control">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-sm-4 control-label">Hora Salida</label>
                                <div class="col-sm-7 controls">
                                      <input type="time" placeholder="00:00:00" id="horas" name="horas" value="<?php echo $horas; ?>" class="form-control">
                                </div>
                              </div>
                            </div>
                          </div>
                          
                        	<div class="col-md-12">
                        		<div class="col-md-9">
                        			
                        		</div>
                        		<div class="col-md-3">
                        			<button class="btn btn-primary"  id="savep"><i class="fa fa-save"></i> Guardar</button>
                        		</div>
                        	</div>		
                          <!------------------------------------------------------>
                        </form>
                      </div>
                  </div>
                <!--------//////////////-------->
              </div>
          </div>
      </div>
    </div>
</div>