<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entradas extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
            if (!isset($_SESSION['perfilid'])) {
                  $perfilview=0;
            }else{
                $perfilview=$_SESSION['perfilid'];
            }
            if ($perfilview>2) {
                redirect('/Sistema');
            }
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $data['personal']=$this->ModeloPersonal->getpersonal();
            $this->load->view('Reportes/entradas',$data);
            $this->load->view('templates/footer');
            $this->load->view('Reportes/entradasjs');
    }
    public function consultass() {
        $personal = $this->input->post('pe');
        $inicio = $this->input->post('in');
        $fin = $this->input->post('fi');

        $clientes = $this->ModeloCatalogos->entradas($personal,$inicio,$fin);
        //echo json_encode($clientes->result());
        ?>
            <table class="table table-sm table-hover table-responsive" id="tabla">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Fecha</th>
                                    <th>Entrada</th>
                                    <th>Salida</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($clientes->result() as $item){ ?>
                                    <tr>
                                        <td><?php echo $item->nombre; ?></td>
                                        <td><?php echo $item->FechaEntrada; ?></td>
                                        <td><?php echo $item->HoraEntrada; ?></td>
                                        <td><?php echo $item->HoraSalida; ?></td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
        <?php
    }
}