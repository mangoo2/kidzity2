<?php

    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $html = '
          
          
          <table width="100%" border="0" >
            <tr>
              <td align="center"><img src="'.$imglogo.'" width="100px" ></td>
              <td></td>
              <td></td>
            </tr>
          </table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '30', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
foreach ($aspirante->result() as $datos) { 
  switch ($datos->Habitacion) {
    case 1:
      $habitacion='Individual';
      break;
    case 2:
      $habitacion='Doble';
      break;
    case 3:
      $habitacion='Triple';
      break;
    
    default:
      $habitacion='';
      break;
  }
  switch ($datos->Sexo) {
    case 1:
      $sexo='Masculino';
      break;
    case 2:
      $sexo='Femenino';
      break; 
    default:
      $sexo='';
      break;
  }
  switch ($datos->Nacionalidad) {
    case 1:
      $Nacionalidad='Mexicana';
      break;
    case 2:
      $Nacionalidad='Extranjera';
      break; 
    default:
      $Nacionalidad='';
      break;
  }

  $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Datos Generales</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">Nombre</th>
              <th width="50%">'.$datos->Nombre.' '.$datos->ApellidoPa.' '.$datos->ApellidoMa.'</th>
            </tr>
            <tr>
              <th >Carrera Solicitada</th>
              <th >'.$this->ModeloSolicitud->carreraselect($datos->CarreraId).'</th>
            </tr>
            <tr>
              <th >Semestre</th>
              <th >'.$datos->Sementre.'</th>
            </tr>
            <tr>
              <th >ID</th>
              <th >'.$datos->ID.'</th>
            </tr>
            <tr>
              <th >Preferencia de habitación</th>
              <th >'.$habitacion.'</th>
            </tr>
          </table>
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Datos Personales</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th >Sexo</th>
              <th >'.$sexo.'</th>
            </tr>
            <tr>
              <th >Nacionalidad</th>
              <th >'.$Nacionalidad.'</th>
            </tr>
            <tr>
              <th >Fecha de nacimiento</th>
              <th >'.$datos->FechaNacimiento.'</th>
            </tr>
            <tr>
              <th >Correo</th>
              <th ><a href="mailto:'.$datos->Email.'" >'.$datos->Email.'</a></th>
            </tr>
            <tr>
              <th >Domicilio</th>
              <th >'.$datos->Domicilio.'</th>
            </tr>
            <tr>
              <th >Ciudad</th>
              <th >'.$datos->Ciudad.'</th>
            </tr>
            <tr>
              <th >Estado</th>
              <th >'.$datos->Estado.'</th>
            </tr>
            <tr>
              <th >C.P.</th>
              <th >'.$datos->CP.'</th>
            </tr>
            <tr>
              <th >Lada</th>
              <th >'.$datos->lada.'</th>
            </tr>
            <tr>
              <th >Telefono Casa</th>
              <th >'.$datos->Casa.'</th>
            </tr>
            <tr>
              <th >Telefono celular</th>
              <th >'.$datos->Celular.'</th>
            </tr>
          </table>
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Datos Familiares</b> </th>
            </tr>
          </table>
  ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($padre->result() as $datos) {
  switch ($datos->MismoDomicilio) {
    case 1:
        $mismod='Si';
        $domicilio='';
        
      break;
    case 2:
        $mismod='No';
        
        $domicilio='<tr>
                      <th >Domicilio</th>
                      <th >'.$datos->Domicilio.'</th>
                    </tr>
                    <tr>
                      <th >Ciudad</th>
                      <th >'.$datos->Ciudad.'</th>
                    </tr>
                    <tr>
                      <th >Estado</th>
                      <th >'.$datos->Estado.'</th>
                    </tr>
                    <tr>
                      <th >CP</th>
                      <th >'.$datos->CP.'</th>
                    </tr>
                    <tr>
                      <th >Pais</th>
                      <th >'.$datos->Pais.'</th>
                    </tr>';
      break;
    
    default:
    $mismod='';
      $domicilio='';
      break;
  }






   $html='
          
          <table border="1" cellpadding="4px">
            <tr>
              <th >Nombre completo del padre</th>
              <th >'.$datos->Nombre.'</th>
            </tr>
            <tr>
              <th >Mismo Domicilio</th>
              <th >'.$mismod.'</th>
            </tr>
            '.$domicilio.'
            <tr>
              <th >Celular</th>
              <th >'.$datos->Cel.'</th>
            </tr>
            <tr>
              <th >Email</th>
              <th ><a href="mailto:'.$datos->Email.'" target="_top">'.$datos->Email.'</a></th>
            </tr>
          </table>
  ';
  $pdf->writeHTML($html, true, false, true, false, '');
}

foreach ($madre->result() as $datos) {
  switch ($datos->MismoDomicilio) {
    case 1:
        $mismod='Si';
        $domicilio='';
        
      break;
    case 2:
        $mismod='No';
        $domicilio='<tr>
                      <th >Domicilio</th>
                      <th >'.$datos->Domicilio.'</th>
                    </tr>
                    <tr>
                      <th >Ciudad</th>
                      <th >'.$datos->Ciudad.'</th>
                    </tr>
                    <tr>
                      <th >Estado</th>
                      <th >'.$datos->Estado.'</th>
                    </tr>
                    <tr>
                      <th >CP</th>
                      <th >'.$datos->CP.'</th>
                    </tr>
                    <tr>
                      <th >Pais</th>
                      <th >'.$datos->Pais.'</th>
                    </tr>';
      break;
    default:
    $mismod='';
      $domicilio='';
      break;
  }
   $html='
          
          <table border="1" cellpadding="4px">
            <tr>
              <th >Nombre completo de la madre</th>
              <th >'.$datos->Nombre.'</th>
            </tr>
            <tr>
              <th >Mismo Domicilio</th>
              <th >'.$mismod.'</th>
            </tr>
            '.$domicilio.'
            <tr>
              <th >Celular</th>
              <th >'.$datos->Cel.'</th>
            </tr>
            <tr>
              <th >Email</th>
              <th ><a href="mailto:'.$datos->Email.'" target="_top">'.$datos->Email.'</a></th>
            </tr>
          </table>
  ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($aspirante->result() as $datos) { 
  switch ($datos->Solidario) {
    case 1:
      $Solidario='Padre';
      break;
    case 2:
      $Solidario='Madre';
      break;
    case 3:
      $Solidario='Otro';
      break;
    
    default:
      $Solidario='';
      break;
  }


  $html='
          
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">Solidario</th>
              <th width="50%">'.$Solidario.'</th>
            </tr>
            
          </table>
          
  ';
  $pdf->writeHTML($html, true, false, true, false, '');
}

foreach ($fotro->result() as $datos) {
  $html='
          
          <table border="1" cellpadding="4px">
            <tr>
              <th >Nombre completo del responsable Solidario</th>
              <th >'.$datos->Nombre.'</th>
            </tr>
            <tr>
              <th >Parentesco</th>
              <th >'.$datos->TipoNombre.'</th>
            </tr>
            <tr>
              <th >Domicilio</th>
              <th >'.$datos->Domicilio.'</th>
            </tr>
            <tr>
              <th >Ciudad</th>
              <th >'.$datos->Ciudad.'</th>
            </tr>
            <tr>
              <th >Estado</th>
              <th >'.$datos->Estado.'</th>
            </tr>
            <tr>
              <th >CP</th>
              <th >'.$datos->CP.'</th>
            </tr>
            <tr>
              <th >Pais</th>
              <th >'.$datos->Pais.'</th>
            </tr>
            <tr>
              <th >Celular</th>
              <th >'.$datos->Cel.'</th>
            </tr>
            <tr>
              <th >Email</th>
              <th ><a href="mailto:'.$datos->Email.'" target="_top">'.$datos->Email.'</a></th>
            </tr>
            
          </table>
  ';
  $pdf->writeHTML($html, true, false, true, false, '');
}

foreach ($emergencia->result() as $datos) {
  $html2='
          <table border="1" cellpadding="4px">
            <tr>
              <th colspan="2" border="0">En caso de emergencia favor de proporcionar los datos de algún familiar o amigo cercano diferente al padre o a la madre, con quien nos podamos comunicar en caso de no poder contactar a ninguno de los primeros dos:</th>
            </tr>
            <tr>
              <th >Nombre completo</th>
              <th >'.$datos->Nombre.'</th>
            </tr>
            <tr>
              <th >Parentesco</th>
              <th >'.$datos->TipoNombre.'</th>
            </tr>
            <tr>
              <th >Domicilio</th>
              <th >'.$datos->Domicilio.'</th>
            </tr>
            <tr>
              <th >Ciudad</th>
              <th >'.$datos->Ciudad.'</th>
            </tr>
            <tr>
              <th >Estado</th>
              <th >'.$datos->Estado.'</th>
            </tr>
            <tr>
              <th >CP</th>
              <th >'.$datos->CP.'</th>
            </tr>
            <tr>
              <th >Pais</th>
              <th >'.$datos->Pais.'</th>
            </tr>
            <tr>
              <th >Celular</th>
              <th >'.$datos->Cel.'</th>
            </tr>
            <tr>
              <th >Email</th>
              <th ><a href="mailto:'.$datos->Email.'" target="_top">'.$datos->Email.'</a></th>
            </tr>
          </table>
          ';
  $pdf->writeHTML($html2, true, false, true, false, '');

}


foreach ($datosescolares->result() as $datos) { 
  $opt1=$datos->Opt1;
  switch ($opt1) {
    case 3:
      $opt1='Sobresaliente';
      break;
    case 2:
      $opt1='Bueno';
      break;
    case 1:
      $opt1='Promedio';
      break;
    case 0:
      $opt1='Deficiente';
      break;
  }
  $opt2=$datos->Opt2;
  switch ($opt2) {
    case 3:
      $opt2='Muy Frecuente';
      break;
    case 2:
      $opt2='Comunmente';
      break;
    case 1:
      $opt2='Rara vez';
      break;
    case 0:
      $opt2='Nunca';
      break;
  }
  $opt3=$datos->Opt3;
  switch ($opt3) {
    case 3:
      $opt3='Muy Frecuente';
      break;
    case 2:
      $opt3='Comunmente';
      break;
    case 1:
      $opt3='Rara vez';
      break;
    case 0:
      $opt3='Nunca';
      break;
  }
  $opt4=$datos->Opt4;
  switch ($opt4) {
    case 1:
      $opt4='Si';
      $pregopt4='<tr>
                  <th >¿Cual?</th>
                  <th >'.$datos->Preg1_Opt4.'</th>
                </tr>
                <tr>
                  <th >¿Por qué tuviste que repetir año?</th>
                  <th >'.$datos->Preg2_Opt4.'</th>
                </tr>';
      break;
    case 0:
      $opt4='No';
      $pregopt4='';
      break;
  }
  $opt5=$datos->Opt5;
  switch ($opt5) {
    case 1:
      $opt5='Si';
      $pregopt5='<tr>
                  <th >¿Cual?</th>
                  <th >'.$datos->Preg1_Opt4.'</th>
                </tr>
                <tr>
                  <th >¿Por qué tuviste que repetir año?</th>
                  <th >'.$datos->Preg2_Opt4.'</th>
                </tr>
                <tr>
                  <th >¿Por qué tuviste que repetir año?</th>
                  <th >'.$datos->Preg2_Opt4.'</th>
                </tr>';
      break;
    case 0:
      $opt5='No';
      $pregopt5='';
      break;
  }
  $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Datos Escolares</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%"> Preparatoria en la que estudiaste</th>
              <th width="50%">'.$datos->Preg1.'</th>
            </tr>
            <tr>
              <th > Promedio</th>
              <th >'.$datos->Preg2.'</th>
            </tr>
            <tr>
              <th > ¿cómo calificas tu desempeño en tus últimos 3 años escolares?</th>
              <th >'.$opt1.'</th>
            </tr>
            <tr>
              <th >¿Por que?</th>
              <th >'.$datos->Preg3.'</th>
            </tr>
            <tr>
              <th >¿Cuál ha sido el mejor momento que has vivido de todos tus años escolares?</th>
              <th >'.$datos->Preg4.'</th>
            </tr>
            <tr>
              <th >¿Cuál ha sido el peor momento que has vivido de todos tus años escolares?</th>
              <th >'.$datos->Preg5.'</th>
            </tr>
            <tr>
              <th >¿Cómo es tu relación con tus compañeros de clase?</th>
              <th >'.$datos->Preg6.'</th>
            </tr>
            <tr>
              <th colspan="2" >¿Has tenido problemas con tus profesores en tus años escolares…?</th>
            </tr>
            <tr>
              <th >Por desempeño académico:</th>
              <th >'.$opt2.'</th>
            </tr>
            <tr>
              <th >Por disciplina:</th>
              <th >'.$opt3.'</th>
            </tr>
            <tr>
              <th >¿Has repetido algún grado escolar?</th>
              <th >'.$opt4.'</th>
            </tr>
            '.$pregopt4.'
            <tr>
              <th >¿Alguna vez has tenido que interrumpir tus estudios?</th>
              <th >'.$opt5.'</th>
            </tr>
            '.$pregopt5.'
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($universidadanahuac->result() as $datos) { 
   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Universidad Anáhuac</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">¿Por qué elegiste a la Universidad Anáhuac Puebla para tus estudios universitarios?</th>
              <th width="50%">'.$datos->Pre1.'</th>
            </tr>
            <tr>
              <th >¿Qué carrera y semestre comenzarás a cursar en la Universidad Anáhuac Puebla?</th>
              <th >'.$datos->Pre2.'</th>
            </tr>
            <tr>
              <th >¿Por qué quieres estudiar esa carrera?</th>
              <th >'.$datos->Pre3.'</th>
            </tr>
            <tr>
              <th >¿Qué metas te gustaría alcanzar terminando tu carrera profesional?</th>
              <th >'.$datos->Pre4.'</th>
            </tr>
            <tr>
              <th >Para tu carrera universitaria ¿Qué habilidades tienes que te ayudarán en tu desarrollo académico y profesional?</th>
              <th >'.$datos->Pre5.'</th>
            </tr>
            <tr>
              <th >¿Qué significa para ti terminar con éxito una carrera universitaria?</th>
              <th >'.$datos->Pre6.'</th>
            </tr>
            <tr>
              <th >¿Qué crees que podrías aportar a la sociedad como universitario Anáhuac?</th>
              <th >'.$datos->Pre7.'</th>
            </tr>
            <tr>
              <th >Para ti, ¿Qué significa ser Líder de Acción Positiva?</th>
              <th >'.$datos->Pre8.'</th>
            </tr>
            <tr>
              <th >¿Hay alguna actividad especial que te gustaría realizar en tus años universitarios?</th>
              <th >'.$datos->Pre9.'</th>
            </tr>

          
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($familiares->result() as $datos) { 
  $opt1f=$datos->Opt1;
  switch ($opt1f) {
    case 3:
      $opt1f='Casados';
      break;
    case 2:
      $opt1f='Divorciados';
      break;
    case 1:
      $opt1f='Vuelto a casar';
      break;
    case 0:
      $opt1f='Otro';
      break;
  }
  if ($datos->Pre3>0) {
    $hermanosp3='
            <table border="1" cellpadding="6px">
            <tr>
              <th width="50%">Nombre</th>
              <th width="25%">Sexo</th>
              <th width="25%">Edad</th>
            </tr>
            </table>

    ';
  }else{
    $hermanosp3='';
  }
   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Relaciones Familiares</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">Tus padres están</th>
              <th width="50%">'.$opt1f.'</th>
            </tr>
            <tr>
              <th >¿Con quién vives actualmente?</th>
              <th >'.$datos->Pre1.'</th>
            </tr>
            <tr>
              <th >Para ti, ¿Qué es la familia?</th>
              <th >'.$datos->Pre2.'</th>
            </tr>
            <tr>
              <th >¿cuántos hermanos tienes?</th>
              <th >'.$datos->Pre3.'</th>
            </tr>
            
          </table>
          '.$hermanosp3.'
        ';
  
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($familiareshermanos->result() as $datos) { 
  if ($datos->sexo==1) {
    $sexo='Masculino';
  }else{
    $sexo='Femenino';
  }
   $html='
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">'.$datos->nombre.'</th>
              <th width="25%">'.$sexo.'</th>
              <th width="25%">'.$datos->edad.'</th>
            </tr>
            

          
          </table>

        ';
  
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($familiares->result() as $datos) { 
   $html='
          
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">¿Quién es la persona que marca la disciplina en tu casa?</th>
              <th width="50%">'.$datos->Pre4.'</th>
            </tr>
            <tr>
              <th >Explica cómo se vive la disciplina en tu casa</th>
              <th >'.$datos->Pre5.'</th>
            </tr>
            <tr>
              <th >¿Qué opina tu familia de la carrera que has elegido?</th>
              <th >'.$datos->Pre6.'</th>
            </tr>
            <tr>
              <th >¿Qué opina de la idea de que vivas en Puebla?</th>
              <th >'.$datos->Pre7.'</th>
            </tr>
            <tr>
              <th >¿Qué opina de que entres a la Universidad Anáhuac?</th>
              <th >'.$datos->Pre8.'</th>
            </tr>

          
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($sociales->result() as $datos) { 
    if ($datos->Opt1==1) {
      $ropt1='Si';
      $rropt1='<tr>
                  <th >¿Qué actividades has hecho en estos grupos?</th>
                  <th >'.$datos->Preg1_Opt1.'</th>
                </tr>
                <tr>
                  <th >¿Cada cuántos días asistías a estos grupos?</th>
                  <th >'.$datos->Preg2_Opt1.'</th>
                </tr>';
    }else{
      $ropt1='No';
      $rropt1='';
    }
    if ($datos->Opt2==1) {
      $ropt2='Si';
      $rropt2='<tr>
              <th >¿Cuál, cuándo y por qué?</th>
              <th >'.$datos->Preg1_Opt2.'</th>
            </tr>';
    }else{
      $ropt2='No';
      $rropt2='';
    }
    if ($datos->Opt3==1) {
      $ropt3='Si';
      $rropt3='<tr>
              <th >¿Cuál, en dónde, cuándo y cuál era tu cargo?</th>
              <th >'.$datos->Preg1_Opt3.'</th>
            </tr>';
    }else{
      $ropt3='No';
      $rropt3='';
    }
    if ($datos->Opt4==1) {
      $ropt4='Si';
      $rropt4='<tr>
              <th >¿En dónde trabajaste y qué hacías?</th>
              <th >'.$datos->Preg1_Opt4.'</th>
            </tr>';
    }else{
      $ropt4='No';
      $rropt4='';
    }
    if ($datos->Opt5==1) {
      $ropt5='Si';
      $rropt5='<tr>
              <th >¿En dónde trabajaste y qué hacías?</th>
              <th >'.$datos->Preg_Opt5.'</th>
            </tr>';
    }else{
      $ropt5='No';
      $rropt5='';
    }
    switch ($datos->Opt6) {
      case 1:
        $opt6='Muy Introvertido';
        break;
      case 2:
        $opt6='Introvertido';
        break;
      case 3:
        $opt6='Extrovertida';
        break;
      case 4:
        $opt6='Muy Extrovertida';
        break;
    }


   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Aspectos Sociales</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">¿Perteneces a algún grupo artístico, religioso, deportivo, social o político?</th>
              <th width="50%">'.$ropt1.'</th>
            </tr>
            '.$rropt1.'
            <tr>
              <th >¿Has obtenido algún reconocimiento, trofeo, medalla, diploma, etc.?</th>
              <th >'.$ropt2.'</th>
            </tr>
            '.$rropt2.'
            <tr>
              <th >¿Has dirigido, inventado u organizado alguna actividad significativa?</th>
              <th >'.$ropt3.'</th>
            </tr>
            '.$rropt3.'
            <tr>
              <th >¿Has trabajado medio tiempo o tiempo completo por más de 6 meses seguidos en los últimos 3 años?</th>
              <th >'.$ropt4.'</th>
            </tr>
            '.$rropt4.'
            <tr>
              <th >¿Has tenido o tienes novio(a)?</th>
              <th >'.$ropt5.'</th>
            </tr>
            '.$rropt5.'
            <tr>
              <th >Para ti, ¿Qué es el matrimonio?</th>
              <th >'.$datos->Preg1.'</th>
            </tr>
            <tr>
              <th >¿Qué opinas de las parejas que tienen relaciones sexuales antes de casarse?</th>
              <th >'.$datos->Preg2.'</th>
            </tr>
            <tr>
              <th >Te consideras una persona:</th>
              <th >'.$opt6.'</th>
            </tr>
            <tr>
              <th >Para ti, ¿Qué es una amistad?</th>
              <th >'.$datos->Preg3.'</th>
            </tr>
            <tr>
              <th >¿Qué valoras de tu grupo de amigos?</th>
              <th >'.$datos->Preg4.'</th>
            </tr>
            <tr>
              <th >Cuando llegas a algún lugar y no conoces a nadie, ¿qué sueles hacer?</th>
              <th >'.$datos->Preg5.'</th>
            </tr>
            
          
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($personalizacion->result() as $datos) { 
   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Aspectos de Personalización</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">Menciona tres cualidades de tu persona:</th>
              <th width="50%">'.$datos->Preg1.'</th>
            </tr>
            <tr>
              <th >Menciona tres defectos de tu persona:</th>
              <th >'.$datos->Preg2.'</th>
            </tr>
            <tr>
              <th >Escribe una palabra con la que te definas a ti mismo:</th>
              <th >'.$datos->Preg3.'</th>
            </tr>
            <tr>
              <th >Si me encontrara a cualquiera de tus amigos y les preguntara cómo eres, ¿cómo te describirían?</th>
              <th >'.$datos->Preg4.'</th>
            </tr>
            <tr>
              <th >¿Qué estado de ánimo tienes la mayor parte del tiempo?</th>
              <th >'.$datos->Preg5.'</th>
            </tr>
            <tr>
              <th >¿Cuáles son las cosas que te alegran y motivan?</th>
              <th >'.$datos->Preg6.'</th>
            </tr>
            <tr>
              <th >¿Cuáles son las cosas que te entristecen?</th>
              <th >'.$datos->Preg7.'</th>
            </tr>
            <tr>
              <th >¿Cuáles son las cosas que te molestan?</th>
              <th >'.$datos->Preg8.'</th>
            </tr>
            <tr>
              <th >¿Cuál es el mayor problema que has enfrentado en la vida?</th>
              <th >'.$datos->Preg9.'</th>
            </tr>
            <tr>
              <th >¿Cuál ha sido tu mayor logro en la vida?</th>
              <th >'.$datos->Preg10.'</th>
            </tr>
            <tr>
              <th >De las veces que te has equivocado ¿cuál consideras que ha sido la ocasión más grave? ¿Por qué?</th>
              <th >'.$datos->Preg11.'</th>
            </tr>
          
            
          
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($creencias->result() as $datos) { 
    if ($datos->Opt1==1) {
      $opt1='Si';
      $ropt1='<tr>
              <th >¿Cual?</th>
              <th >'.$datos->Preg1_Opt1.'</th>
            </tr>';
    }else{
      $opt1='No';
      $ropt1='<tr>
              <th >¿Por que?</th>
              <th >'.$datos->Preg2_Opt1.'</th>
            </tr>';
    }  
    switch ($datos->Opt2) {
      case 3:
        $opt2='Muy';
        break;
      case 2:
        $opt2='Medianamente';
        break;
      case 1:
        $opt2='Poco';
        break;
      case 0:
        $opt2='Nada';
        break;    
    }
    switch ($datos->Opt2) {
      case 3:
        $opt3='Muy';
        break;
      case 2:
        $opt3='Medianamente';
        break;
      case 1:
        $opt3='Poco';
        break;
      case 0:
        $opt3='Nada';
        break;    
    }
    switch ($datos->Opt4) {
      case 3:
        $opt4='Muy';
        break;
      case 2:
        $opt4='Medianamente';
        break;
      case 1:
        $opt4='Poco';
        break;
      case 0:
        $opt4='Nada';
        break;    
    }
    switch ($datos->Opt5) {
      case 3:
        $opt5='Muy';
        break;
      case 2:
        $opt5='Medianamente';
        break;
      case 1:
        $opt5='Poco';
        break;
      case 0:
        $opt5='Nada';
        break;    
    }
    switch ($datos->Opt6) {
      case 1:
        $opt6='Si';
        break;
      case 0:
        $opt6='No';
        break;    
    }




   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Creencias y valores</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">¿Qué ideal, valor, creencia o persona rigen o le dan sentido a tu vida?</th>
              <th width="50%">'.$datos->Preg1.'</th>
            </tr>
            <tr>
              <th >¿Perteneces a alguna religión?</th>
              <th >'.$opt1.'</th>
            </tr>
            '.$ropt1.'
            <tr>
              <th colspan="2">Selecciona las palabras que te describan mejor en el plano religioso:</th>
              
            </tr>
            <tr>
              <th >Creyente:</th>
              <th >'.$opt2.'</th>
            </tr>
            <tr>
              <th >Practicante:</th>
              <th >'.$opt3.'</th>
            </tr>
            <tr>
              <th >Identificado:</th>
              <th >'.$opt4.'</th>
            </tr>
            <tr>
              <th >Indiferente:</th>
              <th >'.$opt5.'</th>
            </tr>
            <tr>
              <th >¿Cómo se vive la religión en tu casa?</th>
              <th >'.$datos->Preg2.'</th>
            </tr>
            <tr>
              <th >Para ti, ¿Qué es la religión?</th>
              <th >'.$datos->Preg3.'</th>
            </tr>
            <tr>
              <th >Para ti, ¿Cuáles son los tres valores más importantes en la vida?</th>
              <th >'.$datos->Preg4.'</th>
            </tr>
            <tr>
              <th >Nombra un personaje de la vida pública o de la historia que admiras y ¿Por qué?</th>
              <th >'.$datos->Preg5.'</th>
            </tr>
            <tr>
              <th >¿Sabes que ésta es una universidad católica?</th>
              <th >'.$opt6.'</th>
            </tr>
            <tr>
              <th >¿Cómo crees que impacte esto en tu vida universitaria?</th>
              <th >'.$datos->Preg6.'</th>
            </tr>
          
            
          
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($ocio->result() as $datos) { 
     

    switch ($datos->Opt1) {
      case 1:
        $opt1='Si';
        $ropt1='<tr>
              <th >¿Cuál y por qué?</th>
              <th >'.$datos->Preg1_Opt1.'</th>
            </tr>';
        break;
      case 0:
        $opt1='No';
        $ropt1='';
        break;    
    }
    switch ($datos->Opt2) {
      case 1:
        $opt2='Si';
        $ropt2='<tr>
              <th >¿Cuál y por qué?</th>
              <th >'.$datos->Preg1_Opt2.'</th>
            </tr>';
        break;
      case 0:
        $opt2='No';
        $ropt2='';
        break;    
    }
   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Tiempo Libre y Ocio</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">¿Qué haces en tu tiempo libre?</th>
              <th width="50%">'.$datos->Preg1.'</th>
            </tr>
            <tr>
              <th >¿Tienes algún pasatiempo o hobbie?</th>
              <th >'.$opt1.'</th>
            </tr>
            '.$ropt1.'
            <tr>
              <th >¿Practicas algún deporte?</th>
              <th >'.$opt2.'</th>
            </tr>
            '.$ropt2.'
            <tr>
              <th >¿A qué lugares vas cuando sales de fiesta y cómo te diviertes?</th>
              <th >'.$datos->Preg2.'</th>
            </tr>
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($colegiomayor->result() as $datos) { 
     

    switch ($datos->Opt1) {
      case 1:
        $opt1='Si';
        $ropt1='<tr>
              <th >¿por qué?</th>
              <th >'.$datos->Preg1_Opt1.'</th>
            </tr>';
        break;
      case 0:
        $opt1='No';
        $ropt1='';
        break;    
    }
    switch ($datos->Opt2) {
      case 1:
        $opt2='Si';
        $ropt2='<tr>
              <th >¿En cuál?</th>
              <th >'.$datos->Preg1_Opt2.'</th>
            </tr>';
        break;
      case 0:
        $opt2='No';
        $ropt2='';
        break;    
    }
    switch ($datos->Opt3) {
      case 1:
        $opt3='Si';
        $search  = array('1', '2', '3', '4', '5', '6', '7');
        $replace = array(' Acción Social ', ' Turismo ', ' Emprendimiento ', ' Espiritualidad ', ' Integración ', ' Deportes ',' Identidad ');
        $ropt3='<tr>
              <th >¿Cuál?</th>
              <th >'.str_replace($search, $replace, $datos->Preg1_Opt3).'</th>
            </tr>';
        break;
      case 0:
        $opt3='No';
        $ropt3='';
        break;    
    }
   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Colegio Mayor</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">¿Residencia Anáhuac – Colegio Mayor fue el motivo principal para ingresar a la Universidad Anáhuac Puebla?</th>
              <th width="50%">'.$opt1.'</th>
            </tr>
            '.$ropt1.'
            <tr>
              <th >¿Has vivido anteriormente en una residencia Universitaria o Dormitorio?</th>
              <th >'.$opt2.'</th>
            </tr>
            '.$ropt2.'
            <tr>
              <th >¿Por qué elegiste vivir en Colegio Mayor - Residencia Anáhuac Puebla?</th>
              <th >'.$datos->Preg2.'</th>
            </tr>
            <tr>
              <th >¿Qué esperas vivir en la Colegio Mayor - Residencia Anáhuac Puebla?</th>
              <th >'.$datos->Preg3.'</th>
            </tr>
            <tr>
              <th >¿Cómo te gustaría que fuera la Colegio Mayor - Residencia Anáhuac Puebla?</th>
              <th >'.$datos->Preg4.'</th>
            </tr>
            <tr>
              <th >¿Cuáles son los hobbies, deportes o grupos en los que te gustaría participar dentro de la Residencia Anáhuac?</th>
              <th >'.$datos->Preg5.'</th>
            </tr>
            <tr>
              <th >¿Te gustaría participar en algún comité del Colegio Mayor - Residencia Anáhuac Puebla?</th>
              <th >'.$opt3.'</th>
            </tr>
            '.$ropt3.'
            <tr>
              <th >¿Cuál es la clave para que todos los alumnos que viven en la Residencia se lleven bien?</th>
              <th >'.$datos->Preg6.'</th>
            </tr>
            <tr>
              <th >Tú, ¿Cómo puedes contribuir a para que exista un buen ambiente en la Residencia?</th>
              <th >'.$datos->Preg7.'</th>
            </tr>
            <tr>
              <th >¿Cuál debería ser la primera norma de la Residencia para tener una sana y buena convivencia?</th>
              <th >'.$datos->Preg8.'</th>
            </tr>
            <tr>
              <th >¿Qué es lo que más te ilusiona de esta nueva etapa?</th>
              <th >'.$datos->Preg9.'</th>
            </tr>
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
foreach ($salud->result() as $datos) { 
     

    switch ($datos->Opt1) { //discapacidad
      case 1:
        $opt1='Si';
        $ropt1='<tr>
              <th >¿Cual?</th>
              <th >'.$datos->Preg1_Opt1.'</th>
            </tr>';
        break;
      case 0:
        $opt1='No';
        $ropt1='';
        break;    
    }
    switch ($datos->Opt2) { //problema de salud que requiera atencion medica
      case 1:
        $opt2='Si';
        $ropt2='<tr>
              <th >¿Cuál?</th>
              <th >'.$datos->Preg1_Opt2.'</th>
            </tr>
            <tr>
              <th >¿Con qué especialista?</th>
              <th >'.$datos->Preg2_Opt2.'</th>
            </tr>';
        break;
      case 0:
        $opt2='No';
        $ropt2='';
        break;    
    }
    switch ($datos->Opt3a) {//¿Estás bajo algún tratamiento físico, psíquico o psicológico actualmente?
      case 1:
        $opt3a='Si';
        $ropt3a='<tr>
              <th >¿Cuál?</th>
              <th >'.$datos->Preg1_Opt3a.'</th>
            </tr>';
        break;
      case 0:
        $opt3a='No';
        $ropt3a='';
        break;    
    }
    switch ($datos->Opt3) { //¿Has tenido en los dos últimos años algún trastorno físico, psíquico o psicológico que haya requerido atención médica?
      case 1:
        $opt3='Si';
        $ropt3='<tr>
                  <th >¿Cuál?</th>
                  <th >'.$datos->Preg1_Opt3.'</th>
                </tr>
                <tr>
                  <th >¿Con qué especialista?</th>
                  <th >'.$datos->Preg2_Opt3.'</th>
                </tr>
                <tr>
                  <th >¿Cuándo?</th>
                  <th >'.$datos->Preg3_Opt3.'</th>
                </tr>
                <tr>
                  <th >Descripción del caso:</th>
                  <th >'.$datos->Preg4_Opt3.'</th>
                </tr>';
        break;
      case 0:
        $opt3='No';
        $ropt3='';
        break;    
    }
    switch ($datos->Opt4) {// has vivido alguna tristesa
      case 1:
        $opt4='Si';
        $ropt4='<tr>
              <th >Descripción del caso:</th>
              <th >'.$datos->Preg1_Opt4.'</th>
            </tr>';
        break;
      case 0:
        $opt4='No';
        $ropt4='';
        break;    
    }
    switch ($datos->Opt5) { //seguro
      case 1:
        $opt5='Externo';
        $ropt5='<tr>
                  <th >Aseguradora:</th>
                  <th >'.$datos->Preg1_Opt5.'</th>
                </tr>
                <tr>
                  <th >No. De Póliza:</th>
                  <th >'.$datos->Preg2_Opt5.'</th>
                </tr>
                <tr>
                  <th >Fecha de vencimiento:</th>
                  <th >'.$datos->Preg3_Opt5.'</th>
                </tr>';
        break;
      case 2:
        $opt5='Interno';
        $ropt5='';
        break;
      case 0:
        $opt5='No';
        $ropt5='';
        break;    
    }
    switch ($datos->Opt6) { //fumas
      case 1:
        $opt6='Si';
        $ropt6='<tr>
              <th >¿Cuántos cigarros fumas al día en promedio?</th>
              <th >'.$datos->Preg1_Opt6.'</th>
            </tr>';
        break;
      case 0:
        $opt6='No';
        $ropt6='';
        break;    
    }
    switch ($datos->Opt7) {// se droga
      case 1:
        $opt7='Si';
        $ropt7='<tr>
              <th >¿Qué drogas, cuántas veces y con qué frecuencia?</th>
              <th >'.$datos->Preg1_Opt7.'</th>
            </tr>';
        break;
      case 0:
        $opt7='No';
        $ropt7='';
        break;    
    }
    switch ($datos->Opt8a) {// tus amigos se drogan
      case 1:
        $opt8a='Si';
        break;
      case 0:
        $opt8a='No';
        break;    
    }
    switch ($datos->Opt8) {// problemas para dormir
      case 1:
        $opt8='Si';
        break;
      case 0:
        $opt8='No';
        break;    
    }
    switch ($datos->Opt9) {// tipo de sueño
      case 4:
        $opt9='Muy Pesado';
        break;
      case 3:
        $opt9='Pesado';
        break;
      case 2:
        $opt9='Ligero';
        break;
      case 1:
        $opt9='Muy Ligero';
        break;    
    }
    switch ($datos->Opt10) {// 3 comidas
      case 1:
        $opt10='Si';
        $ropt10='';
        break;
      case 0:
        $opt10='No';
        $ropt10='<tr>
                  <th >Descripción del caso:</th>
                  <th >'.$datos->Preg8.'</th>
                </tr>';
        break;    
    }
    switch ($datos->Opt11) {// 3 comidas
      case 1:
        $opt11='Si';
        $ropt11='<tr>
                  <th >Descripción del caso:</th>
                  <th >'.$datos->Preg10.'</th>
                </tr>';
        break;
      case 0:
        $opt11='No';
        $ropt11='';
        break;    
    }
    switch ($datos->Opt12) {// 3 comidas
      case 1:
        $opt12='Si';
        $ropt12='<tr>
                  <th >¿Por que?</th>
                  <th >'.$datos->Preg11.'</th>
                </tr>';
        break;
      case 0:
        $opt12='No';
        $ropt12='';
        break;    
    }
   $html='
          <table border="0" cellpadding="6px">
            <tr>
              <th align="center"><b>Datos Médicos y de salud</b> </th>
            </tr>
          </table>
          <table border="1" cellpadding="4px">
            <tr>
              <th width="50%">¿Tienes alguna discapacidad o impedimento físico?</th>
              <th width="50%">'.$opt1.'</th>
            </tr>
            '.$ropt1.'
            <tr>
              <th >¿Tienes algún problema de salud que requiera atención médica o psicológica continua?</th>
              <th >'.$opt2.'</th>
            </tr>
            '.$ropt2.'
            <tr>
              <th >¿Estás bajo algún tratamiento físico, psíquico o psicológico actualmente?</th>
              <th >'.$opt3a.'</th>
            </tr>
            '.$ropt3a.'
            <tr>
              <th >¿Has tenido en los dos últimos años algún trastorno físico, psíquico o psicológico que haya requerido atención médica?</th>
              <th >'.$opt3.'</th>
            </tr>
            '.$ropt3.'
            <tr>
              <th >¿Alguna vez has vivido una tristeza profunda por más de 1 mes?</th>
              <th >'.$opt4.'</th>
            </tr>
            '.$ropt4.'
            <tr>
              <th >Alergias:</th>
              <th >'.$datos->Preg1.'</th>
            </tr>
            <tr>
              <th >Grupo sanguíneo:</th>
              <th >'.$datos->Preg2.'</th>
            </tr>
            <tr>
              <th >¿Cuentas con seguro de gastos médicos mayores?</th>
              <th >'.$opt5.'</th>
            </tr>
            '.$ropt5.'
            <tr>
              <th >¿Fumas?</th>
              <th >'.$opt6.'</th>
            </tr>
            '.$ropt6.'
            <tr>
              <th >¿Cada cuánto tiempo tomas bebidas alcohólicas?</th>
              <th >'.$datos->Preg3.'</th>
            </tr>
            <tr>
              <th >¿Cuántos vasos tomas en una noche?</th>
              <th >'.$datos->Preg4.'</th>
            </tr>
            <tr>
              <th >¿Consumes o has consumido drogas?</th>
              <th >'.$opt7.'</th>
            </tr>
            '.$ropt7.'
            <tr>
              <th >¿Tus amigos se drogan o se han drogado?</th>
              <th >'.$opt8a.'</th>
            </tr>
            <tr>
              <th >¿Tienes problemas para dormir?</th>
              <th >'.$opt8.'</th>
            </tr>
            <tr>
              <th >¿Qué tipo de sueño tienes?</th>
              <th >'.$opt9.'</th>
            </tr>
            <tr>
              <th >¿Cuántas horas duermes en la noche?</th>
              <th >'.$datos->Preg5.'</th>
            </tr>
            <tr>
              <th >¿Cuántas horas duermes en el día?</th>
              <th >'.$datos->Preg6.'</th>
            </tr>
            <tr>
              <th >¿Qué tipo de compañero de cuarto te gustaría tener?</th>
              <th >'.$datos->Preg7.'</th>
            </tr>
            <tr>
              <th >¿Comes 3 veces al día?</th>
              <th >'.$opt10.'</th>
            </tr>
            '.$ropt10.'
            <tr>
              <th >¿Por qué?</th>
              <th >'.$datos->Preg8.'</th>
            </tr>
            <tr>
              <th >¿Qué comes en cada comida de tu día?</th>
              <th >'.$datos->Preg9.'</th>
            </tr>
            <tr>
              <th >¿Alguna vez has tenido problemas de alimentación? (Bulimia, anorexia, etc.)</th>
              <th >'.$opt11.'</th>
            </tr>
            '.$ropt11.'
            <tr>
              <th >¿Haces dieta con frecuencia?</th>
              <th >'.$opt12.'</th>
            </tr>
            '.$ropt12.'

            
            
          </table>

        ';
  $pdf->writeHTML($html, true, false, true, false, '');
}
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>