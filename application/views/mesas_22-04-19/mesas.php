<style type="text/css">
  .galeriasimg{height: 249px;border: 4px;border-bottom-style:ridge;border-left-style:ridge;border-right-style:ridge;border-top-style:ridge; border-radius: 18px;  }
  .galeriasimg:hover{-webkit-box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);-moz-box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);box-shadow: 3px 4px 22px 0px rgba(0,0,0,0.75);}
  .galeriasimg h2{text-align: center;margin-top: 30%;color: white;text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);  }
  .input-group > .input-group-prepend > .btn, .fc .input-group > .input-group-prepend > button, .input-group > .input-group-prepend > .input-group-text, .input-group > .input-group-append:not(:last-child) > .btn, .fc .input-group > .input-group-append:not(:last-child) > button, .input-group > .input-group-append:not(:last-child) > .input-group-text, .input-group > .input-group-append:last-child > .btn:not(:last-child):not(.dropdown-toggle), .fc .input-group > .input-group-append:last-child > button:not(:last-child):not(.dropdown-toggle), .input-group > .input-group-append:last-child > .input-group-text:not(:last-child) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  .input-group-text{font-size: 1rem;font-weight: 400;line-height: 1.5;display: -webkit-box;display: -webkit-flex;display:-moz-box;display: -ms-flexbox;    display:flex;    margin-bottom: 0;    padding: .375rem .75rem;    text-align: center;    white-space: nowrap;    color: #495057;    border: 1px solid #ced4da;    border-radius: .25rem;     background-color: #e9ecef;    -webkit-box-align: center;    -webkit-align-items: center;-moz-box-align: center;    -ms-flex-align: center;align-items: center;}

  .cursor 
  {
    cursor:pointer;
    cursor:hand;
  }
</style>
<div class="row">
  <div class="col-md-12 regresaprincipal">
    <br>
    <button class="cursor btn btn-raised btn-info btn-min-width mr-1 mb-1">Ver listado de Mesas / Cerrar sesión de mesero</button>

    <?php $_SESSION = $_SESSION;?>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">      
  
  <div class="col-sm-12">
      <div class="card">
        <!-- Modal -->
                  <div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <label class="modal-title text-text-bold-600" id="myModalLabel33" >Ingrese Pin de Usuario</label>
                          
                        </div>
                            <div class="modal-body">
                          <label>PIN: </label>
                          <div class="form-group">
                            <input type="password" placeholder="Pin" class="form-control pinModalUsuario" id="pinModalUsuario" autocomplete="new-password">
                            <input type="hidden" class="form-control" id="idProductoModal">
                            <input type="hidden" class="form-control" id="banderaRealizarVenta">
                          </div>
                          </div>
                          <div class="modal-footer">
                          <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cerrar" id="botonModalCerrar">
                          <input type="button" class="btn btn-outline-primary btn-lg" value="Ingresar" id="botonModalConfirmar">
                          </div>
                        
                      </div>
                    </div>
                  </div>

          


          <div class="card-body">
            <div class="card-header">
                <h4 class="card-title"> Ventas</h4>
                <br>
                <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1 cursor regresaMesaActual"> Agregar nuevo producto a la mesa actual <i class="ft-plus-square"></i></button>
            </div>
          
              <div class="card-block form-horizontal">
                <!--------//////////////-------->
                  <div class="row panelventa1">
                    <div class="card-text getting-started pt-1 mt-2 display-inline-block ">
                          <div class="clockCard px-4 py-4 mr-4 mb-4 bg-blue bg-darken-2 box-shadow-2 selectedmesarapida cursor" data-mesa="0"> 
                              <span> </span>
                              <p class="lead mt-2 mb-0">Venta <br> Rápida</p>
                          </div>
                      <?php 
                        $mesa=1;
                        while ($mesa <= $nummesas){ ?>
                          <div class="clockCard px-4 py-4 mr-4 mb-4 bg-green bg-darken-2 box-shadow-2 selectedmesa cursor" data-mesa="<?php echo $mesa; ?>"> <span><?php echo $mesa; ?></span> <br>
                            <p class="lead mt-2 mb-0"> Mesa </p>
                          </div>
                        <?php 
                          $mesa++;
                        }
                      ?>
                      
                    </div>  
                    
                  </div>
                  <div class="row panelventa2" style="display: none;">
                    <?php foreach ($productospadres->result() as $item){ ?>
                        <div class="col-md-3 galeriasimg flipInX animated classproducto cursor" data-idcate="<?php echo $item->productopId; ?>" style="background: url('<?php echo base_url(); ?>public/img/categoria/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;">
                          <h2><?php echo $item->categoria;?></h2>
                        </div>
                      <?php } ?>

                        <div class="col-md-3 galeriasimg flipInX animated classVenta cursor" data-idcate="<?php echo $item->productopId; ?>" style="background: url('<?php echo base_url(); ?>public/img/ventas.jpg');     background-repeat: no-repeat;background-size: 100%;background-position: center;">
                          <h2>Realizar Venta</h2>
                        </div>
                  </div>
                  <div class="row panelventa3" style="display: none;">
                    
                  </div>
                  <?php //var_dump($_SESSION["mesa"]); echo "<br>"; ?>
                  <div class="row panelventa4" style="display: none;" id="panelventa4" name="panelventa4">

                    <div class="col-md-12">
                      <table class="table table-hover" id="tableproductosv">
                        <thead>
                          <tr>
                            <td>Cantidad</td>
                            <td></td>
                            <td>Producto</td>
                            <td>Precio</td>
                            <td>Total</td>
                            <td>Acciones</td>
                          </tr>
                        </thead>
                        <tbody class="addselectedprod" id="addselectedprod" name="addselectedprod">
                          
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-12 divPadre">
                      <div class="col-md-10"></div>
                      <div class="col-md-2">
                        <h3>Total: $<span class="totalproductos">00.00</span></h3>
                        <input type="hidden" id="totalproductos" value="0" class="inputHijo">
                      </div>
                      

                      <div class="col-md-10"></div>
                      <div class="col-md-2">
                        <label for="montoapagar">Descuento</label>
                        <input type="number" id="descuentoapagar" class="form-control descuentoapagar" name="descuentoapagar" value="0" onclick="habilitaDescuento()" readonly style="color: red;">
                      </div>

                      <div class="col-md-10"></div>
                      <div class="col-md-2">
                        <label for="montoapagar">Pago</label>
                        <input type="number" id="montoapagar" value="" onchange="calculartotalVacio(this.id);" class="form-control inputMontoPagar">
                      </div>

                      <div class="col-md-10"></div>
                      <div class="col-md-2">
                        <h3>Cambio: $<span class="cambio">00.00</span></h3>
                      </div>
                    </div>
                    <div class="vender col-md-10" style="display: none">
                      <div class="col-md-12">
                        <label class="col-md-3">Metodo de pago:</label>
                        <div class="col-md-5">
                          <select class="form-control" id="metodopago" style="margin-bottom: 10px">
                            <option value="1">Efectivo</option>
                            <option value="2">Tarjeta de credito / Debito</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12 cuentakidzity" style="display: none;">
                        <label class="col-md-3">Cuenta KidZity:</label>
                        <div class="col-md-5">
                          <input type="" id="cuentakz" class="form-control" style="margin-bottom: 10px" >
                        </div>
                        <div class="col-md-4 infocredito">
                          
                        </div>
                      </div>
                    </div>
                    <br>
                   
                    <div class="col-md-12 divPadreVender"> 
                      <div class="col-md-4"></div>
                      <div class="col-md-4">
                        <input type="hidden" name="banderaTicket" id="banderaTicket">
                        
                        <button type="button" class="btn btn-raised btn-success btn-min-width mr-12 mb-12 col-md-12 vender" name="vender" id="vender" style="display: none" onclick="clickVender(this.id)">Vender</button>

                        <button type="button" class="btn btn-raised btn-success btn-min-width mr-12 mb-12 col-md-12 ticket imprimir" id="ticket" style="display: none" onclick="clickTicket(this.id)">Imprimir</button>
                      </div>
                      <div class="col-md-4"></div>
                    </div>

                    <div class="col-md-12"> 
                      <div class="col-md-4"></div>
                      <div class="col-md-4">
                        
                        <button type="button" class="btn btn-raised btn-success btn-min-width mr-12 mb-12 col-md-12" name="dividir" id="dividir" style="display: inline" onclick="clickDividir(this.id)">Dividir Cuenta</button>

                      </div>
                      <div class="col-md-4"></div>
                    </div>

                    

                  </div>
                  
                  
                <!--------//////////////-------->
              </div>
          </div>
      </div>
    </div>
</div>
<div class="col-md-12 ticketventa" style="display: none;">
  
</div>

<div class="modal fade text-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h3 class="modal-title" id="myModalLabel34">Autorizacion</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      </div>
      
      <div class="modal-body">
        <label>Password: </label>
        <div class="form-group position-relative has-icon-left">
          <input type="password" placeholder="Password" id="passautorizacion" class="form-control" autocomplete="off">
                                  </div>
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" >Cancelar</button>
        <button type="submit" class="btn btn-outline-primary btn-lg modalsalidam" data-dismiss="modal">Aceptar</button>
      </div>
      
    </div>
  </div>
</div>

<!-- MODAL DE DIVISIÓN DE CUENTA -->
<div class="modal fade text-left" id="modalDividirCuenta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h3 class="modal-title" id="myModalLabel34">Dividir cuenta</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      </div>
      
      
      <div class="modal-footer">
        <button type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" >Cancelar</button>
        <button type="submit" class="btn btn-outline-primary btn-lg modalsalidam" data-dismiss="modal">Aceptar</button>
      </div>
      
    </div>
  </div>
</div>