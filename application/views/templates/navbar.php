<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
if (!isset($_SESSION['perfilid'])) {
    $perfil=0;
}else{
    $perfil=$_SESSION['perfilid'];  
}
if(!isset($_SESSION['usuario'])){
    ?>
    <script>
        document.location="<?php echo base_url(); ?>index.php/Login"
    </script>
    <?php
}

$menu=$this->ModeloSession->menus($perfil);
?>
<!-- main menu-->
<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<!--<div data-active-color="white" data-background-color="purple-bliss" data-image="<?php echo base_url(); ?>public/img/Slides-10.jpg" class="app-sidebar">-->
<div data-active-color="white" data-background-color="king-yna" class="app-sidebar">
<!-- main menu header-->
<!-- Sidebar Header starts-->
<div class="sidebar-header" >
  <div class="logo clearfix" style="background: white">
      <a href="<?php echo base_url(); ?>Inicio" class="logo-text float-left">
        <div class="logo-img">
          
        </div>
        <span class="text pull-left">
          <img src="<?php echo base_url(); ?>public/img/ops.webp" style="width: 139px;" >
        </span>
      </a>
      <a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block" style="color: red;">
        <i data-toggle="<?php echo $GLOBALS['menutoggle'];?>" class="<?php echo $GLOBALS['menutoggleico'];?>"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a></div>
</div>
<!-- Sidebar Header Ends-->
<!-- / main menu header-->
<!-- main menu content-->
<div class="sidebar-content">
  <div class="nav-container">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <?php foreach ($menu->result() as $item){ ?>
          <li class="has-sub nav-item"><a href="#"><i class="<?php echo $item->Icon; ?>"></i><span data-i18n="" class="menu-title"><?php echo $item->Nombre; ?></span></a>
            <ul class="menu-content">
                <?php 
                    $perfil=$perfil;
                    $menu =  $item->MenuId;
                    $menusub = $this->ModeloSession->submenus($perfil,$menu);
                    foreach ($menusub->result() as $datos) { ?>
                        <li>
                            <a href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>" class="menu-item">
                                <i class="<?php echo $datos->Icon; ?>"></i>
                                <?php echo $datos->Nombre; ?>
                            </a>
                        </li>
                <?php } ?>
              
            </ul>
          </li>
        <?php } ?>
    </ul>
  </div>
</div>
<!-- main menu content-->
<div class="sidebar-background"></div>
<!-- main menu footer-->
<!-- include includes/menu-footer-->
<!-- main menu footer-->

</div>
<!-- / main menu-->

<!-- Navbar (Header) Starts-->
      <nav class="navbar navbar-expand-lg navbar-light bg-faded" >
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
              <ul class="navbar-nav">
                <li class="nav-item mr-2"><a id="navbar-fullscreen" href="javascript:;" class="nav-link apptogglefullscreen"><i class="ft-maximize font-medium-3 blue-grey darken-4"></i>
                    <p class="d-none">fullscreen</p></a></li>
                <!--<li class="dropdown nav-item"><a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-flag font-medium-3 blue-grey darken-4"></i><span class="selected-language d-none"></span></a>
                  <div class="dropdown-menu dropdown-menu-right"><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/us.png" class="langimg"/><span> English</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/es.png" class="langimg"/><span> Spanish</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/br.png" class="langimg"/><span> Portuguese</span></a><a href="javascript:;" class="dropdown-item"><img src="../app-assets/img/flags/de.png" class="langimg"/><span> French</span></a></div>
                </li>-->
               
                <li class="dropdown nav-item">
                  <?php 

                    if (!isset($_SESSION['foto'])) {
                      $imgusers=base_url().'public/img/ops.webp';
                    }else{
                      $imgusers=base_url().'public/img/personalt/'.$_SESSION['foto'];
                      if ($_SESSION['foto']=='') {
                        $imgusers=base_url().'public/img/fav.png';
                      }
                    }
                    echo file_exists($imgusers);
                  ?>
                  <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="font-medium-3 blue-grey darken-4"><img src="<?php echo $imgusers;?>" style="width: 36px; border-radius: 14px;"> <?php if(isset($_SESSION['usuario'])){ echo $_SESSION['usuario'];}?></i>
                    <p class="d-none">User Settings</p></a>
                      <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                        
                        <a href="<?php echo base_url(); ?>index.php/Login/exitlogin" class="dropdown-item"><i class="ft-power mr-2"></i><span>Cerrar</span></a>
                  </div>
                </li>
                
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <!-- Navbar (Header) Ends-->
      <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">