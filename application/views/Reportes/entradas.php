<style type="text/css">
  label{
      font-size: .75rem;
      font-weight: 500;
      letter-spacing: 2px;
      text-transform: uppercase;
      color: #75787d;
      width: 100%;
  }
</style>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de personal</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <!-------------->
          <div class="row">
            <div class="col-md-3">
              <label>Personal</label>
              <select class="form-control" id="personal">
                <option value="0">TODOS</option>
                <?php foreach ($personal->result() as $item){ ?>
                  <option value="<?php echo $item->personalId; ?>"><?php echo $item->nombre; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-3">
              <label>Fecha Inicio</label>
              <input type="date" id="fechainicio" class="form-control" value="<?php echo date('Y-m-d');?>">
            </div>
            <div class="col-md-3">
              <label>Fecha Fin</label>
              <input type="date" id="fechafin" class="form-control" value="<?php echo date('Y-m-d');?>">
            </div>
            <div class="col-md-3">
              <label style="color: transparent;" class="form-label" >Consultar</label>
              <button class="btn btn-primary"  type="submit"  id="consultar"> Consultar</button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 cargadatos">
              
            </div>
          </div>
          
          <!-------------->
        </div>
      </div>
    </div>
  </div>
</div>