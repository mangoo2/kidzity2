<div class="container">
	<section class="basic-elements">
		<div class="center">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title mb-0">Ventas de mostrador</h4>
				</div>
				<div class="card-body">
					<div class="px-3">
						<form class="form" role="form" id="formProducto" name="formProducto" method="post" >
							<div class="form-body">
								<div class="row">
									<div class="container">
										<div class="col-xl-7 col-lg-7 col-md-7">
											<fieldset class="form-group">
												<label for="Tutor">Nombre del tutor</label>
												<input type="text" class="form-control" id="Tutor" name="Tutor">
											</fieldset>
										</div>
										<div class="col-xl-7 col-lg-7 col-md-7">
											<fieldset class="form-group">
												<label for="Menor">Nombre del menor</label>
												<input type="text" class="form-control" id="Menor" name="Menor">
											</fieldset>
										</div>
										<div class="col-xl-12 col-lg-12 col-md-12">
											<div class="col-xl-6 col-lg-6 col-md-6"></div>
											<div class="col-xl-3 col-lg-3 col-md-3">
												<a class="btn btn-primary" href="" ><i class="ft-search"></i>Buscar</a>
											</div>
										</div>
										<div class="col-xl-12 col-lg-12 col-md-12">
											<br>
											<br>
										</div>
										<div class="col-xl-12 col-lg-12 col-md-12">
											<table class="table thead-ligh table-striped">
												<tr>
													<th>Tutor</th>
													<th>Telefono</th>
													<th>Menor(es)</th>
													<th>Acciones</th>
												</tr>
												<tr>
													<td>1</td>
													<td>1</td>
													<td>1</td>
													<td><a class="btn btn-succes" href="<?php echo base_url(); ?>Ventas/VentasEstacia3" ><i class="icon-arrow-right"></i></a></td>
												</tr>
												<tr>
													<td>1</td>
													<td>1</td>
													<td>1</td>
													<td><a class="btn btn-succes" href="<?php echo base_url(); ?>Ventas/VentasEstacia3" ><i class="icon-arrow-right"></i></a></td>
												</tr>
												<tr>
													<td>1</td>
													<td>1</td>
													<td>1</td>
													<td><a class="btn btn-succes" href="<?php echo base_url(); ?>Ventas/VentasEstacia3" ><i class="icon-arrow-right"></i></a></td>
												</tr>
												<tr>
													<td>1</td>
													<td>1</td>
													<td>1</td>
													<td><a class="btn btn-succes" href="<?php echo base_url(); ?>Ventas/VentasEstacia3" ><i class="icon-arrow-right"></i></a></td>
												</tr>
											</table>
										</div>
										<div class="col-xl-12 col-lg-12 col-md-12">
											<br>
											<br>
										</div>
										<div class="col-xl-12 col-lg-12 col-md-12">
											<div class="col-xl-10 col-lg-10 col-md-10"></div>
											<div class="col-xl-2 col-lg-2 col-md-2">
												<a class="btn btn-danger" href="">Cancelar<i class="ft-x-circle"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>