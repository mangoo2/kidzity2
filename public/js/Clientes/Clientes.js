var base_url= $('#base_url').val();
var table;
$(document).ready(function() {
    table = $('#data-tables').DataTable();
    $("#savecliente").click(function() {
        var ruta = "";
        if ($('#ID').length) {
            console.log("el imput existe");
            var ruta = 'Update';
        } else {
            console.log("el imput no existe");
            var ruta = 'Registrar';
        }
        var datos = $('#formCliente').serialize();
        $.ajax({
            type: 'POST',
            url: ruta + 'Cliente',
            data: datos,
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error', '404');
                },
                500: function() {
                    toastr.error('Error', '500');
                }
            },
            success: function(data) {
                toastr.success('Cliente agregado');
                console.log("El id de cliente es: " + data);
                var info = $('#formCliente2').serialize();
                $.ajax({
                    type: 'POST',
                    url: ruta + 'SubTitular',
                    data: "ID=" + data + "&" + info,
                    async: false,
                    statusCode: {
                        404: function(data) {
                            toastr.error('Error', '404');
                        },
                        500: function(data) {
                            console.log(data);
                            toastr.error('Error', '500');
                        }
                    },
                    success: function(val) {
                        toastr.success('Subtitular agregado');
                        console.log(data);
                        var enviar = $('#formCliente3').serialize();
                        $.ajax({
                            type: 'POST',
                            url: ruta + 'Menor',
                            data: "ID=" + val + "&" + enviar,
                            async: false,
                            statusCode: {
                                404: function(data) {
                                    toastr.error('Error', '404');
                                },
                                500: function() {
                                    toastr.error('Error', '500');
                                }
                            },
                            success: function(info) {
                                toastr.success("Menor agregado");
                                //if (ruta=='Registrar') {
                                  var url=base_url+"Ventas/VentasEstacia2?cod="+val;
                                  //window.open(url, '_blank');
                                  location.href =url;
                                  console.log('xxxx');
                                //}
                                
                            },

                            error: function(jqXHR, estado, error) {
                                console.log("Estado3: " + estado);
                                console.log(jqXHR);
                                console.log("Error3: " + error);
                            },

                        });

                    },

                    error: function(jqXHR, estado, error) {
                        console.log("Estado2: " + estado);
                        console.log(jqXHR);
                        console.log("Error2: " + error);
                    },

                });

            },
            error: function(jqXHR, estado, error) {
                console.log("Estado: " + estado);
                console.log(jqXHR);
                console.log("Error: " + error);
            },
        });
    });
    $('#aceptareliminar').click(function(event) {
        $.ajax({
            type: 'POST',
            url: base_url + 'Clientes/DeleteAll',
            data: {
                id: $('#clienteiddelete').val()
            },
            async: false,
            statusCode: {
                404: function(data) {
                    toastr.error('Error', '404');
                },
                500: function(data) {
                    toastr.error('Error', '500');
                },
            },
            success: function(data) {
                toastr.success('Se elimino correctamente', 'Hecho');
                console.log("vamos aver");
                loadtable();
            },
            error: function(jqXHR, estado, error) {
                console.log(estado);
                console.log(jqXHR);
                console.log(error);

            },

        });
    });
    loadtable();
});
function DeleteCliente(id){
    $('#modalconfirmacion').modal();
    $('#clienteiddelete').val(id);
}
function DeleteSubt(id) {
    var UR = $('#url').val();
    $.ajax({
        type: 'POST',
        url: UR + 'Clientes/DeleteSubt',
        data: {
            id: id
        },
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', '500');
            },
        },
        success: function(data) {
            toastr.success('Se elimino correctamente', 'Hecho');
            console.log("vamos aver");
            location.reload(true);
        },
        error: function(jqXHR, estado, error) {
            console.log(estado);
            console.log(jqXHR);
            console.log(error);

        },

    });
}

function DeleteMen(id) {
    var UR = $('#url').val();
    $.ajax({
        type: 'POST',
        url: UR + 'Clientes/DeleteMen',
        data: {
            id: id
        },
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error', '404');
            },
            500: function(data) {
                toastr.error('Error', '500');
            },
        },
        success: function(data) {
            toastr.success('Se elimino correctamente', 'Hecho');
            loadtable();
        },
        error: function(jqXHR, estado, error) {
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
        },

    });
}
function Ver() {
    var datos = $('#formCliente2').serialize();
    $.ajax({
        type: 'POST',
        url: 'RegistrarSubTitular',
        data: "ID=" + "1" + "&" + datos,
        async: false,
        success: function(data) {
            console.log(data);
        }
    });
}
function loadtable(){
    table.destroy();
    table=$('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        "ajax": {
                "url": base_url+"Clientes/getlistclientes",
                type: "post",
                "data": {
                    'tipo':0,
                },
            },
        "columns": [
                {"data": "titularId"},
                {"data": "nombre"},
                {"data": "movil"},
                {"data": "ninos",orderable: false},
                {"data": null,
                    "render" : function (data,type,row) {
                        var html='';
                            html+='<div class="row">\
                                        <div class="col-md-12" align="center">\
                                            <a class="btn btn-primary" href="'+base_url+'Clientes/Clienteadd?Cli='+row.titularId+'"><i class="ft-edit-3"></i></a>\
                                            <a class="btn btn-primary" href="'+base_url+'Ventas/VentasEstacia2?cod='+row.titularId+'">venta Estancia</a>\
                                            <a class="btn btn-danger" onclick="DeleteCliente('+row.titularId+')"><i class="fa fa-trash-o"></i></a>\
                                        </div>\
                                    </div>';
                        return html;
                    }
                }
            ],
        order: [[0, 'desc']]
    });
}