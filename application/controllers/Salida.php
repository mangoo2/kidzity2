<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salida extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
    }

    function index(){
        if ($this->idpersonal==0) {
            redirect('sistema');
        }
        
        $data['idpersonalid']=$this->idpersonal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Clientes/Salida',$data);
        $this->load->view('templates/footer');
        $this->load->view('Clientes/jsalida');
    }
    function view(){
        $pulcera = $this->input->post('id');
        $vigente = $this->ModeloCatalogos->pulceravigente($pulcera);
        $precios=$this->ModeloCatalogos->getcostotime();
        $precio=0;
        foreach ($precios->result() as $item) {
            $precio=$item->precio;
        }
        //============================ tiempo libre ======================
            $resultado=$this->ModeloCatalogos->getselectwhere('config','configId',2);
            $valortl=0;
            foreach ($resultado->result() as $row) {
                $valortl =$row->valor;
            } 
            $resultado=$this->ModeloCatalogos->getselectwhere('config','configId',3);
            $valortlbb=0;
            foreach ($resultado->result() as $row) {
                $valortlbb =$row->valor;
            } 
        //================================================================
        date_default_timezone_set('America/Mexico_City');
        $vigenter=0;
        $msj='
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card-inverse bg-danger text-center" style="border-radius: 23px;" >
                    <div class="">
                        <div class="card-block pt-3">
                            <div class="row d-flex">
                                <div class="col align-self-center">
                                    <h4 class="card-title mt-3">Pulcera no existe o no se encuentra vigente</h4>
                                    <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>
                                    <button class="btn btn-raised btn-danger btn-darken-3">Buy Now</button>-->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
            ';
        $msj2='';
        $timepopagomensaje='';
        $pago_devolucion=0;
        foreach ($vigente->result() as $row) {
            $ninoid=$row->ninoid;
            $pago_devolucion=$row->pagado;
            $msj='';
            $tipo=$row->tipo;
            if ($row->paquetefamiliar>0) {
                $tipo=1;
            }
            if ($tipo==0) {
                $hora_de_entrada=date('H:i:s',strtotime($row->reg_abierto));
                //echo $hora_de_entrada; //BORRAR DESPUES
                echo '<br>';//BORRAR DESPUES
                $hora_de_salida=date('H:i:s');
                //echo $hora_de_salida;//BORRAR DESPUES
                    $horaactual2 = new DateTime($hora_de_entrada);
                    $horasalida2 = new DateTime($hora_de_salida);

                    $interval = $horaactual2->diff($horasalida2);
                    $minex=$interval->format('%i');
                    if($minex<=10){// TIEMPO DE TOLERANCIA
                        $minex=0;
                    }elseif($minex>10 and$minex<=30){
                        $minex=0.5;
                    }else{
                        $minex=1;
                    }
                    $horaex=$interval->format('%h');
                //echo '<br>';//BORRAR DESPUES
                //echo 'diferencia Horas:'.$horaex.' minutos:'.$minex;//BORRAR DESPUES
                //echo '<br>';//BORRAR DESPUES
                //echo '<br>';//BORRAR DESPUES
                $tiempototalestancia=$horaex+$minex;
                if($tiempototalestancia<3){
                    //echo '<br> if 3 ---------<br>';//BORRAR DESPUES
                    $cantidadpagar=($tiempototalestancia*$precio)-$row->pagado;
                    //========delimita el tiempo extra
                        $horasalida = strtotime ('+'.$row->tiempo.' hour' , strtotime ($row->reg_abierto) ) ;
                        $horasalida=date ( 'H:i:s' , $horasalida);
                        //echo 'Hora salida:'.$horasalida;
                        if (date('H:i:s')>date('H:i:s',strtotime($horasalida))) {
                            //echo '<br> if 3 a---------<br>';//BORRAR DESPUES
                            $horaactual2a = new DateTime($horasalida);
                            $horaactual2 = new DateTime(date('H:i:s'));
                            $interval = $horaactual2->diff($horaactual2a);
                            $minex=$interval->format('%i');
                            //echo '<br>min-.-:'.$minex;
                            if($minex<=10){// TIEMPO DE TOLERANCIA
                                $minex=0;
                                //echo '<br>min-:'.$minex;
                            }elseif($minex>10 and$minex<=30){
                                $minex=0.5;
                            }else{
                                $minex=1;
                            }
                            $horaex=$interval->format('%h');
                            //echo '<br>hora: '.$horaex.' + minutos'.$minex;
                            $tiempoextra=$horaex+$minex;
                            //echo '<br>'.$tiempoextra;
                            # code...
                        }else{
                            //echo '<br> if 3 b---------<br>';//BORRAR DESPUES
                            $cantidadpagar=0;
                            $tiempoextra=0;
                        }
                        $msjhorafinalizada='<p>Hora finalizada: '.date('h:i A',strtotime($horasalida)).'</p>';
                        if ($row->tiempo==3) {
                            $msjhorafinalizada='';
                            $timepopagomensaje='<p class="card-text">Tiempo libre</p>';
                            $tiempoextra=0;
                            $tiempopagado=3;
                        }
                    //================================

                }else{
                    //===================================================
                    $fechanacimiento = new DateTime('1988-04-01');
                    $fechaactualn = new DateTime('2019-03-20');

                    $intervaledad = $fechanacimiento->diff($fechaactualn);
                    $edad=$intervaledad->format('%Y');
                    if ($edad<=5) {
                        $cantidadpagar=$valortlbb-$row->pagado;
                    }else{
                        $cantidadpagar=$valortl-$row->pagado;
                    }
                    //===================================================
                    $timepopagomensaje='<p class="card-text">Tiempo libre</p>';
                    $tiempopagado=3;// 3 tiempo libre
                    //$cantidadpagar=0;
                    $tiempoextra=0;
                    $msjhorafinalizada='';

                }
               

            }else{
                $msjhorafinalizada='';
                $timepopagomensaje='<p class="card-text">Tiempo libre</p>';
                $tiempopagado=3;// 3 tiempo libre
                $cantidadpagar=0;
                $tiempoextra=0;
            }
            if ($row->tlbebe==1) {
                $msjhorafinalizada='';
                $timepopagomensaje='<p class="card-text">Tiempo libre</p>';
                $tiempopagado=3;// 3 tiempo libre
                $cantidadpagar=0;
                $tiempoextra=0;
            }
            if ($row->tladulto==1) {
                $msjhorafinalizada='';
                $timepopagomensaje='<p class="card-text">Tiempo libre</p>';
                $tiempopagado=3;// 3 tiempo libre
                $cantidadpagar=0;
                $tiempoextra=0;
            }
            
            if ($cantidadpagar<0) {
                $cantidadpagar=0;
            }
            //======================================================================================
            $resultadoc=$this->ModeloCatalogos->getselectwhere('compra_tiempo','compraId',$row->compraId);
            $ninosrow=$this->ModeloCatalogos->getninoscompras($row->compraId);
            foreach ($resultadoc->result() as $item) {
                $titularId=$item->titularId;
            }
            $datostitular=$this->ModeloCatalogos->getselectwhere('titular','titularId',$titularId);
            $nametitular='';
            foreach ($datostitular->result() as $item) {
                $nametitular=$item->nombre;
            }
            $ninodrow='';
            $ninodrown=1;
            foreach ($ninosrow->result() as $item) {
                if($item->ninoid==$ninoid){
                    $ninoresaltar='class="ninoresaltar"';
                }else{
                    $ninoresaltar='';
                }
                $tiempo = strtotime($item->fecha_nacimiento); 
                $ahora = time(); 
                $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
                $edad = floor($edad);
                $ninodrow.='<tr '.$ninoresaltar.'><td>'.$ninodrown.'</td><td>'.$item->nombre.'</td><td>'.$edad.'</td></tr>';
                $ninodrown++;
            }
            //=======================================================================================
            $msj2.='
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="card-inverse bg-success text-left" style="border-radius: 23px;">
                        <div class="">
                            <div class="card-block pt-3">
                                <div class="row d-flex">
                                    <div class="col align-self-left">
                                        <div class="col-md-12" style="text-align: center;">
                                        <h4>'.$nametitular.'</h4>
                                        </div>
                                        <div class="col-md-12">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Niños</th>
                                                    <th>Edad</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                '.$ninodrow.'
                                            </tbody>
                                        </table>
                                        
                                        </div>

                                        <p class="card-text">Hora de entrada: '.date('h:i A',strtotime($row->reg_abierto)).' </p>';
                                        $msj2.='<input type="text" class="inpuoculto" id="tiempoextra" value="'.$tiempoextra.'" title="tiempoextra" readonly>';
                                        if ($tiempoextra>0) {
                                            $msj2.='<p>Timepo Extra : '.$tiempoextra.' hrs</p>';
                                        }
                                        $msj2.=$msjhorafinalizada;
                                        $msj2.=$timepopagomensaje;


                                        $msj2.='<input type="text" class="inpuoculto" id="totalpago" value="'.$cantidadpagar.'" title="totalpago" readonly>';

                                        $msj2.='<input type="text" class="inpuoculto" id="compranId" value="'.$row->compranId.'" title="compranId" readonly>';
                                        $msj2.='<input type="text" class="inpuoculto" id="compraId" value="'.$row->compraId.'" title="compraId" readonly>';


                                        $msj2.='<p>Cantidad a pagar: '.$cantidadpagar.'</p>';
                                        if ($cantidadpagar==0) {
                                            $readonly='readonly';
                                        }else{
                                            $readonly='';
                                        }
                                        $msj2.='<div class="row">
                                                <label class="col-md-4">Compensación: </label>
                                                <div class="col-md-8"><input type="number" id="compensacion" value="0" min="'.$cantidadpagar.'" class="form-control" readonly style="background: transparent" ></div>
                                                </div>';
                                        $msj2.='<div class="row">
                                                    <label class="col-md-4">Pago: </label>
                                                    <div class="col-md-8">
                                                        <input type="number" id="montopago" value="0" min="'.$cantidadpagar.'" class="form-control" '.$readonly.' oninput="restapago()">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-4">Cambio: </label>
                                                    <div class="col-md-8 cambiosalida">0
                                                        
                                                    </div>
                                                </div>





                                        ';

                            $msj2.='
                                    <div class="col-md-12">
                                        <button class="btn btn-raised btn-danger btn-darken-3 col-md-3" onclick="limpiar()">Limpiar</button>
                                        <button class="btn btn-raised btn-primary btn-darken-3 col-md-3" onclick="salir()">Salir</button>
                                        <div class="col-md-3"></div>
                                        <button class="btn btn-raised btn-info btn-darken-3 col-md-3" onclick="devolucion('.$row->compranId.','.$pago_devolucion.')">Devolución</button>

                                    </div>
                                        
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            ';
            //============================================================================================  
        }
        
        


        
        echo $msj.$msj2;
    }
    function salir(){
        $id = $this->input->post('id');
        $pago = $this->input->post('pago');
        $tiempoex = $this->input->post('tiempoex');
        $idpersonal = $this->input->post('idpersonal');
        if ($this->idpersonal>0) {
            $idpersonal=$this->idpersonal;
        }
        $this->ModeloCatalogos->ninosalida($id,$pago,$tiempoex,$idpersonal);

        $resultado = $this->ModeloCatalogos->getselectwhere('compra_tiempo_nino','compranId',$id);
        $ninossalidos=0;
        $compraId=0;
        $totalninos=0;
        foreach ($resultado->result() as $row) {
            $compraId=$row->compraId;
        }
        
        if ($compraId>0) {
            $totalninos = $this->totalninos($compraId);

            $totalninosc=$this->ninocerrados($compraId);

            $totalcerrados=$totalninos-$totalninosc;
            if ($totalcerrados==0) {
                $data = array('status' => 0);
                $this->ModeloCatalogos->updateCatalogo($data,'compraId',$compraId,'compra_tiempo');
            }
        }

        echo $id.' '.$totalninos.' '.$totalninosc;
    }

    function totalninos($compraId){
        $totalninos=0;
        $resultado2 = $this->ModeloCatalogos->getselectwhere('compra_tiempo_nino','compraId',$compraId);
        foreach ($resultado2->result() as $row) {
            $totalninos++;
        }
        return $totalninos; 
    }
    function ninocerrados($compraId){
        $resultado3 = $this->ModeloCatalogos->getselectwhere2('compra_tiempo_nino','compraId',$compraId,'status',0);
        $totalninosc=0;
        
        foreach ($resultado3->result() as $row) {
            $totalninosc++;
        }
        return $totalninosc;
    }
    function salidamanual(){
        $id = $this->input->post('id');
        $id = $this->input->post('id');
        if ($this->idpersonal==0) {
            $idpersonalid=$this->input->post('idpersonalid');
        }else{
            $idpersonalid=$this->idpersonal;
        }
        $this->ModeloCatalogos->ninosalidamanual($id,$idpersonalid);

        $resultado = $this->ModeloCatalogos->getselectwhere('compra_tiempo_nino','compranId',$id);
        $ninossalidos=0;
        $compraId=0;
        $totalninos=0;
        foreach ($resultado->result() as $row) {
            $compraId=$row->compraId;
        }
        
        if ($compraId>0) {
            $totalninos = $this->totalninos($compraId);

            $totalninosc=$this->ninocerrados($compraId);

            $totalcerrados=$totalninos-$totalninosc;
            if ($totalcerrados==0) {
                $data = array('status' => 0);
                $this->ModeloCatalogos->updateCatalogo($data,'compraId',$compraId,'compra_tiempo');
            }
        }
    }
}
?>