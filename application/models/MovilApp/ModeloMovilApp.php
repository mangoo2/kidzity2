<?php
class ModeloMovilApp extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function Insertar($data, $table){
    	if($this->db->insert($table,$data)){
    		$this->db->close();
    		return "Se agrego correctamente";
    	}else{
    		$this->db->close();
    		return "Error al agregar";
    	}
    	
    }

    public function insertarTiempo($data){
        $this->db->insert("compra_tiempo",$data) ; 
        return $this->db->insert_id(); 
    }

    

    function Fiesta($code){
        $query="SELECT * FROM fiesta WHERE Code=$code AND Active=1";
        if ($this->db->query($query)!=null) {
            return true;
        }else{
            return false;
        }
    }

    function GetAll($campo,$valor,$tabla){
    	$this->db->where($campo, $valor);
    	$result=$this->db->get($tabla);
    	$json = json_encode($result->row());
    	return $json;
    }

    function Actualizar($campo, $valor, $table,$data){
    	$this->db->where($campo, $valor);
    	if($this->db->update($table, $data)){
    		$this->db->close();
    		return "Actualizacion exitosa";
    	}else{
    		$this->db->close();
    		return "Actualizacion falló";
    	}
    }

    function SimpleGet($valor,$campo,$tabla,$cont){
    	$this->db->select($cont);
    	$this->db->where($campo,$valor);
    	return $this->db->get($tabla)->row()->$cont;
    }

    function GetNinoID($id){
    	$query="SELECT * FROM ninos WHERE usuario_id = '$id'";
    	$resul=$this->db->query($query);
    	return $resul;
    }

    function State($id){
        $query="SELECT ti.ninoid FROM compra_tiempo_nino as ti INNER JOIN ninos as ni on ni.id=ti.ninoid  WHERE ni.usuario_id = '$id' ";
        $resul=$this->db->query($query);
        return $resul;
    }


    function CheckMail($info){
    	$correo=$info['correo'];
        $sing="app";
    	$this->db->select('app');
    	$this->db->where('correo',$correo);
    	if($this->db->get('titular')->row()->$sing !=0){
    		return "usted ya tiene una app registrada";
    	}
    	else{
    		return "App registrada";
    	}
    }

    function GetSaldo($user){
        $sql="SELECT SUM(cantidad) AS total FROM abono_saldos WHERE titularId = $user AND vence >= CURRENT_TIMESTAMP";
        $res=$this->db->query($sql)->row()->total;
        return $res;
    }

    function SimpleUpdate($data){
        $img=$data['imagen'];
        $id=$data['id'];
        $sql="UPDATE titular SET imagen = $img WHERE titularId=$id";
        $this->db->query($sql);
    }

    function GetDeposito($id){
        $sql="SELECT cantidad AS monto,inicio AS fecha FROM abono_saldos WHERE titularId = $id AND referencia is not null" ;
        $res=$this->db->query($sql);
        return $res->result();
    }

    function GetDeposito2($id){
        $sql="SELECT pagado AS monto,tiempo, reg AS fecha,referencia FROM compra_tiempo WHERE titularId = $id AND referencia is not null";
        $res=$this->db->query($sql);
        return $res->result();
    }

    function compratiemponino($id_compra,$id_nino){
        $this->db->insert("compra_tiempo_nino",array("compraId"=>$id_compra,"ninoid"=>$id_nino));
    }


}
?>