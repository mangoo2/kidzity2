<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de turnos</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <!-------------->
          <div class="col-md-12">
            <table class="table table-striped" id="data-tables">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Fecha inicio</th>
                  <th>Hora inicio</th>
                  <th>Fecha cierre</th>
                  <th>Hora cierre</th>
                  <th>Cantidad</th>
                  <th>Punto venta</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($turno->result() as $item){ 
                  ?>
                <tr id="trper_<?php echo $item->id; ?>">
                   <td><?php echo $item->id; ?></td>
                  <td><?php echo $item->fecha; ?></td>
                  <td><?php echo $item->horaa; ?></td>
                  <td><?php echo $item->fechacierre; ?></td>
                  <td><?php echo $item->horac; ?></td>
                  <td><?php echo $item->cantidad; ?></td>
                  <?php if($item->punto_venta==3){ ?> 
                  <td>Ventas Cafeteria</td>
                  <?php } else if($item->punto_venta==4){ ?> 
                  <td>Ventas Regalos</td>
                  <?php } else if($item->punto_venta==5){ ?> 
                  <td>Meseros</td>
                  <?php } ?>
                  <td>
                    <div class="btn-group mr-1 mb-1">
                      <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i>
                      </button>
                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="#" onclick="ventas(<?php echo $item->id;?>, <?php echo $item->punto_venta;?>)">Ventas</a>
                      </div>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-------------->
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="VentasH" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ventas Realizadas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(document).ready(function() {
    $('#data-tables').dataTable();
  });
</script>