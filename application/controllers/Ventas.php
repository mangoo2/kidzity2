<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
    function __construct(){
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Clientes/ModeloCliente');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloFiestas');
        $this->fechahoy=date('Y-m-d H:i:s');
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
            $this->idpersonald=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }
    function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/VentasEstancia');
        $this->load->view('templates/footer');
    }
    function VentasEstacia2(){
        $id=$this->input->get('cod');
        $comp=$this->input->get('com');
        $data['rtitular']=$this->ModeloCliente->VerCliente($id);
        $data['rstitular']=$this->ModeloCliente->VerSubT($id);
        $data['rsninos']=$this->ModeloCliente->VerMenor($id); 
        $data['saldod']=$this->ModeloCatalogos->sumasaldosdisponibles($id); 
        $data['fiestasl']=$this->ModeloFiestas->fiestasdisponibles(); 
        /*
        if (isset($comp)) {
            $compra=$this->ModeloCatalogos->getselectwhere2('compra_tiempo','compraId',$comp,'titularId',$id);
            foreach ($compra->result() as $row) {
                $diasv =$row->valor;
            } 
        } */
        $data['fechahoy']=$this->fechahoy;
        $data['idpersonalid']=$this->idpersonald;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/VentasEstancia2',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsVentasEstancia2');

        unset($_SESSION['pro']);
            $_SESSION['pro']=array();
            unset($_SESSION['can']);
            $_SESSION['can']=array();
    }
    function VentasEstacia3(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Ventas/VentasEstancia3');
        $this->load->view('templates/footer');
    }
    function compraadd(){
        $titular = $this->input->post('titular');
        $ninosrows = $this->input->post('ninosrows');
        $metodo = $this->input->post('metodo');
        $tiempo = $this->input->post('tiempo');
        $tipo = $this->input->post('tipo');
        $pagado = $this->input->post('pagado');
        $numninos = $this->input->post('numninos');
        $saldo = $this->input->post('saldo');

        $numarticulos = $this->input->post('numarticulos');
        $articulos = $this->input->post('articulos');
        $descuentotiempo = $this->input->post('descuentotiempo');
        $desctext = $this->input->post('descuentotext');
         if ($this->idpersonal>0) {
            $idpersonal=$this->idpersonal;
         }else{
            $idpersonal=$this->input->post('idpersonalid');
         }
        
        //======================================
        if('2019-03-09'>=date('Y-m-d')){
            $tiempo=$tiempo*2;
        }
        //======================================
        
        $compraid=$this->ModeloCatalogos->compratiempo($titular,$metodo,$tiempo,$pagado,$tipo,$this->fechahoy,$descuentotiempo,$idpersonal,$desctext); 
        if ($tipo==0) {
            $pagado=$pagado/$numninos;
            if ($descuentotiempo==0) {
                $descuentotiempo=0;
            }else{
                $descuentotiempo=$descuentotiempo/$numninos;
            }
            
            $DATA = json_decode($ninosrows);
            for ($i = 0; $i < count($DATA); $i++) {
                $ninoid=$DATA[$i]->ninoid;
                $pulcera=$DATA[$i]->pulcera;
                $costonino=$DATA[$i]->costonino;
                $tlbebe=$DATA[$i]->costoninoselected;
                $tladulto=$DATA[$i]->tladulto;
                $pfam= $DATA[$i]->paquetefamiliar;
                $this->ModeloCatalogos->compratiempon($compraid,$ninoid,$pulcera,$costonino,$tlbebe,$tladulto,$this->fechahoy,$descuentotiempo,$pfam);

            }
        }else{
            $resultados=$this->ModeloCatalogos->getselectwhere('fiestas','fiestaId',$tipo);
            foreach ($resultados->result() as $item){
                $precio_total=$item->precio_total;
                $precioex=$item->precioex;
                $cantidad=$item->cantidad;
            }

            //$pagadon=$pagado/$numninos;
            $numninost=1;
            $DATA = json_decode($ninosrows);
            for ($i = 0; $i < count($DATA); $i++) {
                $ninoid=$DATA[$i]->ninoid;
                $pulcera=$DATA[$i]->pulcera;

                if ($numninost<=$cantidad) {
                    $pagadon=$precio_total/$cantidad;
                    $this->ModeloCatalogos->compratiempon($compraid,$ninoid,$pulcera,$pagadon,$tipo,$this->fechahoy,0);
                    $numninos--;
                }else{
                    $pagadon=$pagado/$numninos;
                    $this->ModeloCatalogos->compratiempon($compraid,$ninoid,$pulcera,$pagadon,$tipo,$this->fechahoy,0);

                }
                $this->ModeloCatalogos->updatefiestascant($tipo);













                //$this->ModeloCatalogos->compratiempon($compraid,$ninoid,$pulcera,$pagado,$tipo);
                $numninost++;
            }
        }
        
        if ($saldo>0) {
           $this->ModeloCatalogos->addsaldo($titular,$saldo);
        }
        if ($numarticulos>0) {
            $monto_total=0;
           $DATAa = json_decode($articulos);
            for ($i = 0; $i < count($DATAa); $i++) {
                $cantidad=$DATAa[$i]->cantidad;
                $precio=$DATAa[$i]->precio;
                $monto_total=$monto_total+($cantidad*$precio);
            }
            $idventas=$this->ModeloVentas->addventas($monto_total,'',$metodo);
            $arrayvalores = array('compraId' => $compraid,'ventaId'=> $idventas);
            $this->ModeloCatalogos->Insertar('compra_tiempo_ventas_pro', $arrayvalores);
            for ($i = 0; $i < count($DATAa); $i++) {
                $idproductos=$DATAa[$i]->idproductos;
                $cantidad=$DATAa[$i]->cantidad;
                $precio=$DATAa[$i]->precio;
                $descuento=$DATAa[$i]->descuento;
                $this->ModeloVentas->addventasdetalle($idventas,$idproductos,$cantidad,$precio,$descuento);
                $this->ModeloVentas->descuentostock($idproductos,$cantidad);
            }
        }
        echo $compraid;
    }
    function compraupdate(){
        date_default_timezone_set('America/Mexico_City');
        $compra = $this->input->post('compra');
        $ninosrows = $this->input->post('ninosrows');
        $fechahoy=date('Y-m-d H:i:s');
        $datacompradu = array('reg' => $fechahoy);
        $this->ModeloCatalogos->updateCatalogo($datacompradu,'compraId',$compra,'compra_tiempo'); 

        $DATA = json_decode($ninosrows);
            for ($i = 0; $i < count($DATA); $i++) {
                $ninoid=$DATA[$i]->ninoid;
                $pulcera=$DATA[$i]->pulcera;

                $datan = array('reg_abierto' => $fechahoy,'pulcera' => $pulcera);
                $this->ModeloCatalogos->updateCatalogo2($datan,'compraId',$compra,'ninoid',$ninoid,'compra_tiempo_nino'); 

                
            }

            echo $compra;     
    }
    function ventasg(){
            $pages=4;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Ventas/ventasg/view/';
            $config['total_rows'] = $this->ModeloVentas->filasg();
            $config['per_page'] = $pages;
            $config['num_links'] = 3;
            $config['first_link'] = 'Primera';
            $config['last_link'] = 'Última';
            $config["uri_segment"] = 3;
            $config['next_link'] = 'Siguiente';
            $config['prev_link'] = 'Anterior';
            $config['reuse_query_string'] = TRUE;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["ventasl"] = $this->ModeloVentas->Listg($pagex,$config['per_page']);
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('ventas/listadog',$data);
            $this->load->view('templates/footer');
    }
    function ventasv(){
            $pages=10;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Ventas/ventasv/view/';
            $config['total_rows'] = $this->ModeloVentas->filasv();
            $config['per_page'] = $pages;
            $config['num_links'] = 3;
            $config['first_link'] = 'Primera';
            $config['last_link'] = 'Última';
            $config["uri_segment"] = 3;
            $config['next_link'] = 'Siguiente';
            $config['prev_link'] = 'Anterior';
            $config['reuse_query_string'] = TRUE;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["ventasl"] = $this->ModeloVentas->Listv($pagex,$config['per_page']);
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('ventas/listadov',$data);
            $this->load->view('templates/footer');
    }
    function ventasr(){
            $pages=10;
            $this->load->library('pagination');
            $config['base_url'] = base_url().'Ventas/ventasr/view/';
            $config['total_rows'] = $this->ModeloVentas->filasr();
            $config['per_page'] = $pages;
            $config['num_links'] = 3;
            $config['first_link'] = 'Primera';
            $config['last_link'] = 'Última';
            $config["uri_segment"] = 3;
            $config['next_link'] = 'Siguiente';
            $config['prev_link'] = 'Anterior';
            $config['reuse_query_string'] = TRUE;
            $this->pagination->initialize($config);
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["ventasl"] = $this->ModeloVentas->Listr($pagex,$config['per_page']);
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('ventas/listador',$data);
            $this->load->view('templates/footer');
    }
    function datoscompras(){
        $titular=$this->input->post('titular');
        $comp=$this->input->post('compra');
        $resultadocomp=$this->ModeloCatalogos->getselectwhere3('compra_tiempo','compraId',$comp,'titularId',$titular,'status',1);
        $compra=0;
        foreach ($resultadocomp->result() as $item) {
            $compra=1;
            $compraId=$item->compraId;
            $metodo=$item->metodo;
            $tiempo=$item->tiempo;
            $pagado=$item->pagado;
            $tipo=$item->tipo;

        }
        if ($compra==0) {
            $resultadocomp=$this->ModeloCatalogos->getselectwhere3('compra_tiempo','referencia',$comp,'titularId',$titular,'status',1);
            $compra=0;
            foreach ($resultadocomp->result() as $item) {
                $compra=1;
                $compraId=$item->compraId;
                $metodo=$item->metodo;
                $tiempo=$item->tiempo;
                $pagado=$item->pagado;
                $tipo=$item->tipo;

            }
        }





        $ninos=$this->ModeloCatalogos->getselectwhere('compra_tiempo_nino','compraId',$comp);
        $ninosv=json_encode($ninos->result());

        $ventaId=$this->ModeloCatalogos->getselectvalue1rowwhere('compra_tiempo_ventas_pro','ventaId','compraId',$comp);

        $ventad=$this->ModeloCatalogos->getselectwhere('venta_detalle','ventaId',$ventaId);
        $ventadv=json_encode($ventad->result());


        if ($compra==1) {
            $array = array(
                    "compra"=>$compra,
                    "metodo"=>$metodo,
                    "tiempo"=>$tiempo,
                    "pagado"=>$pagado,
                    "tipo"=>$tipo,
                    "ninos"=>$ninosv,
                    "compraId"=>$compraId,
                    "ventasp"=>$ventadv
                );
        }else{
            $array = array("compra"=>$compra);
        }
        echo json_encode($array);
    }
    function analizareferencia(){
        $codigo=$this->input->post('codigo');
        $resultados=$this->ModeloCatalogos->getselectwhere2('compra_tiempo','referencia',$codigo,'status',1);
        $consulta=0;
        foreach ($resultados->result() as $row) {
                $compraId =$row->compraId;
                $titularId=$row->titularId;
                $consulta=1;
            }
        if ($consulta==1) {
            $datos = array('consulta' => $consulta, 'compraId'=>$compraId,'titularId'=>$titularId);
        }else{
            $datos = array('consulta' => $consulta);
        }
        echo json_encode($datos);   
    }
    function verificarstatus(){
        $pulceras=$this->input->post('pulceras');
        $resultados=$this->ModeloCatalogos->getselectwhere2('compra_tiempo_nino','pulcera',$pulceras,'status',1);
        $consulta=0;
        foreach ($resultados->result() as $row) {
            $consulta=1;
        }
        echo $consulta; 
    }

}
?>