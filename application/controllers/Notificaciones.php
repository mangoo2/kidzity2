<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificaciones extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }

    function index(){
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Notificaciones/NotificacionCreate');
        $this->load->view('templates/footer');
    }
}
?>