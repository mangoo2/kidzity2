<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        if($this->session->userdata('logueo_kids')==true){
            $this->perfilid =$this->session->userdata('perfilid');
        }else{
            redirect('/Login'); 
        }
    }
	function index(){
        $perfilview=$this->perfilid;
        if ($perfilview>=1) {
            redirect('/Inicio'); 
        }else{
            redirect('/Login'); 
        }
	}
   
}
?>