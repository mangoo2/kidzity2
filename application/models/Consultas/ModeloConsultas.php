<?php
class ModeloConsultas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function ConsultAbierto($pagina,$segmento,$X){
    	if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        /*
    	$query="SELECT tiem.compraId, titu.nombre, tiem.tiempo, tiem.pagado, tiem.titularId  
                FROM compra_tiempo as tiem 
                INNER JOIN titular as titu on titu.titularId=tiem.titularId 
                WHERE tiem.status=$X LIMIT $pagina	 $segmento;";
        */
        $query="SELECT comn.compranId,comn.compraId,nin.nombre as nino,tit.nombre as titular,com.tiempo,comn.pagado,com.metodo,com.referencia,comn.reg_abierto,comn.reg_cerrado,comn.tiempoextra,comn.pagotiempoextra,fi.nombre as fiesta,com.titularId,comn.devolucion_monto,comn.devolucion_motivo
                FROM compra_tiempo_nino as comn
                inner join ninos as nin on nin.id=comn.ninoid
                inner JOIN compra_tiempo as com on com.compraId=comn.compraId
                inner JOIN titular as tit on tit.titularId=com.titularId
                left join fiestas as fi on fi.fiestaId=com.tipo
                WHERE comn.status=$X LIMIT $pagina   $segmento;";
    	$res= $this->db->query($query);
        //$this->db->close();
    	return $res;
    }
    function ConsultAbiertosearch($buscar,$x){
        $query="SELECT * from(
                    SELECT comn.compranId,comn.compraId,nin.nombre as nino,tit.nombre as titular,com.tiempo,comn.pagado,com.metodo,com.referencia,comn.reg_abierto,comn.reg_cerrado,comn.tiempoextra,comn.pagotiempoextra,fi.nombre as fiesta,com.titularId,comn.devolucion_monto,comn.devolucion_motivo
                    FROM compra_tiempo_nino as comn
                    inner join ninos as nin on nin.id=comn.ninoid
                    inner JOIN compra_tiempo as com on com.compraId=comn.compraId
                    inner JOIN titular as tit on tit.titularId=com.titularId
                    left join fiestas as fi on fi.fiestaId=com.tipo
                    WHERE comn.status=$x
                ) as datos 
                where 
                    compranId like '%$buscar%' or
                    compraId like '%$buscar%' or
                    nino like '%$buscar%' or
                    titular like '%$buscar%' or
                    tiempo like '%$buscar%' or
                    pagado like '%$buscar%' or
                    referencia like '%$buscar%' or
                    reg_abierto like '%$buscar%' or
                    reg_cerrado like '%$buscar%' or
                    tiempoextra like '%$buscar%' or
                    pagotiempoextra like '%$buscar%' or
                    fiesta like '%$buscar%' or
                    devolucion_monto like '%$buscar%' or
                    devolucion_motivo like '%$buscar%' 

                    ";
        $res= $this->db->query($query);
        return $res;
    }
    function filas($v){
    	$strq = "SELECT COUNT(*) as total FROM compra_tiempo where status=$v";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->total;
        } 
        return $total;
    }
    function getlistConsultAbierto($params){
        //misma que la funcion ConsultAbierto
        $tipo=$params['tipo'];
        $columns = array( 
            0=>'comn.compranId',
            1=>'comn.compraId',
            2=>'nin.nombre as nino',
            3=>'tit.nombre as titular',
            4=>'com.tiempo',
            5=>'comn.pagado',
            6=>'fi.nombre as fiesta',
            7=>'comn.devolucion_monto',
            8=>'comn.devolucion_motivo',
            9=>'com.metodo',
            10=>'com.referencia',
            11=>'comn.reg_abierto',
            12=>'comn.reg_cerrado',
            13=>'comn.tiempoextra',
            14=>'comn.pagotiempoextra',
            15=>'com.titularId',
        );
        $columnsss = array( 
            0=>'comn.compranId',
            1=>'comn.compraId',
            2=>'nin.nombre',
            3=>'tit.nombre',
            4=>'com.tiempo',
            5=>'comn.pagado',
            6=>'fi.nombre',
            7=>'comn.devolucion_monto',
            8=>'comn.devolucion_motivo',
            9=>'com.metodo',
            10=>'com.referencia',
            11=>'comn.reg_abierto',
            12=>'comn.reg_cerrado',
            13=>'comn.tiempoextra',
            14=>'comn.pagotiempoextra',
            15=>'com.titularId',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_tiempo_nino comn');
        $this->db->join('ninos nin', 'nin.id=comn.ninoid');
        $this->db->join('compra_tiempo com', 'com.compraId=comn.compraId');
        $this->db->join('titular tit', 'tit.titularId=com.titularId');
        $this->db->join('fiestas fi', 'fi.fiestaId=com.tipo','left');

        $this->db->where(array('comn.status'=>$tipo));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistConsultAbierto_total($params){
        $tipo=$params['tipo'];
        $columns = array( 
            0=>'comn.compranId',
            1=>'comn.compraId',
            2=>'nin.nombre',
            3=>'tit.nombre',
            4=>'com.tiempo',
            5=>'comn.pagado',
            6=>'fi.nombre',
            7=>'comn.devolucion_monto',
            8=>'comn.devolucion_motivo',
            9=>'com.metodo',
            10=>'com.referencia',
            11=>'comn.reg_abierto',
            12=>'comn.reg_cerrado',
            13=>'comn.tiempoextra',
            14=>'comn.pagotiempoextra',
            15=>'com.titularId',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('compra_tiempo_nino comn');
        $this->db->join('ninos nin', 'nin.id=comn.ninoid');
        $this->db->join('compra_tiempo com', 'com.compraId=comn.compraId');
        $this->db->join('titular tit', 'tit.titularId=com.titularId');
        $this->db->join('fiestas fi', 'fi.fiestaId=com.tipo','left');
        
        $this->db->where(array('comn.status'=>$tipo));

        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        
        $query=$this->db->get();

        return $query->row()->total;
    }

}
?>