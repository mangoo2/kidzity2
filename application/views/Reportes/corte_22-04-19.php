<div class="row">
  <div class="col-md-12">
    <h2>Personal</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Corte</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                <!--------//////////////-------->
                
                  <div class="row">
                    <form id="formcorte" class="col-md-11">
                      <div class="col-md-3">
                        <label>Fecha Inicio</label>
                        <input type="date" name="fechainicio" id="fechainicio"  class="form-control" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                      <div class="col-md-3">
                        <label>Fecha Termino</label>
                        <input type="date" name="fechafin" id="fechafin"  class="form-control" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                      <div class="col-md-3">
                        <label>Tipo Reporte</label>
                        <select name="tipo" id="tipo" class="form-control" onchange="selecciontipor()" >
                          <option value="1">Cafeteria/Mesas</option>
                          <option value="2">Visitas</option>
                          <option value="3">Saldos</option>
                          <option value="4">Fiesta</option>
                          <option value="5">Compras</option>
                          <option value="6">Regalos/Recepcion</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <div class="col-md-12">
                          <input type="radio" name="filtro" value="0" id="filtro0" checked>
                          <label  for="filtro0">Ninguno</label>
                          
                        </div>
                        <div class="col-md-12 radiopro">
                          <input type="radio" name="filtro" value="1" id="filtro1" >
                          <label for="filtro1">Productos</label>
                          
                        </div>
                        <div class="col-md-12 radioninos" style="display: none;">
                          <input type="radio" name="filtro" value="2" id="filtro2" >
                          <label for="filtro2">Niños</label>
                          
                        </div>

                        
                        
                        
                      </div>
                    </form>
                    <div class="col-md-1">
                      <label class="col-md-12" style="color: transparent;"> x</label>
                      <button class="btn btn-primary"  id="genera"> Generar</button>
                    </div>
                  </div>
                
                <div class="row">
                  <div class="col-md-12 datoscorte" style="overflow: auto;">
                    
                  </div>
                </div>
                 
                <!--------//////////////-------->
              </div>
          </div>
      </div>
    </div>
</div>
<div class="modal fade" id="modalmotivodevolucion" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Motivo Devolución</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
        <div class="col-md-12 datosmotivod">
          
        </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>