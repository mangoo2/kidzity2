<style type="text/css">
  .informacionventa{
    box-shadow: inset 0 0 8px 0;
    min-height: 100px;
    border-radius: 5px;
    margin-bottom: 19px;
  }
  .iframevisitantes{
    width: 100%;
    border: 0;
    min-height: 290px;
  }
</style>
<div class="">
  <section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0">Recepción</h4>
        </div>
        <div class="card-body">
          <div class="px-3">
              <div class="form-body">
                <div class="row">
                  <div class="container">
                    <div class="col-md-12">
                      <div class="col-xl-3 col-lg-3 col-md-3"></div>
                      <div class="col-xl-6 col-lg-6 col-md-6">
                        <fieldset class="form-group">
                          <label for="User">Escanear QR</label>
                          <input type="text" class="form-control" id="codigo" name="User" oninput="Npage()" autofocus="true">
                          <input type="hidden" class="form-control" id="codigoid" name="User" value="1">
                        </fieldset>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-1"></div>
                      <div class="col-md-10 informacionventa" style="display: none;"></div>
                    </div>
                    <div class="col-md-12">
                      <button class="btn btn-primary"  id="imprimirqr"><i class="fa fa-qrcode"></i> Imprimir</button>
                      <button class="btn btn-primary"  id="registrar"><i class="fa fa-save"></i> Agregar</button>
                    </div>

                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal fade text-left" id="modalvistantes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Etiqueta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="input-group">
                    <input type="number" class="form-control" id="numeroprint">
                    <div class="input-group-append">
                      <span class="input-group-btn" id="button-addon2">
                        <button class="btn btn-raised btn-primary" id="imprimirqr2">Generar</button>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-12 printetiqueta">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  //======================== solo pueblas

  //========================
  /*
function Npage() {
  var Str= $('#User').val();
  if (Str.length >= 10) {
    console.log(Str)
    console.log("mide mas de 10");
    window.open('Ventas/VentasEstacia2');
  }
  else{
    console.log(Str)
    console.log("no mide mas de 10");
  }
}*/
$(document).ready(function() {
  $('#imprimirqr').click(function(event) {
    var idqr=$('#codigoid').val();
    if (idqr!='') {
      $('.printetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiqueta?venta='+idqr+'&print=0&printn=1" class="iframevisitantes"></iframe>');
      $('#modalvistantes').modal();
    }else{
      toastr.error('Error', 'Código invalido');
    }
  });
  $('#imprimirqr2').click(function(event) {
    var numero=$('#numeroprint').val();
    var idqr=$('#codigoid').val();
    $('.printetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiqueta?venta='+idqr+'&print=1&printn='+numero+'" class="iframevisitantes"></iframe>');
  });
  //===============================
  $('#registrar').click(function(event) {
    registrar();
  });
  $("#codigo").keypress(function(e) {
        if(e.which == 13) {
          registrar();
        }
      });
  //===============================
});
function registrar(){
    var codigo=$('#codigo').val();
    var codigor = codigo.split("|");
    if (codigor[1]==undefined) {
      var cadena=codigor[0];
      $.ajax({
                type:'POST',
                url:$('#base_url').val()+'Ventas/analizareferencia',
                data:{codigo:cadena},
                async:false,
                success:function(data){
                  var array = $.parseJSON(data);
                  
                  if (array.consulta==1) {
                     cadena=array.titularId+'&com='+array.compraId;
                  }
                }
            });
    }else{
      var cadena=codigor[0]+'&com='+codigor[1];
    }
    location.href='Ventas/VentasEstacia2?cod='+cadena;
}
</script>