<?php
require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
class MYPDF extends TCPDF {
	public function Header() {

	}
	public function Footer() {
		$html1='<table border="0">
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>

		';
		$this->writeHTML($html1, true, false, true, false, '');
	}

}
$pdf = new MYPDF('L', PDF_UNIT, array(90, 41), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Recibo');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set margins
$pdf->SetMargins('3','2','3');
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

if (isset($_GET['nomtitu'])) {
	$nombretitu=$_GET['nomtitu'];
}else{
	$nombretitu='';
}
// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 19);

// add a page
//$pdf->AddPage('P', 'A4');


$date=date("d/m/y");
$style = array('position' => 'R');
$styleQR = array('border' => 0, 
				 'vpadding' => '0', 
				 'hpadding' => '0', 
				 'fgcolor' => array(0, 0, 0), 
				 'bgcolor' => false, 
				 'module_width' => 2, 
				 'module_height' => 2);

//                   code,type,      x, y
//$pdf->write2DBarcode('1', 'QRCODE,L', 0, 0, 30, 20, $style, 'N');
//$params = $pdf->serializeTCPDFtagParameters(array('1', 'C128B', '', '', '', 15, 0.4, $style, 'C')); 
$params = $pdf->serializeTCPDFtagParameters(array($_GET['venta'].'|'.$nombretitu, 'QRCODE,L', '', '', 50, 50, $styleQR, 'N'));
if (isset($_GET['printn'])) {
      $pagina=$_GET['printn'];
    }else{
      $pagina=1;
    }
if (isset($_GET['acompa'])) {
      $acompa=$_GET['acompa'];
    }else{
      $acompa='';
    }
$paginai=1;
while ($paginai <= $pagina){
	$pdf->AddPage();
	$html1='<table border="0">
				<tr>
					<td align="center" width="25%" ><tcpdf method="write2DBarcode" params="' . $params . '" width="100%" /></td>
					<td align="center" width="75%">
						
						<b>'.$acompa.'</b>
					</td>
				</tr>
			</table>
			';
	$pdf->writeHTML($html1, true, false, true, false, '');
	$paginai++;
}







$pdf->SetAutoPageBreak(true, '104');
if ($_GET['print']==1) {
	$pdf->IncludeJS('print(true);');
}
//
// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output('plan.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+
