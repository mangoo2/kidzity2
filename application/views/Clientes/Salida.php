<input type="hidden" id="idpersonalid" value="<?php echo $idpersonalid; ?>" readonly>
<style type="text/css">
  #Hora{
    color: #f35626;
    background-image: -webkit-linear-gradient(92deg,#f35626,#feab3a);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    -webkit-animation: hue 60s infinite linear;
    text-align: center;
    font-weight: bold;
  }
  .inpuoculto{
    background: red;
    display: none;

  }
  .ninoresaltar{
    background: #08f727!important;
  }
  @-webkit-keyframes hue {
  from {
    -webkit-filter: hue-rotate(0deg);
  }

  to {
    -webkit-filter: hue-rotate(-360deg);
  }
}
  
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Salida</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-md-12">
    <h3 id="Hora"><i class="fa-clock-o"></i></h3>
  </div>
</div>
<div class="row">
  <div class="col-md-11">
    
  </div>

</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <div class="row">
            
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="text" class="form-control" id="pulcerasalida" autofocus>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 infonpulcera">
              
            </div>
          </div>
          <div class="row">
            
          </div>
          
          
          
          <!--------//////////////-------->
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12 ticketsalida" style="display: none;">
</div>
<div class="modal fade text-left" id="iconFormd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                      <h3 class="modal-title" id="myModalLabel34">Autorizacion</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      </div>
                      <div class="modal-body">
                        <label>Password: </label>
                        <div class="form-group position-relative has-icon-left">
                          <input type="password" placeholder="Password" id="passautorizaciond" class="form-control" autocomplete="new-password">
                          
                        </div>
                      </div>
                      <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cancelar">
                        <input type="submit" class="btn btn-outline-primary btn-lg modaldevolucion" data-dismiss="modal" value="Aceptar">
                      </div>
                      
                    </div>
                    </div>
</div>
<div class="modal fade" id="modalconfirmaciondevolucion" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Devolución</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
         <input type="number" id="Idcompraninod" name="Idcompraninod" style="display: none;">
         <div class="col-xl-12 col-lg-12 col-md-12">
          <fieldset class="form-group">
           <label>¿Desea realisar una devolución?</label>
           </fieldset>                                
         </div>
         <div class="col-md-12">
           <label class="col-md-3">Monto cobrado</label>
           <div class="col-md-9">
             <input type="number" id="dmontocobrado" class="form-control" readonly>
           </div>
         </div>
         <div class="col-md-12">
           <label class="col-md-3">Cantidad a pagar</label>
           <div class="col-md-9">
             <input type="number" id="cantidadapagard" class="form-control" readonly>
           </div>
         </div>
         <div class="col-md-12">
           <input type="number" id="montodevolucion" name="montodevolucion" placeholder="Monto" class="form-control">
           <textarea class="form-control" id="motivodevolucion" name="motivodevolucion" placeholder="Motivo"></textarea>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary okdevolucionnino" data-dismiss="modal">Aceptar</button>
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var iddevolucion=0;
  var mcobrado=0;
  var base_url =$('#base_url').val();
  $(document).ready(function($) {
    $('.okdevolucionnino').click(function(event) {
      var ninoid=$('#Idcompraninod').val();
      if (ninoid>0) {
        var monto=$('#montodevolucion').val()==''?0:$('#montodevolucion').val();
        if (monto>0) {
          var pagado=$('#dmontocobrado').val();
          var porpagar=$('#cantidadapagard').val();
          var pagado_porpagar=parseFloat(pagado)+parseFloat(porpagar);
          if (monto<=pagado_porpagar) {
            var motivo=$('#motivodevolucion').val();
            if (motivo.length>0) {
              $.ajax({
                  type:'POST',
                  url: base_url+'ConfigGeneral/devolucion',
                  data: {
                    id: ninoid,
                    montod:monto,
                    motivod:motivo
                  },
                  async: false,
                  statusCode:{
                    404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                    500: function(data){ 
                      toastr.error('Error', '500');
                      console.log(data.responseText);

                    }
                  },
                  success:function(data){
                    console.log(data);
                    toastr.success('Devolucion registrada');
                    //window.location='';
                    var monto=$('#montodevolucion').val()==''?0:$('#montodevolucion').val();
                    var montos=parseFloat(monto)-parseFloat(pagado);
                    if (montos<0) {
                      montos=0;
                    }
                      $('#compensacion').val(montos);
                    
                  },
                  error: function(jqXHR, estado, error){
                    console.log(estado);
                    console.log(jqXHR);
                    console.log(error);
                  }
              });
            }else{
              toastr.error('Agregar un motivo de devolucion','Error');
            }
          }else{
            toastr.error('No es posible realizar una devolución mayor a'+pagado_porpagar,'Error');
          }



          


        }else{
          toastr.error('Agregar un monto de devolucion','Error');
        }
          

      }else{
        toastr.error('Realizar nuevamente el proceso','Error');
      }
      /* Act on the event */
    });
    $('.modaldevolucion').click(function(event) {
      
      $.ajax({
          type:'POST',
          url: base_url+'Login/vadministrador',
          data: {
            acceso: $('#passautorizaciond').val(),
          },
          async: false,
          statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
              toastr.error('Error', '500');
              console.log(data.responseText);
            }
          },
          success:function(data){
            if (data==1) {
              $('#Idcompraninod').val(iddevolucion);
              $('#dmontocobrado').val(mcobrado);
              $('#cantidadapagard').val($('#totalpago').val());
              $('#modalconfirmaciondevolucion').modal();
            }else{
              toastr.error('Acceso no permitido','Error');
            }
            
          },
          error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
          }
        });
      $('#passautorizacion').val('');
    });
 });
  function devolucion(id,cobrado){
    iddevolucion=0;
    mcobrado=0;
    iddevolucion=id;
    mcobrado=cobrado;
    $('#iconFormd').modal();
  }
  function restapago(){
    var pago=$('#totalpago').val();
    var comp=$('#compensacion').val();
    var montopago=$('#montopago').val();
    var cambio=(parseFloat(pago)-parseFloat(comp)-parseFloat(montopago))*-1;
    if (cambio<0) {
      cambio=0;
    }
    $('.cambiosalida').html(cambio);
  }
</script>
