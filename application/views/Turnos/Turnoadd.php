<div class="container">
  <section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0">Nuevo turno</h4>
        </div>
        <div class="card-body">
          <div class="px-3">
            <form class="form" role="form" id="formTurno" name="formTurno" method="post" >
              <div class="form-body">
                <div class="row">
                  <div class="container">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                      
                      <div class="col-xl-12 col-lg-12 col-md-12">
                        <fieldset class="form-group">
                          <label for="punto_venta">Punto de venta</label>
                          <select id="punto_venta" class="custom-select d-block w-100" name="punto_venta" onchange="verificar()">
                            <option value="0" chacheck>Selecciona</option>
                            <?php foreach ($areas->result() as $item){ 
                              if ($item->perfilId==3 or $item->perfilId==4) {?>
                                <option value="<?php echo $item->perfilId; ?>"><?php 
                                echo $item->nombre; 
                                if ($item->perfilId==3) {
                                  echo '/ Mesas';
                                }
                                ?></option>
                              <?php }
                               } ?>
                          </select>
                        </fieldset>
                      </div>
                      <div class="col-xl-12 col-lg-12 col-md-12">
                        <fieldset class="form-group">
                          <label for="MontoI">Monto de caja inicial</label>
                          <input type="text" class="form-control" id="cantidad" name="cantidad">
                        </fieldset>
                      </div>
                      <input type="hidden" class="form-control" id="fecha" name="fecha" value="<?php echo date("Y-m-d"); ?>">
                      <input type="hidden" class="form-control" id="horaa" name="horaa" value="<?php echo date("G:i:s");?>">
                      <input type="hidden" class="form-control" id="id">
                      
                    </div>
                    
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <div class="col-xl-4 col-lg-4 col-md-4"></div>
                      <div class="col-xl-8 col-lg-8 col-md-8">
                        <div class="col-xl-1 col-lg-1 col-md-1"></div>
                        <div class="col-xl-3 col-lg-3 col-md-3">
                          <button type="button" id="btnabrir" class="btn btn-primary" href="<?php echo base_url();?>Turnos"  ><i class="icon-arrow-right"></i>Abrir turno</button>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3">
                          <button type="button" id="
                          " class="btn btn-success" onclick="listado_turno_mesero()"><i class="icon-magnifier"></i>Inspeccionar</button>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3">
                          <button type="button" id="btncerrar" class="btn btn-danger"><i class="ft-scissors"></i>Cerrar turno</button>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2">
                          <button type="button" class="btn btn-light"><i class="icon-printer"></i></button>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </form>
              <div class="col-md-12 datostabla">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <script >
  
  
  </script>