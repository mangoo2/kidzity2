var base_url=$('#base_url').val();
$(document).ready(function() {
	$('#SaveProveedor').click(function(){
    var Direccion='InsertProveedor';
    if ($('#ID').length){
      console.log("El imput existe");
      Direccion='UpdateProveedor';
    }
    else{
      console.log("el imput no existe")
      Direccion='InsertProveedor';
    }
		var datos=$('#formProveedor').serialize();
		$.ajax({
			type: 'POST',
			url: Direccion,
			data: datos,
			async: false,
			statusCode: {
              404: function(data) {toastr.error('Error', '404');},
              500: function() {toastr.error('Error', '500');}
            },
            success: function(data){toastr.success('Proveedor agregado');},
            error: function(jqXHR, estado, error){
                	console.log("Estado: "+estado);
                	console.log(jqXHR);
                    console.log("Error: "+error);
            },
		});
	});
  $('#eliminarpro').click(function(event) {
    $.ajax({
      type:'POST',
      url:base_url+'Proveedores/ProveedorDell',
      data:{id:$('#deleteproid').val()},
      async:false,
      statusCode:{
        404: function(data){
          toastr.error('Error','404');
        },
        500: function(data){
          toastr.error('Error','500');
        },
      },
      success: function(data){
        toastr.success('Se elimino correctamente', 'Hecho');
        console.log("vamos aver");
        location.reload(true);
      },
      error: function(jqXHR, estado, error){
        console.log(estado);
              console.log(jqXHR);
              console.log(error);

      },
    });
  });
});
function DeleteProveedor(id){
  $('#deleteproid').val(id);
  $('#modalconfirmacion').modal();
}
function buscarP(){
    var search=$('#buscar').val();
    if (search.length>0) {
      $.ajax({
        type:'POST',
        url: base_url+'Proveedores/BuscarProveedor',
        data: {
          buscar: $('#buscar').val(),
        },
        async: false,
        statusCode:{
          404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
          500: function(){ toastr.error('Error', '500');}
        },
        success:function(data){
          $('#tbodyresultados').html(data);
        },
        error: function(jqXHR, estado, error){
          console.log(estado);
          console.log(jqXHR);
          console.log(error);
        }
      });
      $("#data-tables").css("display", "none");
      $("#data-tables2").css("display", "");
    }else{
      $("#data-tables2").css("display", "none");
      $("#data-tables").css("display", "");
    }
}