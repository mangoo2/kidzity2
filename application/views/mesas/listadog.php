<style type="text/css">
  .productoscanceltable td{
    padding: 5px;
  }
</style>

<input type="hidden" id="actualizacionticket" value="<?php echo $actualizacionticket;?>">
<div class="row">
  <div class="col-md-12">
    <h2></h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Ventas de Mesas</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <table class="table table-striped" id="tableListadoMesas">
                    <thead>
                      <tr>
                        <th># de Venta</th>
                        <th>Empleado</th>
                        <th>Método de Pago</th>
                        <th>Neto</th>
                        <th>Descuento</th>
                        <th>Monto Cobrado</th>
                        <th></th>
                        <th>Mesa</th>
                        <th>Fecha</th>
                        <th>Tipo de Venta</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody class="tbody_lcompras">
                      
                    </tbody>
                  </table>
                  <div class="col-md-12">
                    <div class="col-md-9">
                    </div>
                    <div class="col-md-3">
                    </div>
                  </div>
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>

<div class="col-md-12 ticketventa" style="display: none;">
  
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listadoMesas.js?v=191219"></script>


<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modaldevolicion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Devolución/agregar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <div class="row">
                <div class="col-md-2">
                  <label>Categoria</label>
                  <select class="form-control" id="categoriaadd">
                    <option value="0" disabled selected>Seleccionar</option>
                    <?php foreach ($categorias->result() as $item) {
                      echo '<option value="'.$item->productopId.'">'.$item->categoria.'</option>';
                    } ?> 
                  </select> 
                </div> 
                <div class="col-md-3">
                  <label>Producto</label>
                  <select class="form-control" id="productoadd" >
                  </select> 
                </div> 
                <div class="col-md-2">
                  <label>Costo</label>
                  <input type="number" class="form-control" id="costopro" readonly> 
                </div> 
                <div class="col-md-2">
                  <label>Cantidad</label>
                  <input type="number" class="form-control" id="cantidadpro" min="1" value="1"> 
                </div>  
                <div class="col-md-2">
                  <button type="button" class="btn mr-1 mb-1 btn-success btn-sm addproductoextra">Agregar</button>
                </div> 
             </div>
             <div class="row">
               <div class="col-md-12">
                 <table class="table table-striped" id="productosextras">
                  <thead>
                    <tr>
                      <td>Producto</td>
                      <td>Costo</td>
                      <td>Cantidad</td>
                      <td>Total</td>
                      <td></td>
                    </tr> 
                  </thead>
                  <tbody class="productosextrasadd">
                    
                  </tbody>
                   
                 </table>
               </div>
               <div class="col-md-10"></div>
               <div class="col-md-2">
                 <button type="button" class="btn mr-1 mb-1 btn-success btn-sm addproductoextraadd">Agregar a venta</button>
               </div>
             </div> 
             <div class="row">
               <div class="col-md-12 listadoproductos">
                 
               </div>
             </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalconfirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Cancelación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <div class="row">
                <div class="col-md-12" style="    text-align: center;    font-size: 71px;">
                  <i class="ft-alert-octagon red"></i>
                </div>
               <div class="col-md-12">
                 <h4>¿Desea confirmar la cancelación el producto <span class="classdpro"></span>?</h4>
                 <input type="hidden" id="cancelapro" value="">
               </div>
             </div> 
             <div class="row">
               <div class="col-md-12">
                 <label>Motivo</label>
                 <textarea id="motivocancelar" class="form-control"></textarea>
               </div>
             </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white confirmaciondelete" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-raised white" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="modalconfirmacionp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Devolución</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             <div class="row">
                <div class="col-md-12" style="    text-align: center;    font-size: 71px;">
                  <i class="ft-alert-octagon red"></i>
                </div>
               <div class="col-md-12">
                 <h4>¿Desea confirmar la cancelación  parcial el producto <span class="classdprop"></span>?</h4>
                 <input type="hidden" id="cancelaprop" value="">
               </div>
             </div> 
             <div class="row">
               <div class="col-md-12">
                 <label>Motivo</label>
                 <textarea id="motivocancelarp" class="form-control"></textarea>
                 <label>Cantidad a devolver</label>
                 <input type="number" class="form-control" id="cantidadaregresar" >
               </div>
             </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white confirmaciondeletep" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-raised white" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>