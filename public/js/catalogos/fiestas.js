var base_url = $('#base_url').val();
var table;
$(document).ready(function() {
    table = $('#data-tables').DataTable();
    var form_register = $('#formEventos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validatorClausulas = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre: {required: true},
            titular: {required: true},
            precio_total: {required: true},
            precioex: {required: true},
            cantidad: {required: true},
            fecha_inicio: {required: true},
            hora_inicio: {required: true},
            hora_fin: {required: true},
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);
        },
        highlight: function(element) { // hightlight error inputs
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
        },
        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },
        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        },
        submitHandler: function() {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //===================== fin de validador para salas
    $('#saveevento').click(function(event) {
        var $form = $("#formEventos").valid();
        if ($form) {
            var datos = $('#formEventos').serialize()+'&cantidadt='+$('#cantidad').val();
            $.ajax({
                type: 'POST',
                url: 'eventosnew',
                data: datos,
                async: false,
                statusCode: {
                    404: function(data) {toastr.error('Error', '404');},
                    500: function() {toastr.error('Error', '500');}
                },
                success: function(data) {
                    console.log(data);
                    toastr.success('Evento agregado');
                    location.href = base_url + 'Eventos';
                },
                error: function(jqXHR, estado, error) {
                    console.log("Estado: " + estado);
                    console.log(jqXHR);
                    console.log("Error: " + error);
                },
            });
        }
    });
    $('#titular').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Titular',
        ajax: {
            url: base_url+'Clientes/searchcli',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.titularId,
                    text: element.nombre
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
    loadtable();
});

function buscarC() {
    var search = $('#buscar').val();
    var UR = $('#url').val();
    if (search.length > 0) {
        $.ajax({
            type: 'POST',
            url: base_url + 'Eventos/Buscar',
            data: {buscar: $('#buscar').val(),},
            async: false,
            statusCode: {
                404: function(data) {toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function() {toastr.error('Error', '500');}
            },
            success: function(data) {
                $('#tbodyresultados').html(data);
            },
            error: function(jqXHR, estado, error) {
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    } else {
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
}
function ninosview(id){
    $.ajax({
            type: 'POST',
            url: base_url + 'Eventos/ninosavtivos',
            data: {
                idfiesta: id
            },
            async: false,
            statusCode: {
                404: function(data) {toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function() {toastr.error('Error', '500');}
            },
            success: function(data) {
                $('.tablelistaninos').html(data);
                $('#modalninoslistado').modal();
                $('#tabla').DataTable();
            },
            error: function(jqXHR, estado, error) {
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });
}
function preciopaquete(){
    var precio=$('#paquete option:selected').data("valor");
    $('#precio_total').val(precio);
}
function loadtable(){
    table.destroy();
    table=$('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
                "url": base_url+"Eventos/getlisteventos",
                type: "post",
                "data": {
                    'tipo':0,
                },
            },
        "columns": [
                {"data":"fiestaId"},
                {"data":"nombre"},
                {"data":"precio_total"},
                {"data":"precioex"},
                {"data":"cantidadt"},
                {"data":"cantidadm"},
                {"data":"fecha_inicio"},
                {"data":"hora_inicio"},
                {"data":"hora_fin"},
                {"data": null,
                    "render" : function (data,type,row) {
                        var html='';
                            html+='<div class="row">\
                                        <div class="col-md-12" align="center">\
                                            <a class="btn btn-primary" href="'+base_url+'Eventos/Eventosadd?eve='+row.fiestaId+'"><i class="ft-edit-3"></i></a>\
                                            <button class="btn btn-primary" onclick="ninosview('+row.fiestaId+')" title="niños registrados"><i class="fa fa-user"></i> </button>';
                        if (row.titular!=0) {
                            html+='<a class="btn btn-primary" href="'+base_url+'Ventas/VentasEstacia2?cod='+row.titular+'&event='+row.fiestaId+'">Venta Estancia</a>';
                        } 
                    html+='</div>\
                          </div>';
                        return html;
                    }
                }
            ],
        order: [[0, 'desc']]
    });
}