<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $data['personal']=$this->ModeloPersonal->getpersonal();
            $this->load->view('Personal/Personal',$data);
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersonal');
	}
    /**
     * Retorna vista para agregar personal 
     */
    public function Personaladd(){
            //carga de vistas
            $data['getareas']=$this->ModeloCatalogos->getselectwhere('areas','activo',1);
            $data['getestado']=$this->ModeloCatalogos->getselectwhere('estado','activo',1);
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Personal/Personaladd',$data);
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersonal');
    }    
    public function addpersonal(){
        $data = $this->input->post();
        if ($data['personalId']==0) {
            unset($data['personalId']);
            echo $this->ModeloCatalogos->insertToCatalogo($data, 'personal');
        }else{
            $id=$data['personalId'];
            unset($data['personalId']);
            echo $this->ModeloCatalogos->updateCatalogo($data,'personalId',$id, 'personal');
        } 
    }
    public function deletepersonal(){
        $id = $this->input->post('id');
        $this->ModeloPersonal->personaldelete($id); 
    } 
    public function reactivarpersonal(){
        $id = $this->input->post('id');
        $this->ModeloPersonal->reactivarpersonal($id); 
    }
    public function suspencionpersonal(){
        $id = $this->input->post('id');
        $mot = $this->input->post('mot');
        $this->ModeloPersonal->suspencionpersonal($id,$mot); 
    }   

    public function HorarioPersonal(){
        $id=$this->input->post('id');
        $full;
        $x=0;
        $cabeza='<table class="table table-striped"><tr><th>Fecha</th><th>Entrada</th><th>Salida</th></tr><tr>';
        $contenido="";
        $pies='</tr></table>';
        $data=$this->ModeloPersonal->Horarios($id);
        foreach ($data->result() as $key) {
            $tabla="<td>".$key->FechaEntrada."</td>";
            $tabla2="<td>".$key->HoraEntrada."</td>";
            $tabla3="<td>".$key->HoraSalida."</td>";
            $contenido=$contenido."<tr>".$tabla.$tabla2.$tabla3."</tr>";
        }
        echo ($cabeza.$contenido.$pies);
    } 
    function EditImage(){
        $idcat = $this->input->post('personalId');
        $target_path = 'public/img/personal';
        $thumb_path = 'public/img/personalt';
        //file name setup
        $filename_err = explode(".",$_FILES['img']['name']);
        $filename_err_count = count($filename_err);
        $file_ext = $filename_err[$filename_err_count-1];
        
            $fileName = $_FILES['img']['name'];
        $fecha=date('ymd-His');
        $cargaimagen =$fecha.'cat'.basename($fileName);
        $upload_image = $target_path.'/'.$cargaimagen;
        
        //upload image
        if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
            //thumbnail creation
                $this->ModeloPersonal->personaladdimg($cargaimagen,$idcat);
                $thumbnail = $thumb_path.'/'.$cargaimagen;
                list($width,$height) = getimagesize($upload_image);
                if ($width>3000) {
                    $thumb_width = $width/10;
                    $thumb_height = $height/10;
                }elseif ($width>2500) {
                    $thumb_width = $width/7.9;
                    $thumb_height = $height/7.9;
                }elseif ($width>2000) {
                    $thumb_width = $width/6.8;
                    $thumb_height = $height/6.8;
                }elseif ($width>1500) {
                    $thumb_width = $width/5.1;
                    $thumb_height = $height/5.1;
                }elseif ($width>1000) {
                    $thumb_width = $width/3.3;
                    $thumb_height = $height/3.3;
                }elseif ($width>900) {
                    $thumb_width = $width/3;
                    $thumb_height = $height/3;
                }elseif ($width>800) {
                    $thumb_width = $width/2.6;
                    $thumb_height = $height/2.6;
                }elseif ($width>700) {
                    $thumb_width = $width/2.3;
                    $thumb_height = $height/2.3;
                }elseif ($width>600) {
                    $thumb_width = $width/2;
                    $thumb_height = $height/2;
                }elseif ($width>500) {
                    $thumb_width = $width/1.9;
                    $thumb_height = $height/1.9;
                }elseif ($width>400) {
                    $thumb_width = $width/1.3;
                    $thumb_height = $height/1.3;
                }else{
                    $thumb_width = $width;
                    $thumb_height = $height;
                }
                $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                switch($file_ext){
                    case 'jpg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;
                    case 'jpeg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;

                    case 'png':
                        $source = imagecreatefrompng($upload_image);
                        break;
                    case 'gif':
                        $source = imagecreatefromgif($upload_image);
                        break;
                    case 'JPG':
                        $source = imagecreatefromjpeg($upload_image);
                        break;
                    case 'JPEG':
                        $source = imagecreatefromjpeg($upload_image);
                        break;

                    case 'PNG':
                        $source = imagecreatefrompng($upload_image);
                        break;
                    case 'GIF':
                        $source = imagecreatefromgif($upload_image);
                        break;
                    default:
                        $source = imagecreatefromjpeg($upload_image);
                }

                imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                switch($file_ext){
                    case 'jpg' || 'jpeg':
                        imagejpeg($thumb_create,$thumbnail,100);
                        break;
                    case 'png':
                        imagepng($thumb_create,$thumbnail,100);
                        break;

                    case 'gif':
                        imagegif($thumb_create,$thumbnail,100);
                        break;
                    case 'JPG' || 'JPEG':
                        imagejpeg($thumb_create,$thumbnail,100);
                        break;
                    case 'PNG':
                        imagepng($thumb_create,$thumbnail,100);
                        break;

                    case 'GIF':
                        imagegif($thumb_create,$thumbnail,100);
                        break;
                    default:
                        imagejpeg($thumb_create,$thumbnail,100);
                }

            

            $return = Array('ok'=>TRUE,'img'=>'');
        }
        else{
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }
        echo json_encode($return);
    }
    public function creditostabla()
    {   $idpersona = $this->input->post('idpersona');
        $fechainicio = $this->input->post('fechainicio');
        $fechafin = $this->input->post('fechafin');
        $html = '';
        $creditos = $this->ModeloPersonal->getallcreditos($idpersona,$fechainicio,$fechafin);
        $html .='<div class="row">
                    <div class="col-md-12">
                      <table class="table table-striped" id="tables_creditos">
                        <thead>
                          <tr>
                            <th>Fecha</h> 
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody>';
                    $totalsuma = 0;     
                    foreach ($creditos as $item) {
                        $total = $item->cantidad * $item->precio;
                        $totalsuma = $totalsuma+$total;//date('d-m-Y g:i a',strtotime($item->reg))
                   $html.='<tr>
                            <td>'.date('d-m-Y g:i a',strtotime($item->reg)).'</td>
                            <td>'.$item->producto.'</td>
                            <td>'.$item->cantidad.'</td>
                            <td>'.$item->precio.'</td>
                            <td>'.$total.'</td>
                        </tr>';
                    }
                    $html.='</tbody>
                      </table>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-2"><h3>Total:$'.$totalsuma.'</h3></div>
                </div>
            ';
        echo $html;     
    }    
}
