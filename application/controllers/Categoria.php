<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
        $data['categoriasll']=$this->ModeloCatalogos->getselectwhere('producto_padre','activo',1);
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/categorias',$data);
        $this->load->view('templates/footer');
	}
    function categoriadell(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->categoriadell($id);
    }
    function categoriaadd(){
        $id = $this->input->post('id');
        $nom = $this->input->post('nom');
        if ($id>0) {
            $this->ModeloCatalogos->categoriupdate($nom,$id);
            echo $id;
        }else{
            echo $this->ModeloCatalogos->categoriaadd($nom);
        }
    }
    function imgcat(){
        $idcat = $this->input->post('idcat');
        $target_path = 'public/img/categoria';
        $thumb_path = 'public/img/categoriat';
        //file name setup
        $filename_err = explode(".",$_FILES['img']['name']);
        $filename_err_count = count($filename_err);
        $file_ext = $filename_err[$filename_err_count-1];
        //if($file_name != ''){
           // $fileName = $file_name.'.'.$file_ext;
        //}else{
            $fileName = $_FILES['img']['name'];
        //}
        $fecha=date('ymd-His');
        //upload image path
        $cargaimagen =$fecha.'cat'.basename($fileName);
        $upload_image = $target_path.'/'.$cargaimagen;
        
        //upload image
        if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
            //thumbnail creation
                $this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                $thumbnail = $thumb_path.'/'.$cargaimagen;
                list($width,$height) = getimagesize($upload_image);
                if ($width>3000) {
                    $thumb_width = $width/10;
                    $thumb_height = $height/10;
                }elseif ($width>2500) {
                    $thumb_width = $width/7.9;
                    $thumb_height = $height/7.9;
                }elseif ($width>2000) {
                    $thumb_width = $width/6.8;
                    $thumb_height = $height/6.8;
                }elseif ($width>1500) {
                    $thumb_width = $width/5.1;
                    $thumb_height = $height/5.1;
                }elseif ($width>1000) {
                    $thumb_width = $width/3.3;
                    $thumb_height = $height/3.3;
                }elseif ($width>900) {
                    $thumb_width = $width/3;
                    $thumb_height = $height/3;
                }elseif ($width>800) {
                    $thumb_width = $width/2.6;
                    $thumb_height = $height/2.6;
                }elseif ($width>700) {
                    $thumb_width = $width/2.3;
                    $thumb_height = $height/2.3;
                }elseif ($width>600) {
                    $thumb_width = $width/2;
                    $thumb_height = $height/2;
                }elseif ($width>500) {
                    $thumb_width = $width/1.9;
                    $thumb_height = $height/1.9;
                }elseif ($width>400) {
                    $thumb_width = $width/1.3;
                    $thumb_height = $height/1.3;
                }else{
                    $thumb_width = $width;
                    $thumb_height = $height;
                }
                $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                switch($file_ext){
                    case 'jpg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;
                    case 'jpeg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;

                    case 'png':
                        $source = imagecreatefrompng($upload_image);
                        break;
                    case 'gif':
                        $source = imagecreatefromgif($upload_image);
                        break;
                    default:
                        $source = imagecreatefromjpeg($upload_image);
                }

                imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                switch($file_ext){
                    case 'jpg' || 'jpeg':
                        imagejpeg($thumb_create,$thumbnail,100);
                        break;
                    case 'png':
                        imagepng($thumb_create,$thumbnail,100);
                        break;

                    case 'gif':
                        imagegif($thumb_create,$thumbnail,100);
                        break;
                    default:
                        imagejpeg($thumb_create,$thumbnail,100);
                }

            

            $return = Array('ok'=>TRUE,'img'=>'');
        }
        else{
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }
        echo json_encode($return);
    }
    function precioestancia(){
        $horas = $this->input->post('hora');
        $ninos = $this->input->post('ninos');
        $tipoventa = $this->input->post('tipoventa');
        $tbebe = $this->input->post('tbebe');

        $total=0;
        $precio=0;
        $ninoextras=0;
        //===============================================
        if ($tipoventa==0) {
            $precios=$this->ModeloCatalogos->getcostotime();
            $precio=0;
            foreach ($precios->result() as $item) {
                $precio=$item->precio;
            }
            if ($horas>2.5) {
                if($tbebe==0){
                    $tiempolibretipo=2;
                }else{
                    $tiempolibretipo=3;
                }
                $precio=$this->ModeloCatalogos->getselectvalue1rowwhere('config','valor','configId',$tiempolibretipo);
                $total=$ninos*$precio;
                //$precio=$ninos*$precio;
            }else{
                $total=$horas*$ninos*$precio;
            }
            
            $precio=$ninos*$precio;
        }else{
            $resultados=$this->ModeloCatalogos->getselectwhere('fiestas','fiestaId',$tipoventa);
            foreach ($resultados->result() as $item){
                $precioex=$item->precioex;
                $cantidad=$item->cantidad;
            }






            $ninoss=$ninos;
            while ($ninoss >= 1){
                //===========================
                if ($cantidad<=0) {
                    $total=$total+($horas*$precioex);
                    $precio=$precio+$precioex;
                    $ninoextras=1;
                }
                $cantidad=$cantidad-1;
                //===========================
                $ninoss--;
            }
        }
        
        //echo '('.$horas.')*('.$ninos.')*('.$precio.')=';
        $return = Array('total'=>$total,'minimo'=>$precio,'ninoextras'=>$ninoextras);
        echo json_encode($return);
    }
    function preciosasignacion(){
        $tipoventa = $this->input->post('tipoventa');
        $precios=$this->ModeloCatalogos->getcostotime();
        $precio=0;
        foreach ($precios->result() as $item) {
            $precio=$item->precio;
        }
        $tiempolibre=$this->ModeloCatalogos->getselectvalue1rowwhere('config','valor','configId',2);
        $tiempolibrebebe=$this->ModeloCatalogos->getselectvalue1rowwhere('config','valor','configId',3);

        $paquetefamiliar=$this->ModeloCatalogos->getselectvalue1rowwhere('config','valor','configId',4);
        $tiempolibreadulto=$this->ModeloCatalogos->getselectvalue1rowwhere('config','valor','configId',5);
        $paquetemasmenos=$this->ModeloCatalogos->getselectvalue1rowwhere('config','valor','configId',6);

        //==============================================
        
            $resultados=$this->ModeloCatalogos->getselectwhere('fiestas','fiestaId',$tipoventa);
            $precioex=0;
            $cantidad=0;
            foreach ($resultados->result() as $item){
                $precioex=$item->precioex;
                $cantidad=$item->cantidad;
            }






        $return = Array(
                        'preciogeneral'=>$precio,
                        'tiempolibre'=>$tiempolibre,
                        'tiempolibrebebe'=>$tiempolibrebebe,
                        'precioex'=>$precioex,
                        'cantidad'=>$cantidad,
                        'paquetefamiliar'=>$paquetefamiliar,
                        'tiempolibreadulto'=>$tiempolibreadulto,
                        'paquetemasmenos'=>$paquetemasmenos
                    );
        echo json_encode($return);

    }
    
   
    
    
    
    

}