<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listadoventasv extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
    }
	public function index(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('ventas/listadov');
            $this->load->view('templates/footer');
	}
    public function getlistventas() {
        $params = $this->input->post();
        $getdata    = $this->ModeloVentas->getlistventas_v($params);
        $totaldata  = $this->ModeloVentas->getlistventas_v_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    

       
    
}
