<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de productos eliminados</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <table class="table table-striped" id="tablaListadoProductosEliminados">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Mesa</th>
                        <th>Producto</th>
                        <th>Nombre</th>
                        <th>Fecha de Registro</th>
                      </tr>
                    </thead>
                    <tbody class="tbody_lcompras">
                      
                    </tbody>
                  </table>
                  
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>

