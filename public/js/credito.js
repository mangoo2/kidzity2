var productoid;
var base_url=$('#base_url').val();
$(document).ready(function() {
	$(".classproducto").click(function(event) {
    	productoid= $(this).attr('data-idcate');
    	cargaproductos(productoid);
    	$(".panelventa1").css("display","none");
    	$(".panelventa2").css("display","block");
    });
    $('.regresaprincipal').click(function(event) {
    	$(".panelventa1").css("display","block");
    	$(".panelventa2").css("display","none");
    	$(".panelventa3").css("display","none");
    });
    $('#codigobarras').keyup(function(e){
         if(e.keyCode == 13){
            productoselectedcodigo();
         }
    });
    $('#entercodigo').click(function(event) {
        productoselectedcodigo();
    });
    $("#montoapagar").TouchSpin({
      initval: 0,
      min: 0,
      max:9999999999999999999999999,
    }).change(function(event) {
        calculartotal();
    });
    $('#vender').click(function(event) {
        var xx = $('#personalId option:selected').val();
        if(xx >=1){
            var DATA  = [];
            var TABLA   = $("#tableproductosv tbody > tr");
            TABLA.each(function(){    
                item = {};
                item ["idproductos"] = $(this).find("input[id*='idproductos']").val();     
                item ["cantidad"] = $(this).find("input[id*='vscanti']").val();
                item ["precio"] = $(this).find("input[id*='vsprecio']").val();
              DATA.push(item);
            });
            productos   = JSON.stringify(DATA);
            $.ajax({
                type: 'POST',
                url: base_url+'Mesas/addventas',
                data: {
                    personalId:$('#personalId').val(),
                    productos:productos
                },
                async: false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error', '404');
                    },
                    500: function(data) {
                        
                    },
                },
                success: function(data) {
                    toastr.success('venta realizada', 'Hecho');
                    setTimeout(function(){ window.location = base_url+"Mesas"; }, 3000);
                },
            });
        }else{
            toastr.error('Tienes que selecionar un empleado','Error');
        }    
    });
});
function cargaproductos(idp){
	$.ajax({
        type: 'POST',
        url: base_url+'Mesas/cargarproductos_credito',
        data: {id: idp},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
            	toastr.error('Error', '500');
            	console.log(data.responseText);
            }
        },
        success: function(data) {
        	$('.panelventa2').html('');
            $('.panelventa2').html(data);
        }
    });
}
function productoselected(idp){
	$(".panelventa2").css("display","none");
	$(".panelventa2").html("");
	$(".panelventa3").css("display","block");
    $.ajax({
        type: 'POST',
        url: 'Ventasp/agregarproductos',
        data: {id: idp},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
                toastr.error('Error', '500');
                console.log(data.responseText);
            }
        },
        success: function(data) {
            $('.addselectedprod').html('');
            $('.addselectedprod').html(data);
        }
    });
    calculartotal();
}
function deletepro(id){
    $.ajax({
        type:'POST',
        url: base_url+'Mesas/deleteproducto',
        data: {
            idd: id
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                $('.producto_'+id).remove();
            }
        });
    calculartotal();
}
function calculartotal(){
    var addtp = 0;
    $(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    $('.totalproductos').html(new Intl.NumberFormat('es-MX').format(addtp));
    $('#totalproductos').val(addtp);

    var cambio=parseFloat($('#montoapagar').val())-parseFloat(addtp);
    if (cambio<=0) {
        cambio=0;
      }else{
        cambio=cambio;
      }

    $('.cambio').html(new Intl.NumberFormat('es-MX').format(cambio));
}
function productoselectedcodigo(){
    $.ajax({
            type:'POST',
            url: base_url+'Mesas/viewproducto',
            data: {
                codigo: $('#codigobarras').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
               if (data>0) {
                $(".panelventa1").css("display","none");
                $(".panelventa2").css("display","none");
                $(".panelventa3").css("display","block");
                $('#codigobarras').val('');
                $("#codigobarras").focus();
                productoselected(data);
               }else{
                    toastr.error('Error', 'No Existe producto');
               }
            }
        });
}
function productoselected(idp){
    $(".panelventa2").css("display","none");
    $(".panelventa2").html("");
    $(".panelventa3").css("display","block");
    $.ajax({
        type: 'POST',
        url: base_url+'Mesas/agregarproductos',
        data: {id: idp},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
                toastr.error('Error', '500');
                console.log(data.responseText);
            }
        },
        success: function(data) {
            $('.addselectedprod').html('');
            $('.addselectedprod').html(data);
        }
    });
    calculartotal();
}
function calculartotal(){
    var addtp = 0;
    $(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    $('.totalproductos').html(new Intl.NumberFormat('es-MX').format(addtp));
    $('#totalproductos').val(addtp);

    var cambio=parseFloat($('#montoapagar').val())-parseFloat(addtp);
    if (cambio<=0) {
        cambio=0;
      }else{
        cambio=cambio;
      }

    $('.cambio').html(new Intl.NumberFormat('es-MX').format(cambio));
}