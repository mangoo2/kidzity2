var base_url= $('#base_url').val();
var table;
$(document).ready(function() {
    table = $('#data-tables').DataTable();
    loadtable();
});
function loadtable(){
    table.destroy();
    table=$('#data-tables').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
                "url": base_url+"Listadoventasg/getlistventas",
                type: "post",
                "data": {
                    'tipo':0,
                },
            },
        "columns": [
                {"data": "ventaId"},
                {"data": "reg"},
                {"data": "monto_total",
					"render" : function (data,type,row) {
						var html='';
                        
	                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.monto_total);
	                    return html;
					}
				},
				{"data": "metodo",
					"render" : function (data,type,row) {
						var html='';
                        	if(row.metodo==1){
                        		html='Efectivo';
                        	}else{
                        		html='Tarjeta';
                        	}
	                    return html;
					}
				},
				{"data": "nombre"},
                {"data": null,
                    "render" : function (data,type,row) {
                        var html='';
                            html+='<button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket('+row.ventaId+')" title="Ticket" data-toggle="tooltip" data-placement="top">\
                              <i class="fa fa-book"></i>\
                            </button>';
                        return html;
                    }
                }
            ],
        order: [[0, 'desc']]
    });
}