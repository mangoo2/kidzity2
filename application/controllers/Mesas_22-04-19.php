<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesas extends CI_Controller 
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloVentas');
        $this->load->model('Usuarios/ModeloUsuarios');
    }
    
    public function index()
    {
        session_regenerate_id();
        $data['productospadres']=$this->ModeloVentas->getproductopadre();
        $data['nummesas']=20;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/mesas',$data);
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesa');

        for($i=0; $i<$data['nummesas']; $i++) 
        {
            if(!isset($_SESSION['mesa'][$i]))
            {
                $_SESSION['mesa'][$i]=array();
            }
        }
        
    }

    // VIEWS 
    public function listadoProductosEliminados() 
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/listadoProductosEliminados');
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesalistado');
    }

    public function getListadoProductosEliminados()
    {
        $productos= $this->ModeloVentas->getListadoProductosEliminados();
        $json_data = array("data" => $productos);
        echo json_encode($json_data);
    }

    public function cargarproductos()
    {
        $id = $this->input->post('id');
        
        $respuesta=$this->ModeloVentas->getproductoshijos($id);
        foreach ($respuesta->result() as $item){ ?>
        <div class="col-md-3 galeriasimg flipInX animated classproductohijo cursor" data-idcate="<?php echo $item->productoid; ?>" style="background: url('<?php echo base_url(); ?>public/img/productos/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;" onclick="preproductoselected(<?php echo $item->productoid; ?>)" >
            <h2><?php echo $item->producto;?></h2>
        </div>
      <?php }
    }

    // Agregar productos a la tabla de cada mesa
    function agregarproductos()
    {
        session_regenerate_id();
        // Obtenemos el ID del producto y la Mesa en la cual trabajaremos
        $id = $this->input->post('id');
        $selectedmesa = $this->input->post('selectedmesa');
        $valuePIN = $this->input->post('pin');
        
        $banderaUsuario = 0;
        // Inicializamos la cantidad en 1
        $cant=1;

        // Obtenemos los datos del producto que se quiere vender
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','productoid',$id);
        foreach ($resultado->result() as $item) 
        {
            $oProducto = array('productoid' =>$item->productoid,
                'codigo' =>$item->codigo,
                'producto' =>$item->producto,
                'descripcion' =>$item->descripcion,
                'precio_venta' =>$item->precio_venta,
                'img' =>$item->img
            );
        }

        // Primero buscamos si el arreglo de la mesa que actualizaremos ya existe en el arreglo
        // Si existe, entramos a guardar el producto
        if(isset($_SESSION['mesa'][$selectedmesa]['pro']))
        {
            if($_SESSION['mesa'][$selectedmesa]['usuarioPin']==$valuePIN)
            {
                // Si el producto se encuentra ya registrado en el arreglo, solo aumentamos la cantidad
                if(in_array($oProducto, $_SESSION['mesa'][$selectedmesa]['pro']))
                {   
                    $idx = array_search($oProducto, $_SESSION['mesa'][$selectedmesa]['pro']);
                    $_SESSION['mesa'][$selectedmesa]['can'][$idx]+=$cant;
                }
                // Si no, se agrega al arreglo
                else
                {
                    array_push($_SESSION['mesa'][$selectedmesa]['pro'],$oProducto);
                    array_push($_SESSION['mesa'][$selectedmesa]['can'],$cant);
                }
            }
            else
            {
                $banderaUsuario = 1;
            }
        }
        // Si no, creamos el arreglo
        else
        { 
            // Creamos el arreglo para producto y cantidad
            $_SESSION['mesa'][$selectedmesa]['pro'] = array();
            $_SESSION['mesa'][$selectedmesa]['can'] = array();
            $_SESSION['mesa'][$selectedmesa]['usuarioPin'] = $valuePIN;

            // Insertamos el producto y la cantidad
            array_push($_SESSION['mesa'][$selectedmesa]['pro'],$oProducto);
            array_push($_SESSION['mesa'][$selectedmesa]['can'],$cant);
        } 

        if($banderaUsuario==0)
        {
            $this->cargaPanel($selectedmesa);
        }
        else
        {
            echo "usuarioinvalido";
        }
    }

    function deleteproducto()
    {
        // Obtiene los datos a eliminar
        $idd = $this->input->post('idd');
        $selectedmesa = $this->input->post('mesa');
        $row = $this->input->post('row');

        // Guarda el registro del producto eliminado
        $registroEliminado = array(
            'idUsuario' => $_SESSION['idpersonal'],
            'idProducto' => $idd,
            'mesa' => $selectedmesa,
            'fechaRegistro' => date('Y-m-d')
        );
        $this->ModeloVentas->insertaRegistroElimiando($registroEliminado);
        
        // Elimina el producto del arreglo
        unset($_SESSION['mesa'][$selectedmesa]['pro'][$row]);
        
        // Elimina la cantidad de producto del arreglo
        unset($_SESSION['mesa'][$selectedmesa]['can'][$row]);
        
    }

    function deleteproductoindividual()
    {
        // Obtiene los datos a eliminar
        $idd = $this->input->post('idd');
        $selectedmesa = $this->input->post('mesa');
        $row = $this->input->post('row');

        // Guarda el registro del producto eliminado
        $registroEliminado = array(
            'idUsuario' => $_SESSION['idpersonal'],
            'idProducto' => $idd,
            'mesa' => $selectedmesa,
            'fechaRegistro' => date('Y-m-d')
        );
        $this->ModeloVentas->insertaRegistroElimiando($registroEliminado);
        
        if($_SESSION['mesa'][$selectedmesa]['can'][$row]>1)
        {
            $_SESSION['mesa'][$selectedmesa]['can'][$row] = ($_SESSION['mesa'][$selectedmesa]['can'][$row] - 1);
            echo $_SESSION['mesa'][$selectedmesa]['can'][$row];
        }
        else if($_SESSION['mesa'][$selectedmesa]['can'][$row]=1)
        {
            // Elimina el producto del arreglo
            unset($_SESSION['mesa'][$selectedmesa]['pro'][$row]);
            
            // Elimina la cantidad de producto del arreglo
            unset($_SESSION['mesa'][$selectedmesa]['can'][$row]);
            echo 0;
        }     
    }

    function addproductoindividual()
    {
        // Obtiene los datos a eliminar
        $idd = $this->input->post('idd');
        $selectedmesa = $this->input->post('mesa');
        $row = $this->input->post('row');
        
        // Aumenta en 1 la cantidad del producto seleccionado
        $_SESSION['mesa'][$selectedmesa]['can'][$row] = ($_SESSION['mesa'][$selectedmesa]['can'][$row] + 1);

        echo $_SESSION['mesa'][$selectedmesa]['can'][$row]; 
    }

    function deleteMesa()
    {
        $selectedmesa = $this->input->post('mesa');
        unset($_SESSION['mesa'][$selectedmesa]);
    }

    function viewproducto()
    {
        $codigo =$this->input->post('codigo');
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','codigo',$codigo);
        $idc=0;
        foreach ($resultado->result() as $item) {
            $idc=$item->productoid;
        }
        echo $idc;
    }

    function verificarsaldo(){
        $codigo =$this->input->post('codigo');
        echo $this->ModeloCatalogos->verificarsaldo($codigo);
    }
    
    // Agregar una nueva venta
    function addventas()
    {
        $tipo = $this->input->post('tipo'); 
        // Obtenemos los datos del POST
        $banderaUsuario = 0;
        $selectedmesa = $this->input->post('selectedmesa');
        $pinaux = $this->input->post('pinaux');
        $descuento = $this->input->post('descuento');

        // Verfificamos que los datos del PIN correspondan
        if($pinaux==$_SESSION['mesa'][$selectedmesa]["usuarioPin"])
        {
            // Buscaremos los datos del usuario al que corresponda el PIN
            $datosUsuario = $this->ModeloUsuarios->getUsuarioPorPin($pinaux);

            // Si el PIN ingresado no corresponde con algún usuario registrado, regresaremos error y no se realizará la venta
            if (empty($datosUsuario)) 
            {
                echo 'pininvalido';
            }
            // De lo contrario, se procede
            else
            {
                $idpersonal = $datosUsuario[0]->personalId;

                $monto_total=$this->input->post('monto_total');
                $monto_cobrado=$monto_total-$descuento;
                $pulcera=$this->input->post('pulcera');
                $metodo=$this->input->post('metodo');
                $productos=$this->input->post('productos');
                $selectedmesa = $this->input->post('selectedmesa');
                
                // Si el tipo es 1, es para venta;
                // si es tipo 0, es solo impresión de ticket
                if($tipo==1)
                {
                    // Verificamos si existe el ID de la venta para solo hacer la actualización del registro
                    if(isset($_SESSION['mesa'][$selectedmesa]["idVenta"]))
                    {
                        // Hacemos el array para actualizar la venta
                        $datos = array('tipo' => 1, 'monto_total' => $monto_total, 'descuento' => $descuento, 'monto_cobrado' => $monto_cobrado);
                        // Recuperamos el ID de la venta ya registrada y que guardamos previamente en el arreglo de la mesa
                        $idventas = $_SESSION['mesa'][$selectedmesa]["idVenta"];
                        // Actualizamos en la BD
                        $registroActualizado = $this->ModeloVentas->actualizaVenta($idventas,$datos);

                        // Eliminamos los detalles de la venta que existan en el momento
                        $eliminado = $this->ModeloVentas->eliminaDetallesVenta($idventas);    
                    }
                    // Si no trae ID, es una venta directa
                    else
                    {
                        // Se inserta el registro de venta en la BD
                        $idventas=$this->ModeloVentas->addventasMesas($idpersonal,$monto_total,$pulcera,$metodo,$selectedmesa,$descuento,$monto_cobrado,$tipo);
                    }                    
                }
                else
                {
                    // Se asigna el tipo en 0, para indicar que solo es impresión de ticket
                    $tipo = 0;

                    // Verificamos que exista previamente el ID de la venta
                    // Si no existe, será una inserción nueva
                    if(!isset($_SESSION['mesa'][$selectedmesa]["idVenta"]))
                    {    
                        // Se inserta el registro de venta en la BD
                        $idventas=$this->ModeloVentas->addventasMesas($idpersonal,$monto_total,$pulcera,$metodo,$selectedmesa,$descuento,$monto_cobrado,$tipo);
                        // En la sesión se agrega el tipo 
                        $_SESSION['mesa'][$selectedmesa]["tipo"]=0;
                        // En la sesión se agrega el ID de la venta
                        $_SESSION['mesa'][$selectedmesa]["idVenta"]=$idventas;
                    }
                    // Si ya existe, solo borraremos los registros anteriores y actualizamos el registro anterior 
                    else
                    {
                        // Recuperamos el ID de la venta ya registrada y que guardamos previamente en el arreglo de la mesa
                        $idventas = $_SESSION['mesa'][$selectedmesa]["idVenta"];
                        // Hacemos el array para actualizar los datos en el registro ya guardado 
                        $datos = array('monto_total' => $monto_total, 'descuento' => $descuento, 'monto_cobrado' => $monto_cobrado);
                        // Actualizamos en la BD
                        $registroActualizado = $this->ModeloVentas->actualizaVenta($idventas,$datos);

                    }

                    
                }
                // Insertamos en la BD los detalles de la venta
                $DATA = json_decode($productos);
                for ($i = 0; $i < count($DATA); $i++) 
                {
                    $idproductos=$DATA[$i]->idproductos;
                    $cantidad=$DATA[$i]->cantidad;
                    $precio=$DATA[$i]->precio;
                    $this->ModeloVentas->addventasdetalleMesas($idventas,$idproductos,$cantidad,$precio);
                    $this->ModeloVentas->descuentostock($idproductos,$cantidad);

                }
                // Regresamos el ID de la venta para imprimir el ticket 
                echo $idventas;

            }            
        }
        else
        {
            echo "usuarioinvalido";
        }
    }

    function getUsuarioPorPin($pinaux)
    {
        $datosUsuario = $this->ModeloUsuarios->getUsuarioPorPin($pinaux);
        if (empty($datosUsuario)) 
        {
            echo 'pininvalido';
        }
        else echo 'ok';
    }

    function comparaPinRealizarVenta()
    {
        $id = $this->input->post('id');
        $data = $this->input->post();
        $pin = $data['pin'];
        $selectedmesa = $data['mesa'];

        // Primero buscamos si el arreglo de la mesa donde buscaremos el PIN existe
        if(isset($_SESSION['mesa'][$selectedmesa]))
        {
            if($pin==$_SESSION['mesa'][$selectedmesa]["usuarioPin"])
            {
                $banderaUsuario = 0;
                // Inicializamos la cantidad en 1
                $cant=1;

                // Obtenemos los datos del producto que se quiere vender
                $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','productoid',$id);
                foreach ($resultado->result() as $item) 
                {
                    $oProducto = array('productoid' =>$item->productoid,
                        'codigo' =>$item->codigo,
                        'producto' =>$item->producto,
                        'descripcion' =>$item->descripcion,
                        'precio_venta' =>$item->precio_venta,
                        'img' =>$item->img
                    );
                }

                // Primero buscamos si el arreglo de la mesa que actualizaremos ya existe en el arreglo
                // Si existe, entramos a guardar el producto
                if(isset($_SESSION['mesa'][$selectedmesa]['pro']))
                {
                    if($_SESSION['mesa'][$selectedmesa]['usuarioPin']==$pin)
                    {
                        // Si el producto se encuentra ya registrado en el arreglo, solo aumentamos la cantidad
                        if(in_array($oProducto, $_SESSION['mesa'][$selectedmesa]['pro']))
                        {   
                            $idx = array_search($oProducto, $_SESSION['mesa'][$selectedmesa]['pro']);
                            $_SESSION['mesa'][$selectedmesa]['can'][$idx]+=$cant;
                        }
                        // Si no, se agrega al arreglo
                        else
                        {
                            array_push($_SESSION['mesa'][$selectedmesa]['pro'],$oProducto);
                            array_push($_SESSION['mesa'][$selectedmesa]['can'],$cant);
                        }
                    }
                    else
                    {
                        $banderaUsuario = 1;
                    }
                }
                // Si no, creamos el arreglo
                else
                { 
                    // Creamos el arreglo para producto y cantidad
                    $_SESSION['mesa'][$selectedmesa]['pro'] = array();
                    $_SESSION['mesa'][$selectedmesa]['can'] = array();
                    $_SESSION['mesa'][$selectedmesa]['usuarioPin'] = $valuePIN;

                    // Insertamos el producto y la cantidad
                    array_push($_SESSION['mesa'][$selectedmesa]['pro'],$oProducto);
                    array_push($_SESSION['mesa'][$selectedmesa]['can'],$cant);
                } 

                if($banderaUsuario==0)
                {
                    $this->cargaPanel($selectedmesa);
                }
            }
            else
            {
                echo "usuarioinvalido";
            }
        }
        else
        {
            $this->cargaPanel($selectedmesa);
        }
    }

    function cargaPanel($selectedmesa)
    {
        //==========================================
        $count = 0;
        $llave = 0;
        
        $arreglo = array_keys($_SESSION['mesa'][$selectedmesa]['pro']);
        foreach ($_SESSION['mesa'][$selectedmesa]['pro'] as $fila)
        {
            //$llave = key($_SESSION['mesa'][$selectedmesa]['pro']);
            $llave = $arreglo[$count];
            
            $Cantidad=$_SESSION['mesa'][$selectedmesa]['can'][$llave]; 
            $precio=$fila['precio_venta'];
            $cantotal=$Cantidad*$precio;
            //die();
            
            ?>
                <tr class="producto_<?php echo $llave;?>">
                    <td>
                        <input type="number" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                        <input type="hidden" id="idproductos" value="<?php echo $fila['productoid'];?>">
                    </td>
                    <td style="background: url('<?php echo base_url(); ?>public/img/productost/<?php echo $fila['img'];?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;"></td>
                    <td><?php echo $fila['producto'];?></td>
                    <td>$ <input type="text" name="vsprecio" id="vsprecio" value="<?php echo $precio;?>" readonly style="background: transparent;border: 0px;width: 100px;"></td>
                    <td>$ <input type="text" class="vstotal_<?php echo $selectedmesa; ?>" name="vstotal_<?php echo $selectedmesa; ?>" id="vstotal_<?php echo $selectedmesa; ?>" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>
                    <td>
                        <a class="danger acciones" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $fila['productoid'];?>,<?php echo $selectedmesa;?>,<?php echo $llave;?>)">
                            <i class="ft-trash font-large-1"></i>
                        </a>
                        <a>&nbsp;&nbsp;&nbsp;</a>
                        <a class="success acciones" data-original-title="Eliminar" title="Agregar 1" onclick="addproindividual(<?php echo $fila['productoid'];?>,<?php echo $selectedmesa;?>,<?php echo $llave;?>)">
                            <i class="ft-plus font-large-1"></i>
                        </a>
                        <a>&nbsp;&nbsp;&nbsp;</a>
                        <a class="warning acciones" data-original-title="Eliminar" title="Eliminar 1" onclick="deleteproindividual(<?php echo $fila['productoid'];?>,<?php echo $selectedmesa;?>,<?php echo $llave;?>)">
                            <i class="ft-minus font-large-1"></i>
                        </a>
                    </td>
                </tr>
            <?php
            $count++;
        }
    }
}