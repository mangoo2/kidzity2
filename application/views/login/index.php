<style type="text/css">
    .backlogin{
    background: url(<?php echo base_url(); ?>public/img/mancha.webp);
    background-size: 100% 100%;
    background-repeat: no-repeat;
    /*border-radius: 20px;
    
    -webkit-box-shadow: 16px 15px 58px -9px rgba(0,0,0,0.75);
    -moz-box-shadow: 16px 15px 58px -9px rgba(0,0,0,0.75);
    box-shadow: 16px 15px 58px -9px rgba(0,0,0,0.75);*/
    }
    .card-block{
        text-align: center;
    }
    #login-submit{
        background: url(<?php echo base_url(); ?>public/img/iniciarsesion.webp);
        background-size: cover;
        width: 200px;
        height: 50px;
        border: 0;
    }
    #login-submit:hover{
        width: 210px;
        height: 54px;
    }
</style>
<div class="wrapper">
      <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!--Registration Page Starts-->
            <section id="regestration">
                <div class="container">
                    <div class="row full-height-vh">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="col-12">
                                <div class="card-body">
                                    <div class="row d-flex">
                                        <div class="col-md-12 backlogin">
                                            <div class="col-12 col-sm-12 col-md-6">
                                                <div class="card-block">
                                                        <img style="margin-top: 16%" alt="Card image cap" src="<?php echo base_url(); ?>public/img/ops.webp" width="280">
                                                    <h2 class="card-title font-large-1 text-center white mt-3"></h2>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-6 d-flex align-items-center" style="min-height: 270px;">
                                                <div class="card-block mr-auto">
                                                    <form class="form-horizontal" id="login-form" action="#" role="form">
                                                        <div class="form-group input-group mt-2">
                                                            <span class="input-group-addon">
                                                                <i class="icon-user"></i>
                                                            </span>
                                                            <input type="text" placeholder="Nombre de usuario" id="txtUsuario" name="txtUsuario" class="form-control required" required>
                                                        </div>
                                                        <div class="form-group input-group mb-2">
                                                            <span class="input-group-addon">
                                                                <i class="ft-lock"></i>
                                                            </span>
                                                            <input type="password" placeholder="Contrase&ntilde;a" id="txtPass" name="txtPass" class="form-control required" required>
                                                        </div>
                                                        <div class=" text-center">
                                                          <button type="submit" id="login-submit"></button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-md-12">
                                            <div class="alert bg-success alert-icon-left alert-dismissible mb-2 messagecorrecto" role="alert" style="display: none;">
                                                <strong>Inicio de Sesión correcto, redireccionando...</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="alert bg-danger alert-icon-left alert-dismissible mb-2 messageerror" role="alert" style="display: none;">
                                                <strong>El nombre de Usuario y/o Contraseña es incorrecto.</strong>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>
            </section>
<!--Registration Page Ends-->
          </div>
        </div>
      </div>
    </div>
