<div class="container">
  <section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0">Paquetes de fiesta</h4>
        </div>
        <div class="card-body">
          <div class="px-3">
            <form class="form" role="form" id="formPaquete" name="formPaquete" method="post" >
              <div class="form-body">
                <div class="row">
                  <div class="container">
                    <div class="col-xl-4 col-lg-4 col-md-4">
                      <input type="hidden" id="IdPaquete" name="IdPaquete">
                      <div class="col-xl-12 col-lg-12 col-md-12">
                        <fieldset class="form-group">
                          <label for="nombre">Nombre</label>
                          <input type="text" class="form-control" value="" id="nombre" name="Nombre">
                        </fieldset>
                      </div>
                      <div class="col-xl-12 col-lg-12 col-md-12">
                        <fieldset class="form-group">
                          <label for="Precio">Precio</label>
                          <input type="text" class="form-control" value="" id="Precio" name="Precio">
                        </fieldset>
                      </div>
                      <div class="col-md-12">
                        <fieldset class="form-group">
                          <label for="Descripcion">Descripción</label>
                          <textarea name="Detalle" class="form-control" id="Descripcion" rows="4"></textarea>
                        </fieldset>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="col-md-12">
                        <fieldset class="form-group">
                          <label for="Paquete">Paquete</label>
                          <select id="Paquete" class="custom-select d-block w-100" name="Paquete">
                            <option value="" >Nuevo</option>
                            <?php foreach ($Paquete->result() as $item){ ?>
                            <option value="<?php echo $item->IdPaquete; ?>"><?php echo $item->Nombre; ?></option>
                            <?php } ?>
                          </select>
                        </fieldset>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" align="center">
                  <a class="btn btn-primary" id="saveFiesta"><i class="fa fa-save"></i>Guardar</a>
                  <button class="btn btn-danger" id="Delete" type="button" >Eliminar<i class="fa fa-trash-o"></i></button>
                </div>
              </div>
            </form>
            <input type="hidden" id="ruta" value="<?php echo base_url(); ?>">
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/Fiesta/Fiesta.js"></script>