<div class="row">
  <div class="col-md-12">
    <h2>Abierto</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-9"></div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Abierto</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          
          <div class="col-md-12">
            <div class="col-md-8">
            </div>
            <div class="col-md-3">
              <form role="search" class="navbar-form navbar-right mt-1">
                <div class="position-relative has-icon-right">
                  <input type="text" placeholder="Buscar" id="buscar" class="form-control round" oninput="buscarC()">
                  <div class="form-control-position"><i class="ft-search"></i></div>
                </div>
              </form>
            </div>
          </div>
          <table class="table table-striped table-hover" id="data-tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Folio</th>
                <th>Niño</th>
                <th>Titular</th>
                <th>Tiempo</th>
                <th>Cobrado</th>
                <th>Fiesta</th>
                <th>Devolucion</th>
                <th>Motivo Devolucion</th>
                <th>      </th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($datos->result() as $value) { ?>
            <tr>
                <td><?php echo $value->compranId; ?></td>
                <td><?php echo $value->compraId; ?></td>
                <td><?php echo $value->nino; ?></td>
                <td><?php echo $value->titular; ?></td>
                <td><?php echo $value->tiempo; ?></td>
                <td><?php echo number_format($value->pagado,0,'.',','); ?></td>
                <td><?php echo $value->fiesta; ?></td>
                <td><?php echo $value->devolucion_monto; ?></td>
                <td><?php echo $value->devolucion_motivo; ?></td>
                <td><div class="btn-group mr-1 mb-1">
                      <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i>
                      </button>
                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                          <a class="dropdown-item" href="#" onclick="Saldo(<?php echo $value->titularId; ?>);">Agregar saldo</a>
                          <a class="dropdown-item" href="#" onclick="salida(<?php echo $value->compranId; ?>);">Salida manual</a>
                          <a class="dropdown-item" href="#" onclick="devolucion(<?php echo $value->compranId; ?>,<?php echo $value->pagado; ?>);">Devolucion</a>
                      </div>
                    </div>
                </td>

            </tr>
            <?php } ?>
              
            </tbody>
          </table>
          <table class="table table-striped table-hover" id="data-tables2" style="display: none;width: 100%">
            <thead>
              <tr>
                <th>#</th>
                <th>Folio</th>
                <th>Niño</th>
                <th>Titular</th>
                <th>Tiempo</th>
                <th>Cobrado</th>
                <th>Fiesta</th>
                <th>      </th>
              </tr>
            </thead>
            <tbody id="tbodyresultados">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="col-md-7">
  </div>
  <div class="col-md-5">
    <?php echo $this->pagination->create_links() ?>
  </div>
</div>
<div class="modal fade" id="Saldo" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Asignar saldo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
         <input type="hidden" id="IdTitular" name="IdTitular">
         <div class="col-xl-12 col-lg-12 col-md-12">
          <fieldset class="form-group">
           <label for="Cantidad">Cantidad</label>
            <input type="text" class="form-control" id="Cantidad" name="Cantidad">
           </fieldset>                                
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="Send()" data-dismiss="modal">Aceptar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalconfirmacionsalida" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmacion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
         <input type="number" id="Idcompranino" name="Idcompranino" style="display: none;">
         <div class="col-xl-12 col-lg-12 col-md-12">
          <fieldset class="form-group">
           <label>¿Desea realisar salida?</label>
           </fieldset>                                
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary oksalidanino" data-dismiss="modal">Aceptar</button>
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalconfirmaciondevolucion" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Devolución</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="place">
         <input type="number" id="Idcompraninod" name="Idcompraninod" style="display: none;">
         <div class="col-xl-12 col-lg-12 col-md-12">
          <fieldset class="form-group">
           <label>¿Desea realisar una devolución?</label>
           </fieldset>                                
         </div>
         <div class="col-md-12">
           <label class="col-md-3">Monto cobrado</label>
           <div class="col-md-9">
             <input type="number" id="dmontocobrado" class="form-control" readonly>
           </div>
         </div>
         <div class="col-md-12">
           <input type="number" id="montodevolucion" name="montodevolucion" placeholder="Monto" class="form-control">
           <textarea class="form-control" id="motivodevolucion" name="motivodevolucion" placeholder="Motivo"></textarea>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary okdevolucionnino">Aceptar</button>
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade text-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                      <h3 class="modal-title" id="myModalLabel34">Autorizacion</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      </div>
                      <div class="modal-body">
                        <label>Password: </label>
                        <div class="form-group position-relative has-icon-left">
                          <input type="password" placeholder="Password" id="passautorizacion" class="form-control" autocomplete="off">
                          
                        </div>
                      </div>
                      <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cancelar">
                        <input type="submit" class="btn btn-outline-primary btn-lg modalsalidam" data-dismiss="modal" value="Aceptar">
                      </div>
                      
                    </div>
                    </div>
</div>
<div class="modal fade text-left" id="iconFormd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                      <h3 class="modal-title" id="myModalLabel34">Autorizacion</h3>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      </div>
                      <div class="modal-body">
                        <label>Password: </label>
                        <div class="form-group position-relative has-icon-left">
                          <input type="password" placeholder="Password" id="passautorizaciond" class="form-control" autocomplete="off">
                          
                        </div>
                      </div>
                      <div class="modal-footer">
                        <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cancelar">
                        <input type="submit" class="btn btn-outline-primary btn-lg modaldevolucion" data-dismiss="modal" value="Aceptar">
                      </div>
                      
                    </div>
                    </div>
</div>
<script type="text/javascript">
  var base_url =$('#base_url').val();
  var idsalida=0;
  var iddevolucion=0;
  var mcobrado=0;
  function Saldo(id){
    $("#IdTitular").val(id);
    $("#Saldo").modal();
  }

  function Send(){
    var ID=$("#IdTitular").val();
    var Cantidad=$("#Cantidad").val();
    
    $.ajax({
      type: 'POST',
      url: base_url+'ConsultAbierto/SetSaldo',
      data: {
        Id: ID,
        cantidad: Cantidad,
      },
      async: false,
      statusCode:{
          404: function(data){
              toastr.error('Error!', 'No Se encuentra el archivo');
          },
          500: function(){
              toastr.error('Error', '500');
          }
      },
      success:function(data){
          console.log(data);
          toastr.success('Listo');
      },

    });
  }
  function buscarC(){
      var search=$('#buscar').val();
      if (search.length>1) {
        $.ajax({
          type:'POST',
          url: base_url+'ConsultAbierto/Buscar',
          data: {
            buscar: $('#buscar').val(),
          },
          async: false,
          statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
              toastr.error('Error', '500');
              console.log(data.responseText);
            }
          },
          success:function(data){
            $('#tbodyresultados').html(data);
          },
          error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
          }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
      }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
      }
  }
  function salida(id){
    idsalida=id;
    $('#iconForm').modal();
  }
  function devolucion(id,cobrado){
    iddevolucion=0;
    mcobrado=0;
    iddevolucion=id;
    mcobrado=cobrado;
    $('#iconFormd').modal();
  }
 $(document).ready(function($) {
    $('.modalsalidam').click(function(event) {
      
      $.ajax({
          type:'POST',
          url: base_url+'Login/vadministrador',
          data: {
            acceso: $('#passautorizacion').val(),
          },
          async: false,
          statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
              toastr.error('Error', '500');
              console.log(data.responseText);
            }
          },
          success:function(data){
            if (data==1) {
              $('#Idcompranino').val(idsalida);
              $('#modalconfirmacionsalida').modal();
            }else{
              toastr.error('Acceso no permitido','Error');
            }
            
          },
          error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
          }
        });
      $('#passautorizacion').val('');
    }); 
    $('.modaldevolucion').click(function(event) {
      
      $.ajax({
          type:'POST',
          url: base_url+'Login/vadministrador',
          data: {
            acceso: $('#passautorizaciond').val(),
          },
          async: false,
          statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
              toastr.error('Error', '500');
              console.log(data.responseText);
            }
          },
          success:function(data){
            if (data==1) {
              $('#Idcompraninod').val(iddevolucion);
              $('#dmontocobrado').val(mcobrado);
              $('#modalconfirmaciondevolucion').modal();
            }else{
              toastr.error('Acceso no permitido','Error');
            }
            
          },
          error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
          }
        });
      $('#passautorizacion').val('');
    });
    $('.oksalidanino').click(function(event) {
      var ninoid=$('#Idcompranino').val();
      if (ninoid>0) {
          $.ajax({
            type:'POST',
            url: base_url+'Salida/salidamanual',
            data: {
              id: ninoid,
            },
            async: false,
            statusCode:{
              404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
              500: function(data){ 
                toastr.error('Error', '500');
                console.log(data.responseText);
              }
            },
            success:function(data){
              toastr.success('Listo');
              window.location='';
              
            },
            error: function(jqXHR, estado, error){
              console.log(estado);
              console.log(jqXHR);
              console.log(error);
            }
          });

      }else{
        toastr.error('Realizar nuevamente el proceso','Error');
      }
      /* Act on the event */
    });
    $('.okdevolucionnino').click(function(event) {
      var ninoid=$('#Idcompraninod').val();
      if (ninoid>0) {
        var monto=$('#montodevolucion').val()==''?0:$('#montodevolucion').val();
        if (monto>0) {
          if (monto<=mcobrado) {
            var motivo=$('#motivodevolucion').val();
            if (motivo.length>0) {
              $.ajax({
                  type:'POST',
                  url: base_url+'ConfigGeneral/devolucion',
                  data: {
                    id: ninoid,
                    montod:monto,
                    motivod:motivo
                  },
                  async: false,
                  statusCode:{
                    404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                    500: function(data){ 
                      toastr.error('Error', '500');
                      console.log(data.responseText);
                    }
                  },
                  success:function(data){
                    console.log(data);
                    toastr.success('Listo');
                    window.location='';
                    $('#modalconfirmaciondevolucion').modal('hidden');
                    
                  },
                  error: function(jqXHR, estado, error){
                    console.log(estado);
                    console.log(jqXHR);
                    console.log(error);
                  }
              });
            }else{
              toastr.error('Agregar un motivo de devolucion','Error');
            }
          }else{
            toastr.error('no se permite cantidad mayor a la cobrada','Error');
          }


        }else{
          toastr.error('Agregar un monto de devolucion','Error');
        }
          

      }else{
        toastr.error('Realizar nuevamente el proceso','Error');
      }
      /* Act on the event */
    });
 });

</script>
