<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/switchpa.css">
<div class="container">
	<section class="basic-elements">
		<div class="center">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title mb-0">Configuraciones generales</h4>
				</div>
				<div class="card-body">
					<div class="px-3">
						<form class="form" role="form" id="formconf" name="formCliente" method="post" >
							<div class="form-body">
								<div class="row">
									<div class="container">
										<?php $cont=0;
										foreach ($Check->result() as $key) {?>
										<div class="col-xl-12 col-lg-12 col-md-12">
											<div class="col-xl-2 col-lg-2 col-md-2">
												<fieldset class="form-group">
													<input type="hidden" class="form-control" value="<?php echo $key->configId; ?>" id="<?php echo "id".$cont; ?>" name="<?php echo "id".$cont; ?>">
												</fieldset>
											</div>
											<div class="col-xl-6 col-lg-6 col-md-6">
												<br>
												<h2><?php echo $key->descripcion; ?></h2>
											</div>
											<div class="col-xl-4 col-lg-4 col-md-4">
												<fieldset class="form-group">
													<label for="<?php echo "valor".$cont; ?>">Valor</label>
													<input type="text" class="form-control" value="<?php echo $key->valor; ?>" id="<?php echo "valor".$cont; ?>" name="<?php echo "valor".$cont; ?>">
												</fieldset>
											</div>
										</div>
										<?php
										$cont=$cont+1;
										} ?>
									</div>
								</div>
							</div>
							
						</form>

						<div class="form-body form">
							<div class="row">
								<div class="container">
									
									<div class="col-xl-12 col-lg-12 col-md-12">
										<div class="col-xl-2 col-lg-2 col-md-2">
											<fieldset class="form-group">
											</fieldset>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-6">
											<br>
											<h2>Costo Hora</h2>
										</div>
										<div class="col-xl-4 col-lg-4 col-md-4">
											<fieldset class="form-group">
												<label for="precio">Valor</label>
												<input type="number" class="form-control" value="<?php echo $precio; ?>" id="precio" name="precio">
											</fieldset>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="row">
                            <?php if ($idpersonal==1 || $idpersonal==20|| $idpersonal==38 ) { ?>                            
                                <div class="col-md-12" style="height: 100px;">
                                    <input id="panico" type="checkbox" onclick="panico()" <?php echo $panicorest;?> >
                                    <label for="panico" class="labelpanico">
                                        <div class="s">
                                            <!--<div class="d kd1"></div>
                                            <div class="d kd2"></div>
                                            <div class="d kd3"></div>
                                            <div class="d kd4"></div>
                                            <div class="d kd5"></div>
                                            <div class="d kd6"></div>
                                            <div class="d kd7"></div>
                                            <div class="d kd8"></div>
                                            <div class="d kd9"></div>-->
                                        </div>
                                    </label>
                                </div>
                            <?php } ?>
                        </div>
						<div class="row">
							<div class="col-md-12" align="center">
								<a class="btn btn-primary" href="#" id="saveconf" ><i class="fa fa-save"></i> Guardar</a>
								<a class="btn btn-danger" href="#">Cancelar <i class="fa fa-trash-o"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#saveconf').click(function(){
			var Info=$('#formconf').serialize();
			console.log(Info);
			$.ajax({
				type: 'POST',
				url: 'ConfigGeneral/Update',
				data: Info,
				async: false,
				statusCode: {
		          404: function(data) {
		          	  toastr.error('Error', '404');
		          },
		          500: function() {
		           	  toastr.error('Error', '500');
		          }
		        },
		        success: function(info){
				    toastr.success("Editado")
				},
				error: function(jqXHR, estado, error){
		            console.log("Estado3: "+estado);
		            console.log(jqXHR);
		            console.log("Error3: "+error);
		        },	
			});
			//=================================
			$.ajax({
		        type:'POST',
		        url: 'ConfigGeneral/Updateprecio',
		        data: {
		          precio: $('#precio').val(),
		        },
		        async: false,
		        statusCode:{
		          404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
		          500: function(){ toastr.error('Error', '500');}
		        },
		        success:function(data){
		          
		        },
		        error: function(jqXHR, estado, error){
		          console.log(estado);
		          console.log(jqXHR);
		          console.log(error);
		        }
		      });
		});
	});

</script>