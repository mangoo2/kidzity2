<input type="hidden" class="form-control" value="0" id="productoid" name="productoid">
<div class="">
  <section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0"> Nuevo producto</h4>
        </div>
        <div class="card-body">
          <div class="px-3">
            <form class="form" role="form" id="formProducto" name="formProducto" method="post" >
              <div class="form-body">
                <div class="row">
                  <div class="">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <div class="col-lg-6 col-xl-6 col-md-6">
                        <div class="col-xl-4 col-lg-4 col-md-4">
                          <fieldset class="form-group">
                            <label for="Codigo">Código</label>
                            <input type="text" class="form-control" value="" id="Codigo" name="Codigo">
                          </fieldset>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xl-2">
                          <fieldset class="form-group">
                            <br>
                            <a class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow generacodigo">Generar</a>
                          </fieldset>
                        </div>
                        <div class="col-md-12">
                          <fieldset class="form-group">
                            <label for="Nombre">Nombre</label>
                            <input type="text" class="form-control" value="" id="Nombre" name="Nombre">
                          </fieldset>
                        </div>
                        <div class="col-md-12">
                          <fieldset class="form-group">
                            <label for="Descripcion">Descripción</label>
                            <textarea name="Descripcion" class="form-control" value="" id="Descripcion" rows="4"></textarea>
                          </fieldset>
                        </div>
                        <div class="col-md-12">
                          <fieldset class="form-group">
                            <label for="Categoria">Categoría</label>
                            <select id="Categoria" class="custom-select d-block w-100" name="Categoria">
                              <?php foreach ($categoriasll->result() as $item){ ?>
                                <option value="<?php echo $item->productopId; ?>"><?php echo $item->categoria; ?></option>
                              <?php } ?>
                            </select>
                          </fieldset>
                        </div>
                        <div class="col-md-12">
                          <fieldset class="form-group">
                            <label for="Stock">Stock</label>
                            <input type="number" class="form-control" value="" placeholder="Unidades" id="Stock" name="Stock">
                          </fieldset>
                        </div>
                        <div class="col-md-12">
                          <fieldset class="form-group">
                            <label for="Stock">Stock 2</label>
                            <input type="number" class="form-control" value="" placeholder="Unidades" id="Stock2" name="Stock2">
                          </fieldset>
                        </div>
                        <div class="col-md-12"><h5>Precios</h5></div>
                        <div class="col-xl-7 col-lg-7 col-md-7">
                          <fieldset class="form-group">
                            <label for="PrecioC">Precio de compra</label>
                            <input type="number" class="form-control" value="" placeholder="$" id="PrecioC" name="PrecioC">
                          </fieldset>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-7">
                          <fieldset class="form-group">
                            <label for="PrecioV">Precio de venta</label>
                            <input type="number" class="form-control" value="" placeholder="$" id="PrecioV" name="PrecioV">
                          </fieldset>
                        </div>
                      </div>
                      <div class="col-md-6 col-xl-6 col-lg-6">
                        <div class="col-md-12">
                          <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 100%">
                            <div class="fileinput-preview thumbnail previewcat" data-trigger="fileinput" style="width: 100%; height: 150px; border: solid #a6a9ae 1px; border-radius: 12px;"></div>
                            <div>
                                <span class="btn btn-primary btn-file">
                                    <span class="fileinput-new">Foto</span>
                                    <span class="fileinput-exists">Cambiar</span>
                                    <input type="file" name="imgpro" id="imgpro" data-allowed-file-extensions='["jpg", "png"]'>
                                </span>
                                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Quitar</a>
                            </div>
                            </div>
                          </div>
                        <div class="col-md-12 col-lg-12 col-xl-12">
                          <div class="col-md-6 col-lg-6 col-xl-6"></div>
                          <div class="col-md-6 col-lg-6 col-xl-6">
                            
                            <div class="form-check col-xl-12 col-lg-12 col-md-12 mb-1">
                              <input class="form-check-input" type="checkbox" name="TR" id="TR" value="">
                              <label class="form-check-label" for="TR">Tienda de regalos</label>
                            </div>
                            <div class="form-check col-xl-12 col-lg-12 col-md-12 mb-1">
                              <input class="form-check-input" type="checkbox" name="Cafeteria" id="Cafeteria" value="">
                              <label class="form-check-label" for="Cafeteria">Cafetería</label>
                            </div>
                            <div class="form-check col-xl-12 col-lg-12 col-md-12 mb-1" style="display: none;">
                              <input class="form-check-input" type="checkbox" name="Bodega" id="Bodega" value="">
                              <label class="form-check-label" for="Bodega">Bodega</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" align="center">
                  <a class="btn btn-primary"  id="saveproducto" ><i class="fa fa-save"></i> Guardar</a>
                  <a class="btn btn-danger" href="<?php echo base_url(); ?>Productos" >Cancelar <i class="fa fa-trash-o"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>