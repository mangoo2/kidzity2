<style type="text/css">
  .scantidad,.sproductotext,.sprecio,.stotal{
    background: transparent;
    border: 0px;
  }
  .sproductotext{
    width: 100%;
  }
  .scantidad{
    width: 85px;
  }
  .sprecio{
    width: 85px;
  }
  .stotal{
    width: 100px;
  }
  .divtable{
    max-height: 3420px;
    overflow: auto;
  }

</style>
<section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0"> Nueva compra</h4>
        </div>
        <div class="card-body">
          <div class="px-3">
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-2">
                        <fieldset class="form-group">
                          <label for="Cantidad">Cantidad</label>
                          <input type="number" class="form-control" value="1" id="Cantidad" name="Cantidad" min="1" >
                        </fieldset>
                      </div>
                      <div class="col-md-7 classproducto">
                        <fieldset class="form-group">
                          <label for="Producto">Producto</label>
                          <select id="Producto" class="form-control"></select>
                        </fieldset>
                      </div>
                      <div class="col-md-3">
                        <fieldset class="form-group">
                          <label for="PrecioC">Precio compra</label>
                          <input type="text" class="form-control" placeholder="$" value="" id="PrecioC" name="PrecioC">
                        </fieldset>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-10"></div>
                      <div class="col-md-2">
                        <button class="btn mr-1 mb-1 btn-primary btn-lg agregarproducto"> <i class="fa fa-plus"> Agregar </i></button>
                      </div>
                    </div>
                    
                    
                    
                    
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <br>
                    </div>
                    <div class="col-md-12 divtable">
                      <table class="table thead-ligh table-striped" id="table-compras">
                        <thead>
                          <tr>
                            <th>Cantidad</th>
                            <th>Producto</th>
                            <th>Precio</th>
                            <th>Total</th>
                            <th>-</th>
                          </tr>
                        </thead>
                        <tbody class="tbodyproductos" >
                          
                        </tbody>
                        
                        
                      </table>
                    </div>
                    
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <br>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9"></div>
                    <div class="col-xl-1 col-lg-1 col-md-1">
                      <h5>Total</h5>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2">
                      <h5 id="Total">0.00</h5>
                      <input type="hidden" id="totalcompra" readonly>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" align="center">
                  <a class="btn btn-primary" href="#" id="savecompra" ><i class="fa fa-save"></i>Guardar</a>
                  <a class="btn btn-danger" href="<?php echo base_url();?>Compras">Cancelar<i class="fa fa-trash-o"></i></a>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
</section>
