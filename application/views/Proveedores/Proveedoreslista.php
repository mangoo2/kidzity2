<div class="row">
  <div class="col-md-12">
    <h2>Proveedores</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-9"></div>
    <div class="col-md-1">
      <a href="<?php echo base_url(); ?>Proveedores/Proveedoradd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i> Nuevo</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de Proveedores</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <div class="col-md-12">
            <div class="col-md-8">
            </div>
            <div class="col-md-3">
              <form role="search" class="navbar-form navbar-right mt-1">
                <div class="position-relative has-icon-right">
                  <input type="text" placeholder="Buscar" id="buscar" class="form-control round" oninput="buscarP()">
                  <div class="form-control-position"><i class="ft-search"></i></div>
                </div>
              </form>
            </div>
          </div>
          <table class="table table-striped table-hover" id="data-tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Razón Social</th>
                <th>Contacto</th>
                <th>Teléfono</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($Proveedores->result() as $item){ ?>
              <tr>
                <td><?php echo $item->IdProveedor; ?></td>
                <td><?php echo $item->RazonSocial; ?></td>
                <td><?php echo $item->Nombre; ?></td>
                <td><?php echo $item->Movil; ?></td>
                <td>
                  <div class="row">
                    <div class="col-md-12" align="center">
                      <a class="btn btn-primary" href="<?php echo base_url();?>Proveedores/Proveedoradd?Prov=<?php echo $item->IdProveedor; ?>"><i class="ft-edit-3"></i></a>
                      <a class="btn btn-danger" onclick="DeleteProveedor( <?php echo $item->IdProveedor; ?>)"><i class="fa fa-trash-o"></i></a>
                    </div>
                  </div>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <table class="table table-striped table-hover" id="data-tables2" style="display: none;width: 100%">
            <thead>
              <tr>
                <th>#</th>
                <th>Razón Social</th>
                <th>Contacto</th>
                <th>Teléfono</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="tbodyresultados">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="col-md-7">
  </div>
  <div class="col-md-5">
    <?php echo $this->pagination->create_links() ?>
  </div>
</div>
<div class="modal fade text-left" id="modalconfirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirmacion</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <input type="hidden" id="deleteproid">
                  <p>¿Confirma que desea eliminar <span id="deletepronom"></span>?</p>
                </div>
              </div>
              
              
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="eliminarpro">Aceptar</button>
            </div>
        </div>
    </div>
</div>
