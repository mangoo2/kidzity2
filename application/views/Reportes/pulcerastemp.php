<?php
require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
class MYPDF extends TCPDF {
	public function Header() {

	}
	public function Footer() {
		$html1='<table border="0">
					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>

		';
		$this->writeHTML($html1, true, false, true, false, '');
	}

}
$pdf = new MYPDF('L', PDF_UNIT, array(116, 58), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Recibo');
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set margins
$pdf->SetMargins('7','7','7');
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin('1');


// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 12);

// add a page
//$pdf->AddPage('P', 'A4');


$date=date("d/m/y");
$style = array('position' => 'C');
$styleQR = array('border' => 0, 
				 'vpadding' => '0', 
				 'hpadding' => '0', 
				 'fgcolor' => array(0, 0, 0), 
				 'bgcolor' => false, 
				 'module_width' => 1, 
				 'module_height' => 1);

//                   code,type,      x, y
//$pdf->write2DBarcode('1', 'QRCODE,L', 0, 0, 30, 20, $style, 'N');
//$params = $pdf->serializeTCPDFtagParameters(array('1', 'C128B', '', '', '', 15, 0.4, $style, 'C')); 

$html='';
$DATApulcera = json_decode($pulcera);
for ($i=0;$i<count($DATApulcera);$i++) { 
	$pdf->AddPage();
	$params = $pdf->serializeTCPDFtagParameters(array($DATApulcera[$i]->pulcera, 'QRCODE,L', '', '', 80, 80, $styleQR, 'N'));
	$html ='<table border="0">
				<tr>
					<td width="40%" align="center" rowspan="2" ><tcpdf method="write2DBarcode" params="' . $params . '" /></td>
					<td width="60%" align="center" ></td>
				</tr>
				<tr>
					<td align="center" >'.$DATApulcera[$i]->pulcera.'</td>
				</tr>
			</table>
			';
	$pdf->writeHTML($html, true, false, true, false, '');
}











$pdf->IncludeJS('print(true);');
//
// ---------------------------------------------------------
//Close and output PDF document
$pdf->Output('plan.pdf', 'I');
//============================================================+
// END OF FILE
//============================================================+
