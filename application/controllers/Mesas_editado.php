<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesas extends CI_Controller 
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloVentas');
        $this->load->model('Usuarios/ModeloUsuarios');
        $this->load->helper('cookie');
    }
    
    public function index()
    {
        var_dump($_SESSION);
        $data['productospadres']=$this->ModeloVentas->getproductopadre();
        $data['nummesas']=15;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/mesas',$data);
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesa');
        /*
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        
        unset($_SESSION['mesa']);
        $_SESSION['mesa']=array();
        */
        //unset($_COOKIE['pro']);
        //$_COOKIE["pro"]='';
        
        //unset($_COOKIE['mesa']);
        //$_COOKIE["mesa"]='';
        
        //unset($_COOKIE['can']);
        //$_COOKIE["can"]=''; 
        
    }

    // VIEWS 
    public function listadoProductosEliminados() 
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/listadoProductosEliminados');
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesalistado');
    }

    public function getListadoProductosEliminados()
    {
        $productos= $this->ModeloVentas->getListadoProductosEliminados();
        $json_data = array("data" => $productos);
        echo json_encode($json_data);
    }

    public function cargarproductos()
    {
        $id = $this->input->post('id');
        
        $respuesta=$this->ModeloVentas->getproductoshijos($id);
        foreach ($respuesta->result() as $item){ ?>
        <div class="col-md-3 galeriasimg flipInX animated classproductohijo cursor" data-idcate="<?php echo $item->productoid; ?>" style="background: url('<?php echo base_url(); ?>public/img/productos/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;" onclick="preproductoselected(<?php echo $item->productoid; ?>)" >
            <h2><?php echo $item->producto;?></h2>
        </div>
      <?php }
    }

    // Agregar productos a la tabla de cada mesa
    function agregarproductos()
    {

        $mesas = array();
        
        $cookie = array(
           'name'   => 'mesas',
           'value'  => json_encode($mesas),
           'expire' => '15000000',
           'prefix' => ''
        );
        $this->input->set_cookie($cookie);
        $data=json_decode($this->input->cookie('mesas'));
        print_r($data);  die;

        // Obtenemos el ID del producto y la Mesa en la cual trabajaremos
        $id = $this->input->post('id');
        $selectedmesa = $this->input->post('selectedmesa');
        $valuePIN = $this->input->post('pin');
        
        $banderaUsuario = 0;
        // Inicializamos la cantidad en 1
        $cant=1;

        // Obtenemos los datos del producto que se quiere vender
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','productoid',$id);
        foreach ($resultado->result() as $item) 
        {
            $oProducto = array('productoid' =>$item->productoid,
                'codigo' =>$item->codigo,
                'producto' =>$item->producto,
                'descripcion' =>$item->descripcion,
                'precio_venta' =>$item->precio_venta,
                'img' =>$item->img
            ); 
        }

        // Primero buscamos si el arreglo de la mesa que actualizaremos ya existe en el arreglo
        // Si existe, entramos a guardar el producto
        //var_dump($_COOKIE['mesa'][$selectedmesa]); echo "<br><br>";

        if($_COOKIE['mesa'][$selectedmesa])
        {
            echo "existe<br><br>";
            echo $valuePIN;
            var_dump($_COOKIE['mesa'][$selectedmesa]);
            if($_COOKIE['mesa'][$selectedmesa]['usuarioPin']==$valuePIN)
            {
                // Si el producto se encuentra ya registrado en el arreglo, solo aumentamos la cantidad
                if(in_array($oProducto, $_COOKIE['mesa'][$selectedmesa]['pro']))
                {   
                    $idx = array_search($oProducto, $_COOKIE['mesa'][$selectedmesa]['pro']);
                    $_COOKIE['mesa'][$selectedmesa]['can'][$idx]+=$cant;
                }
                // Si no, se agrega al arreglo
                else
                {
                    array_push($_COOKIE['mesa'][$selectedmesa]['pro'],$oProducto);
                    array_push($_COOKIE['mesa'][$selectedmesa]['can'],$cant);
                } 
            }
            else
            {
                $banderaUsuario = 1;
            }
        }
        // Si no, creamos el arreglo
        else
        { 
            echo "no existe<br><br>";

            $cookie=array( 
                "name" => "mesa",
                "value" => 'asa',
                'expire' => '432000',
                'prefix' => ''
            );
            
            $this->input->set_cookie($cookie);

            //setcookie("mesa[$selectedmesa]['pro']", 0, time() + 365 * 24 * 60 * 60); 
            //setcookie("mesa[$selectedmesa]['can']", 0, time() + 365 * 24 * 60 * 60);
            //setcookie("mesa[$selectedmesa]['usuarioPin']", 0, time() + 365 * 24 * 60 * 60);

            // Creamos el arreglo para producto y cantidad
            //$_COOKIE['mesa'][$selectedmesa]['pro'] = array();
            //$_COOKIE['mesa'][$selectedmesa]['can'] = array();
            //$_COOKIE['mesa'][$selectedmesa]['usuarioPin'] = $valuePIN;
            
            // Insertamos el producto y la cantidad
            //array_push($_COOKIE['mesa'][$selectedmesa]['pro'],$oProducto);
            //array_push($_COOKIE['mesa'][$selectedmesa]['can'],$cant);
            var_dump($this->input->cookie()); 
            var_dump($this->input->cookie("mesa")); 
            
        } 

        if($banderaUsuario==0)
        {
            //==========================================
            $count = 0;
            $llave = 0;
            //$n =array_sum($_SESSION['can']);
            $n =array_sum($_COOKIE['mesa'][$selectedmesa]['can']);

            //foreach ($_SESSION['pro'] as $fila)
            //var_dump($_SESSION['mesa'][$selectedmesa]['pro']); 
            $arreglo = array_keys($_COOKIE['mesa'][$selectedmesa]['pro']);

            foreach ($_COOKIE['mesa'][$selectedmesa]['pro'] as $fila)
            {
                //$llave = key($_SESSION['mesa'][$selectedmesa]['pro']);
                $llave = $arreglo[$count];
                
                $Cantidad=$_COOKIE['mesa'][$selectedmesa]['can'][$llave]; 
                $precio=$fila['precio_venta'];
                $cantotal=$Cantidad*$precio;
                //die();
                
                ?>
                    <tr class="producto_<?php echo $llave;?>">
                        <td>
                            <input type="number" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                            <input type="hidden" id="idproductos" value="<?php echo $fila['productoid'];?>">
                        </td>
                        <td style="background: url('<?php echo base_url(); ?>public/img/productost/<?php echo $fila['img'];?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;"></td>
                        <td><?php echo $fila['producto'];?></td>
                        <td>$ <input type="text" name="vsprecio" id="vsprecio" value="<?php echo $precio;?>" readonly style="background: transparent;border: 0px;width: 100px;"></td>
                        <td>$ <input type="text" class="vstotal_<?php echo $selectedmesa; ?>" name="vstotal_<?php echo $selectedmesa; ?>" id="vstotal_<?php echo $selectedmesa; ?>" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>
                        <td>
                            <a class="danger acciones" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $fila['productoid'];?>,<?php echo $selectedmesa;?>,<?php echo $llave;?>)">
                                <i class="ft-trash font-large-1"></i>
                            </a>
                            <a>&nbsp;&nbsp;&nbsp;</a>
                            <a class="success acciones" data-original-title="Eliminar" title="Agregar 1" onclick="addproindividual(<?php echo $fila['productoid'];?>,<?php echo $selectedmesa;?>,<?php echo $llave;?>)">
                                <i class="ft-plus font-large-1"></i>
                            </a>
                            <a>&nbsp;&nbsp;&nbsp;</a>
                            <a class="warning acciones" data-original-title="Eliminar" title="Eliminar 1" onclick="deleteproindividual(<?php echo $fila['productoid'];?>,<?php echo $selectedmesa;?>,<?php echo $llave;?>)">
                                <i class="ft-minus font-large-1"></i>
                            </a>
                        </td>
                    </tr>
                <?php
                $count++;
            }
        }
        else
        {
            echo "usuarioinvalido";
        }
    }

    function deleteproducto()
    {
        // Obtiene los datos a eliminar
        $idd = $this->input->post('idd');
        $selectedmesa = $this->input->post('mesa');
        $row = $this->input->post('row');

        // Guarda el registro del producto eliminado
        $registroEliminado = array(
            'idUsuario' => $_COOKIE['idpersonal'],
            'idProducto' => $idd,
            'mesa' => $selectedmesa,
            'fechaRegistro' => date('Y-m-d')
        );
        $this->ModeloVentas->insertaRegistroElimiando($registroEliminado);
        
        // Elimina el producto del arreglo
        unset($_COOKIE['mesa'][$selectedmesa]['pro'][$row]);
        
        // Elimina la cantidad de producto del arreglo
        unset($_COOKIE['mesa'][$selectedmesa]['can'][$row]);
        
    }

    function deleteproductoindividual()
    {
        // Obtiene los datos a eliminar
        $idd = $this->input->post('idd');
        $selectedmesa = $this->input->post('mesa');
        $row = $this->input->post('row');

        // Guarda el registro del producto eliminado
        $registroEliminado = array(
            'idUsuario' => $_COOKIE['idpersonal'],
            'idProducto' => $idd,
            'mesa' => $selectedmesa,
            'fechaRegistro' => date('Y-m-d')
        );
        $this->ModeloVentas->insertaRegistroElimiando($registroEliminado);
        
        if($_COOKIE['mesa'][$selectedmesa]['can'][$row]>1)
        {
            $_COOKIE['mesa'][$selectedmesa]['can'][$row] = ($_COOKIE['mesa'][$selectedmesa]['can'][$row] - 1);
            echo $_COOKIE['mesa'][$selectedmesa]['can'][$row];
        }
        else if($_COOKIE['mesa'][$selectedmesa]['can'][$row]=1)
        {
            // Elimina el producto del arreglo
            unset($_COOKIE['mesa'][$selectedmesa]['pro'][$row]);
            
            // Elimina la cantidad de producto del arreglo
            unset($_COOKIE['mesa'][$selectedmesa]['can'][$row]);
            echo 0;
        }     
    }

    function addproductoindividual()
    {
        // Obtiene los datos a eliminar
        $idd = $this->input->post('idd');
        $selectedmesa = $this->input->post('mesa');
        $row = $this->input->post('row');
        
        // Aumenta en 1 la cantidad del producto seleccionado
        $_COOKIE['mesa'][$selectedmesa]['can'][$row] = ($_COOKIE['mesa'][$selectedmesa]['can'][$row] + 1);

        echo $_COOKIE['mesa'][$selectedmesa]['can'][$row]; 
    }

    function deleteMesa()
    {
        $selectedmesa = $this->input->post('mesa');
        unset($_COOKIE['mesa'][$selectedmesa]);
    }

    function productoclear()
    {
        unset($_COOKIE['pro']);
        $_COOKIE['pro']=array();
        unset($_COOKIE['can']);
        $_COOKIE['can']=array();
    }
    
    function viewproducto()
    {
        $codigo =$this->input->post('codigo');
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','codigo',$codigo);
        $idc=0;
        foreach ($resultado->result() as $item) {
            $idc=$item->productoid;
        }
        echo $idc;
    }
    function verificarsaldo(){
        $codigo =$this->input->post('codigo');
        echo $this->ModeloCatalogos->verificarsaldo($codigo);
    }
    
    // Agregar una nueva venta
    function addventas()
    {
        // Obtenemos los datos del POST
        $banderaUsuario = 0;
        $selectedmesa = $this->input->post('selectedmesa');
        $pinaux = $this->input->post('pinaux');
        $descuento = $this->input->post('descuento');

        // Verfificamos que los datos del PIN correspondan
        if($pinaux==$_COOKIE['mesa'][$selectedmesa]["usuarioPin"])
        {
            // Buscaremos los datos del usuario al que corresponda el PIN
            $datosUsuario = $this->ModeloUsuarios->getUsuarioPorPin($pinaux);

            // Si el PIN ingresado no corresponde con algún usuario registrado, regresaremos error y no se realizará la venta
            if (empty($datosUsuario)) 
            {
                echo 'pininvalido';
            }
            // De lo contrario, se procede
            else
            {
                $idpersonal = $datosUsuario[0]->personalId;

                $monto_total=$this->input->post('monto_total');
                $monto_cobrado=$monto_total-$descuento;
                $pulcera=$this->input->post('pulcera');
                $metodo=$this->input->post('metodo');
                $productos=$this->input->post('productos');
                $selectedmesa = $this->input->post('selectedmesa');
                $idventas=$this->ModeloVentas->addventasMesas($idpersonal,$monto_total,$pulcera,$metodo,$selectedmesa,$descuento,$monto_cobrado);
                $DATA = json_decode($productos);
                for ($i = 0; $i < count($DATA); $i++) 
                {
                    $idproductos=$DATA[$i]->idproductos;
                    $cantidad=$DATA[$i]->cantidad;
                    $precio=$DATA[$i]->precio;
                    $this->ModeloVentas->addventasdetalleMesas($idventas,$idproductos,$cantidad,$precio);
                    $this->ModeloVentas->descuentostock($idproductos,$cantidad);

                }
                echo $idventas;
            }            
        }
        else
        {
            echo "usuarioinvalido";
        }
    }

    function addventassimulada()
    {
        $banderaUsuario = 0;
        $selectedmesa = $this->input->post('selectedmesa');
        $pinaux = $this->input->post('pinaux');

        if($pinaux==$_COOKIE['mesa'][$selectedmesa]["usuarioPin"])
        {
            $monto_total=$this->input->post('monto_total');
            $pulcera=$this->input->post('pulcera');
            $metodo=$this->input->post('metodo');
            $productos=$this->input->post('productos');
            $selectedmesa = $this->input->post('selectedmesa');
            $idventas=$this->ModeloVentas->addventasMesasSimulada($monto_total,$pulcera,$metodo,$selectedmesa);
            $DATA = json_decode($productos);
            for ($i = 0; $i < count($DATA); $i++) 
            {
                $idproductos=$DATA[$i]->idproductos;
                $cantidad=$DATA[$i]->cantidad;
                $precio=$DATA[$i]->precio;
                $this->ModeloVentas->addventasdetalleMesasSimulada($idventas,$idproductos,$cantidad,$precio);
            }
            echo $idventas;
        }
        else
        {
            echo "usuarioinvalido";
        }
    }
 
    function getUsuarioPorPin($pinaux)
    {
        $datosUsuario = $this->ModeloUsuarios->getUsuarioPorPin($pinaux);
        if (empty($datosUsuario)) 
        {
            echo 'pininvalido';
        }
        else echo 'ok';
    }

    function comparaPinRealizarVenta()
    {
        $data = $this->input->post();
        $pin = $data['pin'];
        $selectedmesa = $data['mesa'];

        // Primero buscamos si el arreglo de la mesa donde buscaremos el PIN existe
        if(isset($_COOKIE['mesa'][$selectedmesa]))
        {
            if($pin==$_COOKIE['mesa'][$selectedmesa]["usuarioPin"])
            {
                echo 'ok';
            }
            else
            {
                echo "usuarioinvalido";
            }
        }
        else
        {
            echo 'ok';
        }
    }
}