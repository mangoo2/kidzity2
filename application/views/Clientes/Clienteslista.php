<style type="text/css">
  #data-tables td{
    font-size: 13px;
    padding: 0.5rem;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Clientes</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-9"></div>
    <div class="col-md-1">
      <a href="<?php echo base_url(); ?>Clientes/Clienteadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de clientes</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <table class="table table-striped table-hover" id="data-tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Menores</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
          
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade text-left" id="modalconfirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 ">
                  <h3>¿Desea eliminar al Cliente?</h3>
                  <input type="hidden" id="clienteiddelete">
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="aceptareliminar">Aceptar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  function buscarC(){
    var search=$('#buscar').val();
    var UR=$('#base_url').val();
    if (search.length>0) {
      $.ajax({
        type:'POST',
        url: UR+'Clientes/BuscarCliente',
        data: {
          buscar: $('#buscar').val(),
        },
        async: false,
        statusCode:{
          404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
          500: function(){ toastr.error('Error', '500');}
        },
        success:function(data){
          $('#tbodyresultados').html(data);
        },
        error: function(jqXHR, estado, error){
          console.log(estado);
          console.log(jqXHR);
          console.log(error);
        }
      });
      $("#data-tables").css("display", "none");
      $("#data-tables2").css("display", "");
    }else{
      $("#data-tables2").css("display", "none");
      $("#data-tables").css("display", "");
    }
}
</script>