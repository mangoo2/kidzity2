<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listadoventasmesas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloCatalogos');
        if (isset($_SESSION['logueo_kids'])) {
            $this->perfilid=$_SESSION['perfilid'];
            $this->idpersonal=$_SESSION['idpersonal']; 
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,29);// 14 es el id del menu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }else{
            redirect('/Sistema');
        }
    }

	public function index(){
        $data['categorias']=$this->ModeloCatalogos->getselectwheren('producto_padre',array('activo'=>1));
        if ($this->perfilid==1) {
            $data['actualizacionticket']=1;
        }else{
            $data['actualizacionticket']=0;
        }
        $data['perfilid']=$this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/listadog',$data);
        $this->load->view('templates/footer');
	}
    
    // Obtener el listado de las Tarifas
    public function getListadoVentasMesas(){
        $ventas= $this->ModeloVentas->getListadoVentasMesas();
        $json_data = array("data" => $ventas);
        echo json_encode($json_data);
    }
    function productos(){
        $data=$this->input->post();
        $idventa=$data['venta'];

       $detallepro=$this->ModeloCatalogos->getventadetalleMesas($idventa); 
       $html='<input type="hidden" id="ventaidadd" value="'.$idventa.'">
       <table class="table table-striped productoscanceltable" >
                    <thead>
                      <tr>
                        <th>Cantidad</th>
                        <th>Producto</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>';
       foreach ($detallepro->result() as $item) {
            $productoname="'".$item->producto."'";
            $html .='<tr>
                        <td>'.$item->cantidad.'</td>
                        <td >'.$item->producto.'</td>
                        <td>
                            <div class="btn-group mr-1 mb-1">
                                    <button type="button" class="btn btn-raised gradient-blackberry white dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-bottom: 0;">
                                    <i class="fa fa-cog"></i>
                                    </button>
                                    <div class="dropdown-menu arrow" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -12px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <button class="dropdown-item" type="button" onclick="canceladototal('.$item->vdetalleId.','.$productoname.')">
                                            <i class="fa fa-times"></i> Cancelado Total</button>';
                                            if($item->cantidad>1){
                                               $html .='
                                        <button class="dropdown-item" type="button" onclick="canceladoparcial('.$item->vdetalleId.','.$productoname.','.$item->cantidad.')">
                                        <i class="fa fa-search-minus"></i>
                                        Cancelado Parcial</button>'; 
                                            }
                                            
                                        $html .='
                                    </div>
                                </div>
                        </td>
                    </tr>';

        }
        $html.='
            </body>
        </html>';
        echo $html;
    }
    function procancelt(){
        $data=$this->input->post();
        $produto=$data['id'];
        $motivo=$data['moti'];
        $dataup=array(
            'cancelado'=>1,
            'personal_cancela'=>$this->idpersonal,
            'motivo_cancela'=>$motivo
        );
        $this->ModeloCatalogos->updateCatalogo($dataup,'vdetalleId',$produto,'ventasm_detalle');
        $resultado=$this->ModeloCatalogos->getselectwheren('ventasm_detalle',array('vdetalleId'=>$produto));
        foreach ($resultado->result() as $item) {
            $productoId=$item->productoId;
            $cantidad=$item->cantidad;
            $ventaId=$item->ventaId;
        }
        $this->ModeloCatalogos->updatestockpro($cantidad ,$productoId);
        $this->updateventasm($ventaId);
        echo $ventaId;
    }
    function updateventasm($ventaId){
        $datosw=array('ventaId'=>$ventaId,'cancelado!='=>1);
        $resultadod=$this->ModeloCatalogos->getselectwheren('ventasm_detalle',$datosw);
        $total=0;
        foreach ($resultadod->result() as $item) {
            $precio=$item->precio;
            $cantidad=$item->cantidad;
            $total=$total+($precio*$cantidad);
        }
        $this->ModeloCatalogos->updateCatalogon(array('monto_total'=>$total,'monto_cobrado'=>$total-'descuento'),array('ventaId'=>$ventaId),'ventasm');
    }
    function procanceltp(){
        $data=$this->input->post();
        $produto=$data['id'];
        $motivo=$data['moti'];
        $cant=$data['cant'];
        //=================================================
            $resultado=$this->ModeloCatalogos->getselectwheren('ventasm_detalle',array('vdetalleId'=>$produto));
            $cantidad=0;
            foreach ($resultado->result() as $item) {
                $productoId=$item->productoId;
                $cantidad=$item->cantidad;
                $ventaId=$item->ventaId;
            }
        //=================================================
            $cantidadnew=$cantidad-$cant;
        $dataup=array(
            'cancelado'=>2,
            'personal_cancela'=>$this->idpersonal,
            'cantidad'=>$cantidadnew,
            'motivo_cancela'=>$motivo
        );
        $this->ModeloCatalogos->updateCatalogo($dataup,'vdetalleId',$produto,'ventasm_detalle');
        
        
        $this->ModeloCatalogos->updatestockpro($cant ,$productoId);
        $this->updateventasm($ventaId);
        echo $ventaId;
    }
    function procat(){
        $data=$this->input->post();
        $productopId=$data['id'];
        $productos=$this->ModeloCatalogos->getselectwheren('producto_hijo',array('productopId'=>$productopId,'activo'=>1));
        $html='<option value="0" disabled selected>Seleccionar</option>';
        foreach ($productos->result() as $item) {
            $html.='<option value="'.$item->productoid.'" data-costo="'.$item->precio_venta.'">'.$item->producto.'</option>';
        }
        echo $html;
    }
    function editarventa(){
        $data=$this->input->post();
        $ventaId=$data['ventaId'];
        $productos=$data['productos'];
        $DATA = json_decode($productos);
        for ($i = 0; $i < count($DATA); $i++) {
            $idproductos=$DATA[$i]->producto;
            $cantidad=$DATA[$i]->cantidad;
            $precio=$DATA[$i]->precio;
            $this->ModeloVentas->updateproducto($idproductos,$cantidad);
            //$this->ModeloVentas->addventasdetalle($ventaId,$idproductos,$cantidad,$precio,0);
            $datavd=array(
                        'ventaId'=>$ventaId,
                        'productoId'=>$idproductos,
                        'cantidad'=>$cantidad,
                        'precio'=>$precio
                        );
            $this->ModeloCatalogos->insertToCatalogo($datavd,'ventasm_detalle');
        }
        //=========================================
        $this->updateventasm($ventaId);
        /*
        $datosw=array('ventaId'=>$ventaId);
        $resultadod=$this->ModeloCatalogos->getselectwheren('ventasm_detalle',$datosw);
        $total=0;
        foreach ($resultadod->result() as $item) {
            $precio=$item->precio;
            $cantidad=$item->cantidad;
            $total=$total+($precio*$cantidad);
        }
        $this->ModeloCatalogos->updateCatalogon(array('monto_total'=>$total,'monto_cobrado'=>$total-'descuento'),array('ventaId'=>$ventaId),'ventasm');*/
        echo $ventaId;
    }
}

?>