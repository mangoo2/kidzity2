<style type="text/css">
  .inputtable{
    background: transparent !important;
    border: 0px !important;
  }
</style>

<div class="vd_title-section clearfix">
    <div class="vd_panel-header">
        <h1>Descarga</h1>
        <small class="subtitle">Agregar Descarga</small>
        <small class="subtitle">Ultimo camion registrador: <?php echo $ultimocamionregistrado;?></small>
    </div>
    <!--
    <div class="col-sm-offset-11">
       	<a class="btn vd_btn vd_bg-blue vd_white" href="Personal/Personaladd">
       		<span class="menu-icon"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>-->
    
</div>
<div class="vd_content-section clearfix">

	<div class="row">
        <div class="col-md-12">
            <div class="panel widget">
                <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i></span> Descarga </h3>
                </div>
                <div class="panel-body">
                	<!-------------->
                	<div class="row form-horizontal">
                    <form method="post" role="form" id="formembarqueg">
                      <div class="form-group">
                          <label class="col-sm-1 control-label">Proveedor</label>
                          <div class="col-sm-4 controls">
                              <div class="input-group"> 
                                  <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue"><i class="fa fa-user"></i></span>
                                  <input type="text" name="proveedor" id="proveedor" class="form-control">
                              </div>
                          </div>
                          <label class="col-sm-1 control-label">Folio</label>
                          <div class="col-sm-2 controls">
                              <div class="input-group"> 
                                  <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue"><i class="fa fa-barcode"></i></span>
                                  <input type="number" name="folio" id="folio" class="form-control">
                              </div>
                          </div>
                          <label class="col-sm-1 control-label">Fecha</label>
                          <div class="col-sm-2 controls">
                              <div class="input-group"> 
                                  <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue"><i class="fa fa-calendar"></i></span>
                                  <input type="date" name="fecha" id="fecha" class="form-control" value="<?php echo date('Y-m-d')?>" placeholder="YYYY-MM-DD" style="padding: 0px 10px">
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-1 control-label">Camión</label>
                          <div class="col-sm-4 controls">
                              <div class="input-group"> 
                                  <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue"><i class="fa fa-truck"></i></span>
                                  <input type="text" name="camion" id="camion" class="form-control">
                              </div>
                          </div>
                          
                      </div>
                      <div class="form-group">
                          <label class="col-sm-1 control-label">Referencia</label>
                          <div class="col-sm-4 controls">
                            <textarea name="referencia" id="referencia" class="form-control"></textarea>
                              
                          </div>
                          <label class="col-sm-1 control-label">Fecha Salida</label>
                          <div class="col-sm-2 controls">
                              <div class="input-group"> 
                                  <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue"><i class="fa fa-calendar"></i></span>
                                  <input type="date" name="fechassalida" id="fechassalida" class="form-control" value="<?php echo date('Y-m-d')?>" placeholder="YYYY-MM-DD" style="padding: 0px 10px">
                              </div>
                          </div>
                          
                      </div>
                    </form>
                  </div>
                	<!-------------->
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel widget">
                <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i></span> Datos generales </h3>
                </div>
                <div class="panel-body">
                  <!-------------->
                  <form method="post" role="form" id="formproducto">
                  <div class="row">
                    <div class="col-md-2">
                      <label class="control-label">Cantidad</label>
                      <input type="number" name="cantidad" id="cantidad" class="form-control" min="0">
                    </div>
                    <div class="col-md-2">
                      <label class="control-label">Producto</label>
                      <select name="producto" id="producto" class="form-control">
                        <?php foreach ($producto->result() as $item){ ?>
                          <option value="<?php echo $item->id_producto;?>"><?php echo $item->nombre;?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label class="control-label">Categoría</label>
                      <select name="categoria" id="categoria" class="form-control">
                        <?php foreach ($categoria->result() as $item){ ?>
                          <option value="<?php echo $item->categoriaId;?>"><?php echo $item->nombre;?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label class="control-label">Marcas</label>
                      <div class="input-group"> 
                        <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue" id="viewmodalmarcas">+</span>
                        <select name="marca" id="marca" class="form-control" onchange="generaclave()">
                          <?php foreach ($getmarcas->result() as $item){ ?>
                            <option value="<?php echo $item->marcaId;?>"><?php echo $item->marca;?></option>
                          <?php } ?>
                        </select>
                      </div>
                      
                    </div>
                    <div class="col-md-3">
                      <label class="control-label">Clave</label>
                      <input type="text" name="clave" id="clave" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <label class="control-label">Bodega</label>
                      <select name="bodega" id="bodega" class="form-control">
                        <?php foreach ($bodegas->result() as $item){ ?>
                          <option value="<?php echo $item->bodegaid;?>"><?php echo $item->nombre;?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <label class="control-label">Cámara</label>
                      <select name="camara" id="camara" class="form-control">
                        
                      </select>
                    </div>
                    
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <label class="control-label">Costo fruta</label>
                      <input type="number" name="pfruta" id="pfruta" class="form-control" oninput="calculartotal()">
                    </div>
                    <div class="col-md-2">
                      <label class="control-label">Costo flete</label>
                      <input type="number" name="pflete" id="pflete" class="form-control" oninput="calculartotal()">
                    </div>
                    <div class="col-md-2">
                      <label class="control-label">Costo movimiento</label>
                      <input type="number" name="pmov" id="pmov" class="form-control" oninput="calculartotal()">
                    </div>
                    <div class="col-md-2">
                      <label class="control-label">Costo caja</label>
                      <input type="number" name="pcaja" id="pcaja" class="form-control" oninput="calculartotal()">
                    </div>
                    <div class="col-md-2">
                      <label class="control-label"> Tipo de caja</label>
                      <select id="tipocaja" class="form-control">
                        <option>Nueva</option>
                        <option>Usada</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label class="control-label">Costo por caja/ kilo</label>
                      <input type="number" name="costo" id="costo" class="form-control" readonly>
                    </div>
                    
                    <div class="col-md-3">
                      <label class="control-label" style="color: transparent;"> _____________________________________________________</label>
                      <button class="btn vd_btn vd_bg-grey" type="button" id="verdetallescamaras">Ver detalle de cámaras</button>
                    </div>
                  </div>
                  </form>
                  <hr>
                  <div class="row">
                    <div class="col-sm-12 ">
                      <button class="btn vd_btn vd_bg-grey center-block" type="button" id="agregarem"><i class="glyphicon glyphicon-plus"></i></button>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 grupopro table-responsive">
                      <!------------------------------------------------------>
                      <table class="table table-striped table-hover no-head-border " id="tableproductos">
                        <thead class="vd_bg-grey vd_white">
                            <tr>
                                <th>Producto</th>
                                <th>Clave</th>
                                <th>Bodega</th>
                                <th>Camara</th>
                                <th>Tipo de caja</th>
                                <th>Cantidad</th>
                                <th class="ocultarv">Fruta</th>
                                <th class="ocultarv">Flete</th>
                                <th class="ocultarv">Movimiento</th>
                                <th class="ocultarv">Costo caja</th>
                                <th>Costo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="addproductos">
                            
                        </tbody>
                        <tfoot>
                          <tr>
                                <td></td>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="ocultarv"></th>
                                <th class="ocultarv">Cantidad Total: <span class="classcanttotal"></span>  </th>
                                <th class="ocultarv"></th>
                                <td align="right">Total</td>
                                <td>$<input id="totalembarque" name="totalembarque" class="inputtable" value="0" readonly style="width: 50px;"></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                      </table>
                      <!------------------------------------------------------>
                    </div>
                  </div>                  
                  <!-------------->
                </div>
            </div>
        </div>
        <div class="col-sm-12 ">
          <button class="btn vd_btn vd_bg-grey center-block" type="button" id="saveembarque"><i class="fa fa-save"></i> Guardar</button>
        </div>
                  
    </div>
</div>
<div class="modal fade" id="modalmarcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header vd_bg-grey vd_white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" >Marcas</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 todaslasmarcas" style="overflow: auto; max-height: 200px;">
            
          </div>
          <div class="col-md-12">
            <input type="text" id="addmarcas" class="form-control">         
          </div>
        </div>
      </div>
      <div class="modal-footer background-login">
        <button type="button" class="btn vd_btn vd_bg-red" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn vd_btn vd_bg-green" id="addmarcassi" data-dismiss="modal">Agregar</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<?php if (isset($_GET['emb'])) {?> 
    <script type="text/javascript">
      $(document).ready(function($) {
        $("input").prop('disabled', true);
        $("select").prop('disabled', true);
        $("textarea").prop('disabled', true);
        $('#agregarem').remove();
        $('#saveembarque').remove();
        embarquedetalle(<?php echo $_GET['emb'];?>);
      });
        function embarquedetalle(ids){
          $.ajax({
                type: 'POST',
                url: 'Embarque/datosEmbarque',
                data: {
                    id: ids
                },
                async: false,
                statusCode: {
                    404: function(data) {
                        notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", 'No se encuentra el archivo');
                    },
                    500: function() {
                        notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", "Ocurrió un evento inesperado, por favor contacte al administrador del sistema");
                    }
                },
                success: function(data) {
                  //console.log(data);
                   var array = $.parseJSON(data);
                    $('#proveedor').val(array.proveedor);
                    $('#folio').val(array.folio);
                    $('#fecha').val(array.fecha);
                    $("#camion" ).val(array.camion);
                    $('#referencia').val(array.referencia);
                    $('#fechassalida').val(array.fechasalida);
                    var tabla =array.tabla;
                    tabla.forEach(function(item, index) {
                      var cantidad='<input type="text" min="0"  id="scantidad" name="scantidad" class="inputtable" value="'+item.cantidad+'" readonly>';
                      var costoinputfr='<input type="text" min="0"  id="scofr" name="scofr" class="inputtable" value="'+item.fruta+'" readonly>';
                      var costoinputfl='<input type="text" min="0"  id="scofl" name="scofl" class="inputtable" value="'+item.flete+'" readonly>';
                      var costoinputmv='<input type="text" min="0"  id="scom" name="scom" class="inputtable" value="'+item.mov+'" readonly>';
                      var costoinput='<input type="text" min="0"  id="scosto" name="scosto" class="inputtable" value="'+item.costo+'" readonly>';
                        $('#addproductos').append('<tr><td>'+item.producto+'</td><td>'+item.clave+'</td><td>'+item.bodega+'</td><td>'+item.camara+'</td><td>'+item.tipocaja+'</td><td>'+cantidad+'</td><td class="ocultarv">'+costoinputfr+'</td><td class="ocultarv">'+costoinputfl+'</td><td class="ocultarv">'+costoinputmv+'</td><td class="ocultarv"></td><td>'+costoinput+'</td><th></th> </tr>');
                    });
                    carculartotal();
                    $(".ocultarv").css("display", "none");
                }
              });

        }
    </script>
<?php }?>