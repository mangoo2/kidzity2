<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticketmesas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones/ModeloConfiguracionmesas');
        $this->iva=0.16;
        
    }
     
    function configuracion(){
        $data['configticket']=$this->ModeloConfiguracionmesas->configticket();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Configuraciones/Ticketmesas',$data);
        $this->load->view('templates/footer');
    }

    function compra(){
    	$comp=$this->input->get('comp');
    	$resultadoc=$this->ModeloCatalogos->getselectwhere('compra_tiempo','compraId',$comp);
    	foreach ($resultadoc->result() as $item) {
    		$titularId=$item->titularId;
    		$metodo=$item->metodo;
    		$tiempo=$item->tiempo;
    		$pagado=$item->pagado;
    		$reg=$item->reg;
    	}
    	//$this->load->library('encriptacion');
    	$data['idqr']=$this->ModeloCatalogos->encrypturl($comp);
    	//$data['idqr2']=$this->ModeloCatalogos->decrypturl($this->ModeloCatalogos->encrypturl($comp));
    	$data['compid']=$comp;
    	$data['titularId']=$titularId;
		$data['metodo']=$metodo;
		$data['tiempo']=$tiempo;
		$data['pagado']=$pagado;
		$data['reg']=$reg;
        $data['configticket']=$this->ModeloConfiguracionmesas->configticket();
        //var_dump($data['configticket']->result()); die();
        $this->load->view('Reportes/tcompra',$data);
    }


    function dulceria(){
        $comp=$this->input->get('comp');
        //$id=$this->ModeloCatalogos->decrypturl($comp);
        $resultadoc=$this->ModeloCatalogos->getselectwhere('ventasm','ventaId',$comp);

        //print_r($resultadoc->result()); die;  
        $metodotext='';
        foreach ($resultadoc->result() as $item) {
            $reg=$item->reg;
            $monto_total=$item->monto_total;
            $metodo=$item->metodo;
            $descuento=$item->descuento;
            $monto_cobrado=$item->monto_cobrado;
            $mesa=$item->mesa;
            if($item->tipo==0){
                $tipo = 'Ticket de Impresión para Cliente';
            }
            else{
                $tipo = 'Venta Realizada';
            }
            if ($metodo==1) {
                $metodotext='Efectivo';
            }elseif ($metodo==2) {
                $metodotext='Tarjeta';
            }
        }
        $data['mesa']=$mesa;
        $data['idventa']=$comp;
        $data['reg']=$reg;
        $data['monto_total']=$monto_total;
        $data['descuento']=$descuento;
        $data['monto_cobrado']=$monto_cobrado;
        $data['metodo']=$metodo;
        $data['metodotext']=$metodotext;
        $data['tipo']=$tipo;
        $data['iva']=$this->iva;
        
        $data['configticket']=$this->ModeloConfiguracionmesas->configticket();

        $this->load->view('Reportes/tmesas',$data);
    }

    function dulceriaTicket()
    {
        $comp=$this->input->get('comp');
        
        $resultadoc=$this->ModeloCatalogos->getselectwhere('ventasm_simulada','ventaId',$comp);
        
        foreach ($resultadoc->result() as $item) 
        {
            $reg=$item->reg;
            $monto_total=$item->monto_total;
            $metodo=$item->metodo;
            
        }

        $data['idventa']=$comp;
        $data['reg']=$reg;
        $data['monto_total']=$monto_total;
        $data['metodo']=$metodo;
        
        $data['configticket']=$this->ModeloConfiguracionmesas->configticket();
        
        $this->load->view('Reportes/tmesas_simulado',$data);
    }

    function updateticket(){
    $titulo = $this->input->post('titulo');
    $mensaje1 = $this->input->post('mensaje1');
    $mensaje2 = $this->input->post('mensaje2');
    $fuente = $this->input->post('fuente');
    $tamano = $this->input->post('tamano');
    $margsup = $this->input->post('margsup');
    $marglat = $this->input->post('marglat');
    $this->ModeloConfiguracionmesas->configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup,$marglat);
    }

}
?>