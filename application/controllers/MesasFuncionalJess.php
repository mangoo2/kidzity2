<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mesas extends CI_Controller 
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->helper('url');
        session_regenerate_id();
        $this->load->model('ModeloMesas','model');
        $this->load->model('Usuarios/ModeloUsuarios');
    }
    
    public function index()
    {
        $this->load->model('ModeloVentas');
        $data['productospadres']=$this->ModeloVentas->getproductopadre();
        $data['nummesas']=20;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/mesas',$data);
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesa');

    }

    // VIEWS 
    public function listadoProductosEliminados() 
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mesas/listadoProductosEliminados');
        $this->load->view('templates/footer');
        $this->load->view('mesas/jsmesalistado');
    }

    public function getListadoProductosEliminados()
    {
        $this->load->model('ModeloVentas');
        $productos= $this->ModeloVentas->getListadoProductosEliminados();
        $json_data = array("data" => $productos);
        echo json_encode($json_data);
    }

    // carga productos en diplay izquierdo ========================================================

    public function cargarproductos() 
    {
        $this->load->model('ModeloVentas');
        $id = $this->input->post('id');
        
        $respuesta=$this->ModeloVentas->getproductoshijos($id);
        foreach ($respuesta->result() as $item){ ?>
        <div class="col-md-3 galeriasimg flipInX animated classproductohijo cursor" data-idcate="<?php echo $item->productoid; ?>" style="background: url('<?php echo base_url(); ?>public/img/productos/<?php echo $item->img;?>');     background-repeat: no-repeat;background-size: 100%;background-position: center;" onclick="agregar_producto(<?php echo $item->productoid; ?>)" >
            <h5><?php echo $item->producto;?></h5>
        </div>
      <?php }
    }


    // ============= FUNCIONES DE AGREGAR MESA Y VALIDAR PIN ==========================================================

    public function getUsuarioPorPin() //regresar si es correcto, en caso de que si regresa la tabla de productos de la mesa
    {
        //print_r($_SESSION); die;
        $id_mesa=$this->input->post("mesa");
        $pin=$this->input->post("pin");
        $data["pin_valido"]=false; $result=""; $total=0;
        $datosUsuario = $this->ModeloUsuarios->getUsuarioPorPin($pin);

        if (!empty($datosUsuario)) 
        {
            $data["pin_valido"]=true;
            $_SESSION['mesero']=$datosUsuario->personalId;
            $productos=$_SESSION['mesas_detalle'];
            $result=""; $coincidencia_mesa=0;
            foreach($productos as &$prod){

                if($prod["mesa"]==$id_mesa){ //si de los productos que hay en el array es de la mesa actual lo toma en cuenta
                    $result.=$this->rowString($prod["id"],$prod["cantidad"], $prod["producto"], $prod["precio"]);
                    $total+=$prod["cantidad"]* $prod["precio"];
                    $coincidencia_mesa=1;
                }
            }
            if($coincidencia_mesa==1){ //si existe la mesa jala el descuento (si no tiene descuento se restara 0)
                $datosMesa=$this->getDatosMesa($id_mesa);
                $data["tipo"]=$datosMesa["tipo"];
                $data["descuento"]=$datosMesa["descuento"];
                $descuento=$data["descuento"];
                $total-=$descuento;
            }

        }

        $data["tabla"]=$result;
        $data["total"]=$total;

        echo json_encode($data); 
    }

    // ============= FUNCIONES DE AGREGAR, ELIMINAR Y MODIFICAR PRODUCTOS ==========================================================


    public function agregar_producto(){

        $id_mesa=$this->input->post("mesa");
        $id_producto=$this->input->post("idProducto");


        $mesas=$_SESSION['mesas_detalle'];
        $result=""; 
        
        $producto=$this->model->getProducto($id_producto);
        
        $coincidencia=0; $coincidencia_mesa=0;  $total=0;
        
        foreach($mesas as &$prod){

            if($prod["mesa"]==$id_mesa){ //si de los productos que hay en el array es de la mesa actual lo toma en cuenta
                if($prod["id"]==$id_producto){
                    $prod["cantidad"]++;
                    $coincidencia=1;
                }
                $coincidencia_mesa=1;
                $result.=$this->rowString($prod["id"],$prod["cantidad"], $prod["producto"], $prod["precio"]);
                $total+=$prod["cantidad"]* $prod["precio"];
            }
        }
        
        if($coincidencia==0){
            array_push($mesas,
                array("id"=>$producto->productoid,"producto"=>$producto->producto,"cantidad"=>1,"precio"=>$producto->precio_venta, "mesa"=>$id_mesa));
            $result.=$this->rowString($id_producto,1, $producto->producto, $producto->precio_venta);
            $total+=$producto->precio_venta;
        }

        if($coincidencia_mesa==0){ //si no existe la mesa la agrega al arreglo de mesas
            array_push($_SESSION['mesas'],array("no_mesa"=>$id_mesa,"id_mesero"=>$_SESSION["mesero"],"descuento"=>0,"tipo"=>0));
        }

        
        $_SESSION['mesas_detalle']=$mesas;

        $data["tabla"]=$result;
        $datosMesa=$this->getDatosMesa($id_mesa);
        $data["tipo"]=$datosMesa["tipo"];
        $data["descuento"]=$datosMesa["descuento"];
        $data["total"]=$total-$data["descuento"];

        //print_r($_SESSION);

        echo json_encode($data);  
        
    }

    //-----------------------------------------------------------------------------------------------------------------------------------


    public function eliminar_producto(){

        $id_mesa=$this->input->post("mesa");
        $id_producto=$this->input->post("idProducto");

        $mesas=$_SESSION['mesas_detalle']; 

        $result=""; 
        $total=0;  $row=0;

        foreach ($mesas as $index=>$mesa) {
            if($mesa["mesa"]==$id_mesa && $mesa["id"]==$id_producto){
                $row=$index; 
            }
        }

        unset($mesas[$row]);
        
        $i=0;
        foreach($mesas as &$prod){
            if($prod["mesa"]==$id_mesa){ //si de los productos que hay en el array es de la mesa actual lo toma en cuenta
                $result.=$this->rowString($prod["id"],$prod["cantidad"], $prod["producto"], $prod["precio"]);
                $total+=$prod["cantidad"]* $prod["precio"];
                $i++;
            }
        }

        $_SESSION['mesas_detalle']=$mesas;

        $data["tabla"]=$result;
        $datosMesa=$this->getDatosMesa($id_mesa);
        $data["tipo"]=$datosMesa["tipo"];
        $data["descuento"]=$datosMesa["descuento"];
        $data["total"]=$total-$data["descuento"];

        if($i<1){ //si se eliminan todos los productos eliminamos el array de la session en mesas
            $this->delete_session_mesa($id_mesa);
        }
        

        echo json_encode($data);  
    }


    //-----------------------------------------------------------------------------------------------------------------------------------

    public function modificar_producto(){

        $id_mesa=$this->input->post("mesa");
        $id_producto=$this->input->post("idProducto");
        $accion=$this->input->post("accion");

        $mesas=$_SESSION['mesas_detalle'];
        $result=""; 

        $total=0;
        
        foreach($mesas as &$prod){

            if($prod["mesa"]==$id_mesa){ //si de los productos que hay en el array es de la mesa actual lo toma en cuenta
                if($prod["id"]==$id_producto){
                    if($accion){ // si es agregar una unidad mas
                        $prod["cantidad"]++;
                    }
                    else{ // si es eliminar una unidad
                        if($prod["cantidad"]>1){ //si solo hay un item no resta más
                            $prod["cantidad"]--;
                        }
                    }
                }
                $result.=$this->rowString($prod["id"],$prod["cantidad"], $prod["producto"], $prod["precio"]);
                $total+=$prod["cantidad"]* $prod["precio"];
            }
        }     
        
        $_SESSION['mesas_detalle']=$mesas;

        $data["tabla"]=$result;
        $datosMesa=$this->getDatosMesa($id_mesa);
        $data["tipo"]=$datosMesa["tipo"];
        $data["descuento"]=$datosMesa["descuento"];
        $data["total"]=$total-$data["descuento"];

        echo json_encode($data);  
    }

    // FUNCIONES DE DESCUENTO Y VENTA ===================================================================================================================

    public function agregarDescuento(){
        $id_mesa=$this->input->post("mesa");
        $descuento=$this->input->post("descuento");
        //guarda descuento en mesa
        $mesas=$_SESSION['mesas'];

        foreach ($mesas as &$mesa) {
            if($mesa["no_mesa"]==$id_mesa){
                $mesa["descuento"]=$descuento;
            }
        }
        $_SESSION['mesas']=$mesas;

        //sacamos el total y le aplicamos el descuento
        $productos=$_SESSION['mesas_detalle'];
        $total=0; 
        foreach($productos as &$prod){

            if($prod["mesa"]==$id_mesa){ //si de los productos que hay en el array es de la mesa actual lo toma en cuenta
                $total+=$prod["cantidad"]* $prod["precio"];
            }
        }
        echo $total-=$descuento;

    }


    //-----------------------------------------------------------------------------------------------------------------------------------

    public function realizar_venta(){ //en venta revisar que si es tipo 1 guarde este en primer lugar y luego actualizar no insertar
        $id_mesa=$this->input->post("mesa");
        $metodo_pago=$this->input->post("metodo");
        $tipo_venta=$this->input->post("tipo");

        $mesa=$this->getDatosMesa($id_mesa); //print_r($mesa); die;
        $total=$this->getTotalMesa($id_mesa);

        //guardamos datos en la base de datos
        $venta=array(
            "personalId"=>$mesa["id_mesero"],
            "metodo"=>$metodo_pago,
            "monto_total"=>$total,
            "activo"=>1,
            "mesa"=>$id_mesa,
            "reg"=>date("Y-m-d h:i:s"),
            "descuento"=>$mesa["descuento"],
            "monto_cobrado"=>$total-$mesa["descuento"],
            "tipo"=>$mesa["tipo"],
            "personalId_abre"=>$_SESSION["mesero"],
        );

        $id_venta=$this->model->saveVenta($venta);
        $productos=$_SESSION['mesas_detalle'];
        foreach($productos as &$prod){
            if($prod["mesa"]==$id_mesa){
                $temp=array(
                "ventaId"=>$id_venta,
                "productoId"=>$prod['id'],
                "cantidad"=>$prod["cantidad"],
                "precio"=>$prod["precio"]
            );
            $this->model->saveProductos($temp);
            }
        }

        //modificamos la session
        if($tipo_venta==2){ //es una venta y se debe de guardar como tal y eliminar de la session
            
            $this->delete_session_mesa($id_mesa);
            $this->delete_session_prod($id_mesa);

            
        }else{  // modifica en en session que la mesa ha imprimido ticket
            $mesas=$_SESSION['mesas'];
            foreach ($mesas as &$m) {
                if($m["no_mesa"]==$id_mesa){
                    $m["tipo"]=1;
                }
            }
            $_SESSION['mesas']=$mesas;
        }

        echo $id_venta;

    }

    // FUNCIONES DE APOYO =======================================================================================================================


    private function getDescuento($id_mesa){
        $mesas=$_SESSION['mesas'];
        $descuento=0;
        foreach ($mesas as $mesa) {
            if($mesa["no_mesa"]==$id_mesa){
                $descuento=$mesa["descuento"];
            }
        }
        return $descuento;
    }

    private function getTotalMesa($id_mesa){
        $mesas=$_SESSION['mesas_detalle'];
        $total=0;
        foreach ($mesas as $mesa) {
            if($mesa["mesa"]==$id_mesa){
                $total+=$mesa["precio"]*$mesa["cantidad"];
            }
        }
        return $total;
    }

    public function getDatosMesa($id_mesa){
        $mesas=$_SESSION['mesas']; //print_r($mesas);
        $result=array();
        foreach ($mesas as $m) {
            if($m["no_mesa"]==$id_mesa){
                $result=$m;
            }
        }
        return $result;
    }

    public function delete_session_mesa($id_mesa){
        $mesas=$_SESSION['mesas']; //print_r($mesas); die;

        $row=0;

        foreach ($mesas as $index=>$mesa) {
            if($mesa["no_mesa"]==$id_mesa){
                $row=$index;
            }
        }

        unset($mesas[$row]);
        $_SESSION['mesas']=$mesas;
    }

    public function delete_session_prod($id_mesa){
        $mesas=$_SESSION['mesas_detalle'];
        $arreglo=array(); 
        foreach ($mesas as $index=>$mesa) {
            if($mesa["mesa"]==$id_mesa){
                array_push($arreglo, $index);
            }
        }
         
        foreach ($arreglo as $a) {
            unset($mesas[$a]);
        } 
        $_SESSION['mesas_detalle']=$mesas;
    }


    public function rowString($id_producto,$cantidad,$producto,$precio){

        return "<tr>"
                . "<td>$cantidad</td>"
                . "<td>$producto</td>"
                . "<td>".number_format($precio,2)."</td>"
                . "<td>".number_format($precio*$cantidad
                    ,2)."</td>"
                . "<td>"
                    ."<button type='button' onclick='eliminar_producto($id_producto)' class='btn btn-sm mb-0 btn-outline-danger'><i class='ft-trash'></i></button> "
                    ."<button type='button' onclick='modificar_producto($id_producto,1)' class='btn btn-sm mb-0 btn-success btn-flat'><i class='ft-plus'></i></button>"
                    ."<button type='button' onclick='modificar_producto($id_producto,0)' class='btn btn-sm mb-0 btn-info btn-flat'><i class='ft-minus'></i></button>"
                ."</td>"
            . "</tr>";
    }


    
}