<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProveedor extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function Insertar($tabla, $valores){
    	$this->db->insert(''.$tabla, $valores);
    	$this->db->close();
    }

    function filasproveedor() {
        $strq = "SELECT COUNT(*) as total FROM proveedores where Activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
        $total =$row->total;
        } 
        return $total;
    }

    function ListProveedor($por_pagina,$segmento){
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq="SELECT * FROM proveedores WHERE  Activo=1 LIMIT $por_pagina $segmento;";
        $resp=$this->db->query($strq);
        return $resp;
    }

    function ProveedorSearch($buscar){
        $strq="SELECT * 
            FROM proveedores 
            WHERE 
            Activo=1 and Nombre LIKE '%$buscar%' or
            Activo=1 and RazonSocial LIKE '%$buscar%' or
            Activo=1 and Movil LIKE '%$buscar%' ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function VerProveedor($id){
    	$Query="SELECT * FROM `proveedores` WHERE IdProveedor='".$id."' ";
    	return $this->db->query($Query);

    }

    function ProveedorUpdate($ID, $data){
    	 $this->db->set($data);
    	 $this->db->where('IdProveedor',$ID);
    	 $this->db->update('proveedores');
    	 $this->db->close();
    }

    function DellProveedor($id){ 
    	$strq="UPDATE `proveedores` SET `Activo`=0 WHERE IdProveedor= $id";
    	 $this->db->query($strq);
    	 $this->db->close();
    }

}
?>