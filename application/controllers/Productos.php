<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloProductos');
    }

    public function index(){
            
    	    $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Productos/Productoslista');
            $this->load->view('templates/footer');
            $this->load->view('Productos/jsProductoslista');
    }
    public function Productosadd(){
            $data['categoriasll']=$this->ModeloCatalogos->getselectwhere('producto_padre','activo',1);
    	    $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Productos/NProducto',$data);
            $this->load->view('templates/footer');
            $this->load->view('Productos/jsNProducto');
    }
    public function Productosadds(){
        $data = $this->input->post();
        if ($data['productoid']==0) {
            unset($data['productoid']);
            echo $this->ModeloCatalogos->insertToCatalogo($data, 'producto_hijo');
        }else{
            $id=$data['productoid'];
            unset($data['productoid']);
            $this->ModeloCatalogos->updateCatalogo($data,'productoid',$id, 'producto_hijo');
            echo $id;
        } 
    }
    function imgpro(){
        $idcat = $this->input->post('productoid');
        $target_path = 'public/img/productos';
        $thumb_path = 'public/img/productost';
        //file name setup
        $filename_err = explode(".",$_FILES['img']['name']);
        $filename_err_count = count($filename_err);
        $file_ext = $filename_err[$filename_err_count-1];
        //if($file_name != ''){
           // $fileName = $file_name.'.'.$file_ext;
        //}else{
            $fileName = $_FILES['img']['name'];
        //}
        $fecha=date('ymd-His');
        //upload image path
        $cargaimagen =$fecha.'cat'.basename($fileName);
        $upload_image = $target_path.'/'.$cargaimagen;
        
        //upload image
        if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
            //thumbnail creation
                $this->ModeloCatalogos->productosaddimg($cargaimagen,$idcat);
                $thumbnail = $thumb_path.'/'.$cargaimagen;
                list($width,$height) = getimagesize($upload_image);
                if ($width>3000) {
                    $thumb_width = $width/10;
                    $thumb_height = $height/10;
                }elseif ($width>2500) {
                    $thumb_width = $width/7.9;
                    $thumb_height = $height/7.9;
                }elseif ($width>2000) {
                    $thumb_width = $width/6.8;
                    $thumb_height = $height/6.8;
                }elseif ($width>1500) {
                    $thumb_width = $width/5.1;
                    $thumb_height = $height/5.1;
                }elseif ($width>1000) {
                    $thumb_width = $width/3.3;
                    $thumb_height = $height/3.3;
                }elseif ($width>900) {
                    $thumb_width = $width/3;
                    $thumb_height = $height/3;
                }elseif ($width>800) {
                    $thumb_width = $width/2.6;
                    $thumb_height = $height/2.6;
                }elseif ($width>700) {
                    $thumb_width = $width/2.3;
                    $thumb_height = $height/2.3;
                }elseif ($width>600) {
                    $thumb_width = $width/2;
                    $thumb_height = $height/2;
                }elseif ($width>500) {
                    $thumb_width = $width/1.9;
                    $thumb_height = $height/1.9;
                }elseif ($width>400) {
                    $thumb_width = $width/1.3;
                    $thumb_height = $height/1.3;
                }else{
                    $thumb_width = $width;
                    $thumb_height = $height;
                }
                $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                switch($file_ext){
                    case 'jpg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;
                    case 'jpeg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;

                    case 'png':
                        $source = imagecreatefrompng($upload_image);
                        break;
                    case 'gif':
                        $source = imagecreatefromgif($upload_image);
                        break;
                    default:
                        $source = imagecreatefromjpeg($upload_image);
                }

                imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                switch($file_ext){
                    case 'jpg' || 'jpeg':
                        imagejpeg($thumb_create,$thumbnail,100);
                        break;
                    case 'png':
                        imagepng($thumb_create,$thumbnail,100);
                        break;

                    case 'gif':
                        imagegif($thumb_create,$thumbnail,100);
                        break;
                    default:
                        imagejpeg($thumb_create,$thumbnail,100);
                }

            

            $return = Array('ok'=>TRUE,'img'=>'');
        }
        else{
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }
        echo json_encode($return);
    }
    function getproductosall(){
        $productos = $this->ModeloCatalogos->getproductosall();
        $json_data = array("data" => $productos);
        echo json_encode($json_data);
    }
    public function getlisproductos() {
        $params = $this->input->post();
        $getdata    = $this->ModeloProductos->getlistproductos($params);
        $totaldata  = $this->ModeloProductos->getlistproductos_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function eliminarproducto(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->eliminarrow('producto_hijo','productoid',$id);
    }
    function getviewproduct(){
        $id = $this->input->post('id');
        $resultado = $this->ModeloCatalogos->getselectwhere('producto_hijo','productoid',$id);
        $array = array('status'=>0);
        foreach ($resultado->result() as $item) {
            $array = array('status'=>1,
                'productoid' =>$item->productoid,
                'codigo' =>$item->codigo,
                'producto' =>$item->producto,
                'descripcion' =>$item->descripcion,
                'productopId' =>$item->productopId,
                'stock' =>$item->stock,
                'stock2' =>$item->stock2,
                'precio_compra' =>$item->precio_compra,
                'precio_venta' =>$item->precio_venta,
                'regalos' =>$item->regalos,
                'cafeteria' =>$item->cafeteria,
                'bodega' =>$item->bodega,
                'img' =>$item->img
            );
        }
        echo json_encode($array);
    }
    function searchproducto(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->searchproducto($search);        
        echo json_encode($results->result());
    }

    

}
?>