<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloUsuarios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function personalnuevo($nom,$ape,$perf){
            $strq = "INSERT INTO personal (perfilId, Nombre, Apellidos) VALUES ('".$perf."','".$nom."','".$ape."')";
            $this->db->query($strq);
    }
    function getusuarios() {
        $strq = "SELECT usu.UsuarioID,usu.Usuario,per.nombre  
                 FROM usuarios as usu
                 inner join perfiles as per on usu.perfilId=per.perfilId 
                 inner JOIN personal as pers on pers.personalId=usu.personalId
                 WHERE pers.estatus=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function getempreado() {
        $strq = "CALL SP_GET_ALL_PERSONAL";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getperfiles() {
        $strq = "CALL SP_GET_ALL_PERFILES";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function mostrardatos($id){
        $strq = "SELECT * FROM usuarios  where UsuarioID=$id";
        $query = $this->db->query($strq);
        return $query;

    }
    public function usuariosinsert($id,$usu,$pass,$perf,$perfs,$pin){
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $strq = "INSERT INTO usuarios (perfilId, personalId,Usuario, contrasena, pin) VALUES ('$perf','$perfs','$usu','$pass','$pin')";
            $this->db->query($strq);
    }
    public function usuariosupdate($id,$usu,$pass,$perf,$perfs,$pin){
            
            if ($pass=='xxxxxx') {
                $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfs',Usuario='$usu',pin='$pin' WHERE UsuarioID='$id'";
            }else{
                $pass = password_hash($pass, PASSWORD_BCRYPT);
                $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfs',Usuario='$usu', contrasena='$pass',pin='$pin' WHERE UsuarioID='$id'";
            }
    
            $this->db->query($strq);
    }
    function verfusu($usu) {
        $strq = "SELECT *  FROM usuarios where Usuario ='$usu'";
        $query = $this->db->query($strq);
        return $query;
    }
    function mostrarusuarios(){
        $strq = "SELECT usu.UsuarioID,usu.Usuario,per.nombre as perfil 
        FROM usuarios as usu
        INNER JOIN perfiles as per on usu.perfilId=per.perfilId
        inner JOIN personal as pers on pers.personalId=usu.personalId
        WHERE pers.estatus=1";
        $query = $this->db->query($strq);
        return $query;  
    }

    function getUsuarioPorPin($pin)
    {
        $strq = "SELECT
                usuarios.*,
                personal.nombre
                FROM
                usuarios
                INNER JOIN personal ON usuarios.personalId = personal.personalId
                WHERE
                usuarios.pin = '".$pin."'";
        $query = $this->db->query($strq);
        return $query->row();
    }

    function getUsuarioPorPersonalId($personalId)
    {
        $strq = "SELECT
                usuarios.*,
                personal.nombre
                FROM
                usuarios
                INNER JOIN personal ON usuarios.personalId = personal.personalId
                WHERE
                usuarios.personalId = '".$personalId."'";
        $query = $this->db->query($strq);
        return $query->row();
    }
    

}
