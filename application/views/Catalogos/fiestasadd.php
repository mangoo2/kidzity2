<div class="row">
  <div class="col-md-12 regresaprincipal">
    <h2>Eventos</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->

<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title"><?php echo $title;?> Evento</h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal">
                <!--------//////////////-------->
                <form class="form" role="form" id="formEventos" name="formEventos" method="post" >
                  <input type="hidden" id="fiestaId" name="fiestaId" value="<?php echo $fiestaId;?>">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" value="<?php echo $nombre;?>" id="nombre" name="nombre">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="titular">Titular</label>
                        <select name="titular" id="titular" class="form-control"></select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="precio_total">Precio total</label>
                        <input type="number" class="form-control" value="<?php echo $precio_total;?>" id="precio_total" name="precio_total">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="precioex">Precio por niño extra</label>
                        <input type="number" class="form-control" value="<?php echo $precioex;?>" id="precioex" name="precioex">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="cantidad">Número de niños</label>
                        <input type="number" class="form-control" value="<?php echo $cantidad;?>" id="cantidad" name="cantidad">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="cantidad">Paquete</label>
                        <select class="form-control" id="paquete" name="paquete" onchange="preciopaquete();">
                          <option value="0" data-valor='0' >Selecciona paquete</option>
                          <?php foreach ($Paquetel->result() as $item){ ?>
                           <option value="<?php echo $item->IdPaquete; ?>"  <?php if($item->IdPaquete==$paquete){ echo 'selected';} ?>  data-valor='<?php echo $item->Precio;?>'   ><?php echo $item->Nombre; ?></option>
                          <?php } ?>
                        </select>
                        
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="fecha_inicio">Fecha de evento</label>
                        <input type="date" class="form-control" value="<?php echo $fecha_inicio;?>" id="fecha_inicio" name="fecha_inicio">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="hora_inicio">Hora Inicio</label>
                        <input type="time" class="form-control" value="<?php echo $hora_inicio;?>" id="hora_inicio" name="hora_inicio">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="hora_fin">Hora fin</label>
                        <input type="time" class="form-control" value="<?php echo $hora_fin;?>" id="hora_fin" name="hora_fin">
                      </div>
                    </div>
                  </div>
                </form>
                <div class="row">
                  <div class="col-md-12" align="center">
                    <a class="btn btn-primary"  id="saveevento" ><i class="fa fa-save"></i> <?php echo $titlebtn;?></a>
                    <a class="btn btn-danger" href="<?php echo base_url(); ?>Eventos" >Cancelar <i class="fa fa-trash-o"></i></a>
                  </div>
                </div>
                <!--------//////////////-------->
              </div>
          </div>
      </div>
    </div>
</div>