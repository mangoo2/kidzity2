<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Usuarios/ModeloUsuarios');
    }
	public function index(){
            $data['usuarios']=$this->ModeloUsuarios->getusuarios();
            $data['personal']=$this->ModeloUsuarios->getempreado();
            $data['perfiles']=$this->ModeloUsuarios->getperfiles();

            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Agendas/Agenda',$data);
            $this->load->view('usuarios/usuarios_view',$data);
            $this->load->view('templates/footer');
            $this->load->view('usuarios/usuariosjs');
	
    }
    public function consultar(){
        $id = $this->input->post('id');
        $datosag=$this->ModeloUsuarios->mostrardatos($id);
        foreach ($datosag->result() as $datos) {
            $idUser = $datos->UsuarioID;
            $usuario = $datos->Usuario;
            $pin = $datos->pin;
            $pass = 'xxxxxx';
            $idPerfil = $datos->perfilId;
            $idPersona = $datos->personalId;
        }
        $array = array("id"=>$idUser,
                           "usuario"=>$usuario,
                           "pass"=>$pass,
                           "pin"=>$pin,
                           "idPerfil"=>$idPerfil,
                           "idPersona"=>$idPersona);
            echo json_encode($array);
    }
    public function guardar(){
        $id = $this->input->post('ide');
        $usu = $this->input->post('usua');
        $pass = $this->input->post('passw');
        $perf = $this->input->post('perfi');
        $perfs = $this->input->post('perso');
        $pin = $this->input->post('pin1');

        if ($id>0) {
            $this->ModeloUsuarios->usuariosupdate($id,$usu,$pass,$perf,$perfs,$pin);
        }else{
            $this->ModeloUsuarios->usuariosinsert($id,$usu,$pass,$perf,$perfs,$pin);
        }
    }

    public function verfusu(){
        $usu = $this->input->post('usua');
        $usuario = $this->ModeloUsuarios->verfusu($usu);
        $verificacion = 1;
        foreach ($usuario->result() as $datos) {
           $verificacion = 0;
        }
        echo $verificacion;
    }
    public function mostrarusuarios(){
        $usuario = $this->ModeloUsuarios->mostrarusuarios();
        foreach ($usuario->result() as $datos) {
            echo '<tr  class="rowlink" style="cursor: pointer" data-id="'.$datos->UsuarioID.'">
                                                        <td>'.$datos->Usuario.'</td>
                                                        <td>'.$datos->perfil.'</td>
                                                        <td></td>
                                             
                                                </tr>';
           
        }   
    }
        
       
       
       
       
}
