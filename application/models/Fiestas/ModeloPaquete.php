<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPaquete extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function GetPaqutes(){
    	$sql="SELECT * FROM PaquetesFiesta WHERE Estado = 1";
    	$resp=$this->db->query($sql);
    	return $resp;
    }

    function Especific($id){ 
    	$sql="SELECT * FROM PaquetesFiesta WHERE IdPaquete = $id";
    	$resp=$this->db->query($sql);
    	$info=json_encode($resp->result());
    	return $info;
    }

    function Editar($data,$column,$ID,$tabla){
    	$this->db->set($data);
    	$this->db->where($column,$ID);
    	$this->db->update($tabla);
    	$this->db->close();
    }

    function Insertar($data,$Tabla){
    	$this->db->insert(''.$Tabla, $data);
    	$id=$this->db->insert_id();
    	return $id;
    }

    function Delete($id){
        $this->db->set('Estado',0);
        $this->db->where('IdPaquete',$id);
        $this->db->update('PaquetesFiesta');
        $this->db->close();
    }

    
}
?>