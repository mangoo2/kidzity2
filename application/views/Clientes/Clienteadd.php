<div class="container">
  <section class="basic-elements">
    <div class="center">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title mb-0"> Nuevo cliente</h4>
        </div>
        <div class="card-body">
          <div class="px-3">
            <form class="form" role="form" id="formCliente" name="formCliente" method="post" >
              <div class="form-body">
                <div class="row">
                  <div class="container">
                    <?php if ($titularId=="") {
                    }else{?> <input type="hidden" id="ID" name="ID" value="<?php echo $titularId; ?>">
                    <input type="hidden" id="url" value="<?php echo base_url(); ?>" name="url">
                    <?php }?>
                    <div class="col-xl-12 col-lg-12 col-md-12">
                      <fieldset class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" value="<?php echo $nombre; ?>" id="nombre" name="nombre">
                      </fieldset>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                      <fieldset class="form-group">
                        <label for="movil">Teléfono móvil</label>
                        <input type="text" class="form-control" value="<?php echo $movil; ?>" id="movil" name="movil">
                      </fieldset>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                      <fieldset class="form-group">
                        <label for="parentesco">Parentesco</label>
                        <input type="text" class="form-control" value="<?php echo $parentesco; ?>" id="parentesco" name="parentesco">
                      </fieldset>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                      <fieldset class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="text" class="form-control" value="<?php echo $email; ?>" id="email" name="email">
                      </fieldset>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                      <fieldset class="form-group">
                        <label for="tel_emergencia1">Teléfono de emergencia (1)</label>
                        <input type="text" class="form-control" value="<?php echo $tel_emergencia1; ?>" id="tel_emergencia1" name="tel_emergencia1">
                      </fieldset>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-7">
                      <br>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                      <fieldset class="form-group">
                        <label for="tel_emergencia2">Teléfono de emergencia (2)</label>
                        <input type="text" class="form-control" value="<?php echo $tel_emergencia2; ?>" id="tel_emergencia2" name="tel_emergencia2">
                      </fieldset>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6" style="display: none;">
                      <fieldset class="form-group">
                        <label for="pass">Contraseña</label>
                        <input type="password" class="form-control" value="<?php echo $pass; ?>" id="pass" name="pass">
                      </fieldset>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <div class="col-xl-12 col-lg-12 col-md-12">
              <hr style="width: 100%; color: black; height: 1px; background-color:black;" />
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12">
              <h5>Sub titular</h5>
            </div>
            <form class="form" role="form" id="formCliente2" name="formCliente2" method="post" >
              <?php if ($titularId=="") {?>
              <div class="col-xl-12 col-lg-12 col-md-12">
                <fieldset class="form-group">
                  <label for="NombreS">Nombre</label>
                  <input type="text" class="form-control" value="" id="NombreS0" name="NombreS0">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="TelefonoMS">Teléfono móvil</label>
                  <input type="text" class="form-control" value="" id="TelefonoMS0" name="TelefonoMS0">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="ParentescoS0">Parentesco</label>
                  <input type="text" class="form-control" value="" id="ParentescoS0" name="ParentescoS0">
                </fieldset>
              </div>
              <div class="container" id="SubT">
              </div>
              <div class="col-xl-10 col-lg-10 col-md-10"></div>
              <div class="col-xl-2 col-lg-2 col-md-2">
                
              </div>
              <?php 
                $c=0; 
              }else{ 
                $c=0; 
                $this->load->model('Clientes/ModeloCliente');
                $Sub=$this->ModeloCliente->VerSubT($titularId);
              ?>
              <?php foreach ($Sub->result() as $key){?>
              <input type="hidden" value="<?php echo $c; ?>" id="contador" name="contador">
              <input type="hidden" value="<?php echo $key->titularsubId; ?>" id="<?php echo "IdSubT".$c; ?>" name="<?php echo "IdSubT".$c; ?>">
              <div class="col-xl-12 col-lg-12 col-md-12">
                <fieldset class="form-group">
                  <label for="<?php echo "NombreS".$c; ?>">Nombre</label>
                  <input type="text" class="form-control" value="<?php echo $key->nombre; ?>" id="<?php echo "NombreS".$c; ?>" name="<?php echo "NombreS".$c; ?>">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="<?php echo "TelefonoMS".$c; ?>">Teléfono móvil</label>
                  <input type="text" class="form-control" value="<?php echo $key->movil; ?>" id="<?php echo "TelefonoMS".$c; ?>" name="<?php echo "TelefonoMS".$c; ?>">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="ParentescoS0">Parentesco</label>
                  <input type="text" class="form-control" value="<?php echo $key->parentesco; ?>" id="<?php echo "ParentescoS".$c; ?>" name="<?php echo "ParentescoS".$c; ?>">
                </fieldset>
              </div>
              <div class="col-xl-10 col-lg-10 col-md-10"></div>
              <div class="col-xl-2 col-lg-2 col-md-2">
                <a onclick="DeleteSubt(<?php echo $key->titularsubId; ?>)" class="btn btn-danger" ><i class="ft-minus"></i></a>
              </div>
              
              
              <?php $c=$c+1;} ?>
              <?php  } ?>
              <div class="container" id="SubT">
              </div>
              <div class="col-xl-10 col-lg-10 col-md-10"></div>
              <div class="col-xl-2 col-lg-2 col-md-2">
                <a onclick="WriteSubTitular()" class="btn btn-primary"> <i class="fa fa-plus"> Agregar </i></a>
              </div>
            </form>
            <div class="col-xl-12 col-lg-12 col-md-12">
              <hr style="width: 100%; color: black; height: 1px; background-color:black;" />
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12">
              <h5>Usuarios</h5>
            </div>
            <form class="form" role="form" id="formCliente3" name="formCliente3" method="post" >
              <?php if ($titularId=="") {?>
              <div class="col-xl-12 col-lg-12 col-md-12">
                <fieldset class="form-group">
                  <div class="col-md-9">
                    <label for="NombreM0">Nombre</label>
                  </div>
                 <div class="col-md-3">
                   <label >Mismo que el titular
                    <input type="checkbox" name="mismot0" id="mismot0" onclick="copiart(0)">
                   </label>
                 </div> 

                  <input type="text" class="form-control" value="" id="NombreM0" name="NombreM0">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="FechaN">Fecha de nacimiento</label>
                  <input type="date" class="form-control" value="" id="FechaN0" name="FechaN0">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="Escuela0">Escuela</label>
                  <input type="text" class="form-control" value="" id="Escuela0" name="Escuela0">
                </fieldset>
              </div>
              <div class="container" id="Men"></div>
              <?php $c2=0; 
              }else{
                $c2=0;
                $this->load->model('Clientes/ModeloCliente');
              $Men=$this->ModeloCliente->VerMenor($titularId);
              ?>
              <?php foreach ($Men->result() as $m) {?>
              <input type="hidden" value="<?php echo $c2; ?>" id="contador2" name="contador2">
              <input type="hidden" value="<?php echo $m->id; ?>" id="<?php echo "IdM".$c2; ?>" name="<?php echo "IdM".$c2; ?>">
              <div class="col-xl-12 col-lg-12 col-md-12">
                <fieldset class="form-group">
                  <label for="<?php echo "NombreM".$c2; ?>">Nombre</label>
                  <input type="text" class="form-control" value="<?php echo $m->nombre; ?>" id="<?php echo "NombreM".$c2; ?>" name="<?php echo "NombreM".$c2; ?>">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="<?php echo "FechaN".$c2; ?>">Fecha de nacimiento</label>
                  <input type="date" class="form-control" value="<?php echo $m->fecha_nacimiento; ?>" id="<?php echo "FechaN".$c2; ?>" name="<?php echo "FechaN".$c2; ?>">
                </fieldset>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6">
                <fieldset class="form-group">
                  <label for="<?php echo "Escuela".$c2; ?>">Escuela</label>
                  <input type="text" class="form-control" value="<?php echo $m->escuela; ?>" id="<?php echo "Escuela".$c2; ?>" name="<?php echo "Escuela".$c2; ?>">
                </fieldset>
              </div>
              <div class="col-xl-10 col-lg-10 col-md-10"></div>
              <div class="col-xl-2 col-lg-2 col-md-2">
                <a onclick="DeleteMen(<?php echo $m->id; ?>)" class="btn btn-danger" ><i class="ft-minus"></i></a>
              </div>
              <?php $c2=$c2+1; } ?>
              <div class="container" id="Men"></div>
              <?php } ?>
              <div class="col-xl-10 col-lg-10 col-md-10"></div>
              <div class="col-xl-2 col-lg-2 col-md-2">
                <a onclick="WriteMenor()" class="btn btn-primary"> <i class="fa fa-plus"> Agregar </i></a>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12" align="center">
          <button class="btn btn-primary" id="savecliente"><i class="fa fa-save"></i>Guardar</button>
          <a class="btn btn-danger" href="<?php echo base_url();?>Clientes">Cancelar<i class="fa fa-trash-o"></i></a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
</div>
<script type="text/javascript">
if ($('#contador').length){
var con=parseInt(<?php echo $c; ?>);
console.log("hay: "+con+" datos")
}else{
var con=0;
}
if ($('#contador').length){
var con2=parseInt(<?php echo $c2; ?>);
}else{
var con2=1;
}
function WriteSubTitular() {
if ($('#ID').length) {
console.log("uno nuevo en edicion");
var input='<input type="hidden" value="" id="IdSubT'+con+'" name="IdSubT'+con+'">';
console.log("Con  vale: "+con);
}else{
console.log("uno nuevo en Agregar");
var input='';
}
var Nombre='<div class="col-xl-12 col-lg-12 col-md-12"><fieldset class="form-group"><label for="NombreS'+con+'">Nombre</label><input type="text" class="form-control" value="" id="NombreS'+con+'" name="NombreS'+con+'"></fieldset></div>';
var Movil='<div class="col-xl-6 col-lg-6 col-md-6"><fieldset class="form-group"><label for="TelefonoMS'+con+'">Teléfono movil</label><input type="text" class="form-control" value="" id="TelefonoMS'+con+'" name="TelefonoMS'+con+'"></fieldset></div>';
var Parentesco='<div class="col-xl-6 col-lg-6 col-md-6"><fieldset class="form-group"><label for="ParentescoS'+con+'">Parentesco</label><input type="text" class="form-control" value="" id="ParentescoS'+con+'" name="ParentescoS'+con+'"></fieldset></div>';
var conte=input+"\n"+Nombre+"\n"+Movil+"\n"+Parentesco;
$("#SubT").append(conte);
con=con+1;
console.log(con);
}
function WriteMenor(){
  if ($('#ID').length) {
    console.log("uno nuevo en edicion");
    var input='<input type="hidden" value="" id="IdM'+con2+'" name="IdM'+con2+'">';
    console.log("Cont 2 vale: "+con2);
  }else{
    console.log("uno nuevo en Agregar");
    var input='';
  }
  var Nombre='<div class="col-xl-12 col-lg-12 col-md-12"><fieldset class="form-group">';
      Nombre+='<div class="col-md-9">';
                    Nombre+='<label for="NombreM'+con2+'">Nombre</label>';
                  Nombre+='</div>';
                 Nombre+='<div class="col-md-3">';
                   Nombre+='<label >Mismo que el titular';
                    Nombre+='<input type="checkbox" name="mismot'+con2+'" id="mismot'+con2+'" onclick="copiart('+con2+')">';
                   Nombre+='</label>';
                 Nombre+='</div>';
  Nombre+='<input type="text" class="form-control" value="" id="NombreM'+con2+'" name="NombreM'+con2+'"></fieldset></div>';
  var Fecha='<div class="col-xl-6 col-lg-6 col-md-6"><fieldset class="form-group"><label for="FechaN'+con2+'">Fecha de nacimiento</label><input type="date" class="form-control" value="" id="FechaN'+con2+'" name="FechaN'+con2+'"></fieldset></div>';
  var Escuela='<div class="col-xl-6 col-lg-6 col-md-6"><fieldset class="form-group"><label for="Escuela'+con2+'">Escuela</label><input type="text" class="form-control" value="" id="Escuela'+con2+'" name="Escuela'+con2+'"></fieldset></div>';
  var conte=input+"\n"+Nombre+"\n"+Fecha+"\n"+Escuela;
  $("#Men").append(conte);
  con2=con2+1;
  console.log(con2);
}
function copiart(num){
  if ($('#mismot'+num).is(':checked')) {
    var nombre = $('#nombre').val();
    $('#NombreM'+num).val(nombre);
  }else{
    $('#NombreM'+num).val('');
  }
}
</script>