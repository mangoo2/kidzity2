<div class="row">
  <div class="col-md-12">
    <h2>Compras</h2>
  </div>
  <div class="col-md-12">
    <div class="col-md-9"></div>
    <div class="col-md-1">
      <a href="<?php echo base_url(); ?>Compras/Compraadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Listado de Compras</h4>
      </div>
      <div class="card-body">
        <div class="card-block form-horizontal">
          <div class="col-md-12">
            <div class="col-md-8">
            </div>
            <div class="col-md-3">
              <form role="search" class="navbar-form navbar-right mt-1">
                <div class="position-relative has-icon-right">
                  <input type="text" placeholder="Buscar" id="buscar" class="form-control round" oninput="buscarC()">
                  <div class="form-control-position"><i class="ft-search"></i></div>
                </div>
              </form>
            </div>
          </div>
          <table class="table table-striped table-hover" id="data-tables">
            <thead>
              <tr>
                <th>#</th>
                <th>Usuario</th>
                <th>Monto</th>
                <th>Fecha</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($compras->result() as $item){ ?>
              <tr>
                <td><?php echo $item->compraId; ?></td>
                <td><?php echo $item->usuario; ?></td>
                <td><?php echo $item->monto_total; ?></td>
                <td><?php echo $item->reg; ?></td>
                <td>
                  <div class="row">
                    <div class="col-md-12" align="center">
                      <a class="btn btn-primary" onclick="Detallecompra( <?php echo $item->compraId; ?>)">Detalle</a>
                    </div>
                  </div>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <table class="table table-striped table-hover" id="data-tables2" style="display: none;width: 100%">
            <thead>
              <tr>
                <th>#</th>
                <th>Usuario</th>
                <th>Monto</th>
                <th>Fecha</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody id="tbodyresultados">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="col-md-7">
  </div>
  <div class="col-md-5">
    <?php echo $this->pagination->create_links() ?>
  </div>
</div>
<div class="modal fade text-left" id="detalles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">detalles</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-12 detallescompras">
            			
            		</div>
            	</div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>