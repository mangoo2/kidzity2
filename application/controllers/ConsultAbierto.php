<?php
error_reporting(0);
class ConsultAbierto extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Consultas/ModeloConsultas');
        $this->load->model('ModeloCatalogos');
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
    }

    function index(){
        if ($this->idpersonal==0) {
            redirect('sistema');
        }
        $data['idpersonalid']=$this->idpersonal;
    	$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Watch/Abierto',$data);
        $this->load->view('templates/footer');
    }

    function SetSaldo(){
    	$id=$this->input->post('Id');
    	$cant=$this->input->post('cantidad');
    	echo $this->ModeloCatalogos->SaldoAdd($id,$cant);
    }
    function Buscar(){
        $buscar = $this->input->post('buscar');
        //$resultado='';
        $resultado=$this->ModeloConsultas->ConsultAbiertosearch($buscar,1);

        //echo $buscar.'---'. json_decode($resultado);
        
        foreach ($resultado->result() as $value){ ?>
            <tr>
                <td><?php echo $value->compranId; ?></td>
                <td><?php echo $value->compraId; ?></td>
                <td><?php echo $value->nino; ?></td>
                <td><?php echo $value->titular; ?></td>
                <td><?php echo $value->tiempo; ?></td>
                <td><?php echo number_format($value->pagado,0,'.',','); ?></td>
                <td><?php echo $value->fiesta; ?></td>
                <td><?php echo $value->devolucion_monto; ?></td>
                <td><?php echo $value->devolucion_motivo; ?></td>
                <td>
                    <div class="btn-group mr-1 mb-1">
                      <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i>
                      </button>
                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                          <a class="dropdown-item" href="#" onclick="Saldo(<?php echo $value->titularId; ?>);">Agregar saldo</a>
                          <a class="dropdown-item" href="#" onclick="salida(<?php echo $value->compranId; ?>);">Salida manual</a>
                          <a class="dropdown-item" href="#" onclick="devolucion(<?php echo $value->compranId; ?>);">Devolucion</a>
                          <a class="dropdown-item" href="<?php echo base_url(); ?>Ticket/compra?comp=<?php echo $value->compraId; ?>" target="_blank">Reimpresión</a>
                      </div>
                    </div>
                </td>

            </tr>
            <?php
        }
        

        
    }
    public function getlistConsultAbierto() {
        $params = $this->input->post();
        $getdata    = $this->ModeloConsultas->getlistConsultAbierto($params);
        $totaldata  = $this->ModeloConsultas->getlistConsultAbierto_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }



}
?>

















