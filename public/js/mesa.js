var selectedmesa=0;
var valuePIN=0;
var base_url=$('#base_url').val();
var accion;
$(document).ready(function($){
    // Recarga la página cada 
    window.setTimeout(function(){location.reload()},1800000);

    // Inicializar el PIN vacio
    $('.pinModalUsuario').val('');

    // Acción al dar click en "Salir a mesas"
    $('.regresaprincipal').click(function(event) {
        // Reiniciamos el PIN en el modal
        $(".pinModalUsuario").val('');
        // Mostramos y ocultamos los divs correspondientes
        $(".panelventa2").addClass("d-none");
        $(".panelventa3").addClass("d-none");
        $(".panelventa4").addClass("d-none");
        $(".panelventa1").removeClass("d-none");
        $("#vender").attr("disabled")
    });

    // Dar click en el botón de "Cerrar" en el modal de PIN
    $('#botonModalCerrar').click(function(event) {
        // Reiniciamos el PIN en el modal
        $(".pinModalUsuario").val('');
        // Mostramos y ocultamos los divs correspondientes
        $(".panelventa2").addClass("d-none");
        $(".panelventa3").addClass("d-none");
        $(".panelventa4").addClass("d-none");
        $(".panelventa1").removeClass("d-none");
    });

 
    // Selección de una mesa
    $(".selectedmesa").click(function(event){
        selectedmesa= $(this).attr('data-mesa');
        //$("#inlineForm").modal('show'); 
        ingresarPIN();
        setTimeout(function() { $("#pinModalUsuario").focus() }, 600);
    });

    // Acción al dar click en "Ingresar" dentro de modal de PIN
    $("#botonModalConfirmar").click(function(event){
        ingresarPIN();
    });

    // Acción al dar "ENTER" dentro de modal de PIN
    $("#pinModalUsuario").keypress(function(e) {
        if (e.which == 13){
            ingresarPIN();
        }
    });
    
    // Muestra la seleccíón de Productos
    $(".classproducto").click(function(event){
        //console.log('entrando a categoria');
        productoid= $(this).attr('data-idcate');
        cargaproductos(productoid);
        $(".panelventa2").addClass("d-none");
        $(".panelventa3").removeClass("d-none");    
    });

    // Acción al modificar el método de pago
    $("#metodopago").change(function(event) {
        if($('#metodopago option:selected').val()==2){
            var total = $("#totalproductos").val();
            $("#montoPago").val(total);
            calculartotal();
        }else{
            $("#montoPago").val('');
        }
    });

    // Verificar la contraseña para permitir un descuento
    $('.modalsalidam').click(function(event) {
        autorizaDescuento();
    });

    // Carga la función de Descuento al dar ENTER
    $("#passautorizacion").keypress(function(e) {
        if (e.which == 13){
            autorizaDescuento();
        }
    });
});

// Función para mandar el PIN a la función AJAX
function ingresarPIN(){
    valuePIN = $("#pinModalUsuario").val();
    enterModal(valuePIN);
}

// Regresar a Categorías de productos
function regresarCategorias(){
    $(".panelventa3").addClass("d-none");
    $(".panelventa2").removeClass("d-none");
}

// Carga de Productos
function cargaproductos(idp){
    $.ajax({
        type: 'POST',
        url: 'Mesas/cargarproductos',
        data: {id: idp,
               selectedmesa: selectedmesa},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(data) {
                toastr.error('Error', '500');
                console.log(data.responseText);
            }
        },
        success: function(data){
            // Reinicializa el panel de los productos
            $('.panelventa3').html('');
            $('.panelventa3').html(data);
            // Agrega el botón de "Regresar a las categorías anteriores"
            $('.panelventa3').append('<div class="col-md-3 galeriasimg flipInX animated cursor" onclick="regresarCategorias()" style="background-color: mediumpurple"><h5>REGRESAR</h5></div>');
        }
    });
}

// Acción de vender 
function clickVender(tipo){    
    $('.sk-cube-grid').show( "slow" );
    // Total de la cuenta
    var totalproductosConvertido = parseFloat($("#totalproductos").val());

    // Monto ingresado para pagar
    var montoPagoConvertido = parseFloat($("#montoPago").val()); 
    console.log('total: '+$("#totalproductos").val());
    
    if($("#totalproductos").val()!=0 && $("#totalproductos").val()!=''){
        // Si es impresión de Ticket no debe pedir el monto
        if(tipo==0){
            realizar_venta(tipo);
        }else if(tipo==1){
            if(parseFloat(montoPagoConvertido) >= (parseFloat(totalproductosConvertido))){
                realizar_venta(tipo);
            }else{
                toastr.error('Error!', 'Agregar monto igual o mayor al total');
            }
        }
    }else{
        toastr.error('Error!', 'No hay nada disponible para vender');
    }   
}

function habilitaDescuento(){
    if ($('#descuentoapagar').is('[readonly]')){
        $('#iconForm').modal();
        setTimeout(function() { $("#passautorizacion").focus() }, 600);
    }
}

// Validación de PIN de usuarios
function enterModal(valuePIN){
    // Calculamos el total, y le pasamos el número de la mesa que estamos manipulando
    //if(valuePIN.trim()==''){
    //    alert('No puede ingresar un PIN vacio');
    //}else if(valuePIN.length <4){
    //    alert('El PIN debe contener al menos 4 caracteres');
    //}else{
        $.ajax({
            type:'POST',
            url: base_url+'index.php/mesas/getUsuarioPorPin/',
            data: {
                pin: valuePIN,
                mesa: selectedmesa
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(data){ 
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                var respuesta = JSON.parse(data);
                var pinValido = respuesta.pin_valido;
                if(pinValido==true){
                    $('#tableproductosbody').html('');
                    var respuesta =  JSON.parse(data);
                    // Carga los datos de la tabla
                    $('#tableproductosbody').html(respuesta.tabla);
                    
                    escribeTotales(respuesta.total,respuesta.descuento,respuesta.montoPorCobrar);
                    // Reinicializa el panel de los productos
                    $('.panelventa3').html('');
                    $('.panelventa3').html(data);
                    // Agrega el botón de "Regresar a las categorías anteriores"
                    $('.panelventa3').append('<div class="col-md-3 galeriasimg flipInX animated cursor" onclick="regresarCategorias()" style="background-color: mediumpurple"><h5>REGRESAR</h5></div>');

                    // Mostramos y ocultamos los divs correspondientes
                    $(".panelventa2").removeClass("d-none");
                    $(".panelventa4").removeClass("d-none");
                    $(".panelventa1").addClass("d-none");
                    $("#inlineForm").modal('hide');

                    if(selectedmesa==0){
                        $("#ticket").addClass("d-none")
                        $("#vender").removeAttr("disabled")
                    }else{
                        $("#ticket").removeClass("d-none")
                    }
                    
                    if(respuesta.tipo==0){
                        $("#vender").removeAttr("disabled")
                    }
                }else{
                    alert('El PIN ingresado no coincide con un usuario registrado en el sistema, la venta no se puede realizar');
                    $(".pinModalUsuario").val(''); 
                }
            },
            error: function(jqXHR, estado, error){
                console.log(estado);
                console.log(jqXHR);
                console.log(error);
            }
        });
    //}
}

function agregar_producto(idProducto){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/mesas/agregar_producto/',
        async: false,
        data: {
            idProducto: idProducto,
            mesa: selectedmesa
        },
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var descuento = $('#descuentoapagar').val();
            $('#tableproductosbody').html('');
            var respuesta =  JSON.parse(data);
            $('#tableproductosbody').html(respuesta.tabla);
            $(".totalproductos").text("$"+respuesta.total);
            $("#totalproductos").val(respuesta.total);
            escribeTotales(respuesta.total,respuesta.descuento,respuesta.montoPorCobrar);
            calculartotal();

        },
        error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
        }
    });
}
function eliminar_producto_modal(idProducto){
    $("#inlineForm_pin").modal(); 
    $("#idproducto_p").val(idProducto);
}
function eliminar_producto(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/mesas/eliminar_producto/',
        async: false,
        data: {
            pass_usuario: $("#pass_usuario").val(),
            idProducto: $("#idproducto_p").val(),
            mesa: selectedmesa
        },
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
                toastr.error('Error', '500');
            }
        },
        success:function(data){   
            if(data == 0){
               toastr.error('Contraseña incorrecta', 'Error');
            }else{
                $("#inlineForm_pin").modal('hide');
                var descuento = $('#descuentoapagar').val();
                var respuesta =  JSON.parse(data);
                console.log(respuesta);
                $('#tableproductosbody').html(respuesta.tabla);
                $(".totalproductos").text("$"+respuesta.total);
                $("#totalproductos").val(respuesta.total);
                escribeTotales(respuesta.total,respuesta.descuento,respuesta.montoPorCobrar);
                calculartotal();
            }
        },
        error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
        }
    });
}

function modificar_producto_modal(idProducto,accion){
    $("#inlineForm_pin2").modal(); 
    $("#idproducto_p2").val(idProducto);
}
function modificar_producto2(idProd,accion){
    //idProducto = $("#idproducto_p2").val();
    console.log('idprod: '+idProd);
    $.ajax({
        type:'POST',
        url: base_url+'index.php/mesas/modificar_producto/',
        async: false,
        data: {
            idProducto: idProd,
            mesa: selectedmesa,
            accion: accion
        },
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $("#inlineForm_pin2").modal('hide');
            var descuento = $('#descuentoapagar').val();
            var respuesta =  JSON.parse(data);
            $('#tableproductosbody').html(respuesta.tabla);
            $(".totalproductos").text("$"+respuesta.total);
            $("#totalproductos").val(respuesta.total);
            $("#pass_usuario2").val('');
            escribeTotales(respuesta.total,respuesta.descuento,respuesta.montoPorCobrar);
        },
        error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
        }
    });
}
function modificar_producto(idProducto,accion){
    idProducto = $("#idproducto_p2").val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/mesas/modificar_producto/',
        async: false,
        data: {
            pass_usuario: $("#pass_usuario2").val(),
            idProducto: idProducto,
            mesa: selectedmesa,
            accion: accion
        },
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            if(data == 0){
               toastr.error('Contraseña incorrecta', 'Error');
            }else{
                $("#inlineForm_pin2").modal('hide');
                var descuento = $('#descuentoapagar').val();
                var respuesta =  JSON.parse(data);
                $('#tableproductosbody').html(respuesta.tabla);
                $(".totalproductos").text("$"+respuesta.total);
                $("#totalproductos").val(respuesta.total);
                $("#pass_usuario2").val('');
                escribeTotales(respuesta.total,respuesta.descuento,respuesta.montoPorCobrar);
            }
        },
        error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
        }
    });
}

function calculartotal(mesa){
    // Total de la cuenta
    var totalproductosConvertido = parseFloat($("#totalproductos").val());

    // Monto ingresado para pagar
    var montop = $("#montoPago").val()==''?0:$("#montoPago").val();
    var montoPagoConvertido = parseFloat(montop); 

    // Se calcula el cambio restando al Monto del pago el total con descuento incluido
    var cambio = parseFloat(montoPagoConvertido-totalproductosConvertido); 
    
    console.log('total: '+totalproductosConvertido);
    console.log('pago: '+montoPagoConvertido);
    console.log('cambio: '+cambio);

    if (cambio<=0){
        cambio=0;
    }else{
        cambio=cambio;
    }
    $('.cambio').html(new Intl.NumberFormat('es-MX').format(cambio));
}

function autorizaDescuento(){
    var  contrasenaAcceso = $('#passautorizacion').val();
    $.ajax({
        type:'POST',
        url: base_url+'Login/vadministrador',
        data: {
            acceso: $('#passautorizacion').val(),
        },
        async: false,
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
                toastr.error('Error', '500');
                console.log(data.responseText);
            }
        },
        success:function(data){
            // Si la contraseña es correcta, Habilitamos la edición del campo de descuento
            if (data==1){   
                $("#iconForm").modal('hide');
                $('.descuentoapagar').removeAttr("readonly");
                $(".descuentoapagar").TouchSpin({
                    initval: 0,
                    min: 0,
                    max:9999999999999999999999999,
                }).change(function(event) {
                    var descuento = $("#descuentoapagar").val();
                    // Cada que cambie, enviará el descuento 
                    $.ajax({
                        type: 'POST',
                        url: 'Mesas/agregarDescuento/',
                        data: {
                            mesa: selectedmesa,
                            descuento: descuento,
                            contrasenaAcceso: contrasenaAcceso,
                        },
                        async: false,
                        statusCode: {
                            404: function(data) {
                                toastr.error('Error!', 'No Se encuentra el archivo');

                            },
                            500: function(data) {
                                toastr.error('Error', '500');
                            }
                        },
                        success: function(respuesta){
                            console.log(respuesta);
                            // Si es exitoso, cambiará el total
                            var respuesta2 =  JSON.parse(respuesta);
                            console.log(respuesta2);
                            $('#tableproductosbody').html(respuesta2.tabla);
                            $(".totalproductos").text("$"+respuesta2.total);
                            $("#totalproductos").val(respuesta2.total);
                            escribeTotales(respuesta2.total,respuesta2.descuento,respuesta2.montoPorCobrar);
                        }
                    });
                });
            }
            else{
                toastr.error('Acceso no permitido','Error');
            }
        },
        error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
        }
    });
    $('#passautorizacion').val('');
}

function escribeTotales(total,descuento,montoPorCobrar){
    console.log(total,descuento,montoPorCobrar);
    $(".totalproductos").text("$"+montoPorCobrar);
    $("#totalproductos").val(montoPorCobrar);
    $("#descuentoapagar").val(descuento);
    $(".totalproductosdescuento").text("$"+total);
}

function realizar_venta(tipo){  
    $.ajax({
            type: 'POST',
            url: base_url+'Mesas/realizar_venta',
            data: {
                mesa: selectedmesa,
                metodo:$('#metodopago option:selected').val(),
                tipo: tipo
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error', '404');
                },
                500: function(data){
                    
                },
            },
            success: function(data){   
                $('.sk-cube-grid').hide( "slow" );
                // Despliega ventana de ticket
                popup = window.open(base_url+"Ticketmesas/dulceria?comp="+data,"popup","width=700,height=300,scrollbars=yes");
                popup.focus();
                // Indica venta realizada
                toastr.success('Proceso realizado', 'Hecho');
                if(tipo==1){   
                    $(".panelventa2").addClass("d-none");
                    $(".panelventa3").addClass("d-none");
                    $(".panelventa4").addClass("d-none");
                    $(".panelventa1").removeClass("d-none");
                    // Inicializar el PIN vacio
                    $('#montoPago').val('');
                    $('.pinModalUsuario').val('');
                    $("#vender").attr("disabled","disabled")
                    $('.cambio').html('00.00');
                    $("#metodopago").val(1);
                    $("#descuentoapagar").val('');
                    $('.descuentoapagar').attr("readonly");
                    location.reload(true)
                }else{
                    $("#vender").removeAttr("disabled")
                }
            },
        });
}
function ventascredito() {
    window.open(base_url+"Mesas/credito");
}

function limpiarproductos(){
    $('#modallimpiar').modal();
}
function limpiar_productos(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/mesas/eliminar_productos/',
        async: false,
        data: {
            pass_usuario: $("#pass_usuario_l").val(),
            mesa: selectedmesa
        },
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(data){ 
                toastr.error('Error', '500');
            }
        },
        success:function(data){   
            if(data == 0){
               toastr.error('Contraseña incorrecta', 'Error');
            }else{
                $("#modallimpiar").modal('hide');
                var descuento = $('#descuentoapagar').val();
                var respuesta =  JSON.parse(data);
                console.log(respuesta);
                $('#tableproductosbody').html(respuesta.tabla);
                $(".totalproductos").text("$"+respuesta.total);
                $("#totalproductos").val(respuesta.total);
                escribeTotales(respuesta.total,respuesta.descuento,respuesta.montoPorCobrar);
                calculartotal();
            }
        },
        error: function(jqXHR, estado, error){
            console.log(estado);
            console.log(jqXHR);
            console.log(error);
        }
    });
}