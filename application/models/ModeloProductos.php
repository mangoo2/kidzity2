<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getlistproductos($params){
        $columns = array( 
            0=>'pro.productoid',
            1=>'pro.img',
            2=>'pro.codigo',
            3=>'pro.producto',
            4=>'prop.categoria',
            5=>'pro.stock',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('producto_hijo pro');
        $this->db->join('producto_padre prop', 'prop.productopId=pro.productopId');
        $this->db->where(array('pro.activo'=>1));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistproductos_total($params){
        $columns = array( 
            0=>'pro.productoid',
            1=>'pro.img',
            2=>'pro.codigo',
            3=>'pro.producto',
            4=>'prop.categoria',
            5=>'pro.stock',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('producto_hijo pro');
        $this->db->join('producto_padre prop', 'prop.productopId=pro.productopId');
        $this->db->where(array('pro.activo'=>1));
        
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }



}