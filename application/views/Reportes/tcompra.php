<?php
    $datostitular=$this->ModeloCatalogos->getselectwhere('titular','titularId',$titularId);

    $datostitularsub=$this->ModeloCatalogos->getselectwhere('titular_sub','  titularId',$titularId);
    $ninosrow=$this->ModeloCatalogos->getninoscompras($compid);
    $dia1=date("N",strtotime($reg));
    $dia2=date("d",strtotime($reg)); 
    $mes1 = date("n",strtotime($reg));
    $yyyyy = date("Y",strtotime($reg));
    $hora = date("g",strtotime($reg));
    $minuto = date("i",strtotime($reg));
    $ampm = date("A",strtotime($reg));
    if ($dia1==1) { $diaa ="Lunes"; }
    if ($dia1==2) { $diaa ="Martes"; }
    if ($dia1==3) { $diaa ="Miercoles"; }
    if ($dia1==4) { $diaa ="Jueves"; }
    if ($dia1==5) { $diaa ="Viernes"; }
    if ($dia1==6) { $diaa ="Sabado"; }
    if ($dia1==7) { $diaa ="Domingo"; }
    if ($mes1=='1') { $mes ="Enero"; }
    if ($mes1=='2') { $mes ="Febrero"; }
    if ($mes1=='3') { $mes ="Marzo"; }
    if ($mes1=='4') { $mes ="Abril"; }
    if ($mes1=='5') { $mes ="Mayo"; }
    if ($mes1=='6') { $mes ="Junio"; }
    if ($mes1=='7') { $mes ="Julio"; }
    if ($mes1=='8') { $mes ="Agosto"; }
    if ($mes1=='9') { $mes ="Septiembre"; }
    if ($mes1=='10') { $mes ="Octubre"; }
    if ($mes1=='11') { $mes ="Noviembre"; }
    if ($mes1=='12') { $mes ="Diciembre"; }
    $fechaingreso = $diaa." ".$dia2." de ".$mes." del ".$yyyyy;
    $descuentopro=0;
    $ttotal=0;
    $descuentospro=0;

    $horas = date("g",strtotime($reg."+ ".$tiempo." hours"));
    $minutos = date("i",strtotime($reg."+ ".$tiempo." hours"));
    $ampms = date("A",strtotime($reg."+ ".$tiempo." hours"));

      foreach ($configticket->result() as $item){
      $id_ticket = $item->id_ticket;
      $titulo = $item->titulo;
      $mensaje = $item->mensaje;
      $mensaje2 = $item->mensaje2;
      $fuente = $item->fuente;
      $tamano = $item->tamano;
      $margensup = $item->margensup;
      $margenlat = $item->margenlat;
    } 
    $iva=1.16;
    if($metodo==1){
      $tipometodo="Pago en Efectivo";
    }elseif($metodo==2){
      $tipometodo="Pago con Tarjeta";
    }elseif ($metodo==3) {
      $tipometodo="Pago con saldo Kids";
    }else{
      $tipometodo="";
    }
?>
<?php
    //=================================================================
    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    //=======================================================================================
    class MYPDF extends TCPDF {
      //Page header
      public function Header() {
          //$img_header = 'header.jpg';
          //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
          //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
          $imglogo = base_url().'public/img/kidb.png';
          $html = '
              ';
            $this->writeHTML($html, true, false, true, false, '');
      }
        // Page footer
      public function Footer() {
          $html = ' 
          <table width="100%" border="0">
            <tr>
              <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
            </tr>
          </table>';
          //$this->writeHTML($html, true, false, true, false, '');
      }
    } 
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('agb');
    $pdf->SetTitle('Ticket');
    $pdf->SetSubject('Ticket');
    $pdf->SetKeywords('Ticket');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins($margenlat, $margensup, $margenlat);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    $pdf->SetFont('dejavusans', '', $tamano);
    // add a page
    $pdf->AddPage();
?>
<?php
$imglogo = base_url().'public/img/kidb.png';
$html = '<table border="0">';
$html .= '<tr>
            <td colspan="3" align="center"><img src="'.$imglogo.'" width="130px" ></td>
          </tr>';

$html .= '<tr>
              <td colspan="3" align="center">'.$titulo.'</td>
          </tr>
         ';
$html .= '  <tr>
              <td colspan="3" align="center">'.$mensaje.'</td>
          </tr>
          <tr>
              <td colspan="3" >Folio: '.$compid.'</td>
          </tr>

         ';

foreach ($datostitular->result() as $item) {
  $html .= '<tr>
            <td  style="background-color: #9C9B99; " >Titular:</td>
            <td  colspan="2" align="center">'.$item->nombre.'</td>
        </tr>';
}
$html .= '<tr>
            <td colspan="3" align="center" style="background-color:#000000; color: white;"><b>Usuarios</b></td>
        </tr>
        <tr style="background-color: #9C9B99; ">
            <td  align="center">Pulcera:</td>
            <td  align="center">Nombre</td>
            <td  align="center">Cobrado</td>
        </tr>';
$ivat=0;
$costo=0;
$subtotal=0;
foreach ($ninosrow->result() as $item) {
  if($tiempo>=3){
    $html .= '<tr>
                  <td colspan="3" style="font-size: 10px; "  align="center">TIEMPO LIBRE</td>
              </tr>';
  }
  if($item->tlbebe==1){
    $html .= '<tr>
                  <td colspan="3" style="font-size: 10px; "  align="center">TIEMPO LIBRE BEBE</td>
              </tr>';
  }
  if($item->tladulto==1){
    $html .= '<tr>
                  <td colspan="3" style="font-size: 10px; "  align="center">TIEMPO LIBRE ADULTO</td>
              </tr>';
  }
  if($item->paquetefamiliar==1){
    $html .= '<tr>
                  <td colspan="3" style="font-size: 10px; "  align="center">PAQUETE FAMILIAR</td>
              </tr>';
  }
  $costop=$item->pagado/$iva;
  $html .= '<tr>
            <td style="font-size: 10px; "  align="center">'.$item->pulcera.'</td>
            <td style="font-size: 10px; "  align="center">'.$item->nombre.'</td>
            <td style="font-size: 10px; "  align="center">$ '.number_format($costop,2,'.',',').'</td>
        </tr>';
        $ttotal=$ttotal+$item->pagado;
        $subtotal=$subtotal+$costop;
}

 $cotitularesrow=0;
foreach ($datostitularsub->result() as $item) {
 $cotitularesrow=1;
}
if ($cotitularesrow==1) {
  $html .= '<tr>
            <td colspan="3" align="center" style="background-color:#000000; color: white;"><b>Cotitulares</b></td>
        </tr>
        <tr style="background-color: #9C9B99; ">
            <td  align="center">Parentesco</td>
            <td  colspan="2" align="center">Nombre</td>
        </tr>';
}
foreach ($datostitularsub->result() as $item) {
  $html .= '<tr>
            <td  align="center">'.$item->parentesco.'</td>
            <td  align="center">'.$item->nombre.'</td>
        </tr>';
}

foreach ($resultadoarti->result() as $itemvc) {
    $idventa=$itemvc->ventaId;
    $detallepro=$this->ModeloCatalogos->getventadetalle($idventa);
    $html .=' <tr>
                <td colspan="3" align="center"></td>
              </tr>
              <tr>
                <td colspan="3" align="center" style="background-color:#000000; color: white;">Artículos</td>
              </tr>
              <tr style="background-color: #9C9B99; ">
                <th style="font-size: 8; " align="center">Cantidad</th>
                <th style="font-size: 8; " align="center" >Producto</th>
                <th style="font-size: 8; " align="center" >Precio</th>
            </tr>';
      $descuentopro=0;
    foreach ($detallepro->result() as $itemdv) {
      $costop=($itemdv->precio+$itemdv->descuento)/$iva;
      $html .='<tr>
                <th style="font-size: 8; " align="center" >'.$itemdv->cantidad.'</th>
                <th style="font-size: 8; " align="center" >'.$itemdv->producto.'</th>
                <th style="font-size: 8; " align="center" >$ '.number_format($costop,2,'.',',').'</th>
            </tr>';
            $ttotal=$ttotal+($itemdv->cantidad*($itemdv->precio+$itemdv->descuento));
            $subtotal=$subtotal+($itemdv->cantidad*$costop);
            $descuentopro=$descuentopro+($itemdv->cantidad*$itemdv->descuento);
    }


  # code...
}
$styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
$params = $pdf->serializeTCPDFtagParameters(array($titularId.'|'.$idqr, 'QRCODE,L', '', '', 30, 30, $styleQR, 'N'));
$html .= '<tr>
            <td colspan="3" align="center"></td>
        </tr>
        <tr>
            <td  align="center" style="background-color: #9C9B99; ">Fecha</td>
            <td  colspan="2" align="center">'.$fechaingreso.'</td>
        </tr>
        <tr>
            <td  align="center" style="background-color: #9C9B99; ">Ingreso</td>
            <td  colspan="2" align="center">'.$hora.':'.$minuto.' '.$ampm.'</td>
        </tr>
        <tr>
            <td  align="center" style="background-color: #9C9B99; ">Finaliza</td>
            <td  colspan="2" align="center">'.$horas.':'.$minutos.' '.$ampms.'</td>
        </tr>
        <tr>
            <td  align="center" style="background-color: #9C9B99; ">Sub Total</td>
            <td  colspan="2" align="center">$ '.number_format($subtotal,2,'.',',').'</td>
        </tr>
        <tr>
            <td  align="center" style="background-color: #9C9B99; ">Iva</td>
            <td  colspan="2" align="center">$ '.number_format($ttotal-$subtotal,2,'.',',').'</td>
        </tr>
        ';
$html .= '
        <tr>
            <td  align="center" style="background-color: #9C9B99; ">Descuento</td>
            <td  colspan="2" align="center">$ '.number_format($descuento+$descuentopro,0,'.',',').'</td>
        </tr>';

        $ttotal=$subtotal+($ttotal-$subtotal)-($descuento+$descuentopro);

$html .= '
        <tr>
            <td  align="center" style="background-color: #9C9B99; ">Total</td>
            <td  colspan="2" align="center">$ '.number_format($ttotal,0,'.',',').'</td>
        </tr>';
$html .= '
        <tr>
            <td  colspan="3" align="center">'.$tipometodo.'</td>
        </tr>';
        $saldototal=$this->ModeloCatalogos->sumasaldosdisponibles($titularId);
        $saldoanterior=$this->ModeloCatalogos->sumasaldosdisponiblesanterior($titularId,$reg);
        $saldonuevo=$this->ModeloCatalogos->sumasaldosdisponiblesnuevo($titularId,$reg);
          if ($saldototal>0) {
            $html .= '
                    <tr>
                        <td  align="center">Saldo anterior</td>
                        <td  align="center">'.$saldoanterior.'</td>
                    </tr>
                    <tr>
                        <td  align="center">Saldo nuevo</td>
                        <td  align="center">'.$saldonuevo.'</td>
                    </tr>
                    <tr>
                        <td  align="center">Saldo Dulceria</td>
                        <td  align="center">'.$saldototal.'</td>
                    </tr>';
          }
        
$html .= '<tr>
            <td colspan="3" align="center"></td>
        </tr>
          <tr>
            <td width="25%" ></td>
            <td width="50%" align="center"><tcpdf method="write2DBarcode" params="' . $params . '" /></td>
            <td width="25%" ></td>

        </tr>
        ';
$html .= '  <tr>
              <td colspan="3" align="center">'.$mensaje2.'</td>
          </tr>
         ';        

$html .= '</table>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->IncludeJS('print(true);');
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>